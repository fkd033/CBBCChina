//
//  RootChangeViewController.h
//  CBBCpoc
//
//  Created by 万浩 on 2018/5/30.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum SELECT_VCTYPE {
    SELECT_CBBC = 0,
    SELECT_MOA = 1,
    SELECT_DIRECTION = 2,
    SELECT_HOTFIX = 3
}SELECT_VCTYPE;

@interface RootChangeViewController : UIViewController

@property (nonatomic, copy) void (^selectVC)(SELECT_VCTYPE type);

@end
