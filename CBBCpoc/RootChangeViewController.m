//
//  RootChangeViewController.m
//  CBBCpoc
//
//  Created by 万浩 on 2018/5/30.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import "RootChangeViewController.h"

@interface RootChangeViewController ()

@end

@implementation RootChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)selectCBBCVC:(id)sender {
    if (self.selectVC) {
        self.selectVC(SELECT_CBBC);
    }
}
- (IBAction)selectMOAVC:(id)sender {
    if (self.selectVC) {
        self.selectVC(SELECT_MOA);
    }
}
- (IBAction)selectDirectioon:(id)sender {
    if (self.selectVC) {
        self.selectVC(SELECT_DIRECTION);
    }
}

- (IBAction)selectHotfix:(id)sender {
    if (self.selectVC) {
        self.selectVC(SELECT_HOTFIX);
    }
}

@end
