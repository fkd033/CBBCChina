//
//  HomeViewController.m
//  CBBCpoc
//
//  Created by 方克东的mac on 2018/4/19.
//  Copyright © 2018年 Alibaba Cloud. All rights reserved.
//

#import "HomeViewController.h"
#import "ScanCodeViewController.h"
#import "WXDemoViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"home";
    // Do any additional setup after loading the view.
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    ScanCodeViewController *vc = [[ScanCodeViewController alloc] init];
    vc.getSysString = ^(NSString *str) {
                WXDemoViewController *vc = [[WXDemoViewController alloc] init];
                vc.url = [NSURL URLWithString:str];
                [self.navigationController pushViewController:vc animated:YES];
    };
    
    [ self presentViewController:vc animated:YES completion:nil] ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
