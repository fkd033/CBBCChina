// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 45);
/******/ })
/************************************************************************/
/******/ ({

/***/ 45:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(46)
)

/* script */
__vue_exports__ = __webpack_require__(47)

/* template */
var __vue_template__ = __webpack_require__(48)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\Gold\\protfolio.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-21b74e96"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 46:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "width": "750",
    "height": "98",
    "flexDirection": "row",
    "alignItems": "center",
    "paddingLeft": "36"
  },
  "arrow": {
    "width": "18",
    "height": "29"
  },
  "titleText": {
    "marginLeft": "258",
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "width": "678",
    "height": "208",
    "backgroundColor": "rgba(177,191,216,0.5)",
    "borderRadius": "6",
    "marginLeft": "36",
    "marginTop": "60",
    "flexDirection": "row",
    "alignItems": "center"
  },
  "circlePic": {
    "width": "191",
    "height": "191",
    "marginLeft": "42"
  },
  "right": {
    "height": "124",
    "justifyContent": "space-between",
    "marginLeft": "21"
  },
  "main": {
    "flexDirection": "row"
  },
  "item": {
    "height": "63",
    "justifyContent": "space-between",
    "marginRight": "40"
  },
  "itemLabel": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(90,160,241,1)"
  },
  "percent": {
    "fontSize": "26",
    "fontFamily": "DINAlternate-Bold",
    "color": "rgba(68,70,79,1)"
  },
  "floor": {
    "paddingLeft": "53",
    "paddingRight": "53",
    "paddingTop": "56",
    "justifyContent": "space-between",
    "height": "512"
  },
  "floor1": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "32"
  },
  "floor2": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "32"
  },
  "floor3": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "lineHeight": "32"
  },
  "bottomSign": {
    "width": "199",
    "height": "30",
    "marginTop": "200"
  }
}

/***/ }),

/***/ 47:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
var navigator = weex.requireModule('navigator');
exports.default = {
  data: function data() {
    return {
      marginTop: 0,
      arrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAaCAYAAAC6nQw6AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIcSURBVEhLzZTPbtNAEIdnEghxSEKSHoA2BceFOsVSDjhIgMoD9AEq8SK8BLRc6d9H4FD3RF3nUK6EIyIoTdQDysFSxQWkUuJlZrMYTBLXx34He+dn68tmZmW4dKC6J8a27avlcnU1QHHecp23FAWcp/mSFCmZmXsJiM8Rcbmmmyf9fqfLz1LyjQSEEsBnKgKRhh9qmUzEklJl/tW/EhRip3XgHKnyYpFlWZnSTHUNUSyrCAKSuK6zoUpJbLMtazVzc+7nGv3aUxWBELDtuXubqgyZ2uxJEtJsea6zpYoIE3fEktskoeVfCYrNw3fOtqrGGBNJyezZOo34iYqoJ7DRcvd2VDmRiIglt2bPXtMZeayiRBImFN1bWbl2N8iso4BQIiB44x3s76oylnD89/ki4qcYRzi1brc7TC8a3o0gZdFfq3KGgI90YzHo9758lC/FEBn/KclyWuOwUPj1gJo9zxlJm7pRJ1knVjZ2jnz/01DTGl5UBk2jZopejGzigQxlxeESlVJGtqaxYELvuNOW9X9MPdlSlm1410lGExjJAO1psqkihmW57BUvX6zUSXZnlKJdM+pIPYvIYkWM7/vDSinvZbXiEvVKyuhu64aZItkH+RJxoYgZDAZBpcyyQp2mqGT4UF8wP/ePOydcJ/qwMe12+/zb6dcXdGrfq4h3llPLZDv6A++sXMq3clr+O4jUEX2X9ikWo6eXC4DfHm23ffE1u64AAAAASUVORK5CYII=',
      circlePic: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL8AAAC/CAYAAACv6g0GAAAgAElEQVR4Xu29B5gcx3Un/qp70uaERQaWyJEEwQCABEiAQTRJkRAlKvBE2ZJlU7KsQKU7+6zzybrz6WTLNhWsYMmSlc5U+ktiJsEEEgSJQBCRi7QIuwtszjs7qUP9v1fdb6am0TPTMzsbAGzjw7c9HWp6qn/v1e+FesVgapvqgcu0B9hl+runfvZUD8AU+IsMAs75mPQpY4wX+VEv++bG5EVdyr06VuAebZ9NCUf+PTgF/ix9li/Qv/KVsR1Jv/xlyEv7TwlEdoGYAr/UP17BngvkK1cWVwgaG7OD3qtQTAlDujBc9uDPBfhMQM8G8Mb67UXt15XdWzJq/EyCkUsgpgQBiquh8mddE3NHIYB3gj0TwGedqMgJ/JqakrRr+vujOelM+9Jh12ucguEmDFOC4I6znC9qYuBZ/G/NBng37Z4L7E6QOwGNv6CjPDCq/p0ZTlwAeKegOIVitMJwOY0Io3o5xYdocVscDeBlzZ4N6DLAKzp9F/RnWdmFx7z8ypER/QLgD89IHZMFI5tAyMKQ76hwqQvCJQn+fEAva/hMgCetTkCXQe4Ed39/Z7JPa2pKlGuumVlTWRmsCwTUWsZYCFRWpQq6yXyWEHDdAOBg8EHOeSyRMPqGhuK9b73V0d/fHzVJUGpqZqQJgywcJBQkELIwyCNDNkHIRo0uVSG4pMCfCfROWpML8F7ATiBfuLDGf/XVM5f4SvwrR0x21QCHJefN+KJ+bswKM72y19RZt6GzrrjJhnQTYiYwwwQwgYNiKBAwVQhpfiiPBfg0LQgzjRAvD3Ao9QMvC0C4PMg7K/2stbrEPD6rwtyjR7W3DxzoOHn6dL+GgkFC4RQGt5FhtIJwqQnBJQH+0YCeKE0mwJNmJ7Bv3bpsQaAyuL5fV7cMcrg+rLMrGvWY/6g+wpohwnp4gmGnWv8ZcNTX+N9kwDj+B1DEXwbMZGJfMRXxWTEZ+HUVqobKoHK4VHyWN78KMKMc+MxyrteV8Y6ZJWzPFTX604mR+O7HHz9+xk0YMo0KJAiFjAaXihBc1ODPF/REa2QOj6B30hkEPIH9ppsaqubMr7yly/Bt7dXZ5m4dpvVpAsPQChG2l/eybogLwOOGgKd9kenAbUEQ+wh2EKC2BMECPn4WAoF/xb4Cfk2FmoFyKI0Gs5oIARVgbhWYV1TzwfnVsOOKCu2XHeeHtu/Y0TxIwkCjgiwIbtSIBMFpG2SiRBe7EFyU4B8t6GUtT/xdBvzmzQ3VVXOq3n1OUx7s1ZRr2xOgot9FAQCFAWhgwg7oUk5DONl/MvgFk0fQ22k+YhSwwY/7aUAXgE9pfgF+FAh7JKgIl4iRwOtWFgC+pI4bC2v44WW1xo/DvcN/eOmlM31uguC0EZyjwaUuBBcd+N2An4nTOzW9E/QypVm6tC6w5prZd5011U+cTyjrzsXAh9amACuap/gXAMJMg+egTR0EQbmTW6ojLW1vjQIoBdZIIIM/pfUZqAaBH0cEG/QGgd8aFVD7Vw96FwB6KBwVVtRzfUmduX9FjfGNIwfbnj5xojch2wk4GhRDCC7GUeCiAb8XbU+GrBvokdq4aXnk8OHSkr9sTigPnIpCVcxMAR25DIFfAWBh0PjT7Lw6AvoFijjZkVJSZy7woxAg+FUDNb8EfqJCNAqYDMoiIcBRoNCtIgh87SweXjXd/GU1j35TthFkWoSCQJQo20hwKVChiwL8ubR9vqAvKfGzO+9dem0bD36pKQ63NMeEckfNzhhDXW1peRn8JpjsCXZOGYCEK/4I6BeMBlk0v+D8+N8WAOL+ZAdYwmFRItVkgv4E4/5C8Z8cjZbVc+O6OWgfxL/87BMn9kWjGsfRoFhCcLGMApMa/F5Bj28Vtb3suXHT9Aj6d75r2abThv+rh0bYml5NUBphrAqwW4BPgh8PEM9/g3UpJ2AoZ38Rzyfj12nwEgUi4JPXh0YA4vrMVATgZaPYp6tQO1CetCVGJQUAMKcSzPVzzaMra7QvPvvU8R1ehCCTd8htJJjsQpDzZY62gwu93yvwZYrjxunRa4Ogv3vr0vUnzcA/Hgqzq/s1oeFtd6TF6bOBvw9i7CnlHF6Sc8sH/EkBsL09Pl2RPD6SJ8hBf3J5gHI+pOOCmeXAb2wwD62uS3zh6cdP7JKFwGkT5EuFJrMATDrwFwJ6y5NhuSyR15PnBkF/xx0LF5wPlH3rjUG4uTMBjHPU7QL4wixVGYCqcMAkBL8C4FOEMAjtjwKCiH+JtSnnWcRTXxUEftuzI4xfNHYl/z+5RVXbDlANFWr7y/PFt6frZ1eCeetCY+f8QOyT27adOu0mBLnsgYvJFvD0Qj31XBEuKgT4mUC/du3Mct+cur99ZUB56EyMCaIsaXcB/qQXR/Lm4GzBoAoQUK3zEdDgD0qzmjPtkjw6Uj94MXhl+oPcH7U/UZ9MLtCqoVLwa3Z2RBH63dnEqulc39xg/lDr6f3K/v0d4UxCcLGPApMG/NmA72bQZqM49967/I43I/7vvjzIpmscmLBmXcCf9OTI4AcuND6OCCEfwAm1H95ivXlTHovzp1ydssuTXJ1JlycFvDgDX8IKcqWivyn6Q1FgpD3o/RnLDaPJG+fzvhtmax9//qljzxRChSa7HTApwF8I8N0ozsaN8+rK6yv/6aUe5d1vRhniN8nricIQnRHxVonrp3g/T/r18fyuQBvrUPKnPOngt9IaxDFKbZD+JtMdEPy65f0hAXAavUiDApofUPuPxza9DPhdS/jz1drAQzt3tvbmOwpMZgGYcPA7gS8HrGSN7+bJIW6v15QoH3jH4nf0ab7v7Rti0/bGLWXvBD99tkcCTp4c2a1pjQY86d9/NnhGiTHDE85kvm+BP5XeIDS/5PZMCgFeZwe3yO8vwO/C+yn66zMUqO2v8PRMxbgIlcCmBj5082ztI799qel5X3/UJNcoGcSyLeCWJuEUgslgCE8Y+L3we6cnx03bz1tZX7J6yfT/3TKs/Nm5GCivxhgqUkvLSlSH3Jay5pe9PMT/rfNc2AeccXgidKogyuMEv1uEV3Z7yi5NFbW/nO/DLSOYaA/aAvW9lcXAdV5toGv0nuXmf3ad6/xca2N31MsoIKdITLZRYELAXwyag9r+npvmN0Cw9NHWIbZCMwBeijM1bGfAJwNUxL3TjFoQWl/8eOT3dgoDCQxpf4OZ8HTodFHAfwHnx3HBzv+R0x0Q5AL8DsAngW8fr++pygu4xboYUybuWWaeXlAW2frkjpbmTKNAJmN4MgnAuIN/NMBPJHoVdF8Oh3zsA+9YfHt71P+j7ghU4o84YwA7qEl0R/Ljy27L5IgguTMR/NQR1mhgGb06M+EZj+AnPi+D7ILENhnwRInsKC8JgOXulDS/I+VZCIeBmn9iwE+/b/08Hrl9nvYhpEEVMZ3jKBAI1JkyDZrsAjCu4PcKfJnfj4yUKrLfHoH/wB8t/dypQfVLQ3FQ0SePL2RbnPlwGniS19soT/J+27h1A3/SBZqWxGYJwFMlpxQjQ+UQbFt2gab7+C1Pj9PrQyCXjV/Z4LVSHWyKk5bzn6I9wYR/XDl/plGjoRrM+5bq//Dsqye+SgJAtkBZWcTMZgdMhhFg3MCfD/Az+e5ra6sD194051+aepUHY7qVh4OautsEtjOR7t0RXEV2cTpGAsX29uBlQvNLSWwpw5jD9mCLMqRkyeexBeBCYzcz+DN5fZAGWVme1n8rBpDu9sRj5SMhMdllMmw1JcDft5r/9uy+1o/39Q0kZDsABQCfEYUARwGnITzRAjAu4B8t8JHfX7tiWtmsOXWPNvUqm3QzlZ6Amv+QDsoZI+nOTwtmYefLwS1U1ZJxmwx0Jd2eDp//4UA3O+sbvKCf5AOo/QsGv+3tIeOXjF1n7k8y54czqOurhNAoE9yKKTglfuDvXWXs4cN99+19uzuczQ6YTAIwIeAnd6bTlenU+LFYn4I0Z9OymdXls6v/cLKPrRGpCQjQlKbmLyeYOmRankU3F2fSuHXk58tR3jQDWWqn2xdmuwPtWcEvgOSoT5skPDYVE45PMaUxld9PrlCZ9iD4LWpkpThb0x5T2h8jwDO7apNxg2KCeDRtqQrA/SvMxvLh/jt2nugcRBoUCtWamCnqtANyCcB4uUHHHPyZ/PhegX/rlbOnKbWVT57qZ8skvm6lYlq5OfzxOPOJubIu4Bea38X4td2gItCVdInSdZJ9gKz+xZKzSoyl5/BLHWd9sxv4Zc7vAn4n/VFMa4qjPM3RmfyGk1omC+VxCgv287tWmKdmakO3v3S4rWeyC8CYgr8Q4MsenduumlPPaiuePdnLFpLHJg3M6JEBgGfiFt8XYHIAna5Poz+p61K0J4OQ4Khxyt/PGv09zr5KS/dhQoxS2wWa39b4NAIkPUHJSK/F99MjwNKURxH9VWF2R90FE9tHo7GLfS/2wr3LzJa55vCWFw+d787mCZroEWDMwD9a4G9cOqOqZFbNcyd6bY0va2VpP8YBtknGrjw60P4FPv90oAsBkO8jxz4FwWKmGX2ltDmYUHRnNllSAGwK4xCA9Ahv8nloRLCBLwQzqfEtwFNEOGX4MpjeXTXmOT3FEAbst63LzVO1I/2biQJlcoVOpACMCfhHA3xNCyqrVtWU1i+a9tTbXWwNafOkxkcPjzXjyvLFA8DTtuZPan/nCOBCh2SjNy23X7o3ZvCBgThvj+h8cDgUrmyr6VolhQTSNT+ycCsrItmnxOkJ9GnzeSUjWQa+dY1V5QGT22gkQLozrW/8o7qFCgP273tXmo2+7p5b97X0jEzGEaDo4B8t8GfMKPFdecO8Xx7sgFvRTyZrYZGiYAEZdaMQANye1ZhKhfySBq8N+CTd8cD76d6IzvsHE7w9bvAo42CaDAx0jXZX9M3pKx+4QgKErPmtfVsAZNrjBL+T+ljaXU56S9+vCJfCrM6aQnE4YfehEfyBK42dPYfP39scCWuTzQYYV/DTVMNMXp2qREC56c4F/3qwU/kvBgWsCMSWT98qCGVpf2tQYABv6EzpQfenI4tTVsPCqJUFILVv0R4OEDP50GCcdyRMiGKZKeDcRPBzBibH/H8Ao7Oyp2GgbGieA1EC+HbFHjECWDazeyUHca1NfZzAp3Pk8cHaPTO6qycMwKP94pAP+IfWGL859PKZhwYDCTOXAFAu0HgkwhUV/Nm0vjNJjSK35M5E4N/yzvmfP9Dh/1JMT86nTc6mQjQhAMmyZTT3lgEcN0A5YSQHApqLm/TiJLm2HMySjF7d5LHBBHRGDT4s/DYccNBB8Itaa/Z/TO1EZm70VPTW9pUNrOKMZNDW+TZShB63hFdNi/KSxyeZ2mwJnZXtma79sXLbjK7qSevZyUcoMBD2kTX613ZuO/s1pwBQJFhOhRgvASga+POhO27Af8edc257eyD0aG9EpCyILW2mlQ1+0viUsyxq6QCw7XZeTxLoGXz+6aMBN8Ma9IR13ssFXAGjBajtxb4J3FDoOIANfjC5Cj2RQCzUVd5zfcKXqLeBcCEFQmjj9ADOUgFn24cvfmJyPwV8MUm9vwLq+ioApy5eKltDNRgfWJ54YPu2lm3ZBGA8DeAxAX+2IJacq4ORW9T4N2ye1dBjlm5vHoDKtDQDhxDQOYvvpEYHFISdGlP6Lb6dsjrliemSJSoMZZNHBxPQoZuiuKZpgx9bwFQeJCWC9jg1PzAlwVWzE3B0YGAMhobnDJYOLU+oiVl4W5L6WKi1KnWmJrJgtrQYtDBrGg1an6EC5uqURoKiLg+mLjijxZeKAGyYx8Prp0VufOOV9mYUADkSLI8A2QSgmAGwooA/F93BRDXi+TPDIQUrKiDwQ8Mqa2ioKZ25ctpLR7rYUiegybfv/CuPCpSn38OB7dJTWZ1pAiLbAgA8avDBYQ16LXCjqSlAbs1YscCPJWI5an6btVtaH/m/ijJmIj1CO8BU8K8JuuYzAsOh4dlxNV5nKmapwXQOjMcZ5+2KyU/4TH68NGGerYwoHXXh+HBd2B+ti5ZCvxkqi/v8lQlFLQNFKdPBP9tU2DyTKwtNxlZxEK7eyZHIUwQpxCAYb+7Z2NzcH4lVGJwEoKM8ZlLBLMoDGmv6M2rw5wN8rKxAQSx0aeoVfuWPbmn45z1tyocRSW7gJ6DLf6XrhNdH0CMGsE9nSlsGw9fWw8awxnviJkQsTi/4vF1DmSPAUfOL42ng5xh7NTlnShxUswOr6aOQmNwGP4eY3zSbgpp2sjYeP7ugfaDbBJxhABDS8JelNq0k/TOd8UeJ7FlHYn77sxH0t5ZOWxVRg+u5ot5iALsRAAov3VYEAI+mCZwP8KdrjZ+/9lrzp33Dmun3x81sMYCxFICigl+mO7KBG59XpVBJETRwCfj3bp51+/6+0KNDMSkvJ0VnZO9OmhEraXXBM8g2wNzLHRpTYpYsiI1Ys8F5fEiDbpOL0IAAPQKcI5CtfUsQbNpjCwU6neicwRXo4oxHhRdIgRGfbh4uiccOrG3pOYlgR6ATuEs0vNXaEonUvhfgBALJgDVE/dY+CgcKxIivNNjpn3ZLggXvMxR2JwDkX8TTy0OM4TU4I+zBlYkHntp+bhsJAOUBjSf9GRX4Za3vhefLnp2rrqqcFq2s39XUDzVpvnnbl5+m6W2OT3Ntk+/FdnmS5ke5GebAXteZggindhMmj4Q1wErFSU0veL3F9S2NLzw5lksT6YxEd7AdkyvQB4wPApiny+OJHUvbug/UhqMJGewE8kRZpQB+WSJdy8fj2YUgGEyBHu8fCVjaPzBiVYpDoZCFobu8qrzDV3+/ztQ/McEKCF4s200NfGAZ67720KGhHq8GcLHdnwWDPx+6I/N8NHA1LaxsuvvKn+5pU+6ml5WcRC4ZsjLXx+uoHIPjoTkGU+Qo7SAAe1Njahzpg8GHozoMEcClvwLg3DJcJfpjgT+dEsEgY8ar1fHIM9c0tZ1GwJNmR8Aj2AnoMsCR09LvQ/+2F2BiFitdhzYR7ZNgoECgMNDogMKAo8LblfNvSqglnzaB3erleyb6GhTrP15jPHfoxcMP+P3lpmwAjxf/Lwr4c9EdJ8/fumXWO/Z2hx4djAvMJjMr05LSpLTlTLaA/QKt5DT5egDAWV2vRKG7B2FsUxzJc2N5YWyNL48IQstb/hkxIiicv1UVH/7+tafbzxLoZcAT2AnoMsiRy8ogc66r5QSgvJ4XnsMpm3QNCQUJBAqDLAgkBMeqFqyPKIG/4cA2TjTAc33/zHIw/3R19IHHt7cn6Y/M/4Otg2Yu//9ovD8Fgd9N67ulKMv+fOL5a+YHK3xzZu1u7GIzbM1uRUfJLSm7N50RWcc1Uuda2t8+gG219Rk7TnQZB4er1CvCZcpKrrBS1Og2hxc83s6CFp6cpCBYbk7OdGgu17R/X9vRtAO1fCbAE9gJ6DLA3VZUxEeUV1XEz26rOAraJK3kSIJBAoHCkE0QTgSX3Btn/v/FAebnAuFEnr9lIe+ZMXT+6sPnEmGv/L9Y9GfU4Hdq/UxuTaI7t9975f9+tVX5C53q4NtPIGt3ojsUGZIpEb4oSkcgq1aA2Ob/Avj95q6TXfp+BoBrvxkoFpEKNjtWoszTAspMAwBVKgE+afAqJkR9GrQEE/qz13U3/y6oRXUn6FHDy4DPtCAcAcptLV2vYMu2+iMV4BXCZAuCczTo9NeUtgRnf8lkykMAMHb1Db3+IJfrsDbqQ9cZP9j1zOG/KpT+FKr98wa/FyOXvDtId8ifryhRdeOWK644q1W9fn4IcHXmZCpxMqVY0vQylREjg1ReRI4Ap/UnA94zbB443q7v4lzBfAcNffUipsS5mI2iAOhxv1KqBVgJV8DHMGlNB82X4P2qwduq44MvL9TauzKBnuaoYltua13R87itqp5pFXW6x231dnlxa7e1w/BeEoRMQnCodOmGuBr8ngnQMAqcjtmtq2fwxOb6wXWvv3K22TRLDKf7k+iPHPwqhvYvGvjlpDU3ulOujSjX3776V6+1wC2kvUnD2wlqVi2d1PREgXjS/tK1aQlqmItjCwcbiPKmI+e0l0zhlxe3G6CkllExOdcU25jlioIWKPrrxbWqaZ6YHTm/oyo+GCcDFvk8afpMtest8KGFYW1uAHeuip4LRfJ6wG6CkW1NMSrtgpRIHgl6K2dUtfpmPsIZe3eu75+I8w+uMV5teq3xvrC/zMxFf9x8/4Vo/7zAn0nrO7M13bw7d9y9ZN2+ntIneyJWMbQkpbGeQDAYURpc5vw2gtMEgAxkus5+UxGNdx5s1Z41TEBQm4ACgMCmXB0GpgA/B91kikLnOFf0Eki8sjJ88jBqexx6M4E+12qG+ChuQHdb+TwbwOR1gum6bAtkyyOCs8wLCQF62IyygNIYXP5JnSlftotWTATOXb8Ty6P/8YqRu7Y9fXLPeNGfooMftb7Tu6NCTN2wadkzu88p11rV8QWLIS9NKiBlg58eKllploJVlMrscIfqJkQOt+qPRTQzivOgxPK3ppWuwIEZCuMm/uU4CpiYsCYEDQ3eSKUZf6Zh4ORZ1Paocdw0vZdF25IgbcxQ5AcAsq1yjvc7F9aTUeIUCC+L7TnpEEbU0U3aWL3yrrga+OFkC5Ddv8o8dHLfsVsMCBnjof09gz8frS8HsxiLqrfdsWjjjvaS3w3h2ld2dqagPnZqgk1vBHUgrk/DgTMARnUSpCQ380Sn8Wxv2OhA4HMULuufyRkKAk9pf2tiisjjYQzC1YnIb+YOn+4mbU9c01mElaiN2wrmblo9F8jzVbduQuEs245tkrMB993qmqLHDakQjgLHq5ZfE/GFfgMAtfk+z1hdP6MczD9bGXnXiy+e2sl5iVGI7z8f+lNU8JPWl41cHGqvu3H5C3vOsytpMooAsJX+aKUw2LOzCPBpVZOlGVlJwEulBjuHzbfOdpmHbP6OQSshRJwEQWh9MDBN2crXwQQ1ZbCGD/9yutbbk0nbOwsuOSlNrgKsToB4fSmZVp2U23OrZI3n3ZZowneC5+TCAEiFcBRoCcxaFVZKfs8Bpo8VoPNt974V5tun9x+7WR1JmE7j15n6MFru7wn8+Wp98umjkbtly8LrX+8tfao/msrBSZJ8Ar/F7a2lgBzpyrKhmzxnC0tM410HzxnPAQieL1KRRWczLDJuGbKccZGlaY8IOBllsD4y9PN5Rk8fcft8i63SCx2vJXi8LMMqhHOlFerwUt2aRoGj5XNWDysljwHAtHyBOhbX29z/j7a/cHofGr86xAya/eU18utV0RQF/E7XJvn0Ueuv27jsl2+0Krc6/fjCS4O57XbJQTlA5TR60wTAzv3hHLTGNuPJcIIPiSxM5PkcMPPSSlizA1r4LTxFfcIV8cjPliS6uhD4TpqTqb6kAJTE5ccL9E5weREC2TZwrlDprHtKAnCkfN66iBJ8fLJki37wKuPlI3uOvxe1v2z8YiHcYmr/gsCPHezm4ZEzNlHrr18/f2FjvHJn27Dwp5NmT66EaIdkOdbKpPCsrPnd+D9RpY5Bc39zn/m2NanEdm0qVi4+NmcFt3BeiND8KAxGhRb78bJYRyslUuVTWRjbnOjakrIweFnfINcoINtm+8vmv1NTAz+fDF6gJXVcu3P60IY9e1rOOF2fmbS//G6KpvlzUZ5sWv+Wzcu++vwZ5c8lbFOuseD6svGanLAn5ehI2ZoWm7HdnAmDDxxqNZ4VVEfBIh9goNa3Nb7g9zjp3CJTmJHJuN9IPH7VyLm9XiZRe9H0Xjt4LKhBoUIgR9/lFHMMjuFo/Vb5wk/rivL3Y/3MudpHYHz8OuPfX3/t+F+NpfbPqfmd4Peq9UN1laFpS+YfauwCUXNDpj2kve18miTPl/35VKbEkcYs3KOnu8ztfVGz087JARMUjfZFYMvm+JjHg/hXTGPP2pHmx/IF/kTRm1zgyJcOZVreyW0u9d6KRT+ZDIEwTHlW2ltWxnqHYmOl/T2DP1MOj+zhkbn+vbcuvOeZs8EfRbVUESeJxogZWLbXJznxVtAfW8Un0xnS6mYChOP8/Iku43WaiSW4Pnp2sH6VxfuFd0dEcq3wb8e8eNt3gzCUkMtmXAz144shBG7zLLBdGgWcAhA2pleeL5u2EwDk+kT5Psqor68IAv/U2thHn9p++vFCtL+XkTkr+N20PvHIld3TFfQlYzSXuKMPQipy/TALqjdsWfS7HWeVG8l4lbk8gZ7okJ2OLEIAcl19qUyJ6EzU5Ce7zBdG4uYgfrQ8OFj5BjBxnubg4nxD3U5q06uMke9O11vb3IDvli7rxu29dOSo33YRG8i1yF+uMjJnAos2JJTgsxPN/9+zwtjT/PrJu30+zczk92+s7zIz5fzkem95g9+ZwyNHczF5DT08q5fU1/WGph062w9+JOQ0zRDfL+5TiDdpBNs1eFBRp2l/R0rzUIy3nuo29gpBQK+ObcgCB43jWkLC64M5bBbn94H+zIruMy+hV8dtmhy2gx2XzZOTqwOLiNmiNpVrTYRcAnAysPyrpqJ8uqgPlWdjV83kidX+ntVHTnb3yn7/TFMe8T3mY/h6Ar8b5UFD15nDg9FcXfcrt9294E+eaAp8naquEedPgt9eIIJGADR+EbDCCHYKAA0PAPxEp7k9osGw4PXWvEOsbGNgKgOOAgpgyQQKckHX0vC5r5cbAwkvwJ9Mnpw8MZLx8tEIQGf5jLJW34w9MIGZoH4V+MPr4l98+ekzP3Vqf1r/S874dAa9cimujODPZei6ZW5iDo+h+5RrNi16aleryONJbq7BKskQJgHBgSFtJqvdRiQBnU1d5l7B6+0ZWBzTkZHj46R0xqwJs6j1FeAlPPbtlYNNJ/KdGUQPnKvjigXQsW4nHwGQPXeYHXqwatV9GlN/MdbPmK39+1aYB07ubXqH6tNNrzk/XrV/TvDnY+gqSkhduLK2aqi8/u3TfeBPKe1UOQW3SSv2Q4iUB2lf9J4LkgwAACAASURBVAm5QJt7zV2DMeixKI9VQhDBz4ChaxMjujjQUFWGtxfHj31vImvCTCRg3L670DnXJ0IrnuHANk3U71kzkyeWsu4Vpxv7Bk0zZngJeo0Z+J2GrpzKgIbufXfMv/v3TcEfJTBT3lElzQlyaRJLcl0TEgAx3dDm/AkDwic6zdfIw5PKwxcL/aAQCPDbZQb1Em3kaysiZ1qR7ngNiVsjj2jvkt3yFQB0ZBypWLkhpvpemKhOwfW+Pn997CN/2NbydDmPG5TyICu2QqmPq+bPl/KQoatAQL1u44J/e/mMulUkE9utU84OikOG3B2pzqU0Stg93h3mxzuHeYvCQDfQNrAyNnWM3gqtb6VKYIoD+vgPrB088v1cdWCKMRNoogAxmu8tZP71mzVX/gE4u3003zuaez+0xnj8yO4zf25Cwiim4Vsw+GmKIvr2ydBlzK/Ov3HBwQMdiijeak+ySnl73DI0U9MTkxNZHA/FT3abr2oGYE0qHE+EexM46PZKPyZgCoO9lfLoVxs6jjW5GUR4yWgzAUfzEifLvflORT0x/erNmqo+PVHPv3kB7x3Zf3ol55pRTOqTFfxOvp+L8ty8dkbD21rlG+eGmIg6WbTFGgEyaX9ye9oBsDTagW1Edehr6TX320aukZysYmVwigQ2a2IKR85/+tqBI38/RXeyw7QQ+vNm9VUY+JqQwlgLasC4c9rg+lf3dzYT9ZEnuyC1LYT65AS/M53B6eWRKc87b5l3/29OBr6t4VQRexPJxdLkczIE5MqUchwAr0W3DU5pxDt7Rvix3mHeJpLX7DqaguYIy9cqFiv2GfCgYXx3UdfBN7xm/10OPD+TGHgVAIrgH52x9k9NRfnXidD+WOHhs+vin3phe+uvc1GfmpprBR5kn38mW+4C8Hvh+zLlwVlB6OVBynPdbfO//exp//1yo6T9SQBkO8ACn9Wd8j22+hdG8Jle83VNV7CapwB/MoFNDCn2jC0R7OKRWeFzn17kV2Jk5GaL/l3OwCcA5+L/8kj/ts9f0e+bcQoAyidCAD54pfFY494zD6lR3SwW9ckL/M7AlpzOoEBCXbZ+5WtvtikLMoE/bQRI5eVbOlwSBG7X2U8YMNTSBwcwbwdzdXAWFnW8CHRZ0WLB9xnnLy/v2P9Dr1r/UvfseAVorqxdrBRB2r9x9tofca484LXtYl53x2Kzuf1A43oTAgZSH7d0Byf1yeXyzAh+r3yfAlvTqpVQaMGiE8d7WMCpyVPaPUWBnALi0lF8MMZbu8OsGazJ6BjFxdss/75Ni+z7DL9mfG2d3nHAqfWnDNzcEMwmALL23xWadbcO6m9zt1j8K66bw+NlnacW9wyYMbeAV6737qbssoLfje9TLo+ilKpEeYwSn3LL9TNX7uwve74rbC/Bk57DQ7Rc/HVyfHtSuziHRN+2B3j7EH87HGe9lma3c/StCC6NAFbOPkB0VuzMR2eMKPFizvQp/iucnC161f4dAaWkvWJ+60RUfZhTCcb75odv3f5mx9FcvF9e3IK0f9HAT8sJyS7Ou+6Ze9+jRwPfTRjMKszjwufdRoAk/5RwIZLfOJhnes09JlfituNIBLSkdX8ooouUZ9eN8bZ/zCX9Uzw/s/B51f47Q3N+x0GsCzCum08B/lfr45947plzv0eXp1uuj5vi8wz+bMZuLr6/5dZl/+MPx/2fuCCwZXMgObeHeg09QeQSFa6dlMBwzYCR1n7zIE1RFKOCtVIix4Q2O5or7vGZ5g9W97c+me3H03dOcX3vAuA28h+qnf9Jgyn/NK7It7/sY9doP3jj1eP/k3i/l2hvNt6fRnsI/PnyfTR2125c8bOXz6q3CQojLTEkAToZ3SV6Iye+JcFpxQV4JAE9HUNmk01rxDxdzOkh8NtUSDx/CLSHl/e2nSLwu7m7prR+brhmmr/R379PIcP3aPWcq2Kqf3fu1op/xftXGS8d3X30gwj+YvD+nOCXfziWwqNJ6rJ/n40k1EU3L39h1zlludxgMnfH1v4pgF/YMTIlQvD3RaBlIGK2CfCjGWEtImEtHGG7OK2hAmJz+7s+EJw2rBcS6Cj+K7q4W8ykAHHmF47+arjKfzxU3zERvP+uJeaJjl3HNvOygJGL96MCzJXinBH8mYxd4vuyf5+xhFq/btmhg+2KWGLoghweB+rpSwXVcfj5KS7QNQzHRuLmEHp4GFZes9fBJbeovUA7RnaPru1t+XwuyjNFd7wJpZc4z4G6K17lANd7a7F4V93UwPvCbx1dheAvxN/vxEDB4JeN3bIyHvAvX9R0tJuJGvAyv79A+0uAT6tqKeX9YBvtg+ZhLJKMWj+5Phbyfbsv7VlgaDQ8t6av5REEPwW1ckl88V7HpdlSLvrbZCz8LnDlo+P966+ZzRMVzacWjYywRDGMXk/gz2XsXr2kvr7JN31fyyBLzkKUa+7LApE0bO2eo2QeecTAPJ5z/cZ+XOpTUB4ruGXiZ+HtQYeSndvDwPzBQt+ZX+cb4BjvF3cxfV8ux8cZfeEXDFD+z3j/pqXTuLautHPtwcaenkKCXQVpfgK/XJSKglto7N60bs7i1/urXxY+fkfasmVopqcvINDdEudJErGU+LkB87BV4NDO37HyesQ0R9HpAvwoBfr/uiF2YjvWyHfz704ZuvlDNBf1eT207H4T1P/Mv+XR3TG3Coytc/o379hzvkn2+ORKcsvk7syp+d0yOZ3G7q13zLv+2dby3/VFWbIaGzUslx6UGY9bN5BQaAaPdg7z49YELbvyWmoBORpIEPzcB9rD62MnD2YC/xTXLwxwMvWhuj/k/NhdsnSdxn07Cmu58LvqSsH8yPLhd217tXlvPh6fnODPxvOyZXKip+eOuxpu/f2p0p+GExSCSl8cjtBKowBFcSknGY/LWZ5xnYe7hvkpUYnBBr1J+T8i2IsufytdtESLf/Aa49S5Kb5fOKjc7syGh90lixdqPHC0uN+Yu7WyAPBPXR19cNszZ17O5fHxgoek5nf7sbKbM1NaA4H/1yfLfhYXq15ZzknbSLU+umRuOo/Tcs14bVzjQz0jcNrOXSABEHa0zZgw902Av5yPbG3oONbnNaSdu4unrqAecGp/KlvTXrO8ri9U1j7ePRXyAX/4Gm/gRyaQy93pCv583Zx33Nmw9RdHS7+TsOdTycarDHLB0qWcH4u6X7jFND7QF4Fm250pBoikWCUzOwX42Qx9+M667hNDU+AvPhQzgb+3fmllh69CFBMYzy2gAv+v6yJ/8fSTZx8vhruzIPDLbk708SP4f3S49Dtp2txF2zszOZPAdxgDNvjPCfDbVZftheOE5hdan4OKZ9f2nr0xkeg1psBffBhmAj9Ge18LrcQF7sd9+9INkY9ve7b5Cc4DRiHTGmUbsGjg/+Gh0u84itGmlW9IAd9y2QjnvU3oic/QSBDX+WBvBM6JnrUju8LTY29cVPW3JrNsijVumDJ2xwaDFwP4R5PglhP8biXIUfNjmRJ0c4YMVUHN/5O3S7+DKz7T5ixYZUHXQrAN/zQuI7++mM4H+wj8dmlyMYOLAcPcBmkza2PdN8/qb4nn4ndjA49Lu9VMdmB7zZKKvlClSDUfz41oj6z5xxz8cllC5+wthfnVuzbPveX/nSz7KXL+ZOqCa80em8XbMVs57ZnsAexM9Pb0RqHFDmRhlIvm6VpT4i0ZEsfKYuHbg/Vv9ss1N9G1NeXiLA4s3bT/SPf10+ITaPC+9PzZ7Uh7oj7DdM7qylTG0M3d6UnzewH/o01lP4li0UCXPk+5MVN2K2V+ypfTzQmdR3ojgJMmcEtOUEeez1H5Wx4kXGlOCULs3vKKXa0E/mz528WBw+XVihv4hwdvXKopgbfHuycsV+fIh156vuXlmGqYblHecQf/rRtnX/9ka/lvB2MiwptegCrNVUPBWWs+olNSyEukGxDvHuHNdueiD0msUYH/TWDWnF3b3x8wtA9XVO88OAX+sYGiK/jDmzZMRJCrtgSMP1k2/J6Xd7a9aWINH3s+bz55/XkZvG6pDVSDHzk/0p4brp2xeGd/1QsdYYY1Zsl3k1aEiiRC/LU/qCJZ88KxwjDB6B7hWCkgaULQDvn37c9MAePz1RU7np8C//iBvz+85X7O4Zdj842ZW51bCfo9swdue2NfZ9OkAf/ShTX1p9X6PS2DIqszueoKAdQx2TwJfqumTwr8NBEGnUDdw/x0cl0tW6Bshw/N28UbkVH9c03F9h9NgX9soOim+fuHt3yBA3xtbL4xc6uY2HaV0n3dyTP9PZMG/KUhHggsXna0sUtUbkijPQ7tnZbQTK7RtMxO+4beCD+rGaJKg6jdLw5bhWTxE9Ir/MAUMH9dU/Hql6fAPzZQdAN/3/CW7wGAWGhwPLe1s3g8dO748kiMJcYV/P39ncznK1dx5W4n7WGgqTOvXrlvX5tSJ4JQdiQ3qfmlHpLQnx7cpcQF++9QjLfHDYhaJXnEiitYAtGaxSVqFgqTQVEADldXbH/vFPjHBoYZwI+lC9eNzTdmbnVTA+8NN769loPfGHfw4ywuN1cngr/h2pXP7WxRlrk9ughJ2aFZ/EulSST6n0zaoftHErw3qsGQ9Vl4OIUQ2Ln8VmozjgActNq+s2tWll2heSlRN94v7GL/Pif4O8qbfE36YvTxl433b7triXm8/UDjO/y6qmfy9tTUzOButTvHzNWJ4F95w4ofb2tSb3G1YG3PjrNCgy0QF3h98Lq4DuFwgndba7Rbnh5hAyDgU9QK9xUf07ZuiPqPTIG/+HB0gn9nCK7hALhc0bhv711pvHR815kPh7hmjJufHxcsdpYklyO8ms/wbdi85G9+0xj8mJ21kNLX6V2U9ARZFN5KcZD3KZkNZygPxvh5QesttyZtRJDoOE4f+583xuDnU+AvPh6d4H8tCJ8CBt8o/jflbvGhNdq/79rR/BUCf6apjMMzdJ5pZl9erk65VqNbsSpMbIsxv3r3bbPf9R+Npd807AwGOa/NxaBNGcWSAFD8105lgIEIb7HTGUQOP/n6bc2PlAcNXxSCpzfF4C+m5u7mBlA+V7ilN7wWgscB4O582inGtVi06gvXRj/z9Ivn/5AL/F6LV+WM8HoF/8Z19ctf6a18umskOY83mb3gTFt2iQLTAJAcCRDo4Tjv0kwWsw3ctD60DV6aMzxc2wtXQ7NYwAKmorzFgJvwXIhXRXWc4FoI9XHAle/HvVLz7ErQ758zdM/23V1vy+DPt2LzqDW/s2wJav5pZRDUFy061NjFQnbXu0ewrJPJc1TjKol+6VzUgKFYQhi9Vk6DveFSRFZqqPgvbAAf1x/cEPe9OkV9igN88ZIk8OMcjzdCxj0GqH8o3jd4b+ma2Txa135mTV+fHjFN3ZiwfH65QC3O5FIUn4q8v+G6ZS/ubFFw2fqkcpcruGX7qTb4KQAs7kfePxyHLqQ34oQFeMvTb9EdWqwRgJv/uSmu/Lcp8HsHVK4rXfj+T4HBg7nuG4vz71honul/69gWzn0GgZ8KV+EKjaYZMWjZWblyXzYq7Ep78OFR0r1Wa0PwM6arq25e+sgTJ33vkt38SaIucR2cski5PZTT79ZhWJFqOMa7rFUorIUoJI0vKA/l+ADAkBqDNVWNEJ+iPcWBnwz+6EqoiIZEsmFFcVrPr5UHVuuPNb124jNu4DfNEsPvj5vORQhzpblnBT/O2cTqDbkmsBP4N9+24F0/O1byiO5Iuk/SFQcfSub7OIwCKc0BohoMaAZgpWbZbhZVzmlWJBnKCoO/rN4Hv8OvmRKA/MDlvNpZvqTvGhHR/bfRtVrY3aoC/DNro5/d8eKZP2h+VacA12jrdabZnm7RvEyL0Ml1ezC5za8ZvhVXTpt3UKt/qdXK8UkWrM0AfhEIlmdzyV1DSMcMz4gGAzb4ieYQ15efHyMCu2r2wdYp8BcGsrT+dxi7fWthHzC4evQt59/CFdWg3VLbdcv+w10tbtFdZ92emeEEl6s35CxdYoE1Zd3Lk9i9ZHZioIuBT51x9dJdu1qVeqcP36YoqZRPWzicniDqGjKAEfThOHRLal+kNtjXkdGb/MxMuKvmAOyd0vz5gywT+AeugS0mwIuja7Hwuzcv4D1DR45frxs+HT09cnS30HRmwmPyqbKB3y3Qpet+BReiI1+/T9V9qzcs/uYTJ333iMbdZrbYoBfC5tIfeAuVMaHRA1dgt/N88FAyt1+eLGY3pXKAZ+regg/mWo+p8Fdx6d/pQnmeAoBxX5CCevqDq/XHj7xx8jOc+41i+fg9g9/N1+/m7mRMU99xc8MdPz9Z+r2EbkVlUyo5BRpJq0uClx7xle0Bk4MR0aDPBfiW3ZzK/LRokQIba9+ExintX5igykqw/1q4kXMY9+ps9OQlfjA/tSby8edebn4ec3qK5eYsCPwlJX6maUHFWbKQjN5ZDZVVXZVzdh3rtvz96aQ8VbEhfYhNf0kib9lRtCemwbDBQZOivEng2yMEfZUKHLbV7of3Tmn//MHvovVfAoDN+bdUnDuunMmj80fOretsHR7K19ODT5DN9Z3R4MUbifdn8/gYuk+hGV1o9AKo6oIbF/9622n1GjfW46Q6UnBL9BY5+uXrcF83IRHXYcTuUmHw2hqfEt3IGBZ/GYf7vrEVXqBXMDWh3RsYZfB/7nF4t8ng//N259hctXW5ua9134n3ctAN2dgdracnq+aXwU+rcuBEdrlSs1tePxq9N29q+MAvjpf8H8NMJaSRIKSB2iEJBHz8bjmiSwIS02FIlC1PAZ+8PkmMSyND46p2uOFjHwMqooj8KJN9PTZv7iJrNU3r/wbK+kJwCAAWTNTP8KvAP3F19K92vNr8m9EauwLsjvfvqvnxQszncHp8vBi9yPsXza6sOV81e+fxnmSqwwX95+T9Fxi60h10rW6CljAgAgAY4JI9PiTIKepjGdx/94174B+mtL83+Mrg/8yT8HXG4Qve7hybq66cwaOLI203nmob6vdi7HrN5pQ1ZdqTZ6vSRStxO5cmwnV4Kc0BI71IfRZtWvz/njmprnd6fGSa4zYK5DqG1Acjvw7uL49ics5PgnHY+I2tcHhKALIDVAb+w4/BDaDAq7aSGRtke2h163JjV9uupg8CGEY+aQ1eS1deQMud4Mdn9JrmYIZUBXm/4VeULTfN/6NHT5T+a1RP5d/IwBcpDva3O4/L/eIUBoODrhlW9qa9yYEvEoJkzg8DOGT6YNO37krdM0V/0pGXpvGfgQqmw34AWOgBn2N2SXkAzI+uiHxix+st27zw/ZERnXvN6fGk+fEi2ejtKA+wXLzfNFQF/f0lwUAgtHTBzjfPs1o5XYHojRwEo4chL48b+GUBSRgQt7k/aXkn7XFSou9/8154eEr7u2M1Tes/Cb8CDu8bM1R7bHjjfN6jNp25cSAeT8iUh6q0eQlu4Vd5XoQaL3a6ugrl/Qr41HU3z//vvzoaFAuXiTVEuTV/NxPI3ewAx7UC1AYHE/N9JOrjpD0XjgoMHvrmPfCTKQHIrPUffhI+Bxz+2SM+x+wyfMkfWaP9++FXTn/VSXkKWYVRgMPF2ZGR9uANTqNXDnbl8vcj71+9um7+Pr3umeZ+FpSXHXWZuihPYrH2XSgRrUiBp00TNBQCZyhBeiNy6gPuxzjAbd+6F5ILKF/u9CdN4z8hcqLQrZlcVHDM0J2j4UW1PHZTae+dB4/3NMuUR/WFzFzr7+azLptrAoLb9DXK8PRCfSjPh5uqsuKGhT946qR6SxLQYhVpa8Mvd2Z2Snk7yXSeZNozrVFkjySY7y/ZBDLVSYutSQLSbTLY8u174PjUCJAa5T/7BFzHAbYDQOlEAV7+3vevMl44sev0x0307dspDc58nlzJbLkoj0wV0n5zJvCTv9+ryzNgKMq6G2Zcta2r6tedw1ampxP4VOMnLa9fymKzKzInqZIsLCgLhin8+G7eH6cdIH4jBzivMNj0jXusQriXq/and/zZx+FKzkQwsH4yAH9WBdfeO3fg/jfe6DrilfK4uThHDf5CqY/s9WHMpy64fuFvnzupXGWDL9nHF7g9bQhLx4UGp+CXW+AL835s4xcvTc3sSg0uzlEAzzRxH9z6rbvg/OUoAAT8h5+CVWDC8wAwczIAH59h61LzYMu+U+9hCr7WC6O66kjCzDR5JR/Kk1Pzy+DHfXJ55qI+uGAFzuvFgBcavjfeMP26p89V/rw7Yml/AWhbhZPKtj9TlQbxbGnhWIkupQmNZUgb9mkZ/G5BMPkdn+EG3PGt++D05SQABPzPPw7XGgyeBQCssjcptulloL13weCH9rzWsTehmmYmL082yoM/RM7nyfZuXTm/Bc70mfu5vD5u83op4GUGVGXBdQt//WyTkjYZQk5dtuYppm9SKRNLYNzSoe2bDC7W66UtF/Dpuk6Vwbv+5R7YezkIQJLqPAHv5ACPTkQVhmxStnWF8ea5fWce4Fw3VA3Bn5qvm8nLUyjlyaj5ZfDL2p+WoiSvT1mZj1GuD2V5yolusuG7aUP91c+0Vz3abnN/uRPc3JnO4JbTMJZHDxIMHAFc0h6y/U7hBVI4fPKRrfAfl7IASMD/LAf4+mTw6sgYQK7/nnmDH9i5u/sgGboKLkBh1+F3S2STA1v5Up68wS+GFHteL1EfLGCbqZiVaaoKaf8E09UrNy3/we+Pqcn0WNkDZIM7yfHd0iBcbYSUxwhpkpms6Hxh7o+b0pFHvh/UVsDDX94C8UvNCEbg//VvoDIahO9MVPWFXLzqA6uMF0/uPfNxp9ZX1aDpVpmNanLKUxbzoTxZwZ+L+mSa4GKUBRQFAirm+qDhK2v/1Suq5x7Spz3d1MtKZM3tdFc6g2Au7tAkR3K5F6tES/6ijN3uRvn2Kyp89JG74eClIgAI/M89CRtMDr+Y6JSFTG9ieT0fuTbQc9fbxwbOZ9L6sqHrLFEia338Dq+TmDJy/kKoj1y+vERHrW+VMkTDF92eqP03bFn83x5tDH5UN6SlRSWu7uT1FAtw0/oOGyH5WxD8TvvBpeMz/XYNOHyV++H/Yj7QxSoECPr/ug1KE3H4OwD4LAAknQ25tPB4nscyhB++Mv5v+99o/Sclget5WklsCnp77LV25RUXK2I6xxIlWIkZtX6mZWi9UNi8wI8N5mv4OrX/rFpfuTav4ZmdLYpwr8maWwZsmjEsG7rSRTJVSrMhLOM5F/6z/nYAOAIcPvPNrbD9YhMAoe2tiSjI7Sc0QS2XIN3cYJ4vOXf2rs4BY8Sp9eWILhamGgwkTF9/1My06Jys9UcNfq/URzZ8nYtXyG5P1P7o+dlw/YybH28t/7feCMMJ52IVUjcDljouLedHcpHa51MaP3mDtZNFAHIBX35nT3AGf/ute+DQxSAEn32MbzQY/D0D2JILeBN9Hl2b9y8cfmj37s7XZK7v1PqYxCa7NzMZuvlQnpycPxP1wePk+XFbpNrp9nRqf9NMKFfftOybvz3quxM1vJtL0wn85CiRDv6MwJfkwDkC5AN8agbXBXhMVeBrj9zLJqQ2fS6gfvZxfisH+CIHuCvXtZPhPCY4PrBKf+rIzuOfDXCfQX598vDko/UFHhuB5ztnOycQnFmeTurj5vZ04/6y5we1f8OMYE1P3dwn95xjGFZP0/yubk2JIzmN3DT6lIHsSCXOR/fuGbwOHL6vx+D3330/C4+usdHd/Ze/5uX+EvgvnMPHAeDa0bU2vnffMM/snDlw/p4zbbE+jObKfn3Zw1OI1vdCeTxp/lzaX67oJrs9Ufu7eX5QCDDqi9p/4w3zNz3VVv79jmEWkPP78wV/Eu9ZWH4uA6CAV68xgIOcwaNche98+26GKdZjvn3kZR6qGobbOcD7AeDdky1Q5aUDZlbw+LvnhT+++83O19DIdWp9XF8X/fro4cnF9QvV+gWDf7TaH2d6Yc4PNzRl7abFf/uro/4HNdv7c5EA3/mO8bFPMICfGSY80ZmAxt+831ose7Tb373MfX0jsAZMuBEAbgOA2ydiPazR/g66HyelP7ha+9lbr574e0UJmJTDoyYCplyTBz08Y6n1PYPfq/Z3ZnvK2t/QYwrW9MSZXuT6RPpTWc5KKpcswNSH5c6oLnVYhnm/qaS3NN5z4WsaA62fCwsRBtBoctivABzHTFKmwDnFhA5FgYEIA+N797B+bOSLz/Eyg0PQ0KBOAZjGTZhrKnAFmLCEMcBkwFUXo3bP1EF3LzWPhE+cfiAcgZjTyKW0ZdL6mMAWqzB4Jg/PaLT+qMCfj/bHsobyJHcjkFCwxAnm+yP9uWpZ7exjvpm/P9jOaixBS3Wd2M3g3vRCd3LIRS4QT50vYg9cM4v3rOQd9zeeHjnvpDtKzDBpwYl8/Pr4ePkauvSTchq8Ke1rJbrRRrO8hPTVb2fOXH/nTC/K+UHXJxm/Mv25fv3cG7e1lX///LA968v+ootQ6xcRLpdOU3MreeyeOcMfe31X6xtudAeNXHl+Lrk2M0Vz3bS+0OZ51GbyDH436uOm/d1yfmTXZyb6g/z/hpsX/tmvjge/MBizptJl0/ppGn18Dd1LB5Hj9EuqQqB9cHns6ztfOfUfBHzy7qBP30l3ZCNXzuFxRnNHo/Xzoj1O8ONnN+2fzfVJVZ0NI66Q71/2/vhNVVl986Kv/Pa4/31x3V6KyEZ5RvfmFPDHCcKFfU3QB+YHlmuPHn6j6StM9Zu56I4XI7cYWj9v8I9W+6PrMxP9If5f7g/4512/4F9/f8y3BcsdJmlXqu8ns6FbGEIu0btUBvw9y/SXzh449UnDVHUCvojggt9Anp+L7mC+vlsOz2i1/qjBT9rfjf7ISxkNh3ysKhFQnPSHUh+CZkKR+X9VZai0dnnDjx8/rqyV5vamUTQvxu4EeHkuURjn/7MwgvuuZcbeoVMtf97dE4k4eX4x6U6+XJ9+TV6cP6mF7VlebuDHY5mMX4r8yt4fevl4/wAAD/BJREFUpD9O9yfy/1kzSyvY3Hk/e+6kslJKcsuZziC/pinw5w/aYtyBwL9rsXlQb235SHvnSFjm+XElYNLqKpjCgDO0yLsTjxvcbWE5SlkmulMMrV+Q5s/E/d20v9P4DQ2rTJ7xhWnPTv5PyW8oAPPnVdRo02f/9LmTyjJnBqfTGHa+tCngFwPG+beBkfo7FumNpZ3tHz51bmjACXzM3XG6NWWeT96dsaQ7o9L82bi/kE7b9Skbv26pD07+j/5/NIBlAZg3p7xanzHnx881Kaukmj4XeIKmNH7+QC32HULjLzEPsPbzD7VIwMfMfJyMTsAnnu+WwpDJu0NaX/bpF0p3Rg3+bAKA4Cf64+b9CQZV5oz+Ov3/aAArKmNEgfzz5v3gqRPKNSYuyuviBp0Cf7GhnF97aNy+c6mxO9Fy7i/bOsLDqPF9hsnlvB3ZwHUCnyapuKUrF5vuFB38bvwfuT+CH+kPpT5Q8MvJ/9H/7yYAGAFWFYVNqwuV1iye98gTJ3ybYwYomaaqTNGd/EBbjKuDPjDuXaq/PNjU+sXO7siIDHw0bDFvJ5OB6+T5XunOaLV+wZw/TctmMH6d9MeN/2cSAJz6SB4geQTw+/3+5dc1/M1jTf73D8XA7/bipsBfDDh7b6MyCImtSxKPnt7T/A9hLaFlA75s4I6G5xcD+EUHv1P7e+X/gYDK3DxAbgJgmopy3c0NH36hOfhwywC7oLbkFPi9A3e0V86u4CN3Log/cvCVsz/XFMPMF/iUtJaL5+NzFpq/k+03FuTqdDYoT3hxoz+Z+L/s/6cAmN+nMKr8gC7QTAJww4ZZ6w4Ml//j7vPKTJfEt9G+16n7c/TA2pm8/frq4S/s3Xv+TayuQ14dNG6dVMdN48vAd/J8/OqV3Vs4zs5yAr9YWr8omp/6KJcAOPk/zft1EwDZBSoLgKoxhmnQaAQrOmNLVlTPSlRP/5enmpSrE/YKMFOaf2zl1qeAedsCc39tpOPhYycGuzBlwTQ4x7x8L8AvS5icJqKjxp8o4I8b+N34v2wA5xIAHxYQYbqaUBkjN6iuxRnmAlVVBYLzr57z8NOngw92hyE0Bf6xA/+Mch6544rEL5qPtH5rYDAe9/mDnICPiWoYwAoYOApYyWqyxk8kMIBlVWCQPTvZDNyxojvUQ0WhPV60fy4BwIWtZRcoxgCIAvl8jKH/X44DYFkg9AShAOgKY+s2zFp3eKj8/+5uU2ZTWfOxg8Hl1TIGrq6bbbasrQj/9b432vYhv0fgY66O4eecgI9+/IBu8tEAX+BkjOnOmIAfG81GfwoVANkNSgKA88FkTxDSoDkNZVWlc2f99fNn1Xf2Rljw8oLo2PzamhIeu63BeCJ2vv0fz7eFB4nfkw8/YHCOGp8CWHrQ5IVq/PEEflFpj9z1xRQAf1RhOAtMFoCEqjAyhMkOIBqEo8D6dbPWH4mW/92ec8oVupQZOjbwuDRbRW5/3Wzz1OrS8Ff2727bS9rejd/rAc7lAFZIMznNwSWOn4vqjDfwxw38+EWY+y+s+JXAnBFgOQZAQTCiQLIblAQg4VMYGcJkBxANwoAYjgK1M4IlM5fO/rNd7YE/PtnLqi9NiI7Nr1pcy/tunJX4yfkTbT/t64tFZW0v0xzi97rOuZyyUKIZHP34kxn4Ywb+bPQnmwBkMoJJAGJ+hcmeIDSE3WiQPAqsWFY1M1Fd+8XXWv23do1cGBcYG/hcnK1OL+MjG+ca20rD3Y80HhvoQNDLRi1VzySaI/N7TUe6o5sEfJyGiO7M0Wh8AdA8piXm2+tFNXidX56J/uQrACMBhZVrI0rUrzI0hEkAyBB20iAcBTAvSBaCDdfXLWsxa764+7xyfV+UhfLtqEv5+roSHlk329w1Vxl65M23Oo7LoEduT9pe95scUxWI5sj8XisxOdbZSZRVcgI+Vl0gd6abV0emOrhfzKQ1L+9rTME/mhFAjgNgKjSlQpAAoCfISYNUNSa8QmgMy7YAUSHhFbpuxqpzvOJzb7Yr13aPWKXSL9etvpSPXD3T3LvQN/yNPfvaG91AL2t7N5pD/J5cmZir4wxgTUbgjynt8WIA5xoBUAAoFRoFQHaFkiHsizGGcwKcowDaArJHCEcBWQiuubpucY+v+mNHetSbzw6wai81zS8FIUFtN6+a966pN7bXG0M/3H+w83Q20GfS9jLNkYGPk1EwJ38ya3x6j2Ou+emLCqVAJACZDGG0A5yjANkCPk1hskFMVIiEAJ9t0bKq6YFpte9r7Pffd7RHmR3VJmcd+9EKXokfEiunmS3LarTHzK7B3x0/09stgx7bl92XCHrDCHHy3ZNRq4c4x9lXMs2RDVsZ+GVlEbGAplx1YaKpjtyP4wZ+LxRIdIw0EYZSoWUBoGgw2gGBkSGGOUFuowB5hJAKoRDgHGGkQmQP4HfJo0GwQlXXXDVz3dlEyQcPdqrr24dZ1cUeLfYpYFxRzXsW1Rp7rwhGfnlwf8e+SFTDdYuBDFkEPH6WeT2CXqY4xO1R22eiOWjYus3CwrbdpiFOBMd3KpBxBX++AoDXO2eD4QiAAiDTINkbRKOAL64wmQo5hUCAXzKMcTTAY+gmnTG3pHxOQ93GXT2lHz07qCwPJyB4sUSNgypo86t4b0OlcWBRReKx8ye7drV0johq0gh4/Iu+ejJkidOTpkfQo0FLnpygXsKxmBSCHrW97MakXPxMNGcyA3/cOL9T4nJRIHkEIAFwGwUoJcI5CpBLNO6LMhQCnCSPVAi9QiQEZBNkGg1IEDBesGxJ3crzeskdLcPqjR3DbEbnCCvXTauw1kRvqN2nl/Gh2RW8fW659sZMX+z5kyf7jmLFBCfgidqglvcCeqQ4mbg9uTGz8Xv8PszOlKmO06MjQDiG7sxs72fcNT89jFMA8LgcCCMBwL9uGaFoCGcaBdAjJFMhr0IgjwZiX4uL/pFHhSVLaqfVzihZ1qYFNvZEfVf2RJV5PREo74+xMsO8YAX4osqGqoBRW8JH6kv5UHWIn68P6vvnhuKv97QPNx09PdBNXyZreBnwuJ8N9DKvd1IcdGEit3fT9tiuk99PduBPmOYfjQDgvV5sAaRCslsUvUIkBEiH0Cag0QBTJbBdeTQgQRAjgIqpXSlhkAUiGFTVpYtrZ1VVBuZ2a4GVQ5q6YEhT5oxorD6qs4qIBgH8nzDAlzCYD+cgawb4aDI+am6FgTAMQz6ulfggEfID/tVCfj5YFeBdZT7zfIVqnK73J44ODkXajp0cbI/H9WQJdDewC6D7LR9WJsDjOZnekKbPRHFyaXtszyu/n0iNT/ibMM3vRQCE9lhppUWQIYz78rxgpzFMtgBRIac9kE0IZEpkAdwCPVEjemYSBqdAyGoeRwufX1Hq60Jl1VWlpaGQWhYMML/Pp4jZZxiKwL8m58IA1XUzGk9wTYvq4f6wNtLeORzRNFydMLURyOkIcnfalw1XBLsAtn3ejc+jlkdDFjm9G+idfnsyaMmFie07/ffifTloDh6bTFRH7s8JBz8+TDYK5BQA/Ew0CPczjQJuQkB0SHaPkmGMbcmjAQmCOG5rflkYBGBxmWFpk4VCPu7cRzrlBHKm62WAuwGdjsmAJ7BbApAyYC0hyw/0eI8T+DLo8Txq+4sN+Pi8kwL8mQQAjzvtADyWbRTA8272gDwSOIXAbTTAdogW4X42YRDnNWuUkDencGQTCCewndcShcHjbppdHLddlALktscmF+DxPHpwSNPjZ4rSZgK91cdRfrGCnvp20oCfHqiYo4B4sQ7XaCYhwGvRTeoUBDxOI4LYt7U9CoMQEC2l/WmEoN9CI4UX0MvXELjpGNEXAWS/7Zc3rElrqNkJ7OKzbnLU7uKYRGvwMxmx5LJEwMuGLF7jxuvx+KWi7eV+nnTgx4fzKgB4rXMUwGNOKpRLCPC8PBo4BUGA3I4bkDCIY4mUtiehwOMkGE7h8CIEBG66FrV5SgjSgU5gF0C36QzuyzweP8sGrHh+B+gR8Hg8F6/Ha/LV9njPRLkyc/X3pAR/JgHA40SDcF82hvEz2gL4lwxi8UI7fYyMYqcQCKAEVSaPBrIg4D7ZBwLItseIhEH89UkC4EuNArJg5HoJbueRtsjHUaMTyOk4anYZ7LhP2l3sl1jnidaQlsdjMrURv1nKx8HPpOlx343i4HGaboj7bkbtZAa+eLZCXsx43pNrFMhXCKyXmYoR4GcyjnHfTRDwOMYNUBDEvgzymAV+dKNSv+AoIfeRLCC5+o4oixPgBHIhACFLMBDo4vk1668Mdvwsa3gZ8LhPWh733Tw4BHr8eylpe7n/Jz348WHdBACPu40CQiNJtUKtl1sifqdMh0gI8C/ZBZkEAY+jyxT/Ij0iYaCOJKFwCgZ+xtEiF+Cd5wncdJxALgM9E9gF6O2AFAEe/xKXF7/B1vK4j6VD8K9T0+cCPZ6/GLX9RQd+euBiCwG2i5SIBAGFQAAhZB3DEQH/IjUSQLGT6eh5SCBkoXAKhtzZspA4AU/a23mctDkeR/pC51GrJ/czgB3Po4YnwONfp5bHY7jyidUHUfHXqenx2MVOcZz9ip/z1kpujYznsUwCgM9QyEggANJpgV0WBAG2HMJAAoF/McNU7gdZMArtHxng2AZqdPGcidRyHcjd8RgZrZkAL57Vg5bH6yhQdamCnt7HRQd+evBiCIGlCVOUyIsg4DU0MuA+jQ64TyOEDHYcLfIFvwxuupdAngnoeBzpjPWbZiRHBTfA4zVOLV8I6PGeyerJ8dLneb8YL42O5zWjEQJ8TvIQeREECzSdyT6jkYF+rywUdEwWDq/9Imtx+R6iMDLQM4FdCKm9mBu1kQ/g8Z5MnP5iB/1Fr/mdQPIqBELD2flCuE/GcS5BwPNEj3CfKJJTIOi5nILhFfjydaTJ5WNuWj0pfDMsfk8c3no2i8fjRlxe9IGdgyP6wK6QRtdd6qC/5MBPP6hQIfAiCPQd6DWifVkgnEJRCODd7iHqIp9DrU6fvYA9F+Dx/OUC+ksW/F6EAK+RjWMBDGk0cAoCfpbpEX4mW8FNIDIJRj7CIIPbDeR0TNbs2bS7m4bPBXg8fzFz+lz9fdFz/lw/EM/nMxq4CYKbMLgJhJtQeHm+XNc4AU7XyzSGjsl0Zgrw2Xv2sgC/19HAbUTIJAyZBCKTUOQCeKbzbgDPBPRMYPei4S91Le/Wv5cV+OUOyDYa0HVOaiTf76RJzs6VDelCgS8ETzJM3dpxGqt0TTb+LrdzKdOaXP1+2YI/X0HwIhBJjeywH3K9hFznMwFcvs8r2C9HDZ+pf6fA79IzXkYF+bZsI0QuYOd7Ph+QU9uXs3bP1r9T4PeAvnyFwUOTY3rJFNi9de8U+L31k+tVEy0UUyAfxcu7GBPbRvdzJ+bufIVkCtTj856mNP/49PPUt0zCHpgC/yR8KVOPND49MAX+8ennqW+ZhD3w/wMXZZn4p7wwygAAAABJRU5ErkJggg==',
      bottomSign: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMcAAAAeCAYAAACYNAohAAAOXUlEQVR4Xu1cbYxcVRl+njMzuwU0bYGIfPnR8rHTlpbtzLCCyFda/KER/UETf/gLQn+QGP6VnyaGSEHjB9VgCRqEVOkGNSJE44YYiFA7O0ttZadFWQh+IQgFpe3uzsx5zXvn3tkzd+6de+92u9bs3qRNu/fec8/H+5z3eZ/3Pctqrf4DCg/Mzp710DXXXHwCy9fyDCzPgDcDrNbqzwC4HOB+AzxQKg3p/5ev5RlY8jPAam3yaYIbRSAAGgB+BuHuSmXoyJKfneUJWNIzoJ7jlwA3AmIJDyD611skHm01sGdkpPj2kp6h5cEv2Rng/lr9SQNsEMC2Z0EEoChQBHgZxA8HcrO/2rRp07ElO0vLA1+SM8DxWv2nIljvUyqAaGksAqDg4UNoxeDOyvDQcyR9AC3JuVoe9BKbAaVVT0CwAYR6i2kBmgAGCOREMCnGPnjWIJ47fpzXw+B6mzcPj2waenWJzdPycJfgDLA6Xh8FUCTRVGpFYFAo7xN8DHbgR+Xy2vd0Xmq1+mct8BUQ74hgz+wJPnHttUP/Weg5q44fvheUEoDRSqm4O6r9aq1+B4gtEBytlIrb59MHbYOU1QLUKpvXjaVpozoxuYVASYT63Z6+1Wr1klC2wJqpgYHZsY0bNx5N027WZw4ePLh6dnZgS7k8pGu3KNf4+OFbYewa/Vh587qdi/LRBf5ItfryGuaat3rBQys/WqlcNtXvE21wUKVczKrHAKQKK7sqlfUvui9Wq/UbYHAvOrEJDhvwsYWWfqu1+m8AbCHl7rhFUCOFUJ+DAcqlUrGWdR77fce/p23f7bY9PjG5Q4Q6B2OVUnFr+JseaIHvAx5oz4647wkeSVe/seu71Vpdv6HfqlVKxbL3M52TlJcRHs06Z/6mtSNubEmfDjaWpOey3I/bpOLacO0GlK1JmyLHa5OPC1gE8I6Ajxz79+CPb7zx49M9C1s9/Ekx+BogxzUeIZAH0CL5TIPy2NXDxdeyDCx2ACnA4RvIOAD1MLvn4z0SwNE24tAEpgBHx2hBudsdoy5EtVY/aXC4C+yCKG3bfp96wK1j67d+IlSvoYDUazcpfXfd8MbmzN1CmEnQRuQmtWDgqE7U90LwJiwf6Jfb2P/ikQrF3ucF7kID2FkBjcYmII6K8BcrCrNPnSyViDJaf0f23KFz6WLpH6UuPZ4jamd3Xz5F4HgHwOqoxamUihrfeeBQo1Y61/OcUMeodC/Saypts4B6TP1Gx2v4m0Uq4MWBIyO4Eg1cxxuabwVWeA3dRwLPp6DrC7zOS8JapTzUtQn161hmz7Fv4qV1I8Pr6iT7Tu7vJ45sMrD3o50snFFGI0CeEF/BohXK66Dsvmp4/YHE2XMeUC6IXNPjs2jTFs8jgDKqFEB5vE9nUjcbXpzwiwsNDodSRfbRBUecSw921yhwhIBx1ABbs1KjvoYz59UUtPONlRS0unZImn+3LxrPCGWv/my+NNnbIBLi1TTgcONKVicmb0NLJsIxRo8xVQ9fDiP3axKEbXCsEDVlzRmKAoTvicG7FPlApbTuttRWDCDB5Y6BslMD4SxtJgWNpwAcXqzUj+Y5u3MkLRGhvt8Tb/mLqsajxrfgwOjyPCm4eNw6uMaXBRzBWoS9YZb19sfQN15NAw6XOlNplQjOIWXPmYPy+Pr169+P6tTExORHW5Yac5wJsgmRvOY9LHFCCxdJmfbzJZ+rlIoXZBlYEjiSKFLab1Vr9XFSRhU4LjjCyoVjxNspPBqoQnExR9ek29zaOBUkLXVxPYcqUzONgtI1vTrA8D2JAmbnYKExugB0NjLOSju3nnE6QklacHTNHbA9TqFM048kMSczOPZPHH4IgstJaQmknhM+HOWu9/3hTxflm82vAlipHsNLFgoOUGQS4FrQy7J/AsCqSql4fprBhJ9xDSGWWlAiOb22NZhv1uKMxKNupvVKoLY4u5VSCG2zszA9RuwbfCw4fBFB2yalR+YM5OJ5e452+6thc9sC4Dn9n4LNbe3Q0pQTH1asAoUOwhqNnRet0qA9CNSTPHfQTd2wfCo2VSkV16bsfuRjCw4OXxa8FKAFxYpghsCTeTO9d3h4+N2gFwqOXKN5DykrBKwb8Nkm7EoDcx2Ac4SSo+ALAN6bLzhc7gnhznCw5RhE9Bz2oQRO257C0WmLGIV4gWJncRwjVv7dUcSiwOEBenZgByg7MNdWV/8CoHfajelnXMyhXqJQaEwFwO/ytJStfu5FY7UsV5wcHdDDLG11nk3rMfQFdxwUbjvZvM2Cg2P/xOT3KLxUVJZtFx5q+UhLiNfY4t5KZeh3OpB9+w6dlysUbqdI1ebsm7S8QcgrKbAQmQF5M4CPAXijUipeOJ+ZdfT74PUuN9u1W3YrGmrAq/tp1x2d3gfdHDhkK4Qqwaog4H3PpVV+3gKwubWaQIrLcwSigmuoCgodSNhzzFet8nl1kEvRiM/bQOYpk8bmajQ5Gl6/tFJuWo/h00L1GnpNgZIpmRuVo1hwcFTH69+B4VpY2wCpwXZLNOAW5EmxVljNm/wjw8OXvH3o0KGVM838pynmU5YYoLVN0gvOzxfw81BZF/x7pTT0kazgCHHrudedXTZu8K6hxyV2Avcd7FDuLt4xaGK0srm4zb0H8A71LB1D75MEdKiCx98j5MxUcmuclBtSxFLRkPnEAVFrl4avp11zf62V4sZS5KS2ojzUgoNjvHb4uwK5COJlyBtiMEuRHIQ5EtYKDthm89GRkQ1vTU5OnnlsOncraCvaeYqZEYqhYBuAc72fAf8sl4oXJQ0ufN/Z/YIYQEs6VL3pBKHzBYcTb2Cw0Dhb6YkLAI1VOkGvza31YxMvCUhrVvsy4xQpu/tlyDOAI5BLg1xNWNvvKZ2JkIpTJcBON3D4wFDqlkl9DNvLooCjWqvv0p2/HWBzmhDjdYR4HdY8bXPN92nNVTaH3+aauBqwh20OM2jlrieVRsk6AEqpgktpVaaA3NlJFBhqKJ6cKdYoVdLMrZcFny84HOB5ibNIsLSTobeqZwk094CmVWt1VYtGITzq96fLMCOSlEFCq1OzpaUoFmjTCN8bOv3qyvIr5XBFkQi6qa3834EjAhhBTNdTjRC1ubr0cVHAMV6b/JaA50F4TINtAMct+IKR1iRghoSoEGSL3E1rbyGxSoCDRuRgG1TcJe1YI7j+kVXKDRbfA4Sj9Q/km7tnGoVXgoTXfMERFJwFtTjObtqpgfI4sM0dVTUoHDjroqq3iVOrUnF+erGNVw8WgMPh3a4YEMQUc+qZD1wvMRoD0DhPndVz+H2KCu47CT6/IqFH0QrXorl98tvV2C7wGF6SNzwn/RjHooOjOl7/hhh82AhmreBVioyLMaspspnkCtEMuPCY5Fo/oeXNEHM2jadqzYrAGmBWDG6C4KZ2zIFM4HAl1sFCY+1Mo6DafScRpvcj5MvoQD1lAitFjVSm2qquDL+urgOCYLG7qFt3HKXce41H4doVv4FhdrxJUPekwW5S38PGlRUcobxDNnYcM/8RMUabCTh5kTSFgIsOjv0T9a9rYo/CSQGPGcqlFrKKQlWvpq26DZFpS/PzHOQ6C66iaBBuVwK8S7TUgBhjO0uudKJQKRW9+CPtFZecCysfHU08tAhpAnK3L53gPKaGKU5yTWuYwfuxAbnT/4hYq6NCJVCLU0KrPENu5nvigbAK11UbpkWJlKl+eaYoCnnag6Nam7xLaPzyantuGxSaEPRO/Z0QEUuyZYlfG5ERCj4IsCGUzwDUwFyfWwHwr4D8C5RGZfO6kbTA0OeUswdZ3lRlHaEsdBgcrrfp2UnnkoGxdTyLCQ7fc2os4pWGULi9n96fFqDBuLN6jjT0LEQLPRlcvX6/LL13BqWZv8Pd8E57cNRqf7xEmPsShOdaSpPi5TtaIBpKndqTxYZB61mhuVK9jFheCOL2Niig5e1agan/fpuCL5fLxT1ZwBHa1SPrY5wkXs9ZiR5wtIvouqpWg284xhUrhS4mOLRfrvdIMrLTCRz+xhZkuCPnu58dnPbg8BZn/JWVwMy11hj1DIOANL1MOT2jb4PE2BeNzV0GI2eI4E4AQaJvsH1QSvZSeF+5XDw0X2D4kx0JDr+0XjPZPec3YsDRk2dwuW/fw1RBhWrG8xyd3dp/36/E9U4c+vVcnVjGj0G8EnVPQZsro6gNFhpb43bh0w4cbU/c9nwRVQ1pwRGbGHUb8Ev69Uf91Cpf8Ywqe08UFfxksErsY101988fOHJhvtW6luCQ96sVKOoVGiLGgjgCyMUUjoDiVt2+QJF7yuV1T50MKBzD6gFH0g7jgsMNfHs4f7uk2TvN1m+HPhnPEcr8dnI2fslKkASckzA98YrbtCbJPathgO1RNW6nChxJJwkTYg4FuSZKdwaxSNIpO28jdAPyjMaTAI6MrUU+3g2O4JH9B15ab5q82gu+KU2ATViZInGBUCtzvYTfn0Xkm4OFxiML+Wt7wjFH1zkGP4PdE0c4hX/+PY+/u0dV3SRaimOomdSqEC1sq09zV6fkvudMSrsWayyIt0JnNqYGC41y2IMkgcM3OPdUX5Bo7HvGIm3FcFqrS1NjdQrBMUZKqt8LEB5PkEro8Rzug88//5czCoXjVyAnG6hny619A8QXBeZmoTzYNGbXNVde/re0k5X2uTA4nMTRGthcOaocPGRU7SgppET54FCZVLlxz/nviLgn8xlynxZ6J950cdxS+E4fiTEXEOF5CUrRNRsfVaeUCA5HcAi13fc48f8cHClk+JOVctPYYNd5jqQX1Dinm/krCPMhiL3F0nx7ZPNQUDCW9Hrm+1G/FUT70GgU1iSdfFPlx5jW6rjnwtWtWTvnnhI7mXMHSd8Nko5Rz6XpQ5AX0fc18akyaxqak9Svhb7vysb9ZOAO5XZOjC7GeP4LvYbpY52kl9EAAAAASUVORK5CYII='
    };
  },

  methods: {
    returnPage: function returnPage() {
      navigator.pop({ animated: 'true' });
    }
  }
};

/***/ }),

/***/ 48:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    style: {
      marginTop: _vm.marginTop
    }
  }, [_c('div', {
    staticClass: ["header"]
  }, [_c('image', {
    staticClass: ["arrow"],
    attrs: {
      "src": _vm.arrow
    },
    on: {
      "click": _vm.returnPage
    }
  }), _c('text', {
    staticClass: ["titleText"]
  }, [_vm._v("投资组合")])])]), _c('list', [_c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["title"]
  }, [_c('image', {
    staticClass: ["circlePic"],
    attrs: {
      "src": _vm.circlePic
    }
  }), _vm._m(0)])]), _vm._m(1), _c('cell', {
    staticStyle: {
      alignItems: "center"
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('image', {
    staticClass: ["bottomSign"],
    attrs: {
      "src": _vm.bottomSign
    }
  })])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["right"]
  }, [_c('div', {
    staticClass: ["main"]
  }, [_c('div', {
    staticClass: ["item"]
  }, [_c('text', {
    staticClass: ["itemLabel"]
  }, [_vm._v("基金投资")]), _c('text', {
    staticClass: ["percent"]
  }, [_vm._v("87%")])]), _c('div', {
    staticClass: ["item"]
  }, [_c('text', {
    staticClass: ["itemLabel"],
    staticStyle: {
      color: "#58CEFF"
    }
  }, [_vm._v("银行存款")]), _c('text', {
    staticClass: ["percent"]
  }, [_vm._v("7%")])]), _c('div', {
    staticClass: ["item"]
  }, [_c('text', {
    staticClass: ["itemLabel"],
    staticStyle: {
      color: "#32DFD5"
    }
  }, [_vm._v("其他资产")]), _c('text', {
    staticClass: ["percent"]
  }, [_vm._v("6%")])])]), _c('text', {
    staticStyle: {
      fontSize: "24px",
      fontFamily: "PingFangSC-Regular",
      color: "rgba(143,154,174,1)"
    }
  }, [_vm._v("数据更新至2018年第一季度")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('cell', {
    staticClass: ["floor"],
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('text', {
    staticClass: ["floor1"]
  }, [_vm._v("本基金主要投资于博时黄金交易型开放式证券投资基金、上海黄金交易所上市的黄金合约、上海期货交易所上市的黄金期货、债券以及中国证监会允许基金投资的其他金融工具，如融资融券、黄金租赁等。")]), _c('text', {
    staticClass: ["floor2"]
  }, [_vm._v("本基金投资于博时黄金交易型开放式证券投资基金的资产比例不低于基金资产净值的90%。")]), _c('text', {
    staticClass: ["floor3"]
  }, [_vm._v("黄金合约、黄金期货、债券及其他金融工具的投资比例依照法律法规或监管机构的规定执行。对现金或者到期日在一年以内的政府债券投资比例不低于基金资产净值的5%。")])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });