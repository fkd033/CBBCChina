// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 35);
/******/ })
/************************************************************************/
/******/ ({

<<<<<<< HEAD
/***/ 51:
=======
/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(36)
)

/* script */
__vue_exports__ = __webpack_require__(37)

/* template */
var __vue_template__ = __webpack_require__(38)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "C:\\Users\\zjc\\Desktop\\new1\\hzBankDiamond\\src\\view\\gold.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-2bd10d02"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 36:
/***/ (function(module, exports) {

module.exports = {
  "backgroundImg": {
    "width": "750",
    "height": "455"
  },
  "header": {
    "width": "680",
    "position": "absolute",
    "top": "67",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center",
    "marginLeft": "36"
  },
  "arrow": {
    "width": "18",
    "height": "29"
  },
  "share": {
    "width": "38",
    "height": "38"
  },
  "headerContent": {
    "position": "absolute",
    "top": "141",
    "marginLeft": "36"
  },
  "title": {
    "fontSize": "48",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(255,255,255,1)"
  },
  "headerContentLeft": {
    "marginTop": "55"
  },
  "headerContentRight": {
    "marginTop": "55",
    "marginLeft": "91"
  },
  "unit_price": {
    "fontSize": "62",
    "fontFamily": "DINAlternate-Bold",
    "color": "#ffffff"
  },
  "unit": {
    "color": "#ffffff",
    "fontSize": "30",
    "fontFamily": "DINAlternate-Bold",
    "marginTop": "28"
  },
  "remarks": {
    "fontSize": "25",
    "fontFamily": "PingFangSC-Light",
    "color": "#f7f7f7",
    "marginTop": "5",
    "marginBottom": "25"
  },
  "contentLabel": {
    "height": "38",
    "backgroundColor": "rgba(248,148,7,0.7)",
    "borderRadius": "2",
    "paddingTop": 0,
    "paddingRight": "12",
    "paddingBottom": 0,
    "paddingLeft": "12"
  },
  "contentLabel2": {
    "height": "38",
    "backgroundColor": "rgba(248,148,7,0.7)",
    "borderRadius": "2",
    "paddingTop": 0,
    "paddingRight": "12",
    "paddingBottom": 0,
    "paddingLeft": "12",
    "marginLeft": "16"
  },
  "labelText": {
    "lineHeight": "38",
    "fontSize": "22",
    "fontFamily": "PingFangSC-Regular",
    "color": "#ffffff",
    "textAlign": "center"
  },
  "whatsGold": {
    "marginTop": "120",
    "height": "120",
    "justifyContent": "space-between"
  },
  "goldTrend": {
    "marginTop": "120",
    "height": "83",
    "justifyContent": "space-between"
  },
  "history": {
    "marginTop": "120",
    "justifyContent": "space-between"
  },
  "detail": {
    "marginTop": "120",
    "justifyContent": "space-between"
  },
  "orange": {
    "width": "8",
    "height": "33",
    "backgroundColor": "#faeab5",
    "marginLeft": "28",
    "marginRight": "16"
  },
  "whatsGoldTitle": {
    "fontSize": "36",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "whatsGoldDetail": {
    "marginLeft": "53",
    "width": "648",
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)",
    "lineHeight": "36"
  },
  "square": {
    "width": "678",
    "height": "333",
    "backgroundColor": "rgba(255,227,95,0.1)",
    "borderRadius": "6",
    "marginLeft": "36",
    "marginTop": "41"
  },
  "squareContent": {
    "height": "244",
    "justifyContent": "space-between",
    "marginTop": "46"
  },
  "squareContentTop": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "marginLeft": "31",
    "marginRight": "42"
  },
  "squareItem": {
    "justifyContent": "space-between",
    "alignItems": "center",
    "height": "147"
  },
  "squarePicture": {
    "width": "74",
    "height": "74"
  },
  "squareContentBottom": {
    "flexDirection": "row",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "minute": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(254,191,49,1)"
  },
  "squareArrow": {
    "width": "16",
    "height": "20",
    "marginLeft": "12"
  },
  "period": {
    "marginLeft": "52",
    "marginRight": "52",
    "marginTop": "42",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "alignItems": "center"
  },
  "timeItems": {
    "width": "140",
    "height": "62",
    "borderStyle": "solid",
    "borderColor": "#E7EAEE",
    "borderWidth": "2",
    "justifyContent": "center",
    "alignItems": "center",
    "borderRadius": "31"
  },
  "timeItemsSelected": {
    "borderColor": "#3699FF",
    "backgroundColor": "rgba(235,245,255,1)"
  },
  "chart": {
    "width": "750",
    "height": "381",
    "marginTop": "30"
  },
  "table": {
    "marginLeft": "36",
    "marginTop": "40"
  },
  "tableName": {
    "width": "678",
    "height": "80",
    "backgroundColor": "rgba(143,154,174,0.15)"
  },
  "tableBody": {
    "width": "678",
    "height": "80",
    "backgroundColor": "rgba(143,154,174,0.15)"
  },
  "tableLabel": {
    "marginLeft": "117",
    "marginRight": "129",
    "height": "80",
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "space-between"
  },
  "tableBodyLabel": {
    "marginLeft": "133",
    "marginRight": "126",
    "height": "80",
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "space-between"
  },
  "detailArrow": {
    "width": "16",
    "height": "20",
    "marginLeft": "14"
  },
  "detailBody": {
    "width": "678",
    "height": "493",
    "marginLeft": "36",
    "marginTop": "43",
    "backgroundColor": "rgba(250,250,251,1)",
    "borderRadius": "6",
    "justifyContent": "space-between"
  },
  "detailTop": {
    "marginTop": "47",
    "height": "100",
    "justifyContent": "space-between"
  },
  "detailTopTitle": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "marginRight": "60",
    "marginLeft": "60"
  },
  "titleTxt": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "detailTopFlow": {
    "flexDirection": "row",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "circle": {
    "width": "10",
    "height": "10",
    "backgroundColor": "rgba(254,213,33,1)",
    "borderRadius": "10"
  },
  "line": {
    "width": "175",
    "height": "2",
    "backgroundColor": "rgba(208,213,221,1)",
    "marginLeft": "20",
    "marginRight": "20"
  },
  "detailDate": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "marginLeft": "60",
    "marginRight": "52"
  },
  "detailtime": {
    "fontSize": "22",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  },
  "detailBottom": {
    "marginBottom": "36",
    "justifyContent": "space-between",
    "height": "200"
  },
  "detailBottomItem": {
    "flexDirection": "row",
    "marginLeft": "60"
  },
  "buyMoney": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(143,154,174,1)"
  },
  "detailText": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "marginLeft": "23"
  },
  "question": {
    "flexDirection": "row",
    "justifyContent": "center",
    "alignItems": "center",
    "marginTop": "63",
    "marginBottom": "133"
  },
  "sign": {
    "width": "33",
    "height": "33",
    "borderWidth": "3",
    "borderStyle": "solid",
    "borderColor": "#8F9AAE",
    "borderRadius": "33",
    "alignItems": "center",
    "justifyContent": "center",
    "marginRight": "26"
  }
}

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var animation = weex.requireModule('animation');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      backgroundImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAu4AAAHHCAYAAADgVt2pAAAgAElEQVR4Xuy9287kOpK2l1+tbfdM2/BN2EAb8JH9334D9tFcjj0902tTVWlIKSopKoLxvgwGU6lPBXSvqlQwdgxSD5mU8uP+j7/fb9efKwOjM/Djj7fbz7+Otvp+9n7/7Xb79rWT3+8y1I/mp88fX+up6/0a2gpopN03svXDT7fbr//WltIPrZl6oWKHzJkk/qHZJXW3ZSOgVT+/77/983b787fFx356t0EH6/35r7ePX/92u930+uI84KSxDo7QmVuO1o9FqUvx/n1c4O5N+tWezsAF7VjKukE7PzFgDvaWOpqffn98Gnyt23tntN1R9jrYCYH21FMTXE0+WhBPxqGJX9CuDpHPAO1cFXHS2NwToTNZjtSNRYdJtfl5gTuW3UuqVwYuaMcyeUE7lqcwqbYJtd9tw2e/PS2j7Y6y18HOBO2//JvN1VLyLRbftKkJk3HUxEVwJ/W3F1rHln19fl9oz/Kw7rTni8JnyrmMcdJYx0bonCxH6cWiwqV8fl7gjmf6kvRm4IJ2LIOfCtp9ExiWUEbK749Pg681E+lWdrTdkfY62PJAe55oCOAlITIGVHyFd7RBe4XFtOzr9xPa++ptRWY8ZzVon7Q8aoqLipNu8hVvBEpG+Qyah8T8Pl7gDiX6EnJn4IJ2LIVdoN0/MWDOeqWO5qfPH19r/rbqzX48TGge+jOFxd7JThXaSxsGmTeBOxkHKv7W0I4GiVXKPPrWM+39dT+8iNBb6NzstG+LjbPOSWNZjtBZWh5hA4t2L9XPtwvcW/vgaodn4IJ2LFcXtGN56i7ln1CPoIFPi99rzuZIe51szdD+19sNPlaikDl0fN25086EPMfDNOB6Ola6v98PaP9XkNv9/RUXApXjMbIH06fOmoMzFpWD5EC0fjhQRbCvfxe4e/vjal/PwAXtWIW4ob3vxIA53SJ1ND/9/vg0+Fq39EDc7l/Nm1FxdrRDQ/sUf2VL3dxtzwXIOEhx8/nX9sIKbskGartzLmhndtkTuOf/tfPFSfTvr8idbC42RDom/gvckdxfMm0ZuKAdy9sF7Vieukv5JlVf6ykYv4a2lIy0+6a2VGi34vGAe1tvNpeRuZBo9CekmZX3NqP33/4ze+Vjmw65VYy/4pyh7LTrHpQ77QN97ZbiPIYo/73ORvj10HmBu7dvrvZyBi5oxyrDBe0REwPmNid1ND/9/vg0+Fpzuc+lR9sdZa+znR9+XN4eU5KtZQcgYUAE7l/LnZqinn7ADrcKegLVbZ4T2ku0Tx1d5lD7vLWPRs8zMTXRI/qHjgj/njovcO/XU5emlIEL2rFauKAdy1NXKd+E6msdNaEjCfJ7jlh5yoyy19lOgvb8LMkHY8Nzxp3IMOOSpPYtwN0b5FmgXcnDbqddwsUauEfkN0Ln6EUBMQ53ohHx73Ve4O7po6vtPgMXtGNVcXpoj5jAsNTKUn5/fBp8rdsjH213pL3OtlZon7K9AA8F7Vm7ssPyZ0E90NwrZI8P7cVItOwVaGnynr09hnDHFI3zVzQNQbtUhJGbB1E5SHFE6zc72RCI8E/WeYG7t6+u9s8MXNCOVUMztEdMDJjLnNTR/PT542sdeaO0esXvuWVhe32UvQA7G2gvoqYht8crIYXM9wybjomrBJ90z0BzT94J2is5KKAdz1bUm4RwD9rrYoSNVu+ifNP1XuDe2ldXu20GLmjHKuKCdixPXaT8E+oRNPCp8HvN2RxpL8BWDdpTImDQBd77COtajAeEfMy3ykQE+lw4b38RlatwWTrWX9FmM7T3iFfSEZWD1G+RZ/F75CQiflvnBe49+u6z67igHauAJmi3BzFmPFrqaH76/fFp8LVu763RdkfZC7LTFdrzXtPofIljvgwQfFDYiOn2GmxpGRXoQ+/ZoD0qW3jPjfBghA084q1klG+Y3gvcW/vtavfIwAXtWCVc0I7lqYsUNvlppnytnzt8XUKhlPg9x82dwNYM7X9dAHqKp+erHI0ftkHAPTrFwLoBr4dWyaggn3r7Qnu8v2Ims532KA/wHhzlwSg7eOQPyQi/OJ0XuLN9dsk/M3BBO1YNNLRzgxhzIkLqaH76/fFp8LVu76HRdkfZC7Szg/Yp+73AHXiNZA3c6bCNRUetsF4K73Sg4BA5J7RHZQtM6iI2wosRNrion9IRvvE6L3Bv7b/P3u6CdqwCLmjH8uSW4ie/3KSvddQuDJIUv+eIldgbl+RBYFwrtGuRFzRLw23eQIljFSmU02HnDQhHgaP4XF0w0nSQhPJ3gnYgD8tOOyBJ5KhFdIQHI2y0xB41v7fHe4F7az9+5nYXtGO9f0pob59ssKSxUn5/fBp8rdloxwN0sjgyzkBbJrRP8QoATDAx1KcluNMhSw0IJwlRKB5YiA4U1LzV2+94zBh/xSA/DbRPOY564w1YPlWx3jXg13eBe49+/Uw6LmjHepuCdv9AxpzySh3NT58/vtZRuzBIH/k9R6yMXyQExzVD+1+Mh0IVou0Juhtov99ud1a5lidSDynO1YwkHdW/7wTtYA5+/uvt9uvf/Cl3aQB9bbKRYP2V86jleFT8fr0XuFt9d11/ZuCCdqwaLmjH8tQs5Z/4jqCBD9/vNWdzpL1gWyu0Kzvqa2J6nXOvZHrzhjuGnq0ckedfNqaTbsYftJosv1E9pdxeb5+d9nH+ipGfFtqj8tpaP7V2vX316tsudC5wj+jzM+q8oB3rVRjavQMZc8cvdTQ//f74NPhat/fHaLuj7A2ws4H21AMaoBrgql72gC8Cy0yeEH35Fw+5brAtXMiM37BS8c0eZ4D2u/CLqExW/LJR/ZU8i9bvzUBP/1p15ZAu5+0Cd28/f4b2F7RjvXxBO5anZqnWifBh0Ne6j4a20P2e43ZPZuuHH263aQdzx6M1QGV33b3gW/OF7Q8CvGfRUj/RvlpUrN9ohcp6/dA+1l8p2vNC+3a3GO3p8XI9a8Cjq8zX/vz/Be7jq+O9LF7QjvUXBO2ewYy50UfqaH76/fFp8LVu75PRdkfZG2SnCdrz3rLOu/eA3p7Qbn2bkMd2V95+2QPco/r3nNA+R3XKnfaoOmifUfWWPX316rLbX+AeUQNn0XlBO9aTF7RjeWqSsiexmlpf68+yyz4yTn+P2GV0v92m4zE/Lw+iiixqAaq1667FYektvdfkW/KE2s50U99EWJlv8dnSma6/E7TjeXg9tOO+oj3V47tN3JZXsmf8Lbry3XV8Hr7A3dvvZ21/QTvWs6eB9pZJB0tRm5TfH58GX+u2mPGJu11/2XJknCNsFdA+havyrAW6wvUPKwZLp9ZzqZ2lv9bz+Vfqkh+K7o1oq/8ev9uW3r7jMeP9FUfeS3faI3IQobPfbLfV1NNXVhcrn3l+v98ucI+qiXfWe0E71nsmtDsGJ+ZBJ6mj+enzx9f6FfBc31Xs1MmCGn+mMN9G2Fls5DvtuXNddt0XG1W2bQXf/TlWLLfWQsCoraa8jFr86XVzPGjHa3yVfBm0475yNRill/PClu7pZ6suot09k/34uN0ucLe7+NNJXNCOdfkF7VieaCliQgtBUZ99Oty5wWibI+2NsGVAe+oUGlLzBgS0T6Iwv78I2Ks5mS4iAUT1bV1vO7S/xt98TjgntEfltW02rbfq5SurJ58UiLY5tGeBXTvuEbXxrjovaMd6rgrtxKDErAVJHc1Pvz8+Db7W7Z002u4oe4PtrDvtCpEmPqbgXYHq2tGbPGyEfdsLx2hJLAionBg7+O54zgntm6hestMeNR6j9LoLqVDQ088WXQncybYXuPcuhJPpu6Ad69AL2rE8UVLkZNZ9SvbZp0LdCI+0ezZbWTwTtP/0l2KTWKHmHg9jqs+TFhdeBu4EtNPfRkTVka23bafd1ts2fnG954R2PP62/PZs1dPXFl0NbRRgX4fr/R9/b9DaM6mXrpdn4IJ2rAtUaH+XIXQ0P/3++DT4WmNFI0mNtjvK3gvszND+6+02nf3c/ZEeLi2FtIc4K9RdXtqF/YGdNGkvoJiW5qImqn9tve8I7buoTrPTbvdXTIG2aO3lK6Mn7a6D307l4gawX+DeUgNnbHNBO9arF7RjeYKlmIlwr9TXetLn1wCH+rJd9pFxjshnYePL9MrHXx/nsdHjHjCcGtvl6bIY9uJPef9uK5hxrdTcRPatrfs40G77qqJaFdpLvT2+qsF9xQssQidunZPs6Suri5CfRQn5JQnXGXeuGs4lfUE71p9vDe38pIAlpVXK788RNPDR+73mbI60N8KWBu1TVmo73NbxFeEB1DnRHnjK2nrUcB3ulx7qK1YzPLRjevlk4Xp3kiq0azq9HYH7iuchQidunZPs6SuqC5UrIgF32Mv4L3DnKuI80he0Y30pQnvjIMUsdpQ6mp8+f3yt23Y2+nSG33POj1H2XmRn3WnPswLuutNMRDfYQv/cPOWpRRfX8y7pYe5hdXMMaMd8VWeXJmifbLZ0Bu4rVydRejkvbOnefjL6QNlVDJRXgr7A3a6G80lc0I716QXtWJ4gKd9E5Wv9Kmj3ew2ldhUaaW+ELcGGCO1LAprPudey/AQoHL+zczTm0Ryuh0OlW1iRcgivGQ7acb2Uu8QRBtEDGton7/IHi5kOichBhE6uB3Dp3r4i+tICC5FdIplFCfkL3PESOLXkBe1Y9+6g3T/YMMNeqaP56ffHp8HXur03RtsdZe+FdmrQPjMP+IDqLItj+ARTUtQQVr0TuKdihwJjRwZeN+8E7WpUIrRrVVTLjdUZeF65HovSy3lhS0f4ieoE5DZsD8jbAc8S1447mKhTiF3QjnXjBe1Ynkwp30Tla91nZ8MMURTwe47bPZstJZ4Z2n8xjhCAx2UmDF95yAKj+YcKd4sCu9XSgxtBuBXe/T0l02ZvVze5+sShndOLpwnX64f2GcGAHVitQ3BfI+LHdUZJviL+Bptzk4Z2lbRd4B5VU0fTe0E71iMbaO872DAHWqSO5qffH58GX+uWHni0GW13pL0RtjzQnkC5tute6DfgffPcmLCbD/MtsUjQay9t3bVXp9kSDsjUtAgQNXO/3e6///N2+/M3QDmhF9D2FMH0VqXmnfZ/zxaYmM66m1LH9NBbWo3QSXUAIRzhK6ITkMmHauPDp1YiLnC3MnSG6xe0Y714QTuWp6oUMLFV2vtavwKeUzB+z7nkj7I3wk7Fxpcflve0T9kByFI8LqNktrIbvr/fyrv5pkeigNmqcDjlh23HVdQs3c0EUTcvh3bcVxva/5YlHder91R55r2HzgvYtxlAcorIlOtVog01VO/XURkqX+8ofEE71mtvB+1RkwKWrr2U358jaOCj93vN2Rxpb4QtFNpRqiR//KjYDZc3yBYhAWqfp98rxNt8XKbMTTeqlkuui3qyZmBoJ/XCgwrXa0N72mnHddpuIkdpbC26RE9fPX4gbSN8RXWCcqsYKI+Evco8dV477lTi3kz4gnasw1ZojxhsmAuc1NH89Pvj0+BrzeU+lx5td5S9A9iZd9qXM+0U+ObgPsVh0GgG7vq32jK47x9ZReEdWYTU+qALYW/LvotKsm7eBNrNqNbjMe0zyWtampG9xq3Zaj52o/zU9CbboN2NGNiGyuxe5wXuVALfSPiCdqyzLmjH8qRK+SYqX+s0wTtDoJv7veZMjrQ3wpZhY4X2BXApcE8PkpY2FDL9WB4+tQB/8yNPD917jT3AHc1/F9J+lqFLHepzVvUQtDfohQYWrteU3JxpN6Uh7+KFjuqntNCO8rWmF7ApigDt6M6VdV7gTifyDRpc0I510gztf2KyL5eKmBQ8Qfn98WnwtW6PfLTdUfYOYmcD7RketzzgCbTZRm3R67Sbb7UQz9MU5abZYfrA8rWxwmm1jM+LT28A7VBUb7nTDkXWWDytzRKwJ99ecTyo9EGIRXWvd05lffk3ghe4t9baUdtd0I71zAXtWJ5EKd9E5Wv9ql320Xb9WcI7eIQtwMYE7T/+Umxll8dUCLI0duqVvSw7bZnerTf6rv5WqSQH5GdVksAGyUUCEjusWQJRuapifEahvUEnFBquF5J8O2iHooIy2V+orNFIX0vdBLAH3Au3KvdxP2G92DC4/+PvkVnq38eXRj0DF7Rj1fE20H60oen3x6fB1xorDklqtN2R9kbYAmyI0J6RZBXCc/0qVa9UWvcGINeaCY18d2rTB0Bu1MK1fC11W/KZoapoo8/mTnujXnNgY3oxqdvtdkG7mXFMQKpPuBcwExspCdoBNaJLvf186ptgfXo51v3xAxLia4avHXeg395C5IJ2rJt+/9ft9u0rJvtSqd4TgzcYnz++1pPvfg1tGRhtd5S9EXZAGzO0//y4SYnAWNt1N+C0AP51B0sF0/JCunku1WOehGF23cH87ArXAnBNL7FTr5po9Pkl0I77CkvO0P5v7FcTbVOPuxUcldvSfue4tUY7u7Kqy3OhA3GV80VdHn/l/tF22XNLF7h78n6Uthe0Yz3xFtD+qslWS6HfnyNowAokl/J7zdkcaW+ELdDGCu1TtlBwX2TVxdx+S/x5M6xulys/nFNvs0cUhe6LNQBXH5vbdqUpkncLqiQuRfQqblWh3aG3mkBcLyz581+WH1dKhoE8tneysyUcldOO1Bwp9FH+NQB7Ckl0sYffFrBPDtTtXOAeULZDVV7QDqT7frtt3tMONHmJSI9Joafjfn98Gnyt2zMx2u4oewezU0K7ykPljrvVs1ug2kZt6ZK/mt59FVAw2/OfBsx1YT1NCdK/oAObzXlE7/tBOxXVBO2//LvwjRCYT6tku16nIutoubSb52akT2nhUEI7EerO3R7+6zoeGwu4jQvcib48nOgF7UCXXNAOJEkQwScRbc+lzW51u8On0mzti9lUvxMYaW+ELcLGBtoLYt+xUL7jnWxYwLR/y/rDSm33PN3wpZ6sn5HZXpV8W/xmftlVLahSP5H3MgfVop0O2/JVvbZQd9pZf1EfcL245HSmXYN2daUJOJx74ElyboqKCvCRFcnHTzlOR/pWAru2GFfi6w7t+9hZUH96+tB1gTtbm0eRv6Ad6Il3gPaRExqQMmLVr2nzReRrjUQoy4y2O8reAe3M0P6TcB7DOsay9NwsVoedx43ROHqzUWMtCNQt9rWcdHjP+qALuOfA2NK/CCgWepEmBUPef//n7fbnb8Vwa/EXGdWYXkwq2bs/HkQVd9pLnzZfUVQcljwo29YWkDGzLpJhXUZbhEgg77NUb13mFuxtVQxsX3Vqq6Md2LOavMA9sogCdV/QDiT3gnYgSV1vqv5pzq+Bj3lqMdLu2WyR8Xz58nwQVews6ShLATIrRMo0uTnPLopIO/g5DGtVVId3GdxLAGYJuK2i660sH5Q+tZplbDEO2vH6wyWXOaGEdoqnK9+6qJ2T2pSGaoa5qNqrSfJBs13GEe2jpJ+wKYoS7cWkyu3bwX2r79pxb6/k17S8oB3I+9Gh3TspACmgRPz++DT4WlOhboRH2x1pb4Qt0sYE7T/8nG2WS3CTg3uuX4Lm7Wf5D5Q8utnacZeOgtTolAH3SlV23XVnq78Wn9GfCLiLx2PIOoFDwvRiUtsdzXWnvfQFyUGqPXqu0ZSnCMrrXGRYWms6R9jHvHxIab4CeVFFgLaqizVYL+qrGqbtwwXuTJ28WvaCdqAHLmgHkpSJ2JNETZ+vdW3y5aLgpf2eczZH2Rthp8HGDtoXsN4lMYF7aaMO7ntot8A907/jJRDe6xyvl8chwR3oUwtah0E74OuSfVwym4vmM+1/qw/v8nQLNxkQ0tIuPBcVbgyF9ij7qKeT/dQBZZuKb7Vm1YUA4tfeLvJax71mLLcXuCN9cgSZC9qBXjgytGMDEgiyk4jfnyNo4JPh95qzOdLeCFsNNhK071jdIEELqOcfKbFoUtt1z3rRsrOKCrayjx6Pw1r+TCKADFdkpHRun+hPY7N+ezyG0Et5j+ttkkSgPdVxMhDenY391TWv+QKCUtxZuNarRo9XL+PV8gioBuopZEYnJ3uBe+eyClF3QTuQ1gvagSQtIswkIWv1afC1xuMkdmPalVZajorzwHZyaGfB3ZB/RG1RUw7uCmmJKjS9GrzfF08sf1rAHVwQwDXcAGFvBO3caMikUWgv8wx0Odw1omBDf8EGuWzBakMEG6A9Hzpi89b4t+323/gxennZC9xDCqyj0gvagWRe0A4kqQu0M1NMBPLjceaSfq85uyPtjbDVaGOGduHtMRvQKaGkdv7keW3rkXG05aP0v75z/uhrFNy35+QfraxvElDSy/1G23CVCktr5jfHYxrrxHQC14tLTkY7QbuyHjTDerkAl63XuWv5qVy3mjW9lGCvdAvtptHGlyEUM979H39HLL2uzz6z5Qvagd4/KrQfbVj5/fFp8LUGCkERGW13lL2D21mhXQBZk0E35082/boejRHhXymBnb1e4L70wW6t0QPctf41k9c+VKyWpekDQTs3Ggrp1p12LV8v7CKrCx/XuWxhOpPe3sFbvhbX80WU2tTSKUW8bSM/U1PT22Iz+bFve+2441U5VvKCdiDfF7QDSXJP1J4pJ/ZGYUXv99yysL0+yt4IOw4bH8tO+3oP14+X7HendaiG382+wom+ABB3xeHFwCRY5GdjCoQX8ay7lPfcHqibK1xOenHneabdUSuqZVwnLilAa29oT/EcoJvk1HLZwgtj0pvqtEfwiJ8KtFedRvTmwCyM9Wn079Roehl79oJhLa9rxx0vzWGSF7QDqT4itHsHKRA2JeL3x6fB15oKdSM82u5IeyNsOWzM0P6j8CrGnGxz/eVNfn/T33qzXBfZIH2o6LfAHNmZ14qS3XWf9OzAXct7DxBqH00iTsw/rvSvvkpXbVj9YVI5hBXupl9EXT9uyXPyIm+7QGyLuqCMxu2y5w5LuWgJyOrZaGDXawYH9krdQSlRcrB8fO24Q0kcKHRBO5DsC9rtJFmTX12Dr/Wk26/BjhHfoWjThbQaFecIO04bK7TPVFpsahdQXYXo50XZI+lNMRK0L36kbqTBvWhfK4cduBttN9DO5P31NPjYaY+AdjwPuKQyF/30l9vHr/9e9CiTW9QD4K1GyDTjkkF9ZY0sCxS2mSqP+LnIJNNIk6Z70VOxfCzGusdBjimZ0BYmz88vcO9WdB0UXdAOJPGC9uMjt2fSAkpAFBltc6S9EbacNjbQroF7ZsMA9/VmWd1Zt58DfZSKtEsvKFZ33PPcKHDXDO5M3nseRWgbZ/ff//N2+/O3tsbVVnge3JIitOfOWQBveZAfr3g1uFu+tnalpNfKW80W4mcmg4jP5mBBVZbbZWfslfkQ2s4f7T+/wL21bnu3u6AdyOjRoN0zSIFwaRG/Pz4NvtZ0uGuD0XZH2XsTOztoL8F9iqMGyttrzxtlDXpyGFf0r/UhUrXhk1aNFTjJLj3/islztW8BkpUPzlqSfjW0c6NBkZ6g/Zd/N178o+WX82BdNFrdhb7/n+q2Fl9RAy+CdiokRliWxR9AZWxpOV50pKGrAHtqfYE7WquRche0A9m9oL2eJN/k4WvN7mwA3Q2J+L2GzLxkgTAitg42RGhfwH3+T7Jh7VQ/rm9vlha4569irNGRSNSN4J7FVhaPuDYxqM2EOsDIrj5ppcYwuN+2P67EjRpdGq8/XLIyFyVoZ1K6Ljg5D54mlhoW11Kazrz/WhZhrb7W+rWms7XeUD8zqIVKD9W7rxX+WAxjS3I+jw3XdYE7VAiBQhe0A8k9ErTjgwsIrIOI3x+fBl/r9gSMtjvK3hvZUaE969XakZjsmvwLqNIRl6JiVh0WPEjwXvsWoFaZ1iLk0fZDfJ97AWKW2zs30N1gWrES8GuhnRsNFekZ2v9NP1vVK11iFqU6YyKb2hsL4Meyt306rbZ8MbTDYcGCS7RbeQ7aWVtlgluAPbW53y5wDyp1SO0F7UCaLmjvsWNVWesDfaCJeCevVtOj7Y6yN8JOJxsztP+ggFAGpztmKT74yHfZS2EJ3AuAqS0MNuWF7rqnXfwESxLR2eD+lEh/y/MOLEjMoVHuykoNvDQaAe14/eGSBrSu0J5y1LpgMzuFF4C6CAF3LluYo5ZOyPnClKUzF59+KADzlBDcLHAmWJ+eE48/FlMEMv/TCi7No5nc/X67//b/XuCOlkV3uQvagZQeBdqtAQaE0lXE749Pg691eypG2x1pb4StTjZWaJ96sgaQtbe/iI9cFaVRwrYAvxsXLJDQgDkHo3ItQoB7mY51173Mu7aIaB8ZeksrJ/qSvv/xGKz+MKnktyH906+PM+3aIi7/vCVVvbqsaruoz+qY6+UQ0gtswhCdWb9C4pAQ8eBpDahRW2UfLO0gWK/U9QLtt+9fL3DvVeaUngvagXRd0C4nqXXyeGjzte6jAeh8QcTvOWd3lL0RdjramKD9yw/Cqx4lGzq4zztc1m782mGTHkV/kll11WCCAOaqvoqN4pIu6dl1L0HOqmwGsB557gvteP3hksBcpEJ7siLlZbkm/jiWlecO16GuYvuf8QvtAcjRzDCqd+lXSBwRkmX0YzFlXSE2tPzmwA7Ua+3unEH7pOk6KsPUdA/ZC9qBLF7Qvk+SZwJ5Z2j3xw0UXOMNhtMc0ae2Bx3zl6B9vnPYlqXXMG7Osqs6hAuirAbimuIWcNeCxeC9nqbag4tIflEZpLOedXJeaC/zVealGCuvgHekq9Bup+XQuYJxEtWZnEWPx1h69evxx2LKxYfmS9q9MGIpoP0Cd7qwnQ0uaAcSeARotyYFIIyuIn5/fBp8rdtTMdruKHtvaGeG9i9bYp93wa2b+HPX/XHDROBZ0QnDO7DrvtnBt+xJ12vg/syLCe5pcFhpbB9ES8uagddCOzcaAGlxp11LYGX32g3uyPgo/AqvAykPQE7XZoyDjN4SdrX+QnTuZcY8eJrZXf9ag/bKoF6bfb/df/v/btPxmPzPtePunhBBBRe0A4m6oAcPtqEAACAASURBVH2fJGSi0lPra71MpkDP9Rfxe477dDZbneOZAGY6HlOerZ3v4daN/AHuu9c8qsCag1Shuwu4Z1Wx6usF7lneF+izsrP/hdkaWNna9JqX2m7rpN9OO15/uCQ4F83QXnl7DD4pPCVpgJeicvRdatqwDrDDZXoAjYHRuXhYP7uShIxwino23SgFzAaK/aXdprmky9C/uyxD+3Tg9QJ3u7L9Ehe0Azl8NbS3DlogtCYRvz8+Db7WTSHPjUbbHWXvTe2s0C70KADu89GYGnCbLGDt0EtwrykVyKcWgwX2tYcEC9gzd95VgbJuzIRpHVV8/lpo50YDKE1BewMFQwBf87Xc3Sf6srKeHTPXWr6mfIJ9tXHaOh5j6dxfH7rDvjNPQrsYngTtT8EL3NurHmt5QTuQpwvat0myJqp6Sn2tXwHPKR6/50CxZSKj7I2wE2CjBu1TFg1wX2+eIvBIQK7BVA3etWslaKT8MKCff6FggUtReUVu2sBd6lPSj9WtvF0EtOP1h0sSc9EK7SngWp5yDxIRA3mtgjsX1cNLwKYUTl7KDesPfnME8bMlfgvYa/0vLxTwh06J2ioXGXO/LfGa0F7Ji3qpDu1z5dz/8feWjHP3x88qfUE70POvhPajlb7fH58GX2ugsxWR0XZH2hthK8DGBtpL0Fm6UdmR3t8866+F3L8xpgQFCbbzUpLgPX1mwK+1q74xjQBMnpunvN5SW8Boo4nwARiQ99/+83b7+jsgWRPB6g+TaljET9D+83Q8BlloaV4Qed0BPBfZM5O1RUOpM69nwVfYfdZXTbEMzlQhVSlb81P3f8hDp/OL36UvhnO/jBxXL5fQLgtf4E5VGiH8w4+32y+/Eg0+o+gF7c9eZyfUbb34WrfuPvSoWb/nnBej7I2wE2RjApOP9MpHDSBkQJVvnsvNf8cA2R3QAujqQ60S/Naqogb6QjvTN60NAu6pbVrcWH1K7BAbA8MP7ZavrbMbrveWQ3uWSm5OaGwIw7LlTanIir8C0+q3ApZOyUfWLyvO7D5TdScK2ltykC0kxeY9gD3pmH5cKX8QVff3Anek1liZC9qBjF3Q/kiSZzI5jgagwwsRf9yczZH2RtgKspGgfUru7t3p+k7f5jWPu46RwD3t2OULAAugtAXA7OzTKvq2m405g8Lmy/lWG0BtGUQB0g9v+guKw+Q80P5XqYCIPObpIRdFpLg8X5VKWsZ1vgCWCgjQmYZjMSy5ObYmnflAQ/u+gf0sKwHU1SAXPR5oV+Pd+ohC+2Nqvo7K9KvNSdMF7UA+XwXtwAQGeN9PxO+PT4OvdXseRtsdZe/N7czQPr3yUWYhjSwfUVvEmR+XSXnSdsolXcVn4u795MfyVTbizy7OSgziJSvmKS3Mrnst7yVgto6+aVfvn87jMVidY1IpDk769uP09pgE7UrBAt2jwzSQ3y7gDtgxRcoFZU7hWeOUj3L4CcPRNInArnSmBILYXDkL7aU8WVezaWuRASwKmDin97T/nnbaMX8vcPdV6Lb1Be1ANi9ofyQJG6BaQn2t/faBjlZE/J7jts9mKzCeEtpVFt/S0NMji5KkXz/VwF0zbsln+Vl3yGvVUurTYpgWA9o1dKf+6YeVKXPNsQokQCtj1EjMC+14/eGSDXORCO0lvC8erMk2s14k0dq5Fq6zJvCJjJfMWR5t3c1/pfd3HysLDOH+OB8tX/zb7rZrlcZV4DNFtQdmAWBXy1nLCQ/t8+x47bijVW3IXdAOJPIV0N46gIFwmkT8/vg0+Fo3hdxhocLbHRXnCezM0D7dFREYecg83xhTApPRUxsTtV10iSJq8oJd8/V9kr7SbgLAGtVY8L69DvERJCQtcPJ6zJWMgXZuNHDScw//+MvjQVQ1PwqxmrWg1W0yJPlaLvzYWUpbeLF6OsjD9abZMvpyvVyDdZl6uTfFNCwE8/uTGEb5oSAEtStyt+60/0l34AXudMqEBhe0A1m8oP3aZQfKpItIAxA02R1hJ9jGCu0SBEos/7H9MaUaiK85zQBlBwgMvEuyKT8KeVSBrQbuRd4ZPVItbX6QyfBZ6Yq9WmWRMQv2gna8/nDJRriaoX06HqP9NoC1aHTTqbQ63H6Wc37VHFADTXNSQ6PmtIA9vhEDIDjfGFDD0WyDPuWwnmzsmuYfKHpVczU/pl39aaf9P2+37zy0z6P72nFvKPS8yQXtQAJHQzszeAH33SJ+f3wafK3bwx9td6S9EbaCbcyb7NmZdmPH/bnzhcJ27v/SxgXuOZAuuld9LeBeAO6sQtmxnS9phAOQjxi+0Q5QW/V3iaX9TDtefxGSm3lnhfaszzb5AUG4eecdmQVr40KiQ6iDEcPF0cu8hgEbgMjDCbyXV6fXJlLb52f6UZgy/B7AnvWFCOt5/gR7TbCe5W9zph3s3kLsAve2vD1aXdAOZO+CdiBJqkjDVFno8mto83+03VH2RtgZYeN2u33JoT31sgwf26+rNUDZ7vLu6waA941qiSgKsLbkVxjXqjhTgACM9WNSlJke4F4fnW3QjtcfLtkIflOzDbQXdbp785FSx3mazJoQFpzUJKiNDy1b5WIRKURpISA5WUJ8scBRTXE9K6ZnVlEH9jUK01xNwGycuZfJis3yD3tBe24zP9NOFdVG+AL31txd0A5k7jNDOzOZyKk8ggagk1+8UPBnCY9xhK0RNiZon+7YGhg/MzIDu3hzl4C3BJAdLT0+2Omr7VSWiwFUdrFtQlpyaMm7tSNL77pnCRTXCHHwfh5o/4v91iKrRjWm3RWjNP4YkNZkU30hsw1qj50rCogXzbA6l3jyeWJVoUOw/TpHZKHH+LrI7ppgC4v6lw2aH8XnHXba12XpdVQGGUiFzAXtQNJGQjszgAHX3SJ+f3wafK3bwx9td5S9s9hZ7q4qtG+pWof29fbxLBWINRYhCtwLsDYfoBUcsWB8s74AAoF33fO62Z7L3lqp2ATckcZrJLRzo4GT3sQy77RP0F6u9pTVpJqrWn7LhWFt9kvgi3SKsohFmsqr28wxR06T7l7QnrqiCsR7f/kHTst+QXPAALuyWBBNgbCe3L5/X860f22/vWYtrx13No0XtAMZu6AdSJIogk5Hun6/hjbfR9o9m60R8SDQ/gSk/VtjpKpId//07vQSsMo2GrgX7apwo1xcP2bAXQNAaxdcu57lY5eu/QOVTy2WvfXub+88T4cT6Pe04/WHSyoQhE4uP/68PIi6WVUJRy+K3ImprAA3DNJKLaPxJLmc52HbtbpqcGBnl+vV1aLaLF3YCtiwrix2diEi/mrAno+lXHGms6q+dlG51hna59ny2nEnCv+CdiBZo6AdGbyAu91E/P74NPhat6dhtN1R9s5iJ8XxsRyPyQli3+uPm2t2Zzd3MbM81eB5NaXBuwRgme8bVyU4r8e1f7BU073oqe3SV4/L1OpmHzsO74a/i9tR0M6NBk56V4UTtP/0V/s3vcqaMkqgqhCG6HIh0T5zrs9BN9lusSu9jcfoq3xtC3XrJLQXtKEdicdywIJvrb3VrrYIrfg0B53eHtNnp30t8QvckYK5HkTFsnRBO5anrZQ1Hdk6/RpsG5LEaLuj7I2wM9JGCe0FnCfoW12qgbFRKXPTGomg4J7nR9Jn7bSW143dcCmsJniv5acV3JdcGEd+OGjH6w+XrAEOOMOI0G7VglBzzIITWqiKBSJ8mJMuGLO54CD1iOIltIPADnd+EnwhtKu+1oJYrokiQDuta2Zon47H/PN2+94X2qfFwLXjjoyJa6cdyNIIaIdnEcDfHiJ+f3wafK3bMzDa7kh7I2xF2yj0T6983IDMlmr2u2EWuCf9GjxbW4g1eM98r+7gk+BurSfEs/NGHCtIo7Amf5Px+FSyVfZjPmKLPqSOx+D1FyGpzjsztGdn2ucQJQ+0fhFqAu2a1AdW6e6cry0IzaLbaptU7fxFFi3ATL5Z9Cm9ind2YXBquG3c5+FTZCHoBG8K9oEEpcCn4zF/9Ib2p/0L3K2av6DdytBj0P7+2+32rffKMjcNDBrA034iPn98rZEJrV+kW01+zznPRtkbYecFNnbQvt3e02+wGhgbIGHuuBcwA2zOPzw2dt0rC5O13qpQJi1WEHBXFjFqkcuLlj28C7Uixjj9Iup/3W5ffweGFV5/uGSnuWgD7UUo5u65vqDZl45F8vLiCkiuIkKvBJZyL3ugUc8uAcvzKFwHV8J/Qns/WE/mLCenHzLSXKu11dppbSq6NpfSIiIW2ufZ8DoqU6nJC9qB+eqzQbs1mdgpO4IG28tSwu81Z3OkvRG2XmAj/biScM+/3y0QKAFG8l+CXQ2yFbja7KrWdtENcBfNpi3M5WJ3cJcqGsxrLVRC7Wmg/cdfl7WZuO1cOX2F7r6zINwT4PNdeas+soUQVa9pQVIuTHLb3AxsS99v9/t9fnzEhnZm/rNkEyBLHhrArq4zjwrtsl8XuGvVeUG7PW7Dd9qtAQy42FXE749Pg691eypG2x1l7yx2hDg2v4i63bDePYCqFkbaHa7lSSNQC1AyoFhFtYVAcrAF3ou2DAypZ8pTPqSH/ay4s0VExlhIq83m6bRp+HvfnXZuNHDSaolNO+0rtJdSLfUgLRALX2e1VsYtcC8BmZldNdtATpn6XWME9DLuz7LfAVhXKblirTd4J1PBu+xzqL132vVcXOAuldAF7cAwjt5pj5hsgLBUEZ8/vtYtE6An1ryt33Pck7PZGhEPAO0Zu0Ln2VdClICHgSsLjhZdGzF2172AsLl5BtY7d2s+SaCYf1bmuhXcM583/k44ieUMg3a8/nDJjnNRgva5jxYPdosloR5EZsbytln87P+RVcukbzG0Uy1li7C/qUnHjnhycbcOafUFnalr0M5V0tZiELSLXwdotpTPNx/n/1j+Pv24Urcz7XYOL3Ava/WCdmD0RkK7XbSAgx1F/P74NPhatyditN1R9s5iR4mj3GlfburyV9nkDV4V12C7trOo7Wg6wX2CrVWFZF+C7XyUlPYtsCr1MTmd2m77EQH3ntDOjQZOujr3/DA9iPrrIlIuEMscSgsqSTuY+4zJn1q0tsXCrWoCtN8+KdstmR8bs7VlEo8+mo7GyH/SaqqlRgBg360XDTvzZUmmFdgrugZD+zylX2fcszK8oB0Yyhe0A0l6THKooCrn19Dmwmi7o+yNsPNCGxK0m3UowUaKoQbRFdhdFgsPiVJ/nh9JvwI/VRgXqnyW7wDumpqNSQe8b9YvNvjZ0I7XHy7ZZzZbUzZD+y/1qcnaeUf7xZoAm+oKVmoJ9r2u1rzHzLNKZGjnqmjvidK+emgesLlrDy4Ocgc3TdI/Cj0vgPYL3PNOuqAdGN1R0A4MRMC7fiJ+f46ggc+H32vO5kh7I2xF26joF6B9c+8SmbAG1QoZQXpyZgZ2rFPR1MCjGbBYeC92WBP8Wzutu+u1BlM/boh9WeLUwf010N65pn/4adlpVxaHm/VgmQ9xq9w+rl47frQxoS1i0QXsppCldzty0yMj3R3at/3+gPaetaDpan1TTHJP0ot+tiR8Fc/bCTq6QTuf12vHfeqrC9qBKeKCdiBJHaY2fhAjftkyo+2OsjfCzottWNBe3Z2sgbUAMipbanCTYKtWgUtbC85FOFEA0NI1wVw1lrJPLflyg19Tnut9xm0dkalDO15/uOTUX5y0OcfM0C7ttFcWLOLRD6nWhLxuHKoviuTfOTB0KuuIfR7KxWAakMDixUxqqrsyvmJxKOpJMvV+fu6096qHUs/yb1W9YVcE7Vr9ShBeJqgG7Y/FRZ8z7W05vcD9gnZgarigHUhSh9tc2yBGfNNlRtscaW+ErRfbWMHmceNWv2E2uEWvD3S3UYJ8BB72O88qUW9gXMp7bfEgREidBwbPxu/SUFsYLQA3q9Y76AHtf5BndvfxcpXKSZtz0ATtP/58m98dKP5phffFz03zBhvJp+o4US5CYwtZwFZX2Erepmc6vhS1kdccaldW33enXaopDdqxRcXjplvRuwlLqenNx8qi4jG7LqamB1H/y/mLqL7x9bnB/YJ2c76N+XElX9ECTpMifn98GnytyWAz8dF2R9k7ix0jDhTaVR4gwbrKFdKOn7YbWFbsc+f5eUVaCKSrmt9amwrIbS6lfEvy0gJDiAOCuIIS1/D3jZ/Qnscu/V2fBbjRwElDc890n/3xl2JT28ixpNhaaIm5Lz+0OmgC4ZpMBd6R4QQlzPIx638rJ5C9XOjZ//qDqKxSBazVUjNqEAbtBPXK4gXWkx/feT20z9Pwp3049YJ2YPRF7LQH3BiASHrd1ko9/mj8GtrCH2n3bLZGxNMR2nfAneu2IKG4bonPeJbpn+XNRk+ZVVRqA7wxRlkTPD7OdeYgXua6ApXVUMpd+RrJZYuVRSzfdb/Pv5Hx38tOewnqeP3hkgly2mYTtdUC7cVSpRGOU/INQt70EbNAYOq2AvDuFOZxSuNn8TMI2h/f2nGVI4esALuoHrC3ipSyWtuKTlVX4dzmK0wvtAMxgrXzOcH9gnagPHpDe7+iBZwHRPz++DT4WgMBKiKj7Y60N8JWtA1Af3bDxn9MKXErAqlV+t0zsFWMEGBIO9o5HKFQJQBVdTGwOL9rVtmpheDdWhwZC6IJ2r/9IfDTdyvb83WgijI9nDTkwCQ0Q/vPu4Xblqu1ZKJg7JQrF5p5cNCis2bfqgE4k3tBaExh+tPO+vMXUL31kLcXdO0+MuxtLhu615Atu5VFRRpBhV3f8RhvTrd9+fnA/YJ2YDRf0F5Lkn8I+jUAnSiIjLY7yt4IOwexsYN2aUdO6PqZL7QYrB3xGgwnW5PuGsRYNoo4VmhSfK5CVQ2KpcWAdn69Zed9apN/MyD1j5HPBO2bLkOB5QjQfn9A+w/Lg6hCX/HwnuJn+gp9YNiYTSFIFupKLHlkHPTwR9cxLfZ1SG+Z52ptHG+JEaesyZZhTwpdhf98/tIeEjrOTnvy9nOB+wXtAO/1hPaWSQBwsVnE788RNPDh+73mbI60N8LWQWwsAIH/+ml2U1p4sv4mFa2Xa6CZ56Y3uFuLAW3RIoD7qiolIo91kYdBqya/6N3lO1eu5HPy8Y9lp32zHirhR69HvFJxSXrsf3mcaV//bCld+biySNqBWg94r9SWFDAE79qC2cogAfOSH0Uo+fzwBHSTZunvacyHQsUSA+tOBe2yPaBvFtHksoWA4u/j7THfrE4UrgO+NWidZ71Pc8b9gnagRC5o15LkH4J+DUAHDp08ZH9GxXkWO2AcKrRr8FrsJK1bNSKVGKWlgbvmu7bjbQFKsWu5/rO2cNB0Grvam4gz2V2zGlDmO7opF0jsSaYgrhLac9hQQeYRCFhFS9ScNDbvLDpnaJ+OxxR1qfTl82OrNkovyMWWqh60W6tFK0GQCU0or6tifGSr8H2PlgsTq8+Z62lVWgZe210HqhSGdWVuy90xxstzxNS/ETgitE++fw5wv6DdmloeU//vv91u374CspaINQlY7Xtf9/njaw1MWL3DbbiV+13wZwn3YYStY9mYd9Got10I/rvgpYSGPZs9+08xNH8MUUyhqtIGfr1gOr5igL54uQLvH1KdlAsBA/6nufePf91u3/7MEDzTq+wEAvgiDKmIupagPZnWFkXPzzfZWfuzBE9pdtDgvVZ/2iwD1uXsnwbFho7q5VJv3k91vffaOX1zUrXqwbq+3N+MGsXdyBXVbCvXugD7I6Y2aEfyZWZDEXjqPj+4X9AOVEkvaI8sWiCMnYjfH58GX+uWiB9tRtsdZe8sdrg4tr+AaoBnrf9NcKhVXGpc+I4uBqAdSwHWrHbVxYDgs7X4ocB9ycWuTQ6rtaQvsFaD9spwnt88A/9hZFGlmc7NTnvZXoL3fV7krkaAujw6I7XJ+woZQ0oONk5qOUV8Rtewuq62HpVaNWhamzj1qaDdoHduosWSPke+FUhn2tnjMQ15RIdaEde5wf2CdqAsLmiXkuQfgn4NQOcN2lHTPBkZ4whbx7Mh/qBS2HlbADIrTLa9pO16VqBqVVDIVOEcfVd85l1tlx5aiNQWGKUdA+JWaE/QUanB7NLjr2i9onLMjFNC+09LYwCKlcXYR76TXVsMiW4uDTbtlMXm1B7+pqaSk2qtCDWCrIt3MrIRrkdr0oSmnWj+Qfr7shi1SkkFdqmuDR83iwjJ/iKg/jpdcnaSa4F2IodWXnbXZd3nBfcL2oES6QHtkUULhAAWOqPJF5GvNePnVna03ZH2RtiKtsHpN+8xrfA+Q4tUZTXAzG/KQltVnwa4eQMtL+UubcU/EQQr4KQuBCT4S/FqEFj6mclbfbSD9gqMJ/ZY04/UEyLTMuMU0P7DAu0KkD8t1BZkT50PKekBVGMRJLZR4qsuBi07S21ZYikOKMVFX82144V2q/8r19PwqcJ6DrxAkKYuyR/Fx83HWhzpc2SX/fFGmfuf0y+iMjvtVo6BvKgiuu5zgvsF7UC1XNBeJsk/BP0agI4TREbbHWVvhJ3j2TChfWXDHCYhini0rO4UWqNCaFw1zcqXoJz7qxjafJzvuNXkNadr8G4seHJwhaEdhJUNeyA1i8iws02hczoeM91rU0GZ4C4UX7VLPfBeWbjtxk8tD7VFprQQKXWletTqTegnBdq5HrWkhesqrEsLSkt/kQcTtEt9FrBbsL7Yr06mmY43gvZ5FJ3urTIXtAOzsRfayUELeOQT8ftzBA18DvxeczZH2hth61g2YGBfO036eXYD4GHIruUGARYBwKt+I1BccZ5ajCDHa3JoTLkwYD9x6ccX4QHGLL4/f1t+ERWEj3QoZha3ata6zo34p3QJ7T/cbmmnvVwNVnezc3ivPzD8yHa5kGpZoFp1U9OZL45BwPeMsXBoL/pRLBeJ4Im6yofL2gytdQvYa2MgX91a/nqg3dLdOsaQ8f3QfS5wv6AdqJgL2ssk+YahrzXQYYrIaLuj7J3FDhdHE7SvbJyTAgA2JuQS0J5Xp6i3/HDRbe1IlzCY8570lcH8dhcArHf+VtrMl/JcALv0lSMOs+npeMz35e0x1RJ5XNyImEXC1Rw29wg6v5TQvhbiU6UJ75l1RfaZ7Zad90U/8i0AVItYtjYJqI4HIa9C7XA9akkv13diImlnAVt6F1F1EaDlLm9QsWHCfzZSZll0kdByPAbMBVsu+9Fe1XAecL+gHSiVC9rzJPmHoF8D0GmFyGibI+2NsHUsGyaL7QpE8X8DHwC8b0A4GanBb+mIYIMBFQjsNBv551k+rDfGSIPN8mPngpLbyfbOfiE77bR/+/3phVGKm7fGmIUSUdcKtH/5yX5OYhO61F9Fbmh4VxaEa3az61Yfb9YdSm2xrzFlztuv9qdvauDyIO8L0jlvq2as65Kz+RyCALQH2LO2VbhXbFDHY4hc0Dd1Tvf8y7enOCpzQTtQKh5o5woLcMYp4vfHp8HXuj340XZH2TuLHS4Ok8VQaF9v/J6d99J3C/5RcFeqHdkJnZtKgFcZQS1vC9mBXbGAMeF9gXZ14XK/3f78/Xb79sd+N1AomedHy9/Qc7rtEwsGgdNO+3SuPe+T6mKtfD7BqLHKwutZLmWN18ZcCe/ianWfNbX0rTFRWdzOTZOv0thJnz3erVOVZfF+rh9uboLkNypR/UlOg2l06ZLGRpKX9FUWDtM6Bn4QFY2tZQByutNU8P7gfkE7UC0XtNeGN5DA5n0QTndNmhvgPrtnszUiHs4GB+2E7hV+AMhYRVhoX1cKBvTkoFLA8MpQgJ80vEuAVrFjwafKe1NDYae91DfvtE/HY4RZqEj99p8TXdT6nqgLeEJQdK7QLvR9bWFjdm8NYrdOPySlYzNacNqir+bUEn/LAnDnRrKTdEp+buunX4/mYMtqrcinYUxDuwHs61rF8jW7vv6Vhfbp7TH/Dbw9xvIFHlSCIKe7nAbeG9wvaAcqpxXaucICHHGK+P3xafC1bg9+tN2R9kbYirbB6eeAfb2bceVjnbPOAdK1u6g03uwu5q7jkCYh2+OzHIIsGC+vW/JlmqUd21zmi/Frtrfb7WsJ7UWf5nxVmr9/V/qdqzm8eFBoZ+Dd+sXaBcZ3ZSL31ebT5za8ESIK8Fn8LceurERLC5zMTluv1iCb1WgA+yY+Rvcki+rW5DzAng+y94f2ecS87VGZC9qtaeIxWH6fbhxfAdlchBmUpOomcZ8/vtaN8NQU5+7O3UULrsSfKczWCDvHszEE2lemWnaD1Q7JAVgSMrdKl0alXE2vBO4CBGJFVIfm2ZQWg+CzCGqGv/PbY7Q/99vt63Q8Jt9pl+dYsS7U4w0RdV3R+eXLcjxGy+eSo12qJp3lwgftj9pbf+63j92bZtAakvyp5bO2u2+Nj6R3kityMacy7bRPTzJYuhruzfRkU+Rhl5YyntogLRsrOV4/rvWBBOzSPVnSUbZ9L2ivdeF7gvsF7cCtrQXaI24KgKsWXDhU+CPya+DdH21zpL0Rto5lg76H0udRlQoTd96F3IjcwMBEcSSgxsziG2Es8BLAZ7VR20W3QFHKhdSm3LHNjzdI8gnapw0TrRYfn4vsIkJ7VE2j0F7roxLec8jLarO6mFKKZrOgevq6wvummVWzJbiDM7P51hll8bpTn/L0XBA8IirhXql3ZF6YFTK1ooFxch7VBcL6Rq2le7m+EUPtlG0RaLf8AetFFON0W/eM9wP3C9qB6rmgnZ2+9knlBhrQKaDIaLuj7J3FDheHNQGH190G3iu+u+A9gUcJaVLJK3ClQl3pswDRtZFlwmLR2Np5n3dK8532ErJyaNfhZ/vGmMyHYdBu1PEU4/QgKlwXCUolvUyflbL1aXO/+47C++InXB/l4iT3KwdvK6+TbHoA1bglCOvhtYUw5B7XiPlpNzmlWiZ0iDYr7ddLmkzxuQrtlfa7S+8D7ej94r3A/YJ2AP4uaGennXB4AnqNmnAhfZaQP0uWwnXhzwAAIABJREFUhef1EbaOZwOdhOkbLp74h6S5Y7go3DFPuRNIGoahrzzVolKJfPzFim8DZ6lOKoCn6dtBu5CP+XiMdDTxWZ/S32beOhq0z7Wj9Xktf2UbQRY+Q77ApLre03bRLYDPF5mELDOedql7fFPDzVQ5TJcLhNJvQXM+CU05l89lkQMbhG5x3VqLXnplZamEAfbHQqb+ICrXG1yiON3M/eJ9wP2CdqBmWGjnCgtwwCni98enwde6PfjRdkfZO4sdLg5mAg6H9vVhztp5bA3cU0VrYJOgolL5LPhBDCXtyhoNZ1DM+5EEdxe0p16uQMf3mdyzRHI1h809gM55p/2HbUGYCzBhMSQuAgsvrQVXLl59VaS2S6+BrbYKgIrv+WYboPyfIXzc7ky8JN7PdqCJJ18IYFXzlDKgO2fszXCz6g4B9geE7/9kn+0unxPa57XjWzycekE7MMI+N7RbU4OdQL8G24YkMdruKHsj7BzPBnTvXMsg2v9Cf/VhSm13VYKZ0m8LmrWRIe3EoqOI3MVFQFIDRRTavy877UK3Pj5S+vv79PaYo0H7jAc5d6Id82xn5XzzTUiuXlgIzJf1h0XlM+95DFLNCuSt+qSEby5qHj4cB9qJbty8dx6E9k2Zt7TJ6V/6u1AnopkatEfOu5xu7n7xTO7xwf2CdmCkMdDOFRZg3Cni98enwde6PfjRdkfaG2Er2ganv3UCbq+fWsuK703wXgOgAvAkt6Bd98VnalcSgfc8FxL4WTvvH7fb9GYV9c90pv2P7J3Q293DfU8Un2ygnas5vHYAvZud9lLzkqNNqgTo3TTToL/2bYnkZ9k/9fe4y2fea/Gju/VlTrL4jQXK3Rpzq2qgn3ZuIG0QmRySy2+ntErL9G5MWPZqO+w5+Wt67Pb68RjLN3xU7SU53Z57xrHB/YJ2oIo+L7Rzw0RKpV8D0EGCyGi7o+yNsHM8G54JuK1+GqE9NbNAAj0xoIEaDO/JUNGnMLxb4C7UivXwae77DLMltBc6N9CeQYfKFjnsfM+ON0TUNahzE6fW+QneEbgWFnMbtcw3LZI/KLxbiwtj0SnWShm/7sv8mkf4h5zAvsrrE5p4UL2oXA745ZdIlg4buJ/hlbq0RULpz/12//ov5ceVLP88szGuG+q2jSt73ccF9wvagSp6V2jHixxY6wN5Ogq0++Pmgh1pb4StY9noMQFz/ekE9hJKNXVN4L6uCmStok4DrEyAZwAwcwuB90lmPetdwEH659c/b7d0PKa40dZrYwKYBO1RNQ3qnaA9X8ipR0WKXVgLxOd81I7a5AqmH2myig7feX/sui/xr81q+pVru1wYi5ZMzePd7GmxI7/K3TX2dw8yK4vgHQAicsY8s0lDrc7S+F5kRNH8Q02XF9rBsdDUIZzuXveMY4L7Be1ACaHQzhUWYNgp4vfHp8HXuj340XZH2TuLHS6OXhNwez1ttt7a1NR23lcIyfNiwdVmZbD3adM86TV0mkBXAOIEbEibGrzD0P5tifGZo83fVA75prxBpq0bt62IOi6hfZNKq18kX5XdcWNNt9EEv21mcXZn8llX8trCquHGxeAC6bN18fcTpHxlfSXFXU4ym7fCWP1cjltLvlJ7c9PUHtVjwbal02pf+HHXdtpRf1vGHq6bv1/k+RGm0sM9nHpBO1BBnxPa8WGipdCvAeicQmS0zZH2Rtg6ng1uEo70v4NuDd4/JN0W9BjgPkMNqReB8HyHdeNCxV8N3HfQLvg7/Rrq9wXa18ubN7M/gb5sfj8wtCdwn3xe81MCYJZgMb0ovM9GBCJhd8bzoyqyr9tNd6SGi28DZi/rx3NWzFKhvbTbOnZr7Vp1Knc18T3v1h3QAu7UvrYQWK6p4SDQ3jkXm7A53dz9og7s6xr4UOB+Qbs1Kh4r399/U94VXA4KQN0QEa7Qjb2JBo/99huMcj+G0WbgRYuEUfmMtsPpj5iA27ud871qZwfv0651rQUCP4kCcz2Lz7vm1u4uaq/w2YL+8vq8A728CnE2aUD7IvKQkvqjONM7Q/v0Bpnef8hakHbaS5fUlGcXYHDPa6GsLcUQvfOu5eChf/7/LcEbnTAJFzoVn1YpeKe9tf8HQHsTrGf1Xy3F/KImyJyDf7wCc3+mnRwPVHdwuqPuGcc5KnNBO1A+nw/auWHSH/mBTlFE/J5ztkfZG2HneDa4CTja/wD9M7xnehO3IABXXxUsV0sI0hqxIJf0su2S/bSTOp1pT78Umvmaq8932vOoVvGyX5Z/T/+Zz7SnozXcyK5Lk7WwvtqythhKcA3ufrMLMWlNV64UYXCf4l8cqCwkNpe2FC+kN9O5u7rdeX9mv/YgalJS02vVRBC0rxObsFAx3yWf1XfVfWVcbNrUdCmx76CdHAtWyiX/wDbc/SJb+ID6jwHuF7QD3WVBe2TRAu7tRPz++DT4WrdE/Ggz2u5IeyNsRdvg9EdPwFydcb7TuqVjM9SuqrZwFpS07OhvYE7KhWQH2K2f36pSQHvOWNPfv2fHY9SXaUg+3R/Hag6x0z7BZXpLjpaXIgb0rShVeM9XP1mN7Gl6W0DWNyYSVIuLgucizQb4PH4tR4/P7/nl6puatHEL1Gb1ntI4H8yTmgTq+QJDG8spwQi0e4FduZ8uk/J2p70xF9Akyekecc94Pbhf0A6UzueCdm6Y1CYYILVdRfyec+6MsjfCzvFsjJiA8f6Oyk8JasI7y5vhHYAgRrd47rrMILnzPgHXdA+aFtxlipOqb1+XM+0PAbEnNBia3jrTHdobamHdac/zleeqotMF71pSFz+a4D1fVQn9X/mWSF5fpE+xxeBDKtt5b4J2fOQ/Kq4G2oau2eF88VSrH+Ma5IqkQ4H4aikXF7MJ+YjQPvJ+8Vpwv6AdGL3vBO0NN5QiA0fQAHRKd685m/4s4fZG2DqWjZETsN0PkblRdLt33jEASvyzz4GxEwltVAK77+tO++KvBCUTeKcHUWf8qfTHBpCidtob6kGE9pR1AAirR0uKPG/+KQG29e2LtfDK45dkl88q8J72jPX6k0blssu+Wffkx2Mkgw19VZpWz5wvC4cdlKtLS4Tu6/c1MxxNIH2eXRdFK6uCNQ/TmfbfNgtpew5tkTCD3Sgdfc94Hbhf0A5UUw3aucICjDlF/P74NPhatwc/2u4oe2exw8UxegKu1x3nO1fDhu4S3uGdcU1vAsQKdcvboUtYSa/9do9HAwPcE7SX7/vO3Z+hfXqYtMR1C96/BxyPaayFGdpTLjS4hFZC5ROeWbmV8G7BtdA/5s67Vt2a7zbArygOHMl5RJTZSg+igqnjxubjocv9n8YaMI0LYE3xvwXsmTI1BOXCJg/32/3Pfy3fYEXlggp8zuwr7hmvAfcL2s2hVH97TGTRAq7tRHz++FrzA60lQrmN33Pcl7PZGhEPZ4ObgDndeD8nySj9hF4K3lG9DLhXYM0EpsrO7QztP9RPH6QjLusJmjy+GrhPO+29j8eguS3zNT1wm+ch/zsC14W+6s57AlvpjUQGXCczKrxPvloLNn73fbOUrMD7PvvT24e0+lp8Vc9dATNB85tdAN0P1JQXBdnaGHtUq1aX2WLALF8G2iMe8G6bc7n7RV9OGQ/uF7QDo0vbaTdHAKC7p4jfH58GX+v2TIy2O8reWexwcbxyAh63q9Z449j9qqY0avI7vjWqTOIuNssTCAlQCqja7bxPwP7D8spHrUwW8N5cNmFqkZ7Ow3c7087V8TZDJbQnsNbqAEpmZdddgvycyKV+LHfqhT7eEGQLvC9xb0xtfdmuF57/krOvfYNh9JWYXuHDuc5ogrYGXSdYR+ox9199IET2V52I0077Be3riBr6HvcL2rEBJr6nHRk0gPpuIj5/fK0bIaRL7H7POTdG2Rth53g2Pge0O/NePfPOQHsOcpVRMPNM7nNlx9bkzUzgS3oQNbNdpkaC9tWdUjj9+4DQnnaEK8C67wEzmY8mu91mpQYgWNXgvXXBpuy872oqX8jkr3h/tLehnZvFN9Kbrf7lymzQOU43wF/Tt9ihzFnC2VioigoXq5NwNLRbcW37+Sj3i3E77he0AyNd2mnnCgsw4hTx++PT4GvdHvxouyPtjbAVbYPTf5QJ+FGPnO9cDXfSTf1IEwKAmkztx59qUFbLyrT7nL/yUQH3719vd223fE1jns/p7weG9s06SYHrXdqQvpMfIZCfKygNVI60rKJ5DbB9bvhfWUykS2sPSz/QtQknrwUwb2qZlnWFjPK0AkBkl3mGmg4s4QzWkwtqk4qul+20W/EdE9rnJeeQHfcL2oGRdX5o54aJlDK/BqAjBJHRdkfZG2HneDY+B7QH5H2Fd+uXVedbCzDUSpnMZ2i3drFTNXV/nGf/4aenP5OZknnu3x7QPruAQEYmM7/jvccvonr7TPoRoDKnVr9Y1wtC2+2+K+03H2vwrvW/AftmraQOz0py10bze/lcWrhWK7wssFpeW6AdGF6bUkZri5Vb5FlgNyfhaaf9vzseOyvzhcb5modPrd6NB/cL2q0+eNwodsdj8MICDDhF/L4cQQOfBL/XnM2R9kbYOpYN816x66xI/99R9wTsxXveq5yHQqAySkwgS+20s88CtEs7gwnaN9eU/pk/ziD9XaB9t5ZC+qbyrcia+pqe7JoJ7lYNVOBdrZO8DwshtLbS22M2b5TpPHatxWLuazKdPpMWoZtUWr5a16UBs3xmTqiCbrPNY3w93x7D3UFtaTTehybIXSrftod1iYf/seB+QTvQSyW0c4UFGHCK+P3xafC1bg9+tN1R9s5ih4vjWBMw5ztXw1G6M70J3rtAeyIPC9zy68bu6OOWu99pLxnkY9rQm37VdPIhi08FqUUmXS/e8c71UwWIaEXGTnupb05fC7gLtVV9laIG7rUABb/Wj1h4l+wgPqVd9um/ij9G2UJdaAE7pKQmhMwFlgwL3xV95iT8GIcXtGt9+sxtHLhf0A4Mu3NDuzUl2Anya7BtSBIj7Z7N1oh4OBvm/eIFOyZtdem9SbdYVXINvW0GPR4AQCS6Y5vkyuMxZejTax7nozH5EZcavBfX5p1271suuDpWe2/zLQigswrC2gKponeC92mQIcdmzK7WFmW1hVv+jUvpZwX2k0pRZPpmSdrqNhY9ZnyL0RBoT6uJMgdATeyKS2lTOY+u1qc5AS/APslNP67U7a1MbYtj0100Vy3Trdhm3xcx4H5BO9BlObS3DCzARLOI3x+fBl/r5rBDHxJ85QJhVD6j7XD6jzUBc75zNRyl29ALHZuRSAaBqywD6LGGxFQfP9xuP6Yz7VPjfRz36Uz6DhAKuRWuis+/eaG9Y39tflyJ0LvLKUqclcpUd9+lYyq5ryxcK/IfUvxaXMbOPvCjTI9MGHkr2X8uxweo6n/ymq2NH7m22x92F3xaP6r5K7VDa/GC9vpcL+exP7hf0A7cc88L7ehw1ZPk1wB0gCAy2u4oeyPsHM/G54D2yLyDus2d9xI8LGAxeGZzOenOdH75MYN2WZcM7bnsom8tokz/IaF98o8EbwjcU9yEbhF4hZ1r8xuUGliXW+WFn1BsAnTP7fJjRw3xr2Wk5cyC9ra716MVOGYt2Z0aSa9iy5x4y3b32z1kp53JxRHPstf7sy+4X9AOjLoE7X8CsiNFuELvv3/st9+WrdF2R9obYSvaBqffvG/sioTTz9XYO+pu8BnaeUczR+xglq+GWaFdAPpk/vv3ByhUISflYP3Z1If8YaEd2PmV0q/Cs1YDIMCju9WrupZd8RRz7mumR1RZ8385bjPXsjN+sdQn2z3ePOS58wJjeyPSE9hLEH3ovqC9rT/7gfsF7cCd6ZzQDkwHRm78GoDkCyKj7Y6yN8LO8Wxc0N42Cp6tHH1afWA16QXBr7Z7vKqYdGb6pjPtP/5SJKCI575A+/qxBSYZuH/7w3Gm3ZFXEbi1IxJofjOlO3iv+Uro38C70v/mrruyIKm60Qjvk79VaK+NLSQv+WIRkUfHslVblet5t+zEyg9qelgfjgHtx7pfpP62cvmQ6wPuF7QDo+yI0I4VSS24I2gAkl+/ofMKyBb+LOEGR9g6lo1jTcCRuYnS3UnvDt5LvQyw1GSLH+gRob3Y4UvQng+kXeEosPL1HaA9BcbkeGmjLYbESQfUv4J7nlPm+EtuvGwH/EBTNR2FvvWVj9IDtvjMuyCVcK+Rxle5+Mr/jeRYG7PAWE5rXlU0LTK0BSL6nkQd/vvvtANxZz1zrHtGMVcBJecH9wvagTRP0P6v2+3bV0B2lAhX6JJXPg2+1u1ZGm13lL2z2OHiONYEzPnO1XCU7s564R9pQuBEksn8nS7Px2PKnfYNnc8HWB/HY0qYFz4rO+Xr74077Z3zOvuVgZSaPiSvZZCLr3NTtD0gh/q4k5Maps+K/l/zso3pIfWxf9HNTr78DYAS6rlR+sxfVlsJlFlVq3wJ0GRtmeK5gCEMTbiSjudnF7Qr44+oDx+4X9AOpPp80G7OA2ZW/BpME6LASLtnszUiHs4GdA9Z64DTzddXlP431Au95x2BxAQsys7fdP+pQvu0Mzi9PeaenVou8ikBfer8Q0F7UZFVbgagWhoXazO0vSaXcqz9MJa18573uzESBZ+fPfy0Ix+7l95/n+wVO+BoSjbu5s9IEGsiNWRiLkiLhWoTENipibY0uLUxq+r2ykciH+iXBGX/8TcCsgUXw1qd93/8va3lBe1ABx0N2tu6Og/Up8HXGki4IjLa7ih7Z7HDxUHdR+aK4PRzdfaOuqN8zvSab5vJAYnL+CyNQvvU9wU/Peoh83X+a5GT+XhMyzekUbntDe4VP2GAN74RWbtYo14E4PO+qcvf1W8LHu32r5mfPkeJfFmEJCC24LpWBqhJDSJrPpjlVxJ9pQE80Wo60ueP/17Q3o+k2nbcL2gH7jTngnZzPjAz4tdgmhAFRtsdZW+EnePZgO8lay1ExhClO0pv5CJG8Bnaea/triqEkqA9/fCPNO7nd7QvPm3/k0H67sLjWhO0R/RZhdAg+LN2xJUZddPMMiQcYRHVSrvvFrhLimR/7uZiozg2s6F4K0bDj7V5Xk9lPQA2SpG1+7OVZ/5FAHRDzGuo9EmoWXqCrUF7BuyTr1122rlxRocTusmTOoyLQaw+esf9gnZjuCydcpgz7f4i8WnwtYbmpk8F7JHw1W9HwO43ri6ONQFzvtu5GJH3KJ8NvfDO+7wnmiVCAZ/p/vPTr085qTDmz4pX783q8iMzxU10uf4W0J6XC8CD+3PXQEVS8A7om7tXclaD9wSc0uJl22atlPnjekI+1gdRNVEooUvAi+z0w0+rE9Y4Y/RbusQVK9AZ0cC+jLXczAXtXb/55XbcL2j/VNDeMm1sE+TXAMxCgshou6PsjbBzPBsXtLeNgoxuvQqU9mCtQDvvCbo0nR+P4zE5tK/snbXZvKO9/PzRYGth+df0n/lMO3M8Boyfzn6uFwC9qgi9Rbv1tgrDElRrwWrn3SXYzt8ao8H4h7w3qu68328PaJ/e0678EfMI5H9VF1UPmsMt9uQxgZWoZW+6Pj0InmubvsH6XfiVYsxiyxx2rPvFOkmxAVflcXC/oP2NoN0aYHYNHUGD7WUp4feasznS3ghbx7JxrAk4MjdRug+kt9x5T65tQKvi7w8/ydCew7v4w0pJ53NXdG/lfrv9eQRol+IHwDFnc1GcAWxhBhRhuFxcoDZqD6wW8Vd2/R+SSm527RK0l/JI+zwftW+E0pIQ6K/qTabMY68xnBaorD5L/jm+9vN1D2i37G+Teax7xn6bgOOLyhoTOipzQXsl31lhHeJ4DFfoUmA+Db7W7YU92u4oe2exw8VxrAmY852r4SjdB9Rb/sJqhb02Ofzy0+32c3Y8Rpy0prfHlLtbRQ6yotpc+fM3Yqf9FXkFYXAn1grXGrwju/eIr7Xd98z2TpW0y47A93S+XZIzfEVCmd1dFoWH27FvhXVkl7gG7EtO3Dvt3Fg71j0jDtrnadME9wvaPw20c8OkP/JzwJNL+z3HbZ/N1oh4OBvcBMzpxvsZuXnx2p4tovw+uN4dvBvHImhoTzdMLQ/FzvvLoR3tL5AiV7FSb4JuUE9Z2nAzVDDBe7m40BYNEx4T8D2dO5//TNBeOR6zyFRH8sbspDf7QJusdumu5QWtgdLL3BdBx/yRR7eWlfx3ERQOcEE75zN3v/DkhJnvuRgYzTa4X9Cu5LPolJfvtPuLxKfB15ot2nj40Sesdl+ZlqPyGW2H03+sCZjznend9pupZSXK58568x9pSiFJxyIgaJ9APPdP+3u2EEvs/jbQ/gBQ7E+5EGom8KLhklf5heiFLOgruEP9fGNMLQ9JWdb/H+VOO+iXmOsUfxYqOiyqZouFANbJBZAvq4SNP6hz2bio2n7qe/6YmdBgGovfPGfaOb+Pdc8YtyjQd9wvaP8U0M4NE2V1DU82PQX9nnPejLI3ws7xbBxrAo7KT5TeyBtGhM8TXBa7oCXcfPkZOB4z+Wa90UPwf/pBJhjag+LnJh8Q3Atfq8CIQqykE2lL7DDvX7C+ZmdjfVUJ2N9Be+sCJm2da/c+wJe86WZ9kRZZNRtGoSTuD4P2h+Jpfn68fVUbD8s4bIZ2bpwd636BLn7oQS+tjObPZHC/oF1N2ObCS3fauULvj9x++21lPNruSHsjbEXb4PQfawLmfOfqN0r3G+vV4J2B9gdWVL7E2Ofn/ue/brdvyNtjInLbQ2cJi4JOmCcJwJ6JYf0/oPwl3VL88jved5LStzKp/1dvkq4v8tsnRa/hZC2tS88c7bWmCcjTmhxie7S2ELknsD+CrrVZvvX69kfj22MQf54dd6x7hpUbYJjAIs887cH9gvbTQzs3TPojP1ynO0G/55ztUfZG2DmejWNNwJH5idL9TnoVX0t4n94e8/Nfi2FatN0djVmuV9OxgAgE7UfPa057Wl4X1jKZEoXrokug3W9ggbED7hwRBd928C5A9FxTT/LNf6dLP+lT8xXI92MfFLi9vKK2Ui4s29vrz2F2DGg/1v0idbWVU6AkTJG9jS24X9B+QXtDEZlN3AIjBkfu5Eh7I2wdy8axJuDI3ETpPpneBO87aFeAYy2gPA+1XffHrth8PObbn8ZsFJHbnjpRiMzCNHnSq1MzsDkXYt8FPsq3xih61Xim3fby4vbfNri/uK/sLGGMskoh8SyL2p2o1jb7fD7T3rLTjvh17bJr33Q8wf2C9vouT7r6suMxXKH33yf322+ak5qfim+zFvfgYOnPqHxG2+H0X9DeWpfROzxcP+JRYHrvP/5y+xB32oszAiK0P8B8/qOYexyPqUE75iced2R/pQUN4U0V3qFzGHtj6tEVwq9FdM2+Ad5rJ+92/CVoT36k+B6Nkol0bhv3tiHvuPIGSQCsVa15LOWPJmkDKVtIz6YvaG/oNLBJfT56gPsF7aeGdv8tya8BrFasH9qUGa1GxjjC1vFscNAe7X+U/ktvlaCFUXj/8ecF2mt0mcB8+m8tx/ud99dA+4g6MLfSDdjOFxcNumYKzuE4N1fGr+vfZar6zvVMela56F3bWHHI1z/W10dK8dT60rKX2m4XD/5bmOQTWnNbue28XOoQdDZDO+rfMoNw4o5XXzK9QTvFKC+XsWrbj/v//X/cb78YP27RYPo9m1Q65SU77f4i8WnwtW6vgdF2R9k7ix0uDg7Ytd2e9mratuR856xG6T6n3jmqGdr/DVhUJ3C36iOXu93OA+0eeMzSu+PMXK8FoUo37eBd8nWvW49Ielh1khb8m4E9+zyH+WpVycd4yjVDPnfJx2xyPbl/1phFcl3GPP3b0qsF/Wj3fDuMJHdBu14yrXln7iC4jY/7f/wPXJrx4e1kzwXt/k71a2grgdF2R9kbYed4Nj4HtEfmPUp3hF5cZxO0i8yuwMa08V49HoP7is9jETqthQoCgEIEux3mJNOo74GFwhnz0vZDfz1TaRc9tU3ShW8ltIshcPHkcK7+thKnEi8fU7Klvp5t6nNxrruws/knezyG8/lY94uy/swOcghwebrA3VrBDt9p5zoQWTfz1eT34fg2R8Y4wtaxbBxrAo7MTZTu8+qtQ3sOadPfK3mYL0nX77f7H7Uz7RG5fZXOliMYtR9paqXSJX7gB5rkX0CVFhcpNoHIZzetX0TVXl+pLAR2Lshyeoh5DbTmsccdXTqzrt2RUWBfxhr1ICo3Jo51z7CXlzzjIH2Aaf3k4G4U1ptBOzdMekwQWJHZUn7PbRu5xCh7Z7HDxXGsCZjz/Rh1FOVzhF5c5yppHo+pwMRuGJf2a9CO+/r6OmB8ReE9wfUS3fTPHV+iuirzqXJcZRuRBbaZcxvRtBtfHJGpdZhlam5rHXPRlTxAvuyvfNHRklN0tzfl6WGfm3sRaM9kgqCd83kUUDPjj5stntLtNj4xuF/Qvi239iJqLVvrC9N2vf1Wtu0+jMjnsWwcawKOzE2U7vPq3UD7L8uZdjVcBty3gHP/47+Vt8dE5DZCZwuYlJBobMqUx7tVJrWIV4lfAPe9ZE13ks5k1r9qb48hfFW3zMsdfimPpZ1nZPaXDXDis5VV6YMH1MvFgNJ/m4+Xf8DQzo2JY90zWsZeKyFweSqtfEJwBxI2dKcd8MeoDZ8GX+vWsj0vtI/KZ7QdTv+xJmDOd66Go3SfU+8mqmmn/Zd/f0LJLmQtB0Zu5svTTrsE7e+U1x6+6mCp1nkTuBu+Ljrrx2IQX9MO+7IrbtOx8DVC4WtNR5rIqnBf3w1PP/gEubru9kuLVf1bAH6+TVAq9JsE6nmxXNDO3SKq0j3G+O32ycAdSNobQTsQTSjyt1ez33Pc9tlsjYiHs8HdRDjdeD/Xb6a8Hmmny69lryEqH6/Xq0N7YncJVrQcV+K5j4T21+e1XoXlri5Ys9UNa+sYiWDjYzm2oRyb2bYAdronCm565WOlv6z0QEX3AAAgAElEQVQnUXHy1tdESx4YVff7ffM7UvU3wSD9O+VAycOy6FW1QNDOjQnufpEWHEicHhkuhjZL/Wx8EnAHEzYM2kF/KtXh0+Br3Va0owZg7t3IOEfYirbB6T/WBMz5ztVwlO5z6t1FtdlpLzKv/qASvnC6/z7ttP9RKua6GJKO6K8InVAweyER3vPz2bivq6QJ7pOkccwlvT1G9a8xXquZSNspMutojqX8sQ55zqHb3G6v2brqGwJFv+26UenXTwHteE239MKjTX8bnwDcwaS9CbSD0YQh/5GKt+6LP1NYrCPsHM/G54D2yLxH6Y7Qi+ukoH3za6eIjb3MHtoRPdjIfkpF6Iy5oW99boDLXZPpg++LWlufmCkV3gEIbnrlI7AYQEpg9xOrWh3YeRl3ryp91KDdqGkT2rkxcaz7ReoNLgakZOoLqDYNUqsTgzvRKUOgnfBH6d8jaOBLz+81Z3OkvRG2jmXjWBNwZG6idJ9XLw7tGkwgMPts+77QHl0DXpDMN8EBuF4m6GpUO3gvpSWftQdR7U365z0DyYUF+sj76ZNFxF5vcJQyv3wmdkqtp6b3tP95u93TYk26+3L1e6x7BjLHcMShS3N5YqyeFNyJhH0KaCfywVSPKTva7ih7Z7HDxXGsCZjz3SzVjUCU7nPqFaNSj8coOVg/RnK0nGn/mo7HIG24CnhIR+gdpZMBSCE3c3MJrveQC0UEuVMIfQDvaZ/chHRLghawlzmQftEVqSvJQShrFeUGfNPAvsTaEdr5+0XUmMvT6M070t/xNk4I7kTS3gDaiWjC9umRUt3K+L3mbI60N8LWsWzwE3Ck/++oO8rn1+vtAu27Dcg6lMxvjwmF9tfnFZ//ar7CRFuYq/1AUxJ96IYztVkIaH5ln2/Ol9fiWDxYRayYpQdhWbD2ADycsaxP0DbTzwW3Qqq10476sCx5OXGmkvChMWQzpnSHDrwpnhOBO5mwcGgn/RG6z6fB17qpmripvN3EiQckcStszB9XFxe0N6Z5bcblG7f2Wr2q9XWnPcHQJAn6CkDH/Y//WqAd1IkndJGM0PtqnRbMVmBPaQr/+ml+T4DgunI8ZtaVO1TkdeMrE/N2MYLVayu8SwU5xSEtKIji3U3UTM1d0E5kWhFl8u21dj/L6yDJpB0c2slouiN/e1n5Pcdtn83WiHg4Gxy0c7rxfk6SUfqj9E5+R+iO0Mn5qkP7L7ePX5f3tG+Kh/C5Au+x0E74SBVvhN5WncDOdRmb0ORhPQdNQm8V3tPFfOddSnYFcjf6ExCXC5MWqNf8QI/qpD7zAnplkdU039Sgnasz7n7BzTnUkNsIczG02RlhY5uvN99xb0hYKLQ3+FNUik+Dr3Vb0Y4agLUJq91zu+WInEbb4PQfawLmfLf7c0QdRfn8Wr1V6z9m0D6nuPzaHpwnFHB/P2h/bV/p44A9FpJpml5fuFOsQbAR/9wsb6v8fac+B+DKaC933/NJbWc7r00v1Jc78R3qQFQxfejVfWZo9+YGuZOMsJH82Np6Y3BvSNqBob0hmq7Ij5SpLOP3nLM9yt4IO8ez8TmgPTLvUboj9OI669D+8+3j179lwziTVkDcHPNru+lB1Ol4zO9mE14Aj5/THaG3p878GBMW2Wp9x7W+hcAT3mvAvFz7KHNgQLYW5m7XP9frBfd8QZLpRdRO4uKGvNT3nnpY2qoPouK6j3W/kCEXq3BWCs8Rq3krL9t5Q3BvTFgYtDf6I9/mGvvY7wNveLTNkfZG2DqWjWNNwJG5idJ9Xr11aM932gXJFnDPuf+Pf74RtL9DDSSCnF73h9CksK9bhXcwBxt4xvyY71HQwgG4m0EmISHBWMpB0T5BeQ7n0MmZPKdgftUU1KCd032se0b+rQnQ/y4RLk/tpnQ7bwbujQk7LbQ35qO9kpaWo+2OsncWO1wcx5qAOd+5Uo7SfU69ZlTS8ZiyQ1rAfbkH3//4z+ztMVxP16XNyBqMvYvORIqTv/mW9B5S1YhEns31genTflxp07yAYJWlSched++tdtb1asEXF3NdKV/5f9PKRMt8a40V7cSddlw3f78YAdW4/2B1VhZk7RrwlvV43gjcGzvmoNDeGE3XfXq8iJKk32vO5kh7I2wdzwY3CUf6/466o3x+vV7Tgx20K3PETpGp+XE8PgTaAdvcBBW4kRHhq6RTBtOq9V2TXBoEXRPaSw+y7WoY3qXFRKZ388rJWseDMbnPnGs+tNZC0W6a7L9/LX5cidPN3S/OAuwj4uAY6w3AnSusTemHQLvDny7TvN9+070pbFLqPVmx0Y3KZ7QdTv+xJmDOd66Ho3SfUy8UlQjtCZJKWGDH9/12/306HpN+XInrbV0aiow09i46LfB4gul9nucBUJ1F8IXAJrErMGt2rLwu7QA3H3bzbxYyT/KjK0jMG115RJa/ZFltxFndivwF7Y2dwOa/0czcDLd1cHDHA9ml64DQ7oimC/K3l5Tfc872KHsj7BzPBgft0f5H6Y/Sy02wr697PA+QpLrTbuwY7ZRL1iKgHYqK6ybyJosrj/AV1/mURIh4+ZGmHecDbas/roT6W4F30SdD76zO8l26jvqLV8FDktFryO6gndF9u3H3C9Z3Ni/GPNOqTmzH5andNG/noODOB7JJWndod/pDDsN9AfjttxXVaLsj7Y2wFW2D03+sCZjznavfKN3n1AtH1Qrt6r02t/wu0A5niytZCtRQ1bivOLQLOkXeVSB4dzwFejpTCbiA99w1i8FzjRtZpiHaD6wc2m+A3OmgHYiZTfdOfoQN3wLkgODuTNrBoN0ZjRv522vY7zlne5S9EXaOZ+NzQHtk3qN0R+jFdcKSJrRXbkSTEZHPkvXe0A5HxU1RL4Zr3Fk8/r1kDVwrehF4V8+Ul3pZeM6LK2vb9PpI1jbeK5hkre/wfn1s2pdn2vH2x7pf+CAXy/tIG/5vJQ4E7nhRqR3RFdr9/hxBA1e0/oI6tj1/j9jxHcvGsSbgyNxE6T6vXjiyH8v3tNujQPyqf2dw+uCCdiSbuAzcq5VlyObw92Ia0FuD9xna0wquBkksOEsrwsXOThWgexVBj8VIceG99ZS0gB38ZiKf8NcHUYG+y1w+1j1jJJNweWrp5Ucbv52DgLs/kNupoL1DPpqqarTdUfbOYoeL41gTMOc7V75Rus+pl4qqF7Rv7ldPD+6/93zlIxUZWGLvohMHgoiI1mRKsCwCtOUFANliDya9+c57LgjonUVKuZq/yo4/WGE6yKWFjpWrtLbK5Bqgnb9f4DUHp2InCMbebqALSOPm+8RzAHDvEMiBoN0fjV8DXkS1nQ9eC95iZIwjbB3PBjcJR/r/jrqjfH69XsqDJmg3buZZYfaDdioqfJrqsDO2NxbhK64Tlyz7EQDeHe8KbUA1j7wxwlJkS/uWnffZPLjLLVZUbrT0LV3LP+d6ZmOynOxnaP9G1PkRH0B15IOLnJJuF+4bzwvBvVMg3aDd749Pg6/1UQrK9mNUnGexw8XBAXv0jgnnu107uUSU7nPq5aK6327zmfa/cV2ypQml7fSS9tvyysffHfojNx24bOFBROjFdGJSWk4ZgE68XTlqAr8/Pc9szQctOie4p4Vbk79IVXC9UoX1dPGCdiTxb7nLvhkN9//4H47qAXO0E+tk8iDQ7o/Gr6GtJ0bbHWVvhJ3j2eCgPdr/KP1ReiMXMRE+4zpxySUHbmjXctkT2rmo8PkxQu9rdXLWJen8HHe6XgFp5KhJEwyj584V4K+eXy8rJMtDk69axXG9sUenSvvvfxY/rlSveu5+ETk/jtiQGW0jNl+Dd9ydRZvnvgu0+/3xafC1xm9GlUmpXQnRcmScI2xF2+D0H2sC5nwniihwlyTK59fq5awv0l2gXdm5vacHUb077VxkWI1F6Iy6eeO+Rkg+8qmA+w5yDQ8oKG45ulL4aR6bEfydfJwmWcpXqerw3lhb53ZrE/3bQ3tDbrCBnUmNsKHMfbSv9QYDwb1j0g4A7f5o/BraamG03VH2Rtg5no3PAe2ReY/SHaEX14lLZnB5eGjnosLnxwi9r9XJWWekmZ12UC8NxJNe9PiOdc6+dha9qCAXwIO5mIfjskhAJ/fvf8CH1FGVz8gJv/EBV0iexcYYaJ+XzWOOynTsGDe0+305ggZ+jPi95myOtDfC1rFsHGsCjsxNlO7z6sUjyyQjoX1ikd+mt8d4dtrxqF4/T0X4iuvEJVu+EciPzWSZ3hyP4Tx4bOAjIF7qRdoI3w6Iu+6kzzDEE3pZYJ/O4M877ZgNUGzgDjXmNzeeO33T0WR0VDzh4N45kLeH9s75gItrtN1R9s5ih4vjWBMw5ztcsrNglO5z6uWiioB22YNjQjuXLaxuI3Ti44CzzknL8WcEvIK3Q28V3jW9DfC+aTLpRXUoVZAgPl22ciEdu6En9Qva7THpqEVb+Qu+NXiaDNxx75y0F0O7Pxq/BrqWwsBH82RkjCNsHc8GN79H+x+l/9L7GGF4HnDJQm+3nfYIaOeiwufHCL2v1clZ56Tref1wvjYx37V3PnwKFUC+2Eh5UL5FgPQZO7wJ4rmJu2J58RncaefN9qyNz7D7HZ2vMocPewHgHhCIC9r9/vg0+Fo3zx3ETb/dRt5yVJxnscPFcawJmPOdq68o3efUy0VVSK/Q3vLQX+pV3QPfTjsXGVZjETq5BRbmJ66Ti4iTNoF9Jgg8Ilhyhd3vjybVnXjWgaLWN81ZXaPB9IJ2u4Z61rhlbaSt7ZzQGdwDAnkhtPuj8WuwSke+PtruKHsj7BzPBgft0f5H6Y/Si0MQP9YifMZ14pJCDjbQbkM4O8+0QzsXFd5nEXpfq5OzzknbeU077QtcPwjbboZIzEdPMr3m+XenXeo1kVoAvfOrbIoBO+3c/SJyflRiQGqgSSayD0bHUrfXCdyDEtYM7X5/fBp8rZtqdm402u5IeyNsRdvg9B9rAuZ852o4Svc59XJRCdIitLPzh+7FsaCdyxZetxF6cZ0RknjsE7RPHuS714l+k2etMK1E5t51r/jl3nXHewPPsbCYfktoj8xN64YD3wvPFiPisRcJHcA9KJAXQbs/Gr+GtrIabXeUvRF2jmfjc0B7ZN6jdEfoxXXikgqIq9COgnvdgzZo56LC58cIvRE60dyzWzMRviZor/VCZ2ifN/MtnbXreR5qb8VJMVm2bLDCa7QmmfyeHkT9Wn17zLHuFyOBOqLGtT45ji0nuAcF0gTtfl+OoIEf8H6vOZsj7Y2wdSwbx5qAI3MTpfu8ernIBOkL2rmpbifN9QBmDNeJS+ILAcxHQQpiW0gI+6a4GdylrCl+ia+JfAXEZT5PN4T7u0E7V6nNNTjshMGoePAFTyO4BwbyltAemI9qVY+2O8reWexwcVzQ3j6FP1py+catvVYvZ12R/vHX28evf6uEbFmpXz/OTrsVB97rT8kInXi9ctY56ZZsbNpU2Rx5WwvhLwXviN5WgEd0t2b2gnY7c5H5L62PtIXPCQ3gHhjIC6DdH41fg12oksRIu2ezNSIezgYH7Zxuvr6i9F962QUGl7EatP975eHBqV3tzTK9oZ2LCq/fCL2v1clZ56SxvCadAuRKJWOCPL6ruPPPBPepBfqGpMqiQtx5j8htHmGuf9pln/Yh9J127n6BAyFWE6/kkeh+cNRne/LoTScC3IMTRkO73x+fBl/r9j4ebXeUvbPY4eI41gTM+c7VcJTuc+rloqpImzvt6E1dtsHvtHORYTUWoRPNC+Yhu3vPRcRJ4x5XoD0xspQmFd7LB1hxT2ZJE9zTAhTRKz1Yu7QT/Wd0I/YrgGgcjznWPSNqnJQ5jKrxVy5C2hcJILgHJ20wtPuj8WtghjY76bfpfmUBj8jn8WwcawKOyk+U3sgbRoTPuE5c0sjBDO21nXY0hz2gnYsKn8Mi9L5WJ2edk+bzip5TzzQjJ2RWcQKIoV9WbfD3sSrYpmYTQ5njVhsSqKX4FxvTf5Sd9mPdL9qhE6/BkTbQuZD3Xm/RPnYNcG9XDIdHQbvfH58GX2s4JzvB0XZH2hthK9oGp/9YEzDnO1fDUbrPqxePzJA0d9pzYKrpOiq045l6fc3ivkZItsfvBNRN8xLQCWDPA9jBe0+oLuO9G6+mz79BqK1Wko/lNw6Z7+mv09tjhGd1jnXPGAW5+GjgavyVm5R9FiMVcB+QtIHQ7o/Gr6GtuEbbHWVvhJ3j2TjWBByZnyjd76QX9xWXBG6aXXbadY/w4zFcVPj8GKE3QifQV0vQnHVOui2vTmjfbGLX/CXtzD/ONAF17Sw7qXOToAKuqW8PykxrPgr5mD4SoP1Y94s+0InVY1SNvxra+8SlgHsf5dUOgqHd78sRNGDFmkv5veZsjrQ3wtaxbBxrAo7MTZTu8+rlIjOku0B7As79meQL2tFZFe9VXBJfCKBePuRKDzzgm1mefqRpUg0/sAp6XYX2zYoBVJjEgAdbXanJ8lym/C2gnatUMvmZ+Nns9F/wFOA+KGFvA+2D8rGr8NF2R9k7ix0ujgva26dwGSy8+vpPpFuPsPrApAhfIWgn9BVA91po57KFVUiEThyuOeucNBZ/ILSn2hFOn2xBniXhtNtei5DVmY+JhrZ5k7ybyqNCaRFzQbvSeVE1LpkbaQufE9Bxm4H7oEAGQbs/Gr8GtBNabvptustWI2McYet4Njhoj/Y/Sv+ll11gcBkDpCFoB/RspoinPAbtrH50FovQ+1qdnHVOGsuqpFOibElbDW4LvfsvbYQdeBSWM93mG2ZQnSVUMe2K3JQb9rOq5VsHrVOKnXbuftEfCGU3I+rvVSwyIpY8thh7H/f/+L9iNEsVAEG73x2fBl9rbNI83wpQj3tUPqPtcPqPNQFzvnM1HKX7nHq5qEDp6i+iem8i99vroB2MnytY+p3JmHrMV0wq3wHGrHNSNWi3PCSgHWZ+C5YFn7qBO7KA4bL7lK7l8n67ff+2qcVj3TPOtCiIHk9afVhjqbWubrdx4D4A2v1p8mto64rRdkfZG2HneDaONQFH5SdKb+QNI8JnXCcuSeTA3GnnrD7mr2cbG9pb9COzZITe1+rkrHPSSEbLvn22Ac52bwyUb1Mh6lVk9MbFgBvetRxbCwkr21bfbaH9WPeLkZBr5cnKM3p9lJ1xuRsD7ia0+xPr0+BrjZbPXm603ZH2RtiKtsHpP9YEzPnO1XCU7vPqxSPDJW8mtKNAJdt8DbQT8VNFG6EX1xkhSYUvvGKQa59Ls6C/tFV5WLoAZKwZ3AHd60H8SZYBeUv30aHd8r+9arYtz2ZnHLRPluLBPRja/d3v19BWyqPtjrI3ws7xbFzQ3jYKnq2i+jRCL64Tl0Qhe8kYBO2ozr2XdWjnosIrI0JvhE40r9IbuWvZeK2veD/lkEJAbeL9XRP0bH3hYRO4e3JsxWrpfkL7se4XI6HTyhFfhXqLs9qKBvc//nW7fZ1+UED640/qETTwZeb3mrM50t4IW8eycawJODI3UbrPq5eLjJCu/rjSpEd6IlCbNS5oj5pPiR596bl7Lv580WKBbEXz2rQR2udtR8u+9roXPuLHmMrHVq4j7+n0d8H2cqb9WPcMfBHakrVtG25EtNsbZWfkgmebjbgd90ND++iOfVUHj4rzLHa4OI41AXO+c5NilO5z6uWi4qT7HY+RwWP8TjsZP1S4ETpxwOGsc9JQ+LNQtF4LmhVPd/zLLDILnSa4z3TfKReangTz+X+F2D81tEfVYsyGMD7GIsdZzYt70FGZQGj3l4BfA9exr4D2kTGOsHU8Gxy0R/sfpf/S+xi9eB5wSU7v7AZ0PAbxQJbRoR3R2TIrRuh9rU7OOieNZzhCr6SzAd4bmqhxQ+COZ42TJHL8/dvtzt0wqDmH81tesLfrsFoSebJUVa+PsvMKntv3Wf8ddxXa/Yn1afC1bq+p0XZH2TuLHS4Oev4loI+vMc53Tn+U7nPq5aLipDFoR3TqMmOhHfGVq1Z2gYVrx3zFpKJv+pwXvhzUjrgYdD4E3lMuehpr6L9DQntUnYxeFDT0B170FckR+ZPMP+32BfcgaPenya+hrb9H2x1lb4Sd49n4HNAemfco3RF6cZ24ZMMu+zLxfPz1f7ndvvyoTEOoB7KcDO2oTnZmjND7Wp2cdU4az+6r9OZHSGrnwLNIZpae/O0A1eKuexS0czm+f9u+p93uS06/ra8Of23tkVYj4mifS5EIZJlRcZXW93b7gbsI7f5AfRp8rd+ng0fGOcJWtA1O/+cA9siJkMs3Pu5erxf3AJcU4//4cvv4y/98u335IaE88HW6bXMctNu+4P0evbOH+xohyeUB92CMXmu3PffXCe47aC9z4dS/SRiX5+NBO+c/VyvR4/FVC5BXxGVD+yTRB9wDoN1fZn4NbcU72u4oeyPsHM/GBe1to+DZKqpPI/TiOnHJjguiFd6nnfeaB9M1+6G/94Z2rgfwKsb0YlLJKifd21dcH+Ov9rBmDZbvxSa7E6w34K7l2GljTgnef/P9ovhF1Hr+cd18PzL92a790XJEHCPjeYUtfKHgB/cdtPs78Aga+DL2e83ZHGlvhK1j2biAnavGvXRUf75eL+cBJ21mfbfz3rYbtYf2zn6ubkXofa1OzjonbfZ/KCShvpbQjsDxonsnirStZAVqDgkJRtB8LOh6OGjn/Mdqr22+adeNw2wfG6+0hy8WfOB+OGgfVahliYy2O8reWexwcVzQ7p0CuXzj1l6rl7POSWM5SPCTjs3kZ95xe2OgHfcHiz1yVw/zFZPCb7543CNgAo2O3WnP9Ir83ArVS06g5pBQ0R1oPj4ztHM5aqv36PGkeTUyNj7GdnDvDO3+NPk1tBXWSLtnszUiHs4GB+2cbr6+ovRfepfbLdwlXMY4acyJQufm2Aw+8W+hPcLPKMCO8BXXiUtGxX9kvRoYlzVbVnoLUKc8LG0hFZDQ4hzZ00kcPh7D6cfmhldsJI6II7LmjwTsfJxt4L6Bdn8H+jT4WrcNDD7R7Xbwm7Lfxsi4ovuN088Be3SeON+5fo/SfU69XFScNN5vil7o2MzTSjy0D44fT6AgifmKSUXP0YwX9rMNWUUQGazttE/+5XCs1atkjoHqct79IF5Mg9hh8ny7rfeMw0A75z/R+ZnoCBvR4+m9oX3yngf3jtDuLwG/huMXbzQk5hkYkc/j2fgc0B6Z9yjdEXpxnbhk1BgFPADh/QntgM6mSTFC72t1ctY5aTzFjN78dYxWO+s67uFDElgwuI/KlD4zu+4pHskJLheb+wUE7Zx+NvMP+bPYGBXLaO6RerW9zzhwX6G93WC/tZTfB36AjLY50t4IW8ey8TmAPXIijOrP1+vFPcAlufmG0GvAeyy0E35SCYjQi+vEJaPGF+cBBM5hgKftxmcd7nooVcpFphDZTF9dKYW5PB8P2jn/qSG4ET6bnX4k2pbT9nxOLXFw7wTt7e6+b6JHdyxnz98jtr3j2big3e61ukRUn0boxXXikkcBtqWXFHh/P2jnegCvYkwvJhV9L+K8eEA70gaRQTKa7+wT42DDzCht13xmjspMfjb6PWW3dMPcae+V61p/nMVG9Hjqu9uNjBBdxtdnqTUG7p8a2n2J5jt5pL0Rto5l4wJ2viK3LaL68/V6OQ84aSzrTp0FvD+g/TfMNC3l9FW091qdnHVOGktvi04E2lv0Wh4DR2NKFRS4Iz63gPvk1HfqV1u394z77fZ9al/zD/Hdyu+rgZ1YkHlCmdtG56t0cLQ9/8Kk9NgG9xna/3xx17xfotsSNirOs9jh4rigva0qn624fOPWXquXs85JYznoqHOG9//pdp/n7Qho7+jrmpwInTgQcNY5aaz/cV/r+iSIj/IXj2zz3OrarLbjjvjMQnsJT9iO//F22pHcEH0zbAGt+TQintz2aHv+sS15XAf3DtDuT5NfQ1sZj7R7Nlsj4uFscNDO6ebrK0r/pffRF3gecElOL14TnAeY3gid54yfyxQnjfVVa16tnfbp+rQz3PuPZVewB59vZ/LLgnvSnfuvw7t4v6gej2F8b+2Ts9horfnWvL3CXrlQbPNd63Ed3P/479vt69c2a0srX5n5Wrc7PtruSHsjbEXb4PRzwB49wDnfuRqO0n1OvVxUnDTebxF630Vn1FjD44+QxPveE78Fz3hkvL/YTvVGL3REhvU5e6PM1LTqVq5by91Twf6eYR2PYX3nss5sRLCan/LRMeSejbTlGWft2Xy0bI/TaimDuxPaLaN2OvwabBuSxGi7o+yNsHM8G58D2iPzHqU7Qi+uE5f0Tb76HMR5gM1lETrPGT+XKU4a66uIvCYg1fy1YN/y3KRjXcHKxBpdN+T44+PBRTCwS+5td99FYJ+aqWfaG/y20ry7fhYbETVvJXNE7vpzJOJ1Ae732233i6hWcrbXEaN1jX4NnMdnL6gR+TyWjc8B7JF1G9Wfr9eLe4BLcvNNhN530RlVs3j8uOTrfd3XVU6peSQWtFsVWtuFnuwkW+Ruu7nTzvXGM4rlmEwV3BHdz7gf9wzhrTMvg3bEf6tfketns5NiHhVXmeN2u0zLDNx90M4YlcvJrwEp073MaLuj7J3FDhfHBe1to+DZiss3bu21ejnrnDSWgwidR4RLLRuvjZ+zzklj/e/tK+thzlafrZ34coGAR7vfDdcWHoTOWTTTI6YFz8XzfpEUZW1PDe14jtjeeS1jjYwrj9Rnl229gPtnhHY2Vd7yHWlvhK1j2biA/aj1GVUnuF5c0gtXI6GViwqvjgi9r9XJWeeksbx6dVpHTLz6sSiapHa77j181R5M5XRX7xkitHP6+XxF6x+9Gz0qntFxvWaXPbf6cf+P//PuOR7j6xpfa35gvKqDR8V5FjtcHBe0t4+ER0su37i11+rlrHPSWA4idEb1V4SvETrx+DnrnDTW/7ivuj4L2qWWSCzaTntqSx6LKd0Qj8kgfiGZzf3q34gAACAASURBVI7KTOKzLU7354R2LkdIT+gyI23x/e+LrQ9HejL0cf9//vd7y9tjPEZjYcHqEr/nloXn9bPZGhEPZ4ODdk433s99BvJrJsGInETo5CZnzgNOGquLd9HJ5RWL/fU6uexz0uNyoMGz9gNCPeJwgruT9/HcZkdbCJvm/WK3094jp1ZUZ7ERNe5r+RuRu9bFcdwd/eP+j/+NjpxusPHf19oaAnGpYi2PjHOErWgbnH5zAt51F6ef6+131B3l82v1ctY5abwmIvS+i86omzcef4Qk3ve94pdoVIsMiTh/8LKUL2F9+jdBwyk5DU24vE7S+fn2O/BmmWwbzUrTcGi3HOKzs28xwkb0xpWWh5Gx5T60221vubVPgbvfqF9DWymPtjvK3gg7x7PxOaA9Mu9RuiP04jpxyV5wVc5GnAfYXBah85zxc5nipLG+6pHXlqMxOTiVgL595eH2DTFTDhohvUyIeJa9k+6NrSwecKEA3S820B5VG33gb1wtopZG5Gt07qTYfXH6Wm/nFhjcexpFy8Ev5/ea82GkvRG2jmUDmoA3HRbp/zvqjvL59XpxD3DJ14/9CF8jdPYAVt+NkouKk8bqINdZwjK6wGN22bV8STvrNZjHolOlctWzUPog5QOka9iNTC+gGrpnDIX2iNrzjR049aLgqHjyxanP47bW7XG2t9QXKxC4+wz7WrclOepGUvNmVJxnscPFAU3AF7RXCpTLNz7uXquXs85JYzmI0Bk1f0X4GqETj5+zzkk/+798kLOkVenMeQ1cp2s5dZb/joaU1jwUI2K3Ppk+0M7fY6OpLrXkyVoXTd8joCGeDtrRwHv1Rw89qI6RsfUZg36PZQ1VcI8yinZTm5zfa87uSHsjbB3LBjwBr50W6f876o7y+fV6OQ84aWwOeBedOAhjcfe5qcm28Jzikkj8JZzn3u22lem3mLw2r/nCgMsa7PeHtviANQCbDvn5dv2UD3zPWKE9KCdD7kl52qLjiBz3tToZFVfpQ7vd9pZYf6rg7jPsa90+1EfbHWXvLHa4OOAJeMgEyfnO1XCU7nPq5aLipLF+i9CJwCXm3VYqwtcInXj8nHVUuoTzBKLlf6PhBfWXrYWOenc73nmOJr+AMyyw+7nfBbgLOuB7xqmgvWPfmv0y0hY+J5huUwK+GH2tsfllB+5+o34NVI6HQFu/lRgf24h8Hs8GPAHPCY32P0p/lN6onET5i+vFJd8pB1xU+BwSofe1OjnrmrS0g45nNW6+4aLDPe6kd5e2pDcBdQJ43DNdsvS5Du3U/WKG9ul/0X865b3q5ggbUXNpLbBRcfVlO7/XmIYNuGNNtGT7WrcPodF2R9obYSvaBqefmoDDoZ3znavhKN3n1MtFxUnj/Rah9110Rt288fjbJXvtAOMe4DUVldeOesV1ToL0nrCu7TYW/Vf8k7pnDIH2qDrJq2qEDa0/uOrmpUfG1ienfTzGtczgjotf0M4XYUsLf4/YVo9ngy/EyBiidEfp7Xij3hVPhM+4TlwyKgecB/bYi/IzSu9r4+esJ+lesB4NL1x0WG11rINqGl8L7fT94vu3N/52tg9g4vXTsYZgo1FjwXLAZ9fXui3PH98bfoBpmwa/21Za99dH2xxpb4StY9mgJ+DQozGRuYnSfV69eGS4JDffROiN0Nl2A7BzEeErrhOXjHqjCe6BncsCvmrPw3LKMmmnv9KbI1ftUYuiWu0W2/3LQoK+Z4RDuzPvcH+fzU70wthKbHs+21v6F2EOcO/jtpXWzwPto/IZbYfTT0/AF7QXQ4LLNz7eXquXs85JYzmI0HlOuMbyyd2gseyX5Du16rnTjnlBxT+7t+jN3e1iqlEJ8HrF7bn+njlGxsTTXlOEp4D2psip0nwKj7SF9H9jGNVmvhh9rf0xN4K7322+K0bbHGlvhK3j2eCgPdL/d9Qd5fPr9XIecNLYvPMuOv03ADkfr43ftp7vwka8N9z2AKujQmp+ZeLyJ/21ZOBm00DD/BnSyQ3JtsrkvRdFaO1m0K7lTO2M++22eU97U69VGgE572LybHa4RXyXFG6UtOezvWXugF8LCe5+g22dMNruKHtnscPFwQE7OsG3VVbcGyIi/ebyjWfmtXo565w0loMInVF1EOFrhE48fs56BEjivmL1lEnl0F5rnMNpCrH2X+0byNouupm6kvDpaIEGSG8X0E69DOgM0I7kCEg1JDLSVuA4q8bqi9HXuu9ihQD3Pm5DNdRpdcTbGllQI/J5PBsctEf7H6U/Sm9kfUb4jOvEJaNywHmAzS0ROs8ZP54p7Sy7SaNAl+FeAMoeIvnRGLjRIphDfNl2Dvde33eAT7SUB9trhtkgJMdRHR/7X0A1Y1pycp++iQnoz9n1KL15XkbYGBXLK+Ly1N2+Pv294deQewWAe1+D6JAdMzheVVAjchptg9PPAXv0hML5jtdspN9RPr9WL2edk8b7LULvu+iMqlk8/rpk9LGYjvHvdrnxHIi1qvHzNJlqANu8fsl32ZuVGEOOy8f9LgRpvuVmekV7FLRz/uPzz5k55FWx9bHbp8f7aCHAvb9BrJhH2x1lb4Sd49n4HNAemfco3RF6cZ24ZEe42kxAnAevnbvexVfcT12yPBOB68T6qMNNXXojzAbcA3yeVS56u4B7udII8HlNNa57e78oAhXjXnRP/wnbacf952swtRhh4+y2OoztRYW/N/watFqq7LjHGdULe7TNkfZG2DqWjc8B7FFQeW69eKXiktwNM0JvhM6oOojwFde5lbTOP+B68Rpw6lTPWzN6JfqvRcDoTnry3E7tpcPvLXrRTHO6q9A+mdyVSqY/ZKed8x/Nyl7ubHZesUDoA+19eqKPFgLcYw1+Pmgflc9oO5z+C9rbp/BHSy7fuLXX6uWsc9JYDiJ0RvVXhK8ROvH4n9YtWMd1Yv3ecFOX4DxnXxUiyzPj0sOePR4ATc7kucr1Sn9Psj3sW5nnau1xzyiTOh0L+thPh7PYon/6T8hOO+e/lQ35+ggbr4LokbH1idHvsV8DUkfFjvsYo1vHRtscaW+ErePZ4KA92v8o/ZdedoHBZYyTRia7mMVQhJ9R0BrhK67zTr1jHdeL9X0lp7VNaYkldwaTr9JiJIfrsmEeY49jKzVbNdt4BjFJru/294tiUTGBe77eyM/4h0A75z+WE0nqbHb6wPMr8tmnJ/poQeJfwH2cwc8B7aPyGW2H088BexScjJg8uLwgA5GFYFxnZJ6xPGBSkf3GeYDnNkJvhM6oGqj5+oTY+/rt0Yt32bVTIohbalHUoN2qJAm002dRdRBVC23jV75nSB0lvKHnbaE9sm9HLtBeuRjJbfvy6WvdVvfWzGBd//j+j/+1j9+Wpd310WZH2Rth53g2OGiP9j9Kf5TeyJtphM+4TlwyKgecB9g0FqHzDPHvCZjLFCeN9VX22kT1XDqsqRDM/XXRf0Vvq2+joYrrO/1+8ZEdw88WNvmrL1M/dj8ew8XQ1jMjbETNJbWIR8XVd2Hi99qvoaWOXgDuowMdaW+ErWgbnH4O2KMnFM53bsBE6T6nXi4qThrvtwi976IzaqxJ8cvQimcKl4T6ft2wXfT2ZOrZgXcD9qhaaNtt3D+AWuSzPLo05bt8bKArtHeuP7FIR9ho6w9oTFWFRsaWO9Jut71lH/venA8G9z7pwoMeZW+EnePZ+BzQHpn3KN0RenGduGQUUHAeYPNJhM53il8C1iknB95lxzoWkMrPXKc66L0aiKqvqBrjIfF5v1Aems1PyKQFUik6KZkV9cpXLz1H2Y0eEQ/f98AgI0R8MfpaR48nLA2DwN2fKiycVxTUiNiOZeNzAHvkAI3qz9frxT3AJV8/9j+7rzKgS/3CZYqTVutgw9CtOrUnVI/39hV8PLTmArHA6d5Du2Bjs6u+6C+7pdtOO+c/khFZ5mx2XsFYfXa5+/REHy3t9fRoOQDcRwc6yt5Z7HBxXNDuHXJcvnFrr9XLWeeksRxE6IxavEX4GqFztwWqdgVnnZPeGC03ujdrCkZvbce8XKgwerFqfUhF6T2W7udrHrO8SuukNXXTrnr2yvnuZ9oj8/4KsB0RTx94ZkbHVtYXo6919HjisxIM7v504SGdzdaIeDgbHLRzuvF+jp4Yo/w+r14uMk4aq4t30Rl1A+gZP7/DzFnnpB/bS1YVNOhclZaQPgpQWny28hBVX21zbnWXvTyzPvdxlpMc3OdL3zssdKJyXvbL2ey09T9SrZhMez7bW46aB7AMlIuYIHDvky48pFH2zmKHi4MD9mPdPPAaivSbyzfu82v1ctY5aSwHETqj6uDovu6eCDS7gIsIkFaOPtcdAfRi9F+YQfWaaRqkN6pu26BN/QVUrczSW2Okb1TeBtqjakaqsZG2omtLG0O+GH2t2+qenQ14+UdUAeDeJ114QKPsjbBzPBufA9oj8x6lO0IvrhOXjJr0OQ+w+SRC51HjL4/B4Mdi+IiMvFaPv9R6TtKbb+Xmnprb9pmhd6oDvjewsdAGLvobY5bXPO6MC8dictMXtA9c+B1hgdBWd6Xn/hHs18CNM0t6609HcB8d6Eh7I2xF2+D0fw5gj7zpcfm2hu3z+mv1ctY56dfmIMLXCJ09arY8DhMF7ZX4GY6WYG/zfsD8Zu9RfNT+Yhcv+EiqS3L5uN+lulrqdfr100kd+iDxJOuGds7/tqyNsNEHZvn4RsaWe9dut71lH/t8jpEW+6g6gXufdCEhPGRG2Rth53g2LmjHK1GWjOrTCL24TlwyaoxyHmC9GKHziPHncC5tcdvZ4jJlSFcfTmRA1QPqI27WXNbsXhjhc61+y/NM99sD2AsqrzD8bToWY3XxBe1FhiLrqOyMkbb61bPfa78Gbvxa0ro/TnAfHehIeyNsHcvGBezWQLKuR/Xn6/VyHnDSVlbjFusRfh4B2lEqxuPHJZX4NZDDOj/brGk6CF+xwkUGuxu6wTTS56KW0s75kojZk8Tsya3aWiq/yUhykw4XtEfmph9k4nU0Kp7k0Wh7frt9PO6jBe9XS7LujwPcRwc6yt5Z7HBxXNBuDSTrOpdvS9vz+mv1ctY5aSwHETqPANdY9Ny3i9phcYmosLxiUsDNt5m3uWM8aFbjFoNRtQXkmAs+k045zntboe9sc33ma2GzXXcDqKbv35qj4MZKqxkghlbVu3YjbUXXrZYUX4y+1q+KuVYgWESN4I4p71O/Z7M1Ih7OBgftnG6+BqL0X3pZWOEyxkljdfEuOqNuAEj8EtiWx2JycEd0spVSxO86vVJ+W4BVCieF54DTG1UHEXqBPJeltTSZgX36A/czkO9J5H5Be8vI42u0bAH0j9+IoMFn19c6Ykx5k4RHRII7rtgbwtgCHhVXtB1OPwfs0YXO+c7VV5Tuc+rlovr/2/sWJEdyJTmypkfat1eQVn/d/wbzzrXa6SpZkkwyCQIJ94hwfJIcs12bNxXw8PAIAJ7ZLDYXjfdNgTsLpmWvlT63Dhi0pCmcSrdo2MTtTQDPFZ+nNZKrDsdX4dZm4fUz56+vwbcPboBp275F35a1/H4pXSa44Nv6Pe0gPt7IcIPJpW5Rz5ZR63y1ea6rFcM4BqXOFo3g+BDGnQNG6ZbjWuVrkWe8HLID2NR4lT4qXP/h03bucR3wSJUGHANs3BSYvesvvA7NCoLXX418eplfjcbac4n6mPa8WIvG6edR1v+2zmDy89zHV168O9c72X2x0DB/pp2rgRjGTWiLHKqzZK/iVnUBD4pEY/ys/QgEXSDUxgcw7jZggHEhpGW+FrnUOTh82QFsajjHnUuhwj4uLl4ZHtm/ZwquCkzm8mZ+0xPnWox8eZuOY+b7X/rAuxe3NG2NcS+/wOnImVtf+hON1NdvPf1Wjqf/znHjSyHwTW/aCXzuAEqij5ZnLa9VXXGmPYZxDIprpJ4W2/lUjLsd2FZcq3wt8oyXQ3oA0w1X6qPCngkX54pHMuaSGQiOAYaswByhfuYzKZgGWNT64veH+Gxz+ia9xxtAuDpsrO5RIG7WhN+M/fKzu5dadN080GyNdsmMk4yv4SDvJRIPvTEhFwxr2sk6TH3oYaJb1pU+NdpF8rP2I9jZ51b6+RSMux+YK7Rlvha5xsohP4CpZiu1UWEfF5erjIvGxmIWTM70YLWzmKhpxzXdjXx5sY/gMh/hYevHVWUMKoMKmd+tId8a822iXAxhqsM5J4DyO4M27cjs8aq8rjhanh4PCDGmPaYTMSgRkwWdHWCijHFvXWirfEfJw9UhP4DBQYsc2nxKThec9jFxuaq4aExbBabKCCq4spixpr2YPZsG4Yryi7nU92cM4YtN6XPUBjdrvC2YLYwVp4f8zhjStHMaeTqtfajMMWtZW8w8+xn7EXw9TlfH8kmMeyx4n8O11+C20I7LwR3AHDY/1Cr8Dy77UMQpxkVjczEL5igPAogpxjXNRhZToLgIR7VpR7liU3r56MpyiF5M+g17/d/3/w5iFcOCOT/l4bC5+8K4N97atHP98E5W+weEj2lv9VLxZtyPOlCt6lLn4fCbHMDwqcJxh2EvgSrsY+JyVXHReN8UuLNgWmYWMcRY/fcoBBLaWxBQMhoYV3yenGZha87Tg3Nr2HlCwAqVFvycNbkzKNOu1MY5M0BnX0Na1KN+MEYKt9dpXzlC3SVtYqpK0c/f//y/GuQubxhaN7CFdFyOJgcwsn+nNNb8hQdLARkhHO0aic8GHsnh4ow5BhiuAnOk+mvGGKx/eXGMCXqLKkXX+NSScCxqaI+fG3GrH3cx4kLEVdgcbrP74m1NO9cPaHR2g1rni3kI8rP2I/i1b+c/Gxr3lsK2yKXOweE3O4Ch6ea4Q5D3IBX2cXHxyvDI/j1TcFVgWh8EaiZ5h2uyFK8qF1njgUwCzgBBMxv2p4++7GVS8bXOAqrK3gPX68+a3RmwaVfq3s5UmecTbXMxrpV+KQF7XvvKHv1EGxRT1V62RsZdX8i1yBZ5xsvR7ACG5lapjwp7JlycKx6p2jscA2i8ZPtcwdWKWTPLBVyzYU/7X8uPd0p3Jle0Xd+m059Ft/YM0cSDnX5bzzbf+jPMuDe9L4Yy7R79kf72MpMt64qr0c/aj8B2dT++HR+xcW9XiO6CiBtUbEhwzZoewFXyOO8q1EuACvu4uFxlXDTWv1kwR3to2TPNGU0Lf5cRp34aHWHcOQbYTAG92n5vevWjMHFvDusX+tZ4r//+8n2bycunNC6XZavz+u/bddv/lvtu9lzvc5h4h+6RkGlXzUmr3vbKs+ZtpV9cnTGMY1AMU11Y0paP0Li3KuQoebg6Pqbdu+U4vfFsfXG57Fw0poECEzBsGLkkSsHVg7ljmM8YLhaVexkRYdbVZqJQHW3Q40wIP3bPRvq6fvlvFv2Rbj+w63dGiofgFxQYxrQ7aqCb2zKX6kysFe2r0bdafb7Uaq89OFvW29YIjHtMa7ByWuQaL0f9AM5dzJiifJRKnw/u41LHusIpxkUrGPTDVF16Hk0T43b/nz+wr8Oyr8DfRrNY6xrGooby/HOVYVfNwd7HWrjK89GcxjH3xfoAAOR+O9MOaBLR9jtG63wxhtnP2o8Q2oYmH83OMw427q2EPUoerg7uAFZdSjGbeH8Dcbrgm/GYuFxVXHRfbRVcFZjOvVZ8Y4xxxaIWjkpDibPAZ2qjq/uteprVwzf92Mu4L0rsd8Zq1HPd2jHxVdPu0R2dnBY5WtyDJe1RHSLj7JraV7bcV6xWMVWxWdf4QOPeqpAWecbLYT+AraOxt06ljwrXaax2JVRwxjHxSJUGHANsGhWYg9afNaR4/fnI0scucFysT2LzcvHHN86hxh3RofSQs9U2Z24RbE7dazSH678vSsZ9+98TTm9l2rl+WDr+vKZ1vpi97WftR/BrP94DRIBxbylsi1zqHBy+/wCOHFuOO5dZhX1cXLwyPLJ/zxRcFZi8mXrRNsS0I5+NHrT+VJD7L5YqzDrSL0TL0g5RaYzwTiweTYVZUDDuu6adwedOoD7mtlU9MebZrqi9TvvK8Uzyg1FMVfZ+PFY6jXurQlrkGS/Hx7R7R1zVUwUujolH8pc+pjjHAMOciWtA/bRpf3wDyTU7ajIDuGYbGIj7pEUg7hPvPdzCV/PAg6vizO2JtvfFpubupl2pfzoER80VZ5j9CvkR4K0LBY7G53QyGveWhbTINVaOtgdwbXKV2qiwj4vLVcZF1ybh+vNZMAfm+mLaU03LRpJTn4vu0v/7X4qEZbdFbXVAvmIRzaLQ1/aWtf2dcau9aNqV2sSZTLTTmnNvL3sr/eIeTGIYx6Dgfa1Fjsbnytdg3FsVcpQ8XB3tD+BehwenS217PX5+TFyuKi4a01aBObC5fhElov7z7UU5/0uiXHYuGut/YK+qDy44o/3IVQf0TyeYvCqNeZ373Bk/p1NX067UP87MMhOlv8N0d31MN2JQbJr37jnHmjDuLUVtkWu8HNwBrOavwv/gXrcorgMeyeHiRwXHAMNVYI5c/2LatwYS/zpGTikuGuuVU9fdv8VUyfdj2J/7G6X1u5j2KL3QXdY638rLl9e32nm2oNJScf6KqHSGYNC4tyrkKHm4OjjDrh50jjs3cyrsY+JyVXHReN8UuLNgRu211LRj9WNRMZdveR44Fk84u98E48AtkuX/JAPfB1GzUMrI6dHvziiZdo4/p7t6xnNsWtSzzds6n1/TGMYxKLZ5GqHvFuY/yEdlWgnbIs94OfodwC2HVqm7CluBi2PikSpDwTHAjh8F5uj1b007Xj8eqarfgAt/ZSNXXX22tr8TUI+2RURzthmnvvfFO5h2VZ9jHthssxt/1/tV8iPEaWE462KTE2g/p9PXrz3j3lLYFrnGytH3AE7nRKmNCvu4uHhleCRxMlAf48FxFVwVmJGH+Kym3ahr1bgbcbNDln6fOj6JXGQkZ9+52/3O+P6dnA1KbXq8jW5Vj+2hjZvbvWh7nfaVPfqJKhZTFZrNHncz7b/+UTLurQo5Sh6uju4H8NPkcNy5oVNhz4SLc8UjI81liwOVqwybsdExedPOVcRFY5oa5qpq1iNNSu4bYVQ6GLTARaYejvn7QsC9i2lX9tb3EEW1+iW4ZV1x57uftR/Bp3vPnnuY33Rb3rT/+sflq4CTz7i3FLZFrrFy8Aewkv+M2CrO/XE5Blw0dmTMgikwJReBIutfTTuOiUdGcyWNNWzUSdzikO79gimnGrYPVPra9Bjizngy7SrNe5mqVvXY+o/PbC3SXqd9ZdxDQ606/ucxVfF52RWpaV/WPxn3VoUcJQ9XxxAH8H1mOO7cqKmwj4nLVcVFY31TYKrMj4JrNCZn2rnsXDTWf7BXtGEHcXdJlky7SocIznsFcbyHuDOam3ZOI3zGc5Etc6lnq6SEr0bf6t4PKyP03DqhG+U3b9pXtPP3P/9PTG8gfi1SjZeDO4DV/FX4H9zrFsB1wCM5XGgrklz7Ys5S//o97Zha/fuf0XX3qxuxuth9cI1H/2IkTjWcsQqXn13uvuDxYU0Oa9qVvR7JLPrq9K0WziU8wGmgvyJzamrhvmm/nJbtjHsL0dQ5OPxhDmCZSVM/UXN643ujLy6XnYvuq4GCqwJTcamoTHuH+k1v161nQW/DrpiF7S7M9S/9m3EfMcPcGXfTrpq/mkb4ScZFtqinV20xeWMUikHhersXPRof4AHv/qZ9fbHxWNPAuLcQbLwcwxzAUtOu1F2FrcDFMfFIlaHgGGAHpwJzpvqXVyDYX/bDKcVFY71KdAV5Y9gWvohuFlyMMfMnZM+I6FdQptzz9V7vi+X/WXHReoG4Q5p25QwBJgyQPSbEV6dvterM9ijjr8iTHV+b8Pz643T+9a/J8sfZITbuLUQbK8d7GHblBlX1sz8uzgCPxA8GVc8UXBWYqvpnMu0/8AOGZq4Qkx7z1rDO3zJj27/gae9PCjjsV9OOzupq9Ll8u9pcTPvyt/q2+CeQ9y7dVnlWEq3z+fPGMI5BiZu80fiUKkNMe/LaQPNRmVaCqfNw+B/T7t1ynN54tr64XHYuGtNAgYmaC4zhI0rBVYF5Ywy8seayc9FZdXP+eDmcAK5st/A31rOZdpYvb56e74u177WHg735cM5OM9Pu5EkN6VFzxT3g+hXyI1AtrQaPxgc07EtY9k3763rBG/cWoo2V42PYqzupEqDqZ39cjgEXjak+C+ZsDwLn0+nrXPWtnPpc9KX/qc/DhiIgiuHKmmAGmyllD7dmmGt5OM6aO4Pj8FTR99/UL9bX1Cj/3MGRStoqD//ARpVRDbbXaV8Z99BQLY8OiKmKTksvyPAETfvl2I97495KMHUeDl9zANNTcFvAceeyqLCPictVxUVjfVNgzmauMaW4qEXXxbR/vRqd24+KKqUtuXvF2w/SjzfnTPkW48UPbxdwVXHRyGyxZl01Wzljlepk4Wo3L/F3BtKPnQ43Me1OjtSAtsylnttS4b4afat7P6zkNImpiBozU3CB5/mP0/nP61+uhPwTZNxbiDZeDu4AVvNX4atwVQeeii+Oi0fOpAFXFXL4XGMUuErM1bTnjNv10L1mTzhsTH1em5/n303MBu0Z8/TAV2hQ6xd26RTrx4eGiMzp4OE5kmHfGij0l1oT6Q5l2lUzrzHPxBAnob46fatrZ4C9KvtKf0X23MzKGNO+ZAww7i1EU+fg8DnDrh50jjszZhpTpdRDpQWGi0VtL1uuG1g0x+JYmKrZumm6fEb8vLxpL//zrP6eW8/1afvfrOZS0f+SrhGf01Hx3XLOPfBY9bXtX92dserH/mnLz+n09D3t2EnARSl7mzJpmUt1ziDq2uu0r7Q/rCIV+WJiqvJxqK3e4Ui+aV8zOYx7C8HGy6E7gGvNz/1cpY8KV3ngKTjjmHikSgOOATZtCsxJ66+Ydk4pLhrrlUrXPdOOM8tHqnRITfvWbHsNO6fzpcL1+S19LsuWbVPBQQAAIABJREFUb9WEWXck087U7Z1XrvcR2R4Yvjp9q3vWXVLRX1Fsfww8jaZ9yWQ07i1EGyvHexh25QZV9bM/Ls4Aj+QOFQWuAlM1XwquG8zhTbui/r1eec2vim9q2FfHzO2mcjTHu+jLc78He0nK4b/yrK2//fwwb9pr9Ub1ffvgF42J4NnrtK/c8opBQSrFYkbj09a0G4x7K8HUeTj8j2nHtlPUhYdn4/oYjctl56IxrgrMCAORY6/gqsBM6t8x7Vx2Lhrrv6pXs5v2y9WGSwhF4v2rRqbPE5cF1VUQy0dQitfCtEfXsFdyy1yK/iDt9NXoW92r5pF6jvTIcNedv07nP5e/XMl+RhFv3P1jUJdhvBycaVfynxFbxbk/LseAi67vE9WhquA5E9ek/qFNu6pX2PTZolSct7jr62z7pfhcG8F5+XbQNTw150XBbr+UbBN0s6rEc/PfpW/aCZ1ctbbKs5Jsnc+fN4ZxDIqr1U+LR+NTqqzCM8C0X15L1L8OspVg6jwcPmfYVebEv4nrG4fTpY6n5tyXL5edi8a0VWCq5lfBVYGZqX/5JdSnX0S95uWyc9FY/3kWOK4qUqVDqkWUWTecYVvTDst406Vk8mnZVqDt08ONjMy00yRhdV4DW+bqtc98NfpWG+be0U1saUxFWC5PFMAzyLQDxh0g46nVcBXa0nF1cKadw+b5q/BVuMoDT8EZx8QjVRpwDLBZU2AeoP6vP17+KPNh3ZFv8JhJV2xSbFEtdOho2OmHOWBvmCXbLtz8+/Sm3SyIbWTJx3NjkswyX52+1cBcxhUKIvkrAhM5wwCegaa9YtwBMs5y2fdXfDquBs6wqwed485po8I+Ji5XFReN902BOwumaq9V6t+Y93zk8l9L358drW00Hj55tkgl3xQ70rgXeBee1fgqKyvWkeKBb236fm6XxLSbyRlGqWUu1TmDlG2v075yyysGBakUixmNT441yDHYtBeMO0gGU78QNV6O9zDtSt1V2ApcHBOPVB36HANsWyowj1n/T+bNe1ljVFfWnaG4WPf1UUq+qXYNTPsq2Ma88xVmVqylpA3hwQ9o2s0iGMe7db6Vpi+vb7XqzDa24LLMX5EnO74W5Ckw7RnjDpLBq8tEjpXjPQy7ckOo+tkfF2eAR3JbR4GrwFTNl4IrjnmPhMw7jnudAcS1rZjp697a36Cacil+/yA3jtVoVoMq4CaghB1h3Anens+yR5Sbxdjyv/371G/aiX4wmhZjW+fzm/YYxjEoIS04qmn/9a+n0/IlB8H/3H45tVUD1Xk4/I9p904TpzeerS8ul52LxjRQYB7TXGN6chdlVv2iee/Vq62pT1XYPhSkcb34cp16jt5y3n40KeJCTPTYeSayKWdbhfuYFqbdUQPd9pa5VGdirWhfjb7VvWre08RfUU3xmJ8TPJc37SLTvtRy/v7n/ybYWMtvkIL8IxbOtCv5z4it4twfl2PARWO7ZxZM1QXQt/7d7C/mvS9XbJ64hxYOU42d0zfCrBOzezPyfKfJFdtnrZXe7u9Ap/i3/x3+pp2swzZAHT4i0aqu3EO1TaQYxjEotgpyq0bjU6qM4Ck27Y2MO1GwaRo4fM6wEwd8A+5cCk4XHPuYuFxVXDSmrQJTNb/H4wpXdDHvqn9gFiSB2XBzc5t+3IeU4Ck80SP3+8XRn2dH6G5pUab9pleoaVfNzAjmrWVtMQ+3MYxjUJBRxmJG4xMwmxfT/o/kq4QxNZgo4Rv3Fk3hcnCmncNmRL/GqvBVuLNxxnXAI1UacAywWVNgHrN+Tqmf0+nrF9YCKopjgUMfBTfiL1bitOCiA/YGlDAX9HM6TWnaoYLxUa9Gts43imnvVXepIaPxCeLZyLQvbEXGXd0YDp8z7AEH8O4BwnGvnkV7b5K4xTvRKs59cbnsXDQuvQJ3FkzVXsPrN0dCv7CKTAHOAEF7xMyGi8yC52MynB5cNMId7F4xcekHkaadrxqsKhPWMldgf+iC7XXaV25JxqDQZRcXjMYnR9TAsaFpFxh3Q8H0RHA53sO0c5pwkquwFbg4Jh6pOvQ5BljPFJjHrJ9TqhDtfvPOscBmQNUvJe4edsQvo+I645FrN/gVL32sQnxMOz77aWRVXDv07kpfXt9q9V61SOavyJKVX2Pg2di0Bxt3Q8G0qniO9zDsyg2Ka821sT8uzgCP7K/Bu3PF68cjgf1lMu8cA3y2VLiADjjJJBLhvH3TvsSjb94R7AcdLjpQk23ip8+27zGa8U07r7B5rC4LW+fzP8zFMI5B8Wm/XT0an1JlBp4dTHugcTcUTE0Fh/8x7ZS4mWBObzxbX1wuOxeNaaDAVF1QCq4KTLx+LjsRTZl3AhcbqlvUbLh4364FombdZp549fgVUDvvsC1Mu6iGbKEtc7GzBXUGCPLV6Fvdq+Y9WfwVAaIHhBh5ns+Zr3yM+F2ceknOz7gbC67z2kRwOTjTzmFTtC/BKvwP7rUXuA54JIeLzwTHAMNVYB6zfk4pLvrSq6p5N2BCQ6DCVc2BBxcx75weXLSHe9LMJXF6x1/+20/lWIt6085XDo3jS1CrPLYHNltNuVX2Ou0rtzxiUEbQI44DgmTULWvat/mQswrhl49xGHdjwTBXDp8z7IEHcLYejjssCWlWP7iMtVfNhGoWFLgKzL66chVx0S/7q2jenbjFjTwbrmcWkM+6c3pw0R7uGdNuujciTDtfNXeP9DSTLWuLeVCIYRyDYu9zunI0PrEPWssT9/lP5G9EtX7Er94Jg3Fv0RQux3uYdk6TeutbHLAKzjgmHhl4KT8JzzHAeqbAPGb9nFJcdLFXL+Y9CPcl4bvh7r3B4rS43BfUCzEOf/c5K73L78G1HDOZ9lot2EmHR7XON4pp71V3qTOj8VHwXEy793vaqcMnWwRp3NWN4fDfw7CrTNVxcckpwu8HKpJjgUHPgqmaLbx+RSTWo/VjMzgDGPcSOBtuFOfSZcfp8XRnQPcnh7/by/SB4QKN4HtNO5KDm8JydMtcqH5RtcW87IpRKAYlTpnR+Ixq2lde0OFTbA9o3Fs0hcvxHqad04TbhCpsBS6OiUeqDn2OAdYzBeZM9eNcOaW4aKxXN67Vz7zjaI9IIV8LHWhNNOfiK+t9z5yjsXt3RvPe0LtDIzk+pj3fWEQ7aEDJIF9e32r8HCSLcoT7K3IkJ5Z6eUa8aS/R5U08YNy9BSPa4jnew7ArNyiuNdK5kUwFVxkXjWkxC6ZqvvrWz2Xnok39DzPvCq5rRSpsFS7WiW3UhUlKJ3dXLjGX/x7M/Y67QqP4s5h2tB6+d0cx7TEKxaBEdSF8n8QRS5C8uilN+5YqbuArxt1bcK0THP7HtNf0rP2c07uGNopp56riojENFJgCA3EpRsFVgYlz5bJz0Vj/d7jezfvTF3TjsJJ+4doSRDehKo0XDb+pD6gX74zi743duG9LwO/TV7lepEC18Zh2NIetu8+rWuZSz21JD1+NvtW9at6bDX9FEZNXx4jg2cq0r9Vgh03BuEcUXJOVy8GZdg67xhQ4jXmI7AoV7+PicpVx0VhTZ8FUXQB96+eyc9Fh/b+Yd9a4K7iuFamwVbi52d2/4KD7YvtFNekDEnZ/lkdkkQL+y5QyMN9/Gx+ylT3Y8myVRz2ztV3uq9O3WnVm12r+mParAq1NO27eM8bdP2r7Y8HhQwfwU0IOnxvhGbFVnPvictm5aGwmFJiqg/p4XLmKuGis/2SvqI/NDMAXF+EWqeK8p3P+KyJD7owc9GrGEW1WOS7mndHG+qadyYEUMJJ5a1lbzINCDOMYFG+3H+tH45OrLIrj8pcr/eN0+vqKk8+E9PR24f6njRvjHlVw3IanzrtLWmUNKmwVrlIPBWccE49UacAxwParAvOY9XNKcdFYr4y63t+8760fiC8sRm/O18st/L7I/R5s+iZ+a+bTf6fvpNFNu6rPpUFrnW8U096r7tH6AB9AwS8PRjHtufqvB9DNuKsHhcMPP4DZ/j/Fc9y5VCrs4+LileGR/Xum4KrANBrWqsA4V0VklV7keXCYv6RJNQs28/Tzs34uBf2MCzBJe19gs/0YTPbfAfx0rr5/G14+sXm4aX9Et8pj67+1qtd19jrtK7csYlBG0COOA4IUpdvIpv1i2Vfj/r+iKi6oy8F/TDsypHsxnN54NgUujolHqgwFxwDTVYE5U/04V04pLhrrFc61ijf9X9IUqEVWLLx/5fuiZOBx7GofiwFsDsubdjaHvRrtn2SnvFrWFWeY/az9CJ4ORz7AxPKooUXqNrppf2hx/v6n0rjjon4Me21Aaz/Hta4hPf+8Py7HgIvGtJgFU2Wo+tbPZeeiu/X/Yt4VXNeKVNgqXH5263dG+hZeyZ3nf+8//aZdXYd6hkq7rlVdcQ8LMYxjULCzDIkajU+LeZnHtC9qiIw71/j6ARy30epjy3Gv48U91ZdzqTj3xeWyc9FY3xSYlgseYavgqsDE6+eyc9GIotcYIS71C6soYxVfpRY8NndnsN/og2rtOdtvfaJMu7K3Le/YnL4ta4t5MPEz9iNYJrW9j4hlGX8mL6b9X06nrz+iicrwBMadG0buAOawedVU+B9c1gRxinHR2FzMgsmbnhnq59TnovvVn+lVqHlX6KCaL5t54u4LrNP+KIvu7MdjLDkslbXKY+u/paL8Gl+dvtXqPWVRyV+RJSu/JprnfKZ90SzQuHOC8gcwh88NxIzYKs59cbnsXDQ+EwrcWTBVlwpWPxalvvQ5Fu65cpt3FV/VLNj657sz2F9eRbrK6r6Jh9+0szkQ3qWYlrnUs7Wng71O+8otnxgUT6ef147GJ1eZhuP1Kx/nedO+KhNk3DlRfQdw3LhekTjueHYV7myccR3wSJUGHANsFhSYx6yfU4qLxnql0hXANZt3lQ4AZ1zUJJLjHH9foN88E2H+MrUOZ9q5fpjbfl/YOp/tQTGt08/aj+DXfuQHiLYPlBfTfk5Me8TRENukLJrTuHODGH8AexTiuHOZVNjHxcUrwyP790zBVYGpMmk4V0Vk//6TulLmHVeM04HkTINzvGPvjPSz7pZbusa/8vOPaacnJmZBrW/lLPaVI5vkmKpiehPxgMwxyZr2HITliOComKIdxp1rfOwBbKp1s4jjzmVTYc+Ei3PFI1WGgmOAz4ICdxZMvFdcRVx0317hGjzxhMy7SgcjZ1honHf7+yL9Mva0qJT7+r9Lf8VqRhTItOMawbIXA4+aK84w+xXyI/j7HKdHLJf2hn3JePlF1PPyrV7EP4MZeKNxx4ex/QHcZxjm+8gN3kNivKmPHnEMuGiM8yyYKkPVt34uOxfdr/8Bvdo17wodAjjvCs5xHufOSN/QL3Xk/wry6/mf/mwjylCmnesHtpd63buavDEKxaD49V8RRuNTqkzH02TaV5oDmXfSuHOCjnMAj3Ux4RuR03sWXK4qLhrTQIGpmjEFVwUmXj+XnYvG+o9zxfGCL8dmf0mTUgsee6w7g+efnZeqaVfNeI5Ny1xB+tGb0Fejb3XwOUDXPkLPraRjlC9ld5n2HGhHI08Yd05U7gDmsPmxUOF/cK+9wHXAIzlcfCY4BhiuAvOY9XNKcdFYrybS9W7eZ9LBZly4+0LVw3SCAnQfxrQH1IJvMOpOoGCrwb46fatbzWVVhE2AvyImmz1Wy/Nq2pdfRC39iZmd+WVlYxMPGHdO0LEOYI471zoV9jFxuaq4aLxvCtxZMFWXClY/FmUzfX37r9L1hgt95h1X4BHJdYTLwGGPdWcE9nPXtHMacfoLHkAoAi1r2xKz57WvjMlPyQsHx1QFpzMF6jk+TPsewSDnHQRTk7Ji3DlRxzqAOe41oUa88PpyxvXFIwMvzSdxOAaYrgrMY9bPKcVFY71S6doIN9y8qzT+vrUDu73Gui+CHxqHMO2qPpd2Xet8MT3zs/Yj4OcYEjkan47z8utfTuf0Kx+zdLAzC1G/xdv3gnHnGj/WAcxxhxpxD1JhHxcXrwyP7N8zBVcFpspY4lwVkf37r9J1B/fyl4R4Lxe8G4+Pv+38wuW9EUvM1rSveXLfuHJdNNadEdzPj2nntqgrmpnp50T2lVucGBSXBPKXVHHsHkgNdINNe6k+43lbPvbChMwYd07QsQ5gjjunogp7JlycKx4ZfGlO+ZDFqYXNrQIT7xWXnYvG6se54nhrZCe+LvO+x3n7s/SbVBB1cD1+ftjLEMdGmOZjAnMUTXtgjmqhR80VZ5j9CvkRqm2kAkbjUyLfiKfbtC/82bOqUHMQzBY9Me64qB/DTu2qTDCuNZepPy7HgIvGtJgFU2Uu+9bPZeei+/Vf1SvyYYA27zV9az/fU5xb+3pnrA8JpZuNw8dmI878veTrbtrVeqUVt85H7hXZrdur7s5mmN9gyYpGuoWY9rTYQPcdAHUz7pygH9PunWBObzxbX1wuOxeNaaDAVBk2BVcFJl4/l52LxvqPc8Xx/GZhP5dBB9i872Eb8j4Vwq3n7gzLW3+2oxz/KnrWtAfn2CXRMpdyn8U9KGoeM1rrrNOjOtNhAQ01++M/n87hvxO0FaL2sgEQLca4/09KVe4ApqCBijVb8TWxivdxcbnKuGhsMGbBVF16fevnsnPRWP9Vug6Ku2veS/pG6M5hcPfFVmvlB0W5Gqrz19W0B9dSLbZ1vpgHZz9rP0JVWipgND4l8g15yk17auCphj2CWxp3+wFsLK7b2wXVoB0Tl6uKi8YnR4E7C6bKWGL1Y1Exl295HjgWfecqsF9Z855qEakNh6W5Mzy3HscfmpMX0y7IUSTSMlfg3ELCboPsddpXxuSnS4UWxFQFpTIHNebY1LSnohjOJMOSp8eG73/W37hrDmDrRKgGQoWrPPAUnHFMPFKlAccAmzgF5jHr55TiorFeqXSdCPfJvOc0jtCdw2h7X6A3IFcDNH/dTLuglm4vyvYS++r0rVaeAdB0ZYL8FVkzc+sa83wy7Utu9EzgqtqPdv4JIUn5vGfc2x7ANRGVw6DCPi4uXhkeWZuA558rcGfBVF0qeP14ZH+u/edKpcGtsq+vzGXFdaisEYfT785o/AuuH9PObSs6mpu7Lbx9ZTwKXXZxQUxVcXxKSI15Xkz78lW5uX9INxwqjiE3saRo3PsdwDn1lMOgwp4JF+eKR6rMCscA34sK3Fkw8V5xFXHRfXuFa4DzXCNVOmw4V9+886wf3+VeXzvWfYF893y9pmLEk2lX9jZlcNRccYbZr5AfwTFZmaWj8RnEsC80/vhPwC+iOt+Eu5pJOPE0T2Xpi3Ef6wBWDq0K+7i4XGVcNLY/ZsFUmcC+9XPZueh+/Vf1Sm3aM/oW3zxh6j6iEuzlf24vkvV/38Ke7ozUM+c89FkxG2yNxvgupr21Xq3z+fdKDOMYFONkfUw7Ixxk2kuADkPNcLzHGvPtLHsy7h/TburKZpFq4/fF5bJz0ZjiCkyVYVNwVWDi9XPZuWis/zhXHM9vFvZzqXSoaLH89d7Ge+JaT8a03//769sr153h4llSP33K4CeiuOJu2pW9TbO3zKXcZ3t98NXoW60+ByzzF1ORJTO3phNPl2lfK0y/clZyGG3kdOIny+/GnTuA1Q1T4X9ws5fzzm7lFOOisUNiFkzVpde3fi47F431X6XrgXHPX6fTmbgoLm1Le7fXy/Ppel8g/U5fuWeMNUHV/rDkTNLctCPa4juoHtk6X4xh9rP2I9S1ZSJG47P3gMzUFRQbYtpTLs6zgS7NmG991jifTufff9W/VeaZl3KwZsRWce6Ly2XnovE5V+DOgokaI1xN3UObQlNV/TPiGjgz5v3prU29l9xLnhz3wudOjfcZ9gBhvax/Tqfv79PptPxfq3/qPYhl0jqf37THMI5BievFaHxylXXkuJj28y/nnyjWumU+hGrAyc99eUjjrmyaCluFa7hM4dYqOOOYeKRKA44BJqsC85j1c0px0VivVLrOiOvgXDPvdweO9dBv2Evd3/swZ21iMO77KKVfYGtt2iNqqem1/XnrfH7D7tgNm8J71V3qzWh8BuS5mnbr8zezLV5ifSYbO3s4gqBxVw7WjNgqzv1xcQZ4JDeSCtxZMGOupVe98frxyP5c+8+VSoMYg3NB2Zr3p+Zyb5B1pn3PzC+EN38+fKknUJvsBb0Rqembdm7ncbOfi26dz9+3GMYxKH79/XrEcUCQOupWMu1b2qm3Xo8OpDQ4ZhwDDxh3ZcNmxFZx7ovLZeeisX2hwFQZKwVXBSZeP5edi8b6j3PF8dSXo0qHYC1W836nu/wLxp037MHcr08fSct/xH9cvnwy5jesET+P6QqsF/48it6grHw1+lb3rLukj78iVHlfXGeeX3+ezl9/2kqQ+GwJ6K0+HHvHuCsbNiO2inN/XI4BF43tuFkwVRdA3/q57Fx0v/6rejXpw8Bi3kGzfq+QbjW9ABuPu3kv4Ccv5QnQfGgz067UK1da63z+vRLDOAbFPVd3gNH4DPpw4THt25JwP0y0WAK6yb+PXzDuysGaEVvFuS8ul52LxnaAAlNl2BRcFZh4/Vx2LhrrP84Vx/Obhf1cKh2UWtywL+Yd+4d/067UZdUGuCyBkLIC62fa1bWoez2Kaffp6FutPgewffQcFVORJTO3ZgCeUaZ9Ldx1LuypJwPO/Enjg0di3NUNU+GrcFUHrIovjotHzqQBVxV+mClw+2Jy2bnovrqq5nVG3Azninkfz7CnNYCv1+n7tJVpV+2l0q5rnS/GMPtZ+xHwcwyJHI3PaPOy4XMx7b8yBOlN/YoBHh9IR/PgtpX7q17r3hh35WDNiK3i3BeXy85F4yOrwJ0FU2UA8foVkXjvVfXPiKvkvINdMO/jmXZ8Unc/9F760pjL0N5yXH4RlcnHTfw1Wo2fcmqdz2/aYxjHoFg6nF8zGp8cy0E4Fk37lvPuhubbFvA88EgaCrap5Rn3ZtyVTVNhq3CVB6yCM46JR6o04BhgO1CBecz6OaW4aKxXKl1nxFVyBrA35n08ww7wfxm4yqu09cfLuu030yxj/qM27aq9VNp1rfP5Dbul46/V96p7tD7gp3GfB8oCP8i0l2oLNMxhUGFALwb+/Puv/yGadhHspQQV9nFx8crwSO54UODOgqmaWbx+PLI/1/5zpdIgxuDs60N0+vx1+xtQGcUJfAb2HuvFB/4s/HKf/lyvkY9pN3Up0jR7Oz6U+Qyb46C2VGFi1K+mqQV8/bJ/e8wFO9gkh8GFAd0UvOKJjLtyGFTYx8TlquKia3tRe6DOwlXBEzeWXHYuGus/zhXHUxtglQ5KLTjs+1t24hdWdS9MlP1ML85Nbz+mnd9yLyt8e8W3mpv5gGIBCH9FQJKAkIF4uk37Kke0SY56HojnFWzclcOgwj4uLlcZF42dHLNgqi6AvvVz2bnofv1X9UppHsfi/PLRmKp5V8zGdoKU+Ns38Gue5W375XU7NsZ0lAq3RKR1Pv9eiWEcg0K3t7hgND6jzUuGT5hp32JvjfLaE6d5di6P/hOBQOOuHFoV9jFxuaq4aOyQU2CqzI+CqwITr5/LzkVj/ce54nh+s7CfS6WDUgseu/h59vNyM+VuJ6UuPH9+XjI5Lm/aVf+o9Up5t87n71kM4xiUmCkYicteRYPxlJj2XP3Ax+eYQXCZeNfiO8sg464aCBWu//Ap91nBGcfEI1UacAyw/aLAPGb9nFJcNNYrla4f3Gf9ud5Bv4D6ZN45fHw2tpEdcshMe4taWmuX66qvTt9q5Rlgm2Ddn9pY+ZTW+ZUPZXQx7aKvfNwlGmOcLynMUOaFUcZdOQwq7GPiclVx0fiGVeDOgqm6VPD6FZF471X1z4ir5MxjQ6Z9bfTFvLOXf2nNMpE93uIXNPqYdm47v0TjJ0y61L5yhIcVdj84ZQ5dHqN8KKWiac9l8ZvcPPcgXBfMurh0TpZVd7xxVw2ECpe/8PBhVXDGMfFIlQYcA0xXBeYx6+eU4qKxXql0nRFXyZnDpgz7pdG32ZC8ed9eUvhU2SIzMy4x7aq9NJpR9NXpW83NvG1e2FX+itiMtvgBeVKmfa9ql2NOgAOw3BAcgMG4K4dBhX1cXLwyPJI7JBS4CkzVBaDgimPikTPVr+KqxB0L22za75v/7Pij4NwJsk7q+nnTJYa7rLBz6WPaMZ3QKO6E2aLaV8ajoNXW42KqqufxRgzIM8y0q86OwPPIBVVfTBp35TCosI+Jy1XFRWNHhgJTZX4UXBWYeP1cdi4a6z/OFcdbI2fjq9SCw+YN+x7+jnnf+vBqg3P9jDbwhZn5vGmvdicf4NuDvtXczBsLJJf5KyITGsMH5Rlq2rfS2D9uUha4bpyh5phh6gtB464cBhX2cXG5yrhoaCAlX6Om4Km6ABRccUw8cqb6VVzf52GAN+3IJG3MOxK+vgy7xN4W1O+h29v3JR4KTo6pVqYdFQA7RetRrfP590oM4xiUur5oxGh8SrwH5fn1x+m0/K2otxPBssP3OxWPeM0XhOuCyS8GjLtyGFTYx8TlquKisSNMgakybMfjylXERWP9V/VqRlwlZx5bY9rXqbDcPIX5q0JVAzajujPj4W/aVftpJBPmq9G32v/QgJ9hTGRMVUxGPnZgjjfTvlcTs+PrBt768C9m6CrydXHFuKsGQoXLX3j4JlFwxjHxSJUGHANMVwXmMevnlOKisV6pdP3gPuvP9U5r2LfMmJtnpwboT7aRoFamnesHvpdGMuz+PehXyY/g136LMBqf0eYFUBsw7SkKc8rUTTzAkQ5xMjQth427cmhV2MfE5ariovGZVeDOgum/1PI64/UrIvHeq+qfEVfJmcduZ9rXaandOvikPs3fLmz6w0qO0Dftxnq4zbWJbp1vTW3Pa185skmOqco8BtDCwTme/zid/vgTqqQWVDt1auuvP49BeeRy4pmWPxZl3rirBkKFy194WKNVuLgOeGR/rn01PWb9/fuv0nVGXCVnDrvvC7/BAAAcfUlEQVS9Yd/u7tKNw03ry3lhusgSlGlNu1M7/PBNBTOv5Ca2lKZX3bPwmZRnoGlfFYg4HsJM9x3Iycq0/LpoY9yVm0iFfVxcvDI8kjulFbgKzJgr5FUbBVccE4+cqX4V17V7nGr4flDh8nr0Ne25a1SgTfZCW/MUbrsw0y6oZ3fQWufz75UYxjEo+B6uRY7GZ2LTvvyNqLt/kVutF9jPTb43ynQ/UfQxsfxhwM24K4dWhX1MXK4qLhrbDgpM3qD049q3fi47F41pqurVjLhKzjz2GKZ9a95V87f9k+1cjuSi/Jh2fGs7vxHM33E/AlEsEDoan0kN++U18PLtMb/yBYiNvN06LyuXGbAjPAp2YJBLz7//+u+iyRXBOg+e8k7uz5djwEUDJ9jj69uwYDBKwZM3PRhZBVccE4+cqX4VV/+bw/2Z4LqBzZeN81iGPe0neeNQQt16kE1xu3B5cTIMlL3OFdw6n23utsxjGMegUCO0Gzwan5lN+9flKx/hfwRGPuYkikBxYIBLRcZdtSGOictVxUVjG0mBqTJsx+PKVcRFY/1X9WpGXCVnHpv3par5qJk/8MaBBjJTQw6eF+dNTbtvJnyra3MDDYQgKKYqAbEN5Awcl5fVpGkviRZg5mNOoQiUAIwdiGDjrhw0FbYCF8fEI/lLHztUOAb9MI9ZP6c+F431SqXrB/dZf653vCfl8PHZYN+7ei+sQh0pLC9QUnILvVjtbF3ZX+Wr07daeQZYtfJXZM3MrZuEZ5Rp34oTYOAXOO9JFIFwLcvBpLA00LirBu2YuFxVXDR+QChwZ8FUXSp4/YpIvPeq+mfEVXLmsXlPik8SNx9rtAWfvayYHExsrmLvelbF1vk8fbuujWEcg8KqXY4fjU+J6SQ8L6Z9+Uw7u9fBjgYZeKd1vpH11uhYn1kaYNyVQ6bCVuDimHhk3BH6vFU4Btg2U2DOVD/OlVOKi8Z6hXPF8fxmYT+XSgelFhz2eIad45/vH3JhMb1lYnubdi9XfvdF2G4/az+CtfL8utH4TG7YL054a9pXfZG9buzsECY+oj4jRrLMadxVG+K4uFxlXDS2JWbBjDANrS5uXFM8cqb6VVzf52FgPNPOTSp29uTefTF5mNiUkWctXt0jsnU+/16JYRyDYlH8Y9rjVNtFupt2JJ/RqO5Bdzfxnpo8ax9/uOEw7qoNekxcriouGtk+UX/4+ZprFq4Knrhh5bJz0Vj/ca44nt8s7OdS6aDUgsd+H9O+Ne5sb9n47WR51vK7QXfW7nHx1ehbzc+8RVVujb8iLp81ehaet/oo077VxGlYS/IGmHg7M/vKaznW9d/LX8DEfh2katCOi8tVxkVjx8UsmKoLoG/9XHYuul//Vb16n4eB9zLsJSONXF7WPWFdh+2qNi8xEC6+On2r1ecAUn8a46/IkpVfMwtPr2nfUwbZ/4CyXQ28x4Sz9X+ffv7+d9a4qwbtmLhcVVw0MMphv2LU5oJS1K+6VDCuWNSsRpWrDptXVb/UGvO839O052amdnFZ58y6Dp/U58jW+fiZ09jbHnXv9Wg0PjmuM3BMeJvftFv3U+1c2MHtYuIdfME37z+n79Pp73+/+Drwjbty0FTYClwcE4/0H8D5EeYYYNtLgXnM+jmluGisVypdZ8Qdh/N4hl2pzXZS92a8dOlZ9oVlDb6j2rzEQPj46vStbjUziA4tHsoZHrVYv/K1DOE/b27a1wo8ZnijgsPI2xlYV+bXbU37Uhlg3FWDdkxcriouGt+QCtxZMFWXCl6/IhLvvar+GXGVnHns8Uw7Pqnc/KGGPUXdXloWbpY19sr6fJadnztrN8rKtNa51qPR+JT4zsJza3q/TqfzL/vHsWut2/251fwWQJsaeA/357WpaQeMu2rQZsLFueKRvsO37YHKVYXvUwWuAhPvFZedi+6rK64BznONVOmg5Mxhj2fYOf58Ty29ncm0K2d2T21fXt/qVjPDTJu/IiabPXYWnkmFy5v2bqZ9y8VjgjNdMxp4ngW/4sH2ujZn2neMu2rQjovLVcZFYwfGLJiqC6Bv/Vx2Lrpf/1W9shg7TIVrlEJfG+fxTLtSm22PjpbH1n9mavdj7XraV/boJ6pYTFVoNnvcLDxT036+mfbz4zj1+FC7gCmxGCSjeb+YZpoBv+J6i/3cP9Oepsx8VEY1aMfE5ariorH5UGCqzI+CqwITr5/LzkVj/ce54nhqk6LSQakFj/2epl3Z23SCW+bi+8/vt9wKX42+1b1q3lPOX1FMX2oos/DM1LGY2uVN++WfxXSutaR/C1BNA/XPbYb4hZXRxPPZ8RV7pv3SlcfXQaoG7bi4XGVcNDbys2CqLoC+9XPZueh+/Vf16n0eBsYz7Oqeqnv7jobd3zP/ieNHwM4xNGo0PiXes/CsmfZaXwpGFPentQTEzwOSDmLga6Z9Y9xVg3ZMXK4qLhqfVAXuLJj+Sy2vM1Y/FqU2MxyLvnOl6pdaY573eKZdNSfbiWqRo0Wvc7ukZW0xNcYwjkHBz51a5Gh8RpmVmm7Mz8+n09f6pp1Zl8ZmTHSAr8YYBSRqYuBL3x5T/njMtv7z77/+m2hHiGAln1/FueKR/KWPDSbHoB/mMevn1OeisV6pdJ0RdxzO4xl2pTY9TLtqL5V2Xet8o5j2XnWP1gf8NL5GjqYbyz/KtJfyboxqgLfGqnMmMhh4PuNjBfKmfa1bYNxVA9wfF2eAR2IDGHOo53MpuCowVYcjzlUR2b//Kl2VuGNhj2fa8Unl5i+NPloe5TmLKG3X076yx0MYooV6j6MckLgY9ZFMmhi1ad9jfTOuvOMlpTAmMJj3hRiX7bz7i6i5QoONu2qAFbg4Jh6pOmw4BthEKzBnqh/nyinFRWO9wrnieGqTotJBqQWHPZ5h5/jzs6KemRwj5Rz1ehCJzetXyI9gn6XePfcwH003Sy09TfuWb4s38pydflHTYOKRjJcpuv2NqGgHg4y7aoD743IMuGisSbNgqkxD3/q57Fx0v/6reqU2dgp9bZzHM+1KbbaTerQ8tv5jexeJsutpX9mjn4gW6nMJ5YDExaiPZNLFLKb9D/r9sI7Pirx8q406izGBwbwvlZSyWUz7Bc//GXfVAPfF5bJz0dhIKjBVB6OCqwITr5/LzkVj/ce54nhqk6LSQakFj/2epl3Z23SCW+bi+8/vt9wKX42+1epzwKJQTEWWzNyaWXjWqhrVtCc21+iva9U/fm5MYDDwaaaf5SL5/f9Mvx/hMO6qAe6PyzHgorGBmgVTden1rZ/LzkVj/Z9JV7UJUOnLazyeYedrwOdvG6nsQY886pmtqezT07e61czUNOjZd4bbjDxr9a2mfS/OaGhrqeGft/6qSWO9RgPvMe2XRxvbG3f/0ZHvX19cLjsXDc+r5LfTFVwVmKpLBeOKRakvfY5F37lS9UutMc97PNOumpNeRqVFPb1qi8kbo1AMCn7u1CJH45PjOwPHms7rzxHTPqKhb/WXPxkMPGveHW/a713kjbtqiBW4OCYeyV/62JbiGPTDPGb9nPpcNNYrla4z4o7DeTzDrtQmxmDi896qlh51pSr4zgzf6h4616bAX1EtQ8zPZ+GJVOs17SMY+lbfES8y8AGmfekC8cZdNcD9cXEGeCSyjR4xCtxZMFWXCl6/IrJ//1W6KnHHwh7PtOOTys1frMnEc7eqZ2XUOp8/bwzjGBS8r7XI0fiU+M7Cs6b3xerdfhEViY2KMZhfOrXy22iM/Etv4INMO2HcVQOswMUx8UiVoeAY4DOtwJ0FE+8VVxEX3bdXuAY4T78JqedSaczpMZ5h5/jXdR7BqCh73etBJDavXyE/gn2WcitH4zPCPohVOI/Ww7SX6jKa4apMgxn41LwHmnbAuKs2Wn9cjgEXXZ2xS8AsmDNxxTXFI2eqX8VVbdq5bmD7y8Z5PNOu1Gar5NHy2PrPzdZetF1P+8oe/UQVi6kKzWaPm4UnWuFIpv1iOVHihjj15+AN3BcDH2zaK8ZdNcB9cbnsXDQ2aQpMlWFTcFVg4vVz2blorP84VxxPbVJUOii14LHf07Qre5tOcMtcfP/5/ZZb4avRt1p9DlgUiqnIkplbMwtPpqrRTHuOu8EMQxKoTLyBr/ErH/fKzHzGXTXA/XE5Blw0NEuft+yYTFQU3ic8UnnpcyxwKT64D604LcYz7Mr5204UpxM+i+9o2P0983fDj2DvcfxDTCyXPbTRdIuofDHtX+I33BE8txgGUwxRUHyMBuG6fE/7f0g+XZEYd9UA98XlsnPR0NxcghS4s2D2rZ9TiYvu23+VrkrcsbDHM+2q+eth2NW9Hsks2vtmX9mrp8ipF1MVkskeMwNHY3VD/o2oTC2IMUbxVN9Es8dRZ9qXqjfGXTXEClwcE49UXTIcA2wUFZjHrJ9TiovGeqXSdUbccTiPZ9iV2vQweKq9VNp1rfOtPHx5fatbzQx+0mleUDH50Vi/8mim5nHTm/ZUMaGJD4PeAmlN+824/5togkWwxJtrnAEeyW1CBe4smKpLBa8fj+zPtf9cqTSIMTj7+pCd5sJFf1p2ZDNNC8yN/0t063z+mY5hHIPiFH+zfDQ+oz3kxSldRDr/Yfz9zzAHKywymmP0R2gWPL1pFxp3xQbGMfFIlVnhGOCTrsCdBRPvFVcRF923V7gGOE+/CannUmnM6fF5y17vlD9C2euUXctccQ9aftZ+BH+f4/SI5bKHNppuwZWbTTvDI9o8M7m3sVE8gn+JVfSZ9lSl8++/It+4qzYGjotHcpc+Pl4cAwxXgXnM+jmluOjj9Upt2hX62jiPZ9qV2vQwVa3qsfUf27tIlL1O+8oe/US0UN0haG4mLkZ9JmPT2CamPVdRlIH2qBXBIcDANzLti1KBxl21MTBcLEp56HMM8DFV4M6CiV8MXEVcdN9e4RrgPJX7YDzs9zTtqhnPTVnLXMr9sLeDfDX6VrfYU+zpEVMRm5WPn4UnX9l9RTfTPpKRjzDvaz03LAayoWkPMu6qjYHj4pGqQ59jgG1RBeYx6+eU4qKxXql0/eA+68/1bjzDruznVilOJ3zG08hWeXobV1+dvtWtZoaZAn9FTDZ77Cw87RWeFtMe/Q9jWJ9ymxcGVhDFgTDv3/9x/UuWGv7jfOOuIovhYlHqQ59jgfV2FkzVpYLXr4jEejTjXKn6pdaC582fo/gkcfPRQpuWOXrk4vtv61FulX0u7Ct7PIShisVUhWazxc3A0VbZs0/u+D3tkD+GggKEyEFE5d4x8MuY/bQ37Uu1DuOu2Bw4Jh6pOvQ5Bth0KjCPWT+nFBeN9Uql64y443Aez7Artelh8FR7qbTrWueLeTDxs/Yj4OcYEjkan9HmBdEwMOY8umlXmmlGxwgDnzHvHU270birNjCOq4hkRkHzXbV4VThXBabKiOBc8cj+XPFeqbgqccfCHs+0c5PKzcrHtNv1qq20982+skc/azrEPMSgWfxxMer7eYgRepr2tbQIP3wKAQHFjsi1YvycTj9/N/94zLZQ8o27YmPgmHikylBwDMCJEn1PtIKrAhPvFZedi+7bK1wDnGeLS1elMacHb9g5fF7zFvitcrSYo1Rh5VztddOX17e6dT+RqfZXhGTxx8zC01/paQTTXirD7I3NCw2CBuTqbNqXokHjrtoYOC4eqToAOQbYRCkwj1k/pxQXfbxeqc2WQl8bZ960K7mr9l5uQtV12PqB7SWdebbnt+tpX7llG4Nir3+Uhye2gtF0Y/kT8WbTvmi0eVvc8k037ZPpBYSA21BjngFMO2jcVRsDw8WilJcMxwCfIgXuLJi4weEq4qL79grXAOep3AfjYb+naVfNeM+HgxazVdpFPj19q3vWrdGDP6usK2KUt2Zvvu5i2nv8YzS4Oao0FL3AIBCRYxDTXjHuqo2B4+KRKhPEMcCmRoF5zPo5pbhorFcqXT+4z/pzvRvPsCv7uVWK0wmf8TSyVZ7extVXp291q5lhpsBfEZPNHjsLT3uFTyuPYNpTKWC/DAc6xa7kGci07xh31cbAcLEo9aHPscCmZhZM1aWC16+IxHo041yp+qXWguc9nmnHJ5Wbvx6Gne+HvaYWs7XHzt43+8pePUW6FFMVkskeMwNHe3XZld1Mu9lp8wLA3hwO5DlcVhTwBzPtBeOu2Bw4Jh6pumQ4BtiEKDCPWT+nFBeN9Uql64y4Ss4c9niGneOPz14aqZrxXnnmNewxHW/VT3TiRuNT4j0LT1R3IG4Y017iKjDSMCQcCAi9DUlwBzTtiXFXbQwcVxHJdQ1ngOPOghlzLb3qgtePR/bnivdfxVWJOxb2eKadm1RuVrbRR8szr2mP6UQMin2eej+sWZmPppu1DmLd8KZ9r5YgUw3BQEGE8JvQn99dv/JxV+Hff/3b8qWUtsKqqzBcLEp56HMMqmXfAxS4s2Di5o+riIvu2ytcA5ynch+Mhc0bdqXeLbRpmaNHrhb9ye0k35nhW92r5r0TxV8Rf15ZVszC01LbzpqpTXupLofBhpZCQXijvn+LvqYbp1Ax7v9VsDtwSDxSdQByDDDZFZjHrJ9Tios+Xq/UZkuhr40zb9qV3FV7L95kYjPfsh5b//E6apH2ubCv3HKKQalVif98ND4l5rPwxJWHIg9p2tPKHSa7urQaUG/DwG/aV/Ln339FG3dsw2FRykOfY1Dv9mxc+9bPZeei+/ZKaYpUOig589jvadqVvU13RMtcfP/x/bsX6avRt1p5F1jVianImh1fNwtPvCI48i1Me4CJh7w5FPTamglM+0I60LjjGw6PVB36HANs4ykwj1k/pxQXjfVKpesH91l/rnfjGXZlP7dKcTrhM/6Oht3fM383/Aj2HudWjsanVN0sPGO7c0E7L0bTaDYFdPpAkvVD4VDQtdzBPx6z7UmQccc2HBa10uOi8UFT4M6C6b/U8jrj9Ssi8d6r6p8RV8mZxx7PtOOTys1fD8PO98Nek/r8rjGz982+sldPa1r06DvCKY2JUd6SeYg1H9OetIEw2+tKaMlO0ESm/fKc5/uoDL7h8EjVYcMxwDa0AvOY9XNKcdFYr1S6zoir5Mxhj2fYOf747PUyK6q9VKq8db6YBwU/az+CfZZyK0fjM9q8xKptRvuY9h3pIDf+WA+FZ4ImM+1O444fDIpIbqPgDHBcBabKNCi44ph45Ez1q7gqccfCHs+0c5OKnxUf027XCllp75t95ZZXDApSKRYzGp+Pac8rcL59RAbr6ntHQa4cNPEbrAlNu8O4YwcDFhXzxiQ/1BwDfGMocGfBxM0fVxEX3bdXuAY4T+U+GAubN+xKvVto0zJHj1wt+pPbSb4zw7e6V817J4q/Iv68sqyYhaelNnTNx7SjSj3HEQa+Fvr9PfRXPu7pQ35UBt9weKTqAOQYYEOkwDxm/ZxSXPTxeqU2Wwp9bZx5067krtp78SYTm/mW9dj6j9dRi7TPhX3lllMMSq1K/Oej8Skxn4Unrjwf+THtvGbpiporv8WXwiY27UtlhHHHNhwWpTz0OQb4AClwZ8HEDQFXERfdt1e4BjhP5T4YD/s9Tbtqxns+HLSYLY3xi+lGDAp/Tmg0ieOxhzSaZm2qfs1iNe2sfqCx7SVDWF6wzm3Y5KYdNO74wOCRKhPEMcBmR4F5zPo5pbhorFcqXT+4z/pzvRvPsCv7uVWK0wmf8TSyVZ6eht3fM79KfgR7j0d4ULOyH003ax3edTXTTuqUhoMe9lEFvcArgHA9WMvPvB+P2YpXeeOODRIWpT70ORbYBM2C6b/U8nrg9SsisR7NOFeqfqm14HmPZ9rxSeXmr4dh5/thr6nFbO2xs/fNvrJXT5EuxVSFZLLHzMDRXh23MjXtFW0s0oHeFecdDoinNkfucD6IaV+kKRh3fGrwSNUlwzHA5kGBecz6OaW4aKxXKl1nxFVy5rDHM+wcf3z20kjVjPfKM69hj+l4q36iEzcanxLvWXiiunviFtO+s34EqSiPTgV7hDOuzfA7kGkvGHd8ihSRXKdwBjiuAjPmCnmtQcEVx8QjZ6pfxVWJOxb2eKadm1T8rOhlplvVM69pj1EoBsU+T73my8t4NN289XjWJ6Z9Rml2ffrIJv7G7WCmPWPcsanCopSHPscA33YK3FkwcfPHVcRF9+0VrgHOU7kPxsLmDbtS7xbatMzRI1eL/uR2ku/M8K3uVfPeieKviD+vLCtm4WmpzbJmY9qPKE3Rsw9k5i+X0vHEv31UBi8Mj1QdgBwDbLspMI9ZP6cUF328XqnNlkJfG2fetCu5q/ZevMnEZr5lPbb+43XUIu1zYV+55RSDUqsS//lofErMZ+GJK++LHMi8+grBV2dL7qjDQU370pDz77/+C7TjoKB7i7lobDIUmKoL8XhcuYq4aKz/ql7NiKvkzGO/p2lXzXjPh4Oept2np291z7r3Tr+YqvDz1RI5A0dLXZ41Hc2qh3bk2t5v4w9s2mHjzm1NLhqblVkwedMzQ/2c+lw0Vr9K1w/us/5c78Yz7Mp+bpXidMJnPI1slae3cfXV6VvdamaYKfBXxGSzx87C014hv/Jj2p8062HgD27aq8ad25ZcNL4hFLizYKouFbx+RSTee1X9M+IqOfPY45l2fFK5+eth2Pl+2Gua17THdDwGxa9/7z4wFYymGcNdGfsx7bvqtvgl1zcw7bvGnduaXDS2dWbBVF2yivpxrlx2LhrrP84Vx1NfjiodlFpw2OMZdo4/PyvqmckxUs5Rmq9lrriHID9rP4J9lnr33MN8NN08tUSu/Zh2Sk3F5+HfxLQXjTu+NfFIqqmS3wJ+d654/XikyjRxDPDZUuGqdFDi8tjjmXZlP+NM5hjzOZJZtPfNvrJHP9HOx1SFZrPHzcLTXqFt5ce023S7rXqRz6DnG5n2F+PObUsuGmusApM3KP249q2fy85FY5qqejUjrpIzj/2epl014yOY6Ja1rfX6cvpW8zOPn1nWSH9F1szcull4clXFRBtMZkziY6Lc5SR0fTPT/mTcua3JRWMTNgum6gLoWz+XnYvu139Vr2KMSFkXhb42zu9p2NVzs+28stcjPCDY5i5eodY610690fiU+M7Cs6a34ueEuVSkPyrmk6wVjd/QtF+M+9/g10FeZ0SxiRWYH65orzj1uWj8XPngPrRSacHvifc07Ur90x3RMhfff3z/7kX6avSt9j80xGjQu++WKmKUt2SeY83HtMv7VDPwb2raSeOu2MizYKouvb71c9m5aHxTf3B1D8U24zKeYVftv16GSjXzo7019dXpW91qZvCTDn2ZwiBqYv3Ka3iNgvox7U07kTPwb2zaQeOu2sQK3FkwVZcKXr8iktvMOINj46pm4SimXTUn26lqkcPWD272c9Eta4vRNIZxDIpf/159tzIfTTdrHcp1H+OuVLeIvcp+GdH3ntPKR2UU4syCqRoORf04Vy47F41v5g/uQyuVFvhMXCJpGvQCfDzukUfJ0cO8tdAu/kHBz9qPYBjUnSWj8SlRnYVnbHd4tI9p5zWLXvGZ1R3jrhBnFkzO9OBj2bd+LjsXjWmgwFT1Sm22VFrweoxn2pXaxLwVxuZdPUOjmTB73+wre/QT7X5MVWg2e9wsPO0Vxqyc1bSn/Z21Dv5ui+n7eCgZ467YxApMVRMVXBWYeP1cdi4aH+kP7kMrlRb4TNwtJU2FXoCPyCVSjd8qxzuZdl/PfKtb9xMZZ39FSBZ/zCw8/ZX6EXqaXXWfetbGdEatA8Olb+z/B9cB5cRdypkkAAAAAElFTkSuQmCC',
      arrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAACF0lEQVRYR8XXTYhOURzH8e8PC4qSQpSNspCykYzBNKVGXhZTbCYpFibKxIIVpajZsCBqppmFl5IoRaGEQl7DgrKhWCikiCxI8dOp//qZc+898zjbZ/H93HPvff7ninFYticAJ4C/kgZaJTQOfWyfBbYAzyQtbSvAdrryXcB7YJmkj20D2D4MHABSdIWkd2PtcLFbYHsvcAT4CnRIejNWPP1eBGB7OzACfAe6JL3MiRcB2E4P2xngZ8Sf58YbA2z3ApeA38AaSfeqxBsBbK8DrkRwraRbVeO1Aba7gJvARGCTpMt14rUAtjuA28AUoE/ShbrxygDbi4EHwFSgX9Jok3glgO2FwH1gBjAg6WTTeDbA9oK48pnAQUmHSsSzALbnAU+AOcBRSftKxXMBn4DZwKik/pLxXMCPeOhOS9r2PwBzgcdAuhXHJe0picgaRrbnA4+AWcB+SYOlEFmAFLO9KF7D6cBOScMlENmAQKTj1Z34F9ws6XxTRCVAILqBGzEHeiVdbYKoDAjEBiANoD8xhtOu1Fq1AIHoA87FQaRb0tM6gtqAQOwAhoBvwEpJr6oiGgECkU7B6TT8GVgu6W0VRGNAII4Bu+NbIJ2IP+QiigACcQrYCrwGOiV9yUGUBKTvwYvARuAFsEpSmiMtVzFA7MIk4BrQAzwEVkv61UpQFBCIyXFm7ASuS1rfVkAgpgF30wiRtKQV4B/AmK4hgCtJbgAAAABJRU5ErkJggg==',
      share: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAEC0lEQVRYR72Ye+hfcxjHX++5jFFsLpmaRTZGRJFRLn/4g0wmabI05B4Wq40Suc3IRsM/JIXMNSMtpWRF7qyU5PLPkjuR+1wevfWc+vzO93x/zvfnd87z3/l8nnPe7/N8nutH9CQRMUXS323h1FZxInoRsRNwGXAWsA/wK/AOcJekp8b7ZmfEImJ/4Blg7hACDwNnS/qrab8TYhGxA7AJ2LcA9TFOqZFYJenqPoktA25PwB+Bi4HHgZ2BG4CLcm8LMFvSF3VyXVnsNeCIBLtQ0r0VcEQY80XguFxbKmltX8S+BWYk2My6RSLiCmBN7t8j6dK+iH0GzEyweZI+KIEj4nrg2lxbI8lHP0a6OsqNwDGJ9Lqk+cVRTs/A2CvXFkmy/3VHLCJ2BVYC5wHlTztnPQrsAlxZpJCvgFmSHASTTywitgYuAG7OyGvKAPW1AGytJzpJFxFxLHAncEgNYAPwPXBmA/APwDmSnh72BxP2sYjYM3PVGbVj+xhwCjAxIuJAYDEwB/gZeAt4SJLJDZVxiUXENsCRWee+BN4FvgMc7tcAOxZf/in9a3WTz7Q521JnKLGIWATcUYS933NZscPuUQNaByyX9OmoBEY6yoiwNW5sAfIecLmkl1rojqQyYLGIOBpwHqr2Ps8SMgvwXrX+NjBf0p8jIbZUbiL2LHByvv8CcKokO60d+XTgsSTncN9P0kctsUZSayLmbqBy6rl14Ih4DjgpUZZIenAkxJbKY4hFxPbAL/nuFklTB7JixCpgRa6vkHRbS6yR1Jos9k2WDn+oqQA7P53Yq8XSj9YDpySwo21B4WPO4m6J/UP2sTmSPhnJFC2VmyzmTuCVog3+GngZcKavmr/q888DyyS93xKvtVpjgo2I5cCtLb/idOEO9TpJdoNJkfEyv2ugyVV9kwF/y979D2AJ4K6iEhfsm3I0G2hjRmX7X7VyK+CwJOcauUmS22bntAOA1cAJNdAPs4j7mK3nersAOBRwk+i8t17S5vHITri7qD4aEQ4UT0TlqOZtE3Oydnmzf5bi4/cAcpUkW39A/jextMq2OXGbhEe0tuJ+7DRJjvAxMinECuu5tfag4W629D+3TPcDDo6FxTzgVxdLeqRTYgXBcgpyIOwtyZOTfc7GuBu4JPVflXRUX8Qe8L1EgjnPVTPkv0sRsVv2dX60v21Xv8OY1KMsLPakfSefB47KV1JZk12L3XxOk/R7abWuiJWFfp2kMQNJRLg7cZdi2Sxpdl9HeTjwRgHmRH0L4JbKOe0+YPfcXytpaS/E0o88B7h6lOJAcGqpxHX44N5ue5LYtLy4O75ujXx2lC6U9GbTfic+VgSBnfxc4HzgoLSWS5FvGldKssUapVNidUTXzWElqK77D7TATjabjI3rAAAAAElFTkSuQmCC',
      squareItem: [{ buy: '购买0费率', adv: '持有30天赎回0费率', squarePicture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABKCAYAAAAc0MJxAAAQ6UlEQVR4Xu1ca5Ac1XX+Tvd0zz70REiAyo5LICGsRY8Vi7S7AmFJGEQcQDjIEgqOCC4nNjGKcSVxYqeMoCp2pSq2A7KBKleq7CqCZHBigWUTqdAuECQh9Fj02BXFriQXTiRYgaVFK+1MT/c9qduP6duPmR3tzCpVKd39s9PT997TX3/n3HPOPXcIl1pFCFBFd126CZeAqpAEl4C6BFSFCFR426gyil/HZKtgfgYa5kNrmE2ZSTNANAXgCb58Z8DoZ3AvGIeg8X7T1l+lxX2nKpT/ot1Wc6C4E2MKwrhPkHY/gW8CoEFvABmXxx6Kkw/J7jUB8BsAnjWswY205NTgRUOjzEQ1A4o7McFi828APAQgYAygjwEZkxQRUgCCcs0DC/CuneHCB0+Z+vl/pptx+v8SsKqB4vXQCjcbf8FE340AJJ9KHwsyfSa5AERAKM0wBSy2TwPO+TNM9A/Z1/JP03rJuIvfqgLqfGf9JzJs/5RByxKiZ8aDjMuilyNgFVmTDpgPFjvnAAkWACLutLXM2oZbhn53saEaMVC5zuxyYv43ADE0AGQmKCDFVC2qWqqapTw7A+yA8yfU704z0Zq6Jfn/vJhgjQgoa7vxZSZ6SkKSENaYBMoEJkoFqZTxLqeOHus4/z7ABXUqm5gfMpcVfnKxwLpgoPId5t8DkPYo2YzJIMMHqahmlRjv8mCx/TFgn0mb8dvZpVa6LDVG8IKAynVk1xH4iTQZyLwCyIxLsUmBLYoDFl/pygDKdlz9ivMw0yN1y/L/UmNcEsNVDFSuI3sXgTdLmxodhUDZKwFdghRjRtEelQJLMegJQx/M4o3J1vuAsNLwkDfck11qvTiaYFUEVO617AxyeA+A8UmQprpuQAhSCigJA16OXemrPztngcLv07HIjBtgc9yNda3HekcLrGGB4udhWpebbwGYGxGCNFD2E4DeCKSuZApbAqZd0H1xMAU499+xFyL9/nrPoWUcNCbW30hN3am0qxbAYYHKd5qPgrE+CdIfQIYmYZNLeZpRTrmWqmal1DYEjCWjJLOCRgZc2+g3AtYbi449Vi0oqTa43KD5V8xZ0LAfQDaURgfVfcp9kxGQPEviXSrJHEWtKgE1buPYAudPenOQ7oFEvofC7th5drT52cV9PbUGqyyj8h3mFgCfC0HKgOqnAVqdAkiar6Syy0VOELBFgF9gB7uyNNb1IPN8dirpok1jsZJBf+QG0GrclwI8W/2AyIHMKb4cURVlFr/OLjomx6ppKwlU4ZVMm9C0nRGa118DaGaUSSXVyF+tWGyDwF9l2w+9U5a9O2dfB9hPEOE2/y34t8dYKnJgLoD0MSmM9lhNwCKj/Wgoew0gKwmU1WFsZ9BSdw7NBNXPAMjwp1RXJvVBIuxyiMXfZRYe/D5RhCYlxWYG2btnfYOZ/0mG1KlBdEQdFXVXVR/cYbYfTcafVQCWCpT1mtHMDknb5NKbGiRIsWjFtQmlDDUcZr4/29q1aSSy5d/89GpiPOuBFYBRKmYMZkjIMt9s7+sayfxpfVKBynVknyLwV92Em8ukACQ1VRIY7eCa6mmLdebCrg3VCGntmvUwgCeTLyPu0cdnKcrzjNl+9KvVyKD2TQDFe2FYH5v90MdMoIZrAdL8l6q+sTRhi+q4yVyw975aCGjtatoI8OpUm5Wa7FPZxWcMc/wUatkXiaZHKlcCqNz27G2UGbOVGmf6zJegxJf1Uj6P+NCwjZnUvquEC31hYvLOpssKhHcAnhz2HO6FhXcSi9uNRX3bLmzW9LuTQL025cda47UPRUO6GFiu2YipnPzMWGcufKsqlYuLGapg3BaphryUOvIzZntvTdQvAZS1b3EvQNOLdE84hmnsYqkIJ83B+qtpyau5WrzBIiy/mZ61LsseJ+arouOW8N+KL9G9+6jZ/q7/LNVJFQGK9988ucCZ/qRNSAtPFMPOAkz8eLZl96PViZPeO/9m02ME+o5r2Eu6BynAMcPIGFfRgu73q5UrAlRu39I7NaKXQoFS7EHCwfQYJohn1N2wq69agdL653Zcf42mU5+3AsbDJFUFk+rIzJ/Ptr/7y2rligCV33freiL2WREIFHcuU9nVY7bsaKpWmHL9rTdndwOYFQEromYxP8sHlYkez7a+UzXTI0BZ+2/dRMAqP/iIvb0Ytb0g1L+HN5gtO9aNKlC7Zz8JhvSt/JYWYMfZ5X7eZLa9U7W7EgGq0HXbTrBoUx/YNdMRusddAykwPWC2vP6z0QVq3lqw+Gk0aC4FViAJy8V7l9l6pL1a2SJA2V23H2NgmrerHTLI+08VKqaO7LSaLW/srlaYsqq3Z+4CCOyOGvNSbkvkVR83245cXa1sMUbdfhqgCSEooT0qp44GO1Op5b/8RFG1IqX35703XFVwbG+DL5GLj73I6D39ZltPmN0boXhRoN6+w3FzQq4gqg0KhYuooi+QUcdjqenVUS2m4ANzGgs5bTCieqnJv1jOnXnQbOuRSf2qWgyoz7nuddI98CePqGNou8z5HcOmlKuS0u9s7W4OlmL/StxVSA9vzNbuquWLA+UxyuO3TExGBUrYLk8wI+tcJEbpKaxVX6xixMMU0KDZ2l1jRh248zRYFnmluQJxz9ezX/LPEJmp1LJ1dG3UWzdeWYA4WXIDo/hSVbDc//vN1kO1tVH2gbuOMXhaaDAV16AYPij09oVjobWaLdtGedVrWQBmb45UY57GLPfm42broRqvegdW7AQCPyrNAw/cBpVdAgTxQKZ5+yj7UQvWgqQfFZiFUkExA8IGxBDYGQKL/Ieac9bNdDLoQxCOQ2CPqVuv0hKkFjSk2dOoZ35gheuZh6qnGvG4oynNmdeIeENm3rbR9cz3LHgSzA+XTj+zLDgDFz4CbLn3Fw9pEo+fB2OLpokfGkvtHcMtNlFjfvCPHwPEd8qrnmLgw+W5x2jeOrqx3p6FhwE0JRcYyfwCOHcCkFUvI2ubHU1/uGHJkNyKTm1Rz/zgvXezW4gRZ1Ja2KLaKobj0PS6lpePjkzO8r1ye9uv0dgJMxNqBsP+GJx7zy04SzTbvxyISu6+aVpVl+z6ETPdX3dreoFaLB+1crKd4X6PtnHfKTDsaVkF997HjXkvVx2lp0GW39O+niCzGjGDbZ8GD70XVTNpRnOy+KyM9knAsgDVUdEZ8ue1wfhi9lYrsXuUcMTsQyt7mXl61BbEDbsKVpFZJzONPI1mvCxFrFnj3juy1sDAcQKuirgG9gD4/LEIGjzkgVQ0T7rPIN8zdBduG0BAPglYPYH8je8ALCa6u25p/jfqQ6QAteoplltV0TSK3ycALN1tYGCdOe/Xtc2Z71n0NRBtiDBcWOBzR/yn9n3jQfaqFzWPKSQ3tAOA4q9Nss7yQRXevi6NkagVb/zI0fR5qs1KAFU4tEpuaW9N0FxVx2C8ol0ohhKnMkbhOmraWqNdmLbLCmYmtgsjwOf7gEK4svNZDyTJDMmQio9Cyfc+xOAcXGBdsMK2ObvMuif4mLKv9+eGnR3o97IIgU2IJOmSyl/cNWYw8yZz3paqE2VSQGvfLRsBrI7Ens4geDAsVik+aKPPohEovcuucxwC7Y+hkbgpcB1Sg0W7e83TzPyVcEtK9lRzVHHDHv3MLB4252350QhkLnax9i39GiA2RGylDJmGesPKOwcQAwwaS2FZRDCCVg9t4i0QA7sAe8C7akyCNq4F4nRnoszRBWuQoY2n4kY+gH/PLrXulV3Taw+6VzcT6/tDuxBPuQR1B3G3obhaygKNP8nM2/LzkYCV37dsFZFbw65H9g+lJR7cW1yRxVnPWJNaz+ZPqM98GtqU1eCho7APLHf7ZOa9AqqbBnHiJ3CO/m1CNHHOG1oLQ2jL4MIV0oMvXfZzeM12uNUsqStcSiomHmsJB4xvZua+9IMLqmbp+uw3GCJWzeKWWAH2R0DOrx6SkcoZQJuY/ir0GU9Am7zC/ZJlBOOcBU1Y7H4WHzwH56isAo81OeZpf0wfGSa6J7vY2lwGqLWLAOeN6K5HyqqXqGqJ772JrYL469k5vypbH5U7sHym7jhPAHy7+3DeI0by9VT4LWB7pztcP0muWGrhn/rcmfHQZ/6rV2SiNM6/B6dnTbTEUflenPdZGtYYfs+4yf5W2YRWoftPtwDsVdzF3YU4QPFDQZG0h/zAvyLGL2zN2Zkde86ruDvbODXjiDYGrwT4zrDiTk1Bh+pN1hFAeEf5xCCgNaYYD9KgXbEWlP0kkJkEarw+CpS0cdYJVyVF/3PJhUkAEiytWKdGLxjthS+UBSrf/WezNDhdAHtldpF6g/I+lSdd3LtXaqoStQvx/cOwr5d+Bsg+CBKe58E56SgmA19q+DS0T0Vrc5M65kt3/Jvg3G+TGhgde2um1Vk+bIq00P3AerjhQ6lMQpAJjSf7ArWpBCxfzaIsjOz8MDM0520Q+0DZydo294m1BmhT1wGytLtcyx2HOCn92GS1tXQP3ZjQJSxv1Rbw8EBx90rTpsY9YMwJff+0apaAEWnbWv5qGEm4+eyKq2xRzWMrqswmicMg/qA8APJb43KQMcW7r+4aUGOzx8KzbwGWjA39kxB2RX7x83qLWDUso+SguZ4HZuigPWAeHwmYS6pjKbB8dYzsnpTLeSmqCoaGPjfvVrbpE0CTvlAmfvF7y+Ntv98IOOU3jxj0Xb1ZfLsioOTQds+XVjD4PwCmcDurGLoEsyubEmnxoK9iiZ2e6OqWrC/wACP0Q6O3hwFqHGicrHMd7tEEeGCbZ7nLNGJeQc14cbjRIkNYPQ9+nYAfehcDY66qUBwI4Z2Ksk6Axt6g9PGZVVSzoF8ZJrr2q4CM3hndtU57SLlkBQcG9ImAcaV3V+F/AMdP7kmAXA+zbMvDwhXUgoELAsqdq/vBb4HoH6OhheKpu5V3trua8Pl3XWpT/dWgMfIoTdwpDeyQmotX2ZWMCHS9C0QV2Kng+SVQGb8GTQIl/HBmOIjk94QX6HpIPR6Wn6nDWUce/DKxPAHKwfkKz3WQCf3zveChPvd0QdBo/CLlzIoSF8ZrrdxshGrEVRfEYx3RGejKOYFhn1drADT/UJhzRqYLhu0SCo6FdD3kgamRAeUx60vLQXgO4IkyPJD5IR46FuaIiihp0C6/y1/LS4RDEbcgzq5AzYtqeTqjd3QDQ/I3FSpoWqiGQoIU23IvNQLjFzQHK4uPUcFMJW/hQ3d/sjDY/TMe+t2SUrse8swKTfhMNByJ2LcACNX4q5mKiPp1ZjRtLfQXbTg4AECpFq7mSRJ9T0HHXJqF4qbuBduo+JDu7x4sNr4ioD1G4PjPZYDGNsOtVy+6BDEPvMimwE1Qd3mK/3/IwKPGnOZniNa7F7kbSyHwMgD1cE4t0LKg4Q5qQoc6WNVABYO5v6QhzL8G4S/VH4rQJv2he7zfWyWDZHXcdYglBkPbJY3KjzKO+D41b05sVvJh3AuGTMfUCizppn+RZuP5OOI1A0oBbEyBjTXub7No9Yu0yff4ZUS+ikWKP+LugGvDBEjsYIFnDTO/ka57STnJmCQMH8YSMGTeq1o1PAWB+2gutqfRsuZAqZPw69MnO5MWL2GgGcRziMW1DPlrP2Kclyjhj8FC/uJPL4gOEosuvWB30vwXLujXfrgbV0JAnlRfNULd+zl0PKLapFFn1AgFrUk3PogbQHgEwOcBlMpUBXMNgfBLCPyA5mDfcAKMKqOGm3y0vudujIEDmc5sAUFm7oLT9QMue4G90PE6NaHiKsH/l0CNxgu4BFSFqF4C6hJQFSJQ4W3/C7pHrcNVnDM2AAAAAElFTkSuQmCC' }, { buy: '10元就能买', adv: '起购门槛低', squarePicture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABKCAYAAAAc0MJxAAAQhElEQVR4Xu1ce5AdVZn/ne6+987NTGYyw2Qmr8kTSiqQQRKIFKKrLEICFXxUEkMW2GVFRC2xBAqNWpqVWmsRl1LK8lGWilRFqaASHiahahdQNAkkq2RjdBdDQhIlZCbJ5DWZ++o+1unTp/vr06fv7TsJFInpf6Zv9+nT5/zO7/u+3/edThjOHpkQYJlanW2Es0BlJMEZBdTI5gXvgcfu9WxvaeslT+/LiEGmZmcEUCNbFk2FV7uP5ccvs1rPhTu08UfF+ev+NRMCGRud1kDxDUuKJfv4nbDzX3AmLi06ExYDvIbyto96bvnI/NbLfvk/GXFo2Oy0BerECws+yBh7wO5613Sn71aw/Phwsu7g06i++uDzxflr390QgYwNTjugypsWzvYsPMiK0/8xN+12WGP7DVPlqGy/A+7wjqVj3rH+0YxY1G122gA19PsPjCtUyystp+2TzuSbHXv8QoDZqZPzjv0vKv+3YlfL8InZ7L3PlU4WrLc8UJyvtEqbX/gIwL5q91zX7Uy+EcxpzzTv6o5/hzv0mxXF+ev/I9MDdRq9pYE68eJ1l1vwvsXGzrnYmXobrDGzmpovL7+O8raPHXV57W1t89e+3tTDWuO3JFDDW66ZaLn2f7JC9zKn7xZmd/0DMEptXNv7Q9T2//wHxUvX3nrGAMW3L8mXThy/Gyy/wpnwoTZn4hLAKp7M/AD3BMrbbvVq1SPz2uavfWm0nb1lGFV6ceEiDtxvd15+rtP3EbDCxJMdW/i8O7ge1d3ffK546forT1ugSpsXvM3j7GtWceqCXN9tsDrmqrmkAdU8gNxD+Y93wBt+RciFx0YDVvMvHc1bTEpn08L2ksU/x+y22+1JN+Sd3vdzMFsfT6PfoudMc/CObOWVl1fsaunEXHbeunKz08j0kmY7rdc+CPc3AOyLdvf7eoWzZk5HPRZlBYsb3mvRa9Ud96J2eOMXx1y67hvNzulNBWp403XzLMu71247f6499RNMJLDBYRpH7Jo7sLHAS/v8idvjr6iyYo8XPCsAEm0bAsrLr6G87ePHanZ13ti56wabASszUHz1Ensg/8erbLB3eUCfBWQOR5btFHLtHec7bRP6nEn/BKvrinCMDGLuaq7+ZTUmDsaYe3CLXfvzVxxeG4iN1em703WmLauFYDEwvxvamXiC8owBtb88jPKe1bvLQwe36kBZNr+7a9H2PSYAMwE1sGb2Ygvsfg42vZlVAGPIFccg1zYOTs81sHsXyXDPOcQ9eajZiJnGD3dwA2o7v5T6Srv3Fjgz/1nrL22aAYruCKp/ugsn9u+EV6vGGrtg/RM+sG1b00BxDnbgiQvvB8ddTQEkzCNfQH5sO+xxb4czaRlYvidGlpilxBCSP3j5EKpbl4evtXpvhtU5HygPorb320BNWo4zYyWsnndqw4tDnliAg8+ivOv7KA0dPDVADT5+4ZfBsbIZkJhtI9/WDqdtCpxJHwZrmx3whlGjIkSKLI2C5+5/Fu7eB/xn7b7PwJ5wVTgMfuI1VLd/VPbbch5ys+8D7BZtIVTziLEhZT0PlR1fQWn/H+CWo3x5VIwafLJ/LlxvS9bw65vZmFY4rZ1wehbCTzti2b22rqHp0WWIJlXb8SD40ef8m7l5a4iZylN3zyPwBn8qgZzyaVi9kZaMGXHCucgL3vE/ofLKAxg5dECaruhzNKY3uObCJwAsysImu9CCXOtY2J2Xwum5Fohl93Jg8cEH5lUZAst3Gl7B4O5+CN7QOglU/8MBY0i098qobr8DqB0EnHPgnH9fSl+UWfFFqe75Lsr7NqI2Mjw6oA6tntfh5ssDAPL1gLIcRwLUNgN27/WwilO1CEaWkzCIH38V3uB6yYYZHze+wht8Ht6+7/n3rIkfg9UjipVx0L3BX8N97TuyTfdi2FMW1xluciy8PIjKjq+idGgA3POaZ9TBxy682mN4Ou2tjFlwfDPrht19Faz2i0kUE9OxwMEli2ImxsBLr8Pd8fmwa2vyp2B1hmlL9Eq3hNrLnwfcIcDuhH3ulwhjokm7/78SvLJLgj7z38BaZ8g+tPemzcXd/yTK+55G9fix5oEaXDNnueMcW1WrtSZclNNShDOmDXbnZbC63g1mCSca00HamIJJBQP39j0FPvRU1CY/FfaMuwCbyLLgEX5wA7zXH5KM6boe1sTr430zBj78Ktxd90psinNgz/pMfMwNBBAXC7Lz6ygd/AuqrtucPBh4fM6Ssa07VwugSiUR1uWRb++A3TYLzvgFQP6c4Go0EhbTRoY1dMtw/3x30FkfUNkrQei5Ceycy4yL7r5yH1CRGtCa9lmwMdMiIBT4e34IfvxF2WbS7WDjNIbqwtNHNXqdd/hFVP76C4wcPdocUEfWTbm2pTDwS9FVpdKFUqnb71WwKd/dD7v3g8lgmEp1MqLqUXh7ZMi3pt4pz90jgN0Ba9pdQC7M+aJZjOyFt+frkjGtl8Dqu1l7NwOqR+C+8oVwAexZnzOCHl2kNBPnHoRjd0f2L+94z69kKNUOIzFL/11YxBgXUc8/XLeIUmk8XLcF+fZxyE1eCtYiHLfmCsLOaVpCdIww0sNbgOoQWM/V4EObwQ+slv10vA+s5xrjBPlffwJ+4neyXe+/gHXQnZcggg6sBz8s3ao1bQVQCCwhVYbIVykr8IZ3gh/4+SPFvR03sqWPupmAKj+TF5J4ld7YdQvweBus4gRYHe9IyUX1pWBBimHJ9MXKAdyV52ISh56VrBLHuPeC5btIB8E6uiXwQzJKCvaxTqHRnPiLRnaDHxOyD0DnlWD5cfI8zPWipI9XB0PtFHYixjL80naUXn7G4qU/oDAdOa/wM3b5xkM+oKYlTAPKuNxZLrIcWEsfYI+Vr/RXOdBETPwVv8X14JxcS/q9NO8sgOAhAFF+LK6JQkN0nx99wTxq5oAVJgN+uiWe8PoLl2z0c783Hii7DawwBbCEJFMRkIKjQKMAStDi8iIAM6IAmSwtERBAFKViQNXAj25OApXrlovpM1W6izcJKCa3uXPKVyhwxDh0oAKwfKapc9nGh5YyMKHNgjkHaUjIHDFVjUn+Pa8Efkz6O/+wWsBapgfZBJU5bwZQvqkJFgltpExMgUBMLDQ3f8QBIBpQqSDpxhCwKgaY55uivCPP4Q6DH39JMjY/EShMSk1nOdw30PTssWIHJUiIVW6mzErzRSE7DCD6rLOCqETMLmSjogQVSTLUS4sT8AS+yQcrMMnaUfDSbrCWGYBV0Cp71CI5YLG35+f+xi/wnUIfFZia06UV5RQ4BIwsDj1gm/RTCnDKutB2tMkqf0WcOPei4qeIuP60IxDjZVDVr4DW7i9c8utT6MytvKSxn84oH6TWwcSGRgyLRz8Z+QRIhKHGMrmaJCmnEz/FfZbpUVBFxKR/57BOIVDC1EQ4DWtPJgYFoJlkAWVLzBQVe0T0C8DX3yFXJTlDZWbSBmPyIAlW4LtixfXAek8NUIGp2e3ECRMWhRGMMirNoZuuG4CKOXy6IBpWijlq8iGrlHM3MSu4Rro6eUb5ptYLsEAbhaBQc1NhnYKgA6KYRuRCzH9FETJufnqfJlIpX0V9kQBKmqXv3GPyge5+KUadTNRz2sFyonJgimhNAKVHvIS2MvmpuHSIVL7JBJVKV0xJc+7KRyVNcHSMErpDAOSnIXRggY+ImVqKQw8TVAVCiimGAaGe+el9GFgVpjS6qaWYYMiw0fooqwCWGy8T2hAk5UR1oOr4qVi9Ssv1EsJSY1TC0dP7aU5d2JgyqaQJhvmg5sOUum9OHghTc8QGAA3paYyipmdqT68ptohnIuaEC6GlM1Hep5lfTDKo92u5H3Xuml8KhagmUKUXy+KjmLXKNzVrjFZRpOGYsCrGFnpd0z56NKS/U/2UBCeUCbTSkIiEgSQIEltlRFKpB2qdnBu1VeDoGwP1q/blzOlaBaZMTUUnzdxCDUM3EYgcMKYo9D5V2nr1QLWL2mRjFV1IqtKJU6cRL1Tt1JkrH8YaCM4N5y1n4KviYi65oxLdT3HoDYHSE+QkOHrdKsaqkFka4KFfJ0D5RDNEwCBZjkwwql1xjlECpQZg9Au0LmzSTilOnia5piJeWNSTfUasolFPpTc649WAVWUhq2OXYDUBlMFp+5c08woLcnSgKYq8LssaMyxufrS9iVWqvpRiggnFLianTNAD5zwro+oBpYnLGFiEUTFglYmSIl7IGF0X6RMnKj3UWXplQe9XY1QspYmDosrIVLFzLkrB9aoHRh9FQEuLcCEoQchPMI2qeT0a1hOhSkJEQMiURuV76nqar6KAUcWualfiry5ChekJeVAXqJnLGaykMw99lCn6ZRGejcSoHgVJHUozWbNUUMAFWit06uI3deQpeWCg5JVj516tEVAq6mUxPc0ETf4rwUCtHlW3kGdiDgv342RUNAlZBXKIVnBCd2tMUVAwSeot7ol61H/VKdxtUIxqBiiqtXSn3qCSQH1OTEymAaoiINmooBEzETAoWHoF1NcNpG4V1dm5J+RBQ6AYMT0a5YKXxpijAaonyIn0h6YvBsakqXUCaHwrq1EgCH1Gigmak2ZvdEBRMEyaiQymIVDUlzQAqk45Rvop5Y/0iKpttOopTejfyeZoWG2QdasmgaIpATk3Rr465pfquwzitK4ZReaY0FRGX2WwhtASlQBVVYY4szxXyIOmTa8eo0ymR528KRes738iUauVY4zRj7AqtiAGLRarjZuEaCQ4PdvrL/TXA+q3s5Yxhp8mcjlayI8xqhFQ9Qp5pnTHBKK+uyy1VWR+AVg602PspE5dkwy+T4/vBTrcms7mrt2tUSTqpLpp5hXcY88nkuKTASrVoWcASt+moh9xhH5K70cvVVPVHjqoepLBdVrRqv6BEXVAIVJ8a39rdXh4CLE6Cwn54aqZ5EMj4ameMWy1UzBjO8KmdCYSlXFWafmiKbeMSSta5CNKnXkbcxetu5zGy9hj6kf1t7Me54yRjyY1NZ4Ay3Rfv0aVNnX8po0KOmHtw45w8rr5mfrRpQhdIGVuREupyMe9e3IXr72/IVCVDTPmA/YmUt4MnqGMod2kASk+JBMOsiy/JPEq/l9/09TuIJUIg2RIVEPptlYkDRKMalShiAFtBOuQ49mz2MVrDjcESjSobDj3YQA3RZQjoTbN/MTePi8HgAhwygCP/+Mcn0viK5LwO4UUP+U31J04ddpyccKP/ammStVyVJwSkET7oLDHGf9Uvv+Jb+mu32h6Phm3zOyoVqznAcxJOnbRoBaAUg0AqcjPDjMc/gaq2NmpV0NPBSrJPskqzbQTv8mC6KxSbt3zVuUuevwm8WV2ZqB8sDZc0FVF6VHw2pXSbAQYVYCLv0SwZQAn1iQ3Xu42+xNUE9C+VolFuzSHLp9NyIRYvwazjgElmcnBf5yzndvYBY9W9OkYo57eiG9BrnIsfw847gGQ7b+xaARcrgssLz7iqpMwNw2UqcYljTP+WSS55lOH7bE4/6xz0WOPpA07E1DqYf4sxlWRW8zBrgbHBeL7WwDBtz6NkNHuOx3y204xCSsK9dGkTM7dVJ+KyimxYp5JtwWLwsGOM7ADjLOtYHjKdnJPmljUlOk1Of0ztnlTjDpjUcgwsbNAZQBJebSMTf++m51lVMb1/xvmBASl8aPBOgAAAABJRU5ErkJggg==' }, { buy: '无需再开户', adv: '宝石山就能交易', squarePicture: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABKCAYAAAAc0MJxAAAOEElEQVR4Xu1baaxdVRX+9jn3nTf0vU60MpRJoCDIUNrSwSCaoiYOwfCHqKDBiaggBmPEGBJxSIzRxIgQx+AQhQSIJgZ/gAUDqLSgYEtnoAxt6fQ6vfYNdzh7mz2sc9be99x7TlsMP3pv0rzbe/Y59+xvf+tb31r7XIHeqxICotKo3iD0gKpIgh5QPaAqIlBxWI9RPaAqIlBxWI9RPaAqIlBxWI9RPaAqIlBxWI9RPaAqIlBxWI9RPaAqIlBxWI9RxwuUegBJfU7/FwTUxwFcDGC44jU7DhP9pwLJafa40GtE68T+ms/5i8a5z7Pj9vNstIjYSe4c7zvyw/LA3wGV0gdHAKxTEPf3j9Z/Lq5Do2gChYyaeGxwXizShwEsOF5w6HwL0qkMHALLoOaG8QlmZ/rHCkDOwQrBF8GCaHzGoQ493Wla/01V/JGhqyd3hAPagNJMasxJVr+pICWnAP2aSR0ivYgNGSAcsMhNPORcBGVw50DRAvhgqfpOqPH13dZ/TTLVWCo+hHrIa++kqcf6vyyEuutNY1IGUsgQHikBo7JDNEl+LgMjA0bDFNll8EKzHSw1sQVq6vWu01NK3Dpwdf2nXYGqP55oXi57M4DKNUlPRL/Murt/9D4ErAA0T7eYZhV8XgyW/g6rYfLw80DrQNn0VvWvaCwvA0pTLim7UtnxXJNoVQmsLl8fCLUdycK1LQEUM66bZsmDTwKqVXb79f4VjYEyoIpmVHZh73g7k8pOD8BQCsh0q1NGPHpmKVnvJuTeTfavaHiC2qau9ceT4wJKaNHun2fDTE84C7lOYOlb6BSGTGOIXZ00yAz1wza0DqoxCjW+rmzVzPH/K1BCA0QgEUCVwSq4/7ZQDL1UGZDcZwmoyVehpl57a4ES/acDA2cASjqGaJY4RmVg6XvsRtgC+5DpEmdLl+zWhVlS24LmvrcOKKEBGjjLhZoGQrL3DrCuYPFsWIVZjkmF2VCD2O7SNcxybBUgC41325eWhl4luE/AQR2s8gmIRMmUe0BV5MQx2QMxeA7E4LnOAjDxVhLKiDWJuK7Q6T0fFzp1utuizzvVh/rzQIuOQrMoX/pdDC2tLSP4ydI1x+ejxOB5EEPnueymJ6+xoJaFFXGlxdx8rifujpn3DqxCy0B+ioSde6tuYPFCOLQPAZgFzt8W0hlsGb/6Ln/02IESQ/OhgXIoOIBC9lijmYOlweFZkAPL2MdDgGfIwr4Vza2gLtQlT6fasIt18DMlcMxAGZCGzi9I+37o2VBTUJmfcsB4/kp/xv0WsY8B4NkJ+lyHmu0UeK8ir9UpDD0gLRuLCuljAkoDpIHKw4lriWYQ1yL7XlGomb/sH4WpAYqHqInVcmkVEWqX/gXyjV9Djv61/RzjoVg4Frn7Tl2HjHHi6BllQTqfhVtYv3GgqL6zbMnBCtnjgG0DsxwnPaLvilV2zSZeRrr9bqixZ3ydCcO1rcB2jOxYG2qg/lZdo8TQBT5IHQtdrUEk1AQKCTs5c86gULc0wI5dFoJAYH2m9S22QNFLja1Guv0eqMmtXQALu6PEPF4O6dNtA7Bv4cpqQFmQLsiZZN6RKGe3mIdVFmIsE7rJZ5aBQjRjUpg1ubgXdBTcQhGjPLAmX4Ea34B0x6+Alq7nwvDr3pYJy55k4WPlQBmQpl3IVjnUpKJsRaygWi8PN1+viH16mmmeHLj4k6aRoFOIuP/3LXoKauoViP4zgKjfMEkMnm1FXk6htfVOqDHaQAgBo0TAsyNhkrMuWfh4d6Aaqy5VBiQSWhMJAVBZCPKQ4MIchGFoRGWTMZVYygHkppVCku5boXbRvWbbS9RGDIsMe+IRiORkyD0PId35O0TTFwND8yF3/RFQ+vs4YAwsI+Dh/2OUAtV84WNkjLqEGqX3MFMVTJaym/FWrJNg2rHEPv6Xv6frc81S6Lt8JVR9uwFbt3dUfRvU5OuQex6AGt9ofFT0tusQn34LoBpId90PufO3dnGyrMj3Fh2ITNyTRU90Z5QPVCddCj8PmJUJM4VjkAXN/HXYkcCzsOX+qo3Jdq59C5+AmtgE1KYD0SDk9rsh969kkhUhmvtRxGfeBuj2r16Q+i6k234MdWQ92+vjTt4PyWTRkyVArfuEvmtmLN2qelmJwoFnK1Z6FJpLyzYLKQ83bRWYc8/KIa55fCEUau/4JXS9Kff+GalmSjrOQLLzi076IKLTPgPoMI+nAc1Re87+lZBv/AaqOdoBMMuuZPE/yoC63gLlgcU0qqtesQlxQ2muRTUfK5ypFmThab+XDCwHKw/z+NzvQ+74JbMDoZ0AxKx3Iz75ejsP3VRMj0D0zTUgKtUwWiZ3PwjISUDEDuicYcnif5UAtf6T2ikG+sG0os3vhKCGDOTss2Dl5Q0Lzcw68ERQpHkKYmQBlN6f6/IS05ch1oyKEsuo2qxgtITa96gxrE68PIYlVzxdBtSngtDj2YybR/peXp5w28A9F+8a2DE+WOTUSbdCq8G+A8qUMOrwfyB3/AKq3vaYgNXs4csQn/lVo2HaQiCibToJdWg1UJtpLFBr7TV+RnS1YHLF6jKgblRZO8QsP1/hnP6++STqB10CE0bBZkMg1nmXwW1Kksh79SNtWFrAapc8ZFdJNo1Oyb1/MqLNX2L4YsRn3wFEQ1BTWyEG3m729FQ6hmj2BwBRM8Nba69l9iAvtpMlz5YAteHTLPQKtCoU6o7CX8S0IuBcS8bTKzfO06pct2rvvM8vhpujRtTVoWeyFosYOBvx/B+ZpCQPrATqb0DMeJfToxpEPAgkp6C17jqmT/kDHcmSf5cB9TmVN9vCYraIXRxMl9G8mrA97EKW5c49LJZ1KPKmoL1W7cJ7256MUfXXTWdS7vw9VGMPUBtBfOqNkAf/iWjmleZpGr05q5r7jair1gGIeAStjZ8teFZLIFn6XAlQGz+v97Pz/XnP6+hD+hifENcPcvGcTSTmgTB7ocVtA12bCmdWcBsbIVG74C6oqe1AbQZE32yoqW0QA2daZqgW0td+CDWx2dSq8VlfB0SfPdTYad+nR4zAi3gYrc03W9CDTkKy9PkyoG7Knbk3mTATFjnosL8UMpBrGAfAamHu3Emv9Jh2nxWf+10r2PGwAcm4bb2g8TTIfY+Yek+XNmL4Im3dEc29FkgPQ+hHkLgz1xq16SY/9MyFNaNKeubNTV9SuWOmti0PLwpHYg9nCok9N6JFTKIQ5de353phmCUSAsuyLD7vB0B9t56mEWu0xqBDT9X3IJp1FdA3B3LHPYjm3WIe8VEHnwCSkyFm6Cd5bJdUuC6pYVRBhzRZtraEUZtuVuZESZmGa0zYcyLy8f4SgUAhSrmoPYRc6mKZMWQWXdfZBqdX8TnfNqwxr+aoKUvE9IVAOgkMnG4T4q77EJ1ygxX9+g4oPX7yJYhpF9utf1MkK6RbbssZxRp+ybJ13YFqbb7VlK/ttoBA6rYFlT1Aylo03E+xtorX26IQs8D7HiuwDZpRZ30TUDq8NsI8GNI3x05WlzLaXDZ2Q449CzFyGYRmnGrY50cbu0zoGU3TgMWDSF+6vTjrlQK15Tbro2gnJaO/0yTPDrCaLdzL80qdTmAFIe310YsE3jIrmnuNNYx9J7lJErNTqOY+CMSQh59DdNL7HYCTRsCVbreI2GpVcz9Uc49tw7Q9BSiQLN9QwqgXv5ZrFBWxhbUXq99CY9ltx8XrbhL43NgSK8nBU9LIw1D3ncTM90Aktnaj/UPVOuSMpzSP+ERGkxyIOtNpd64f0qgNA60JyL0PBs9z2nDUOpYs31gC1Eu3q7wFYvjs2OUXtvkOCok7ZbwiveIPlfGuJi9+U6jGXpudTKrPC/EsFPUCsCxottCmL4GIpzl/NNNeQxe5U69CDF9ugRQCqrEbEAlEbQbU+AuQo4/YroWxO872kJzqZzOXby4D6hus1qNJsRZJgQH0+kpmhZlR5TvE2eqzTKiapl4zGwPpOMSMZWbi3haYyYZ598FKgwNNRBDDC9zutWWhAU7r17SLsqnrjgEmNkMefMomgrDNTPfm/FQ5UC/fUYdKk7zVwoyfp128QA7fF2lX0MNSdfsE3OTLWQbT3Updo2WtlgzwDjs6nGHxdESz3uu6BArQ1x08x1qO8Q22vJH6RwrBiwNG7+XkkeTKbSN8pEcvfaC19VtPQ6llNuR4GBELqFvJQ83vZPqMctegFZOTMM96awaZXjYlnRjRzKtclc+sBN90UKk1pVmrxzGenvJVLZMFxfAlrpknIPWen3lcmu85mpvxQ053QnXW1Fqm0vLHp9NXvnOrUvIn/qoy/xQIe+6mA4/lhZ8E0kMmHNTUq8EenpMRsz02PwhbzuactVa9iLXcY4WdUm5HwgJfj23l4LAn8RTEVwZW1L0fJbQxSr14V7+sHVqtVHqZzShuE8DLVr4gk+y2jdWr1thlDKHtG2Ujff5HA4hmvy9zzcVdTm5YO+xC63s14cgSTNabJxY1TcJQrSO2u9n+WptMNZaU/sTDXG7b9+bJFh5WKl1gb5rCp6wjqUc7kZ16DerIWlvJl7zEyCII3a4lkc5a0bxA5rUlhT//zGXl7GF7JhX6s5YG55ANrU4LBqxJVfzhSj8aojmp9Xcmcqjvi0rIGyDlhYCclmUab5spZ5cxdBNbIA+vqfIzCvtVtZmIZjtjSKnfrBbremahFmhm0VMzZmE1s3S3aMzYBrQO8p+dhcumdyY2KiH+0L+3/rOj+hlaGQNOxONtGnUiglBlzj2gqqDU+ZeGFc8+gYb1GFVxsXtA9YCqiEDFYT1G9YCqiEDFYT1GVQTqf7clVdIbgl86AAAAAElFTkSuQmCC' }],
      squareArrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAUCAYAAACEYr13AAABm0lEQVQ4T6WTQUsbURzEZ55RQULJuVAQPBbKboogRTSBnILtyUMPpR/B4pZCRUgDgigteOuhXvodeggYcdNTD8UklKIg/QBK8KQ0pNo3shqzSTSbiO/4ePN7/GfmT9zz8J56tADKw2A2mYWsRbpaIKFB4CFgx30hIheIBPpmdDTHZz/q/SAhwE/OCcpfCyQcGHu2yMzvoyhICBCIUvKNoFctAXnMc7xjpvyrF+SGiboaZQnAcFP0j8QKU5XCbZBbU5DvOAI/AUg0RSL4Fany525ze8aoLfehHdEGxYmWL4G5p/zA57t/r+8ie6BvT8dsXKuEpjvMZcxj+udhcNe3SPLdeQHvQ2PRoLTMdLUUCbgqluMJfNlmXo3/8ZaZyl7kCPIfxy2G1whOhfNr3wzR40yl1p7GzRi/Tz6SPd8AMB6KUTT1kzyzfxrdUXYAVHImJbMO6EErPnET6fJmr90Im7idfKIh+wVg7FJMNAjlmaoWB6vyjvtaxELzcY2GHmfL+3dYJidhgwpbQxPTx26zBt6Ffj9GmnhXcfD+AlZAlBUASL7RAAAAAElFTkSuQmCC',
      timeItems: ['近一月', '近三月', '近六月', '近一年'],
      i: 0,
      chart: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAu4AAAF9CAYAAABbB+26AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAIpeSURBVHhe7b0HnFzVmeb9Viep1coJ5YCEhCSEBBhMRsiYYEwyHpnBjON4PJ/tXXt3xp7xfjtre7y/nbQz69nPZseLZ2wcsWwwaQi2EZgMMkJCoAQKKIBybkkd6zvPe86pOnX73krdLXVVP//uUyfeUDc+9633nJtKG4QQQgghhBDSp6lxMSGEEEIIIaQPQ+FOCCGEEEJIBUDhTgghhPRTNm99V9a9udXligfTIfQk5a5LKezdf0gDSeadnXtl5Rsb5cjRY66ke3Rnm7e2tun6xIF59vQxWAnQx50QQgjppzz4+Aty8NBR+diS97uS4sB07+7eL5/8yNXS0FCvAuvFV9a62sJcftHZLpXlh0t/o3Gp61IsEHkPPP6ipm+65kKZPmW8pvs6EKjb340Xr8UyafxoGT1ymMvlB6L9qedfk0//4TUyZPAgV1oe3d3mfvprF50nZ54xxZVacLzsN8duXF01Q+FOCCGEVDEQ1ZvejrdMvvzqejne0ipXXDjfleQy0Qi+OPHmBZUXY1jGPfc/5WoLExXnmP7Oux+WuWdMlqsXvceVZnn6hddcqisXnjdHHx7yAUv+Y0+9IqdPGaf5TVt3Vozg8+veHUr5rtjWK17f2G3h3hPbHA+ImC66Lv7hYsJpI+WAEe/+AbI/QOFOCCGEVDFwNVj60DMuVxpRofXSirXS0tImJ4zYX/PmNhVO48aMkJnTJ8iEcaNdq9Lx67jo4rNl4bwZrjTLt+76lUt1JZ/A9L8EQIhCQF57pX0oeOzJ36sgvOi8M+W9587Rsr6KF8DYNjOmlmaxhrU+zmKd5H4Cfr/qTd02eCgbkCCG8+3rntrmcNX51589rvO48ZqLXKn9Tj++d5mce9YMOWf+zNg21QyFOyGEEFLlJPkrP7psuZw40Sq3fOASV5JLVBB794QoXnDnE9hg5LDBOdZ27x4Dq/9xsx6NAxukcUCDloELzpmtghPzhVALXWwKuXRAnP726Vd1faPTgl8/9fvMw8fiSxcW7UpysvHC3Ytv5IsBbf0DUVS4F9pPhTgZ29xb/pfccFnmQSH8Zee2mxepld1vn0p4COsJar9ucGlCCCGEVCGwnMaFjVveVev5BeecGVsfZYER53BN8WGgEdpbtu2SSy+YJ4MaB8qLK9ap9fO9RnDPnDYhJ+w/cCQzD8+qNzZpfKT5uMZDmxo1BhB/kyaMkXFjR+p8x5t46uTTXK3Izj0HdNnnzp+Zs654SPnd86vkqRdWi6RErn/f+bLwrJmuNssMs07DhzbJuo3bZcXqt+TwkWYZO3p47Pc+lcDC/JbZT9iGAwbUyy8efkbzhcK82VNV6L6xYatOO3pUViRje+Ih6oqL5ufsI4QdO/dJfV2tXGWEdbSuszOtrim9vc0xv0fMQyV4/xXnaYzvAqv9O7v3yw1Xv1eGDxui5fheqFu+6k1JmWVPGj9Gy6sVWtwJIYSQvkrrFknv/juRo8/Z/OBLJDX2L0Qaptl8AeCLvnrtFpfrCjqYwtINsZ3E/DnTYjsVQix9/+e/VkHt3RTiLOOefB1hox1TvTuEtxQXY3HH+ngXDQBhCkt+aMGPA200NtsBwM9+0cULetxneuvxtHxrS6e8eNDKrguHp+RL02pkSqNRm3mIWtyjv5786hF7bER/NcE2yWdxj3MvwTZEX4Mk15Oo/3tvbXPv2w6+9JlbdDne1Sb6XTzemo91h3tOtfq8czhIQgghpC/SuknSm28WOfy4SOdRG0xay0xdMcAfHWLZBwgfBJ+HwILYSqpHwDzieOr5VSq8Lj5/riuxrH1rmwqvaMBDQhwQorCuTwus6V6cDh0S77seB4TawcPNKtzgXvGBqy6QOTMn63x9UJccE8IytLnh/e+VO25drALSz6sn2WJE++2rOuSJfWlp7hANSKMMdaUAwRwGT1J5Kexwvu8DBxT3/Xtjm+NBBcegBw9xcI9BGVyyfFkIppkycaw+3KEd2kfbVAt0lSGEEEL6IOl3/4vIiTUuF5BuEWnfJamhH3QFycCNAK4pPnjXFrgaXPre+Tl1p08dL6++vlF9ha+/6r2Z8tDFwgOh9MKKddp21umTXKm12I4ZOcwIv4YuAeWnjR3RxZVh89vWtQP+7COc+8OGjdt1CMQrLjpbamtri3aVmW0EIQKEK1x30D4M7+zcJx3tHXLjtRfnlPv2cOVA6Gn++q1OWdfsMgGtnSJ7WkWuGZNsRw1dZfy+wIMNtjXCuje3aRn2ny+DDR/bDe2SXGVGmAc2bKuQ5a+ulz37D2vAfLBtQt7evss8gB3otW2O73rfo8+rD/wYs75wy0F69fq3tbMshrWEq9DIEUPUhcrzyBMvS/PxFh2RCK44cPc57+wzdBtUG7S4E0IIIX0R7x4Tx9HyRomZ61wMXl/X1X1mzYa3NZ47a6rGScD9Aq4bYNeegyrwAGKM8pEvYN6+vWfDph0aTwxGKnl7+279JaBa3B28e0wcL+SpiwMPTRhJxQf8WoEQlq0xbUoFIt93HMUDGdxfMIrQyQQPA+igfN3i812J7WQL15xix4BHe7hcVcuxE4U+7oQQQkgfJL3ubOseE0fNIEmd+brL5AeC7NXVb7mcdWUBcFcIgVCDaArL586emjPyB0T7Q795KeOb7EeBgX81fK0hIAsR+k9DxEfHb8f6QnyGo4SUMqoM3HKSKManvzeGFbz4hXZ1j4ljUK0R7xfVuVxX4nzcdwQvZPrdi6s1xlj8W3fslmFDm9SCjf1Wio87thvcTPzY/N6fPdwPSWO89+Q2x/fDvP36wMfd44+N6LCh6CMx3Dzo9ca+62vQ4k4IIYT0RQbHD9GoDL7MJQoDsQOx5QNEFEJYhgCi5ZjWgzREO/B+yfBThv/yC7+3Lj2wkkMkIkBoAwg/XwahH3LYzBNlm7ftUoEKvOW/XJeV0D8/GvD9CrXpDdARNYmL8tTFAVELEe4DHpwQkIa/OX6tKHVoSwhyiGTsV2/ZxkMSxPYLr6zL7Jsk4rajD6Vu8/CBgHSFFndCCCGkL9KyUdJbbhXpOOwKHLVDJTXtXpEBXV9UVCqwakI4xY30EsfSB3+nbgwb3343Y+1uaW1TFwdY3EOrp7cUh+Nwx1lG8UDw5HOrMsIRIh7+10tuvMK1KM3ino+47wurf2+7VWw+npY/WtUhR9pdgWNInciPFtTK9Dwjy0Qt7lHCEXmiFvZCFneMvoJOxkmjsWDboKMnfklBR9I169/WB7qTsc3jLO7wgcdoQ7S4E0IIIaRvYYR5avp9IkOvMXfrJhtMWst6QLSXA8R0VLDBuuvL4BYB6y2C913Hmzh9GazzUTAtBBcs8xCQsNCePWe6qy0fCD8MEViIV19/S777o3/v4nvfk0CY/9gI9PeNSklTrWhAGmX5RHsUPKzge4XB+7gjjW0NXlu7uSj/dAzlmG8IReQxUgxAZ+FCYB16c5v7X4CGlTDaULVB4U4IIYT0VRpOl9Sk/yOpM1fbYNIoKxaIKFgjkwIsmhB9cXU+QIyVAtw1NJj5ArzgyZd5t4k48CIeDyzMhdwzCoHRRSBKQ3efOFa+sUkt/L1tdZ9mBPo/zamV5y+q04A0ykqhBdvSbNcwwD0pHNITabwN95DZ3oXAS7RgvcaDU9L3x4MZrO2hhTuJ3t7m23bs0bhUV6BqgsKdEEIIqVLGjhmRM352NGjnUhPi6nw4bUxWUBfCv4wJAcM7ArxV1ZdBVMYBl45f/26F1kMkIoZ4D63GGPrRW+7Vor9xu6uJB6PYgLBjbhS8oKqnLPwnA3QShctJMcF39s0HhHMxgrxYodzb2xwdqzHqTbFuOtUIhTshhBBSpUCUwS88KUBoo2NjXJ0PfkSRYoB1HdZWhBPODQI+8L4sDgg53+kVo9NAJN528yJ130BHS+9OgV8Gwo6z7+yKf6GTB+IO8/Cj6MSxfOUGfUiI8x3va+zee1B/hSglbHvHWqh7CuzffPTmNoebEAT/rBnZ9wb0R9g5lRBCCOmnwA0G7hWw0JZCXMdQuNVAXBcCwg7WdwCLOkYtgdX/9luuzLGkesEOq3CxnVNhiYeo7w7h+vUFIMDx60N3SOqcWuh7Ytm+rwKAaxUIO4z21jbHsek7p/phSPGQGXes4tjrL51TKdwJIYSQfkpPC3fgXWTiwJjj3p0GQJChQ+WVlyzIEe1R0A71YRtvxfcj1gAITVimu8PQoU1FuY+cLLxwR+fdyRNy3zpbiN37Dul+Kle4Y7v/9ulXXc5y2YVn5bwMqbe2eSjc8asMXKluvf7SWLcdCndCCCGEVD2hVbsUMB1cYEIhXcy8yl1efwcPKPkebPIRNy3KMIRnX94PcccYoXAnhBBCCCGkImDnVEIIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKoCqHQ4S43+u37hdNm55R/PDhjbJxHGj874q92jzcVn/1rbMK4IHDmiQieNHy2wzTdxYpxvM/DEWaj4mjBsl408b5XKEEEIIIYSUR1UK9/0HDsvSh56REy2tMmJYkwwb0iQ79xzU/LixI+RD113SRYhDhD+ybLmmp00aq/GhI81y4FCzCvglN1wmI0cM1XLP/Y8+J1u273a5eC67YJ6ct2CWyxFCCCGEEFIeVSfcYTX/8b3LNH3TNRfmWLtXr90sTzy7UoX5zUa8e97dtU9+/uDTKuo/eNV7ZXBTo6uxdQ88/qKmP3Xb1TmC/+6lv5YBRtRfceF8V9IVvPErnB8hhBBCCCHlUHU+7q+t2aSW9cWXLOjiojJ/znQV7bCSQ+B7Vr2xSeOoaAeYB+aFeW7ZtsuVWmCNHz6kSdskBYp2QgghhBDSE1SdcH9z8w6Nk3zZJ08Yo3Homw5RDpJE9pDBtjzOn33MqGEuRQghhBBCSO9RdcIdVnDvox5HS2ubS2VBx1UQWuFDjhy15aNHZkU6XGiioENs0jwIIYQQQgjpDlU7qkwcENY/u/9JOX6iTf70Y9e7UtuZ9Ye/fCK246r3cR8+rEluu2mRK836xd9y7cWyd/8hWb7qzYzlHp1ZZ8+YKJecPy92NBpCCCGEEEJKpV8J9yefWymr1myOHekF4v3XT6+QnbsPdBlVZsHc6V1E+NvbdsmvHntexX6LEexnzZ6mQz/u3X9Yh6CEH33SCDaEEEIIIYSUSr8Q7rC0L3t2pazbuF3FdGg59zy//A15eeUGtZaPGzNcy06Y6SDkMaTkoosWyNTJp2k5eGXVBnnm5TfkzBmTZPGlC7uI83wPCeWwc88BlyKEEAL+6I5PavyjH39fY0II8YwbM8KlqouqF+6wpD/0mxczlvMrL1noarJ40R5nWYfP+sO/fUkF/EduvFxHivHl6Kw6asTQWIs6HhbuvPthFf0fX3K1KyWEENJTvP+amzX+zeP3a0wIIdVO1XVODYFVHL7r8GmHL3qcaIcAh2iHewzqoyIcI81gmEjw0op1GgOUQ8QnucGgHPPEAwMhhBBCCCHdpWqFO1xV4MoC8XzHrYtz3FxC/BCPfpjIOCDSMZ9Cb0klhBBCCCGkt6hK4e79y+H6gjek9sRLkODvHnLPA0/Jt+76lcvFg86t8JknhBDSc8DBM3TyjOYJIaRaqTrhvmHjdhXt6DQa5xoTBT7q4PX1W9QvPQ74ycPHHfP0THEWeiwvjtVrN6ubzNlzprkSQggh3UVFuos9cWWEEFKNVJ1wX/H6WxqfPnW8jrWeFLxIhy/6BQtnqch+5ImXVaSHQJgvfegZTS+Yd7rG4D0LZqk1/ZFly1Wk+/nBZx6+9U88u1JHsEE7Qggh3ScU6J2BSEea4p0Q0h+oulFlCrmveMIRYoAf3jGOuOEgQTj2exT4xF91+bk94qZDCCEkK9g1dIp88IN2VJmHH75fampEalI2pEwghJBqpKqEO6ze+yIW8yTihnHE9Fu27cp0WAV4qVIo8OOAgN+8dafLiUyfMk5GOhccQgghPQMEe4cR7B0dNn3TjVa4P/CgEe5GrNfWmuAEPCGEVCP96s2phBBCKhcV7ka0t7vwoVuscL/vV/dLnRHtCBDvFO6EkGqlKkeVIYQQUoUY4e7dZWB59yCNMjVDIRBCSJVC4U4IIaRiUG0eI85RRs1OCKl2KNwJIYRUDF60h+I9rowQQqoRCndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqAAp3QgghhBBCKgAKd0IIIYQQQioACndCCCGEEEIqgFTa4NJVRWtrm6zfuF02bnlH88OGNsnEcaNl1oxJmo/jaPNxWf/WNtn2zh7NDxzQIBPHj5bZZpqGhnoti2ODWc6mt9+VEy2tOs3pU8fnXQ4hhJDS6ew01/Z2kfYOkTYTL/nwzVq+9Jf3S32dSF2tSIOJa2iSIoRUKVUp3PcfOCxLH3pGhfSIYU0ybEiT7NxzUPPjxo6QD113SRchDvH9yLLlmp42aazGh440y4FDzSrGl9xwmYwcMVTLQ558bqWsWrNZ05iu0HIIIYSUB4U7IaS/U3XCHVbzH9+7TNM3XXOhjD9tlKbB6rWb5YlnV6rAvtmIas+7u/bJzx98WsX2B696rwxuanQ1tu6Bx1/U9KduuzpHiL+yaoM88/IbsmDudLnk/HmZOr+cM2dMkmsXn69lhBBCugeFOyGkv1N1l7fX1mxSi/fiSxbkiHYwf850Fe1btu9Wge9Z9cYmjaOiHWAemBfmuWXbLldqXXGWr3pTLfpXXrIwR9BjORDz6zZuV+FPCCGEEEJId6k64f7m5h0aJ/mYT54wRuMjR49pDCDKQVS0e4YMtuXhNBDxmO6i8+a6klwWzD1d481bd2pMCCGEEEJId6g64Q6fdO+jHkdLa5tLZUHHVRBa4UOOHLXlo0cO0xjs2LlX49Eju/q9A/jDwzd+q+voSgghhBBCSHeoOuH+pc/ckuO/HgL3FljkIahDNxpvHX/4ty9pmxC4uix7bpX6v0+dfJorFTl0uFnjuA6rnnFjhsvO3QdcjhBCCCGEkPKp2uEg4/AjwFx2wTw5b8EsV2rBSDS/fnqFCm1vsfejykQ7n4J7HnhK2+JBIYn7H31O/enztSmWnXv4AEAI6d90mrtVW3tKO6cifOGzn9Dyb3/3B9oxFaG+Li01KS0mhPRjxo0Z4VLVRb8Q7rCiL3t2pXYWheX8tpsWuZoszy9/Q15euUGt8bCUgxNmOohzdEBddNGCHIv7t+76lcYnS7gTQkh/h6PKEEL6O1V/eYMl/Wf3P6miHZbzfKId9RjyEa42CGj7x7dfKwOMmP/VY8/njBCTz4+eEEIIIYSQnqaqhTvGWf/hL5+Q4yfa5JZrL9ZhG6OgQypEO4R4dFhHgJFmMEwkeGnFOo0JIYQQQgg52VStcIc/O16OBEF+x62Lc9xcQvwQj36YyDgg3jEfuL14xo627jSw6CeB9rTME0IIIYSQnqAqhbvvhArXF7i8JI3PXgrwdw+ZOG60xpsTxml/272syQt8QgghhBBCukPVCfcNG7eraD9zxqRY15goo9xwjq+v39JlKEgPrOropIp5emDBR6dVvD01brpXX39L47PdUJOEEEIIIYR0h6oT7iucYD596njtTJoUvNiGT/sFC2fpsI+PPPFyF9cXPAgsfegZTS+YlyvCMdIM3p5636PPZaaDz/xjy5armwzm2xPWfkIIIYQQQqpuOEg/TGMhPnLj5TkvYUJHVvjExxE3HKRn9drN8sSzK10uC9x0irH4VwM4glIcN5kQUoDuXis4HCQhpL9TVcIdVvR9eTqLhsBFJjqCDKbfsm1XpsMqmDBuVI7AjwNWdvi6eyv+9Cnj8r5RtVrwRw5FOyGkWLpz3aBwJ4T0d/rVm1NJz5B7xKTNDZjKnRBSHPaWk71mlHL5oHAnhPR3eHkjJZEV7UhQtBNCSsNeM+z1A9B0RAghxUPhToomFO3Z2y4hhJRG9vrhPnkxIYSQoqBwJ0URFe3BnZcQQkrDXT/sJcR98npCCCEFoXAnBYkT7Yh5nyWElEPm+uGuJZlPGxFCCEmAwp0USa5o1yRvsoSQMvDXjtxrCi8ohBBSCAp3kpckcW7LeaMlhJRDusC1hRBCSBwU7qQIIi4yLsMbLCGkHMJriEva2H0SQgiJh8KdJJIjzDM3VuBuuLzHEkLKIHv9sBeRIKnw2kIIIfFQuJMCOGu7Ay9P0Zsu0jk1hBBSHLh26DUE15JApdsUryuEEJIEhTspii63UhTw/koIKYeY6wcvJ4QQUhgKd1IYd0e11jGb9RYzQggplfAa4q8rCi8qhBCSFwp3kpfc+6jLITIh+IWbEEKKRq8dPijZiwkvK4QQkgyFO4klnyjPWtt5iyWElIOztue5htAwQAghXaFwJ3nIynN/Ew3vpbyvEkLKIe46gmtMtpxXF0IIiYPCnRRNaAHTNO+thJByMNeOLtcTQgghBaFwJ0Xi7qyIgiQhhJRK5toRXE94RSGEkMJQuJP8JN5LeZMlhHSHhGsILy2EEJIIhTspGd5XCSE9Aa8lhBBSGhTupCQ4ngwhpCfw15B8I8sQQgjJhcKdlA/vt4SQcuC1gxBCyoLCnRBCCCGEkAqAwp0QQgghhJAKgMKdEEIIIYSQCoDCnRBCCCGEkAoglTa4NCEZ7FGR1hhJHCZId4Zxp0hTYx0a9hpHjh6TjW+/Ky0trTJgQIOMHTVMJowb7Wrz887OvbJ73yGddtiQJjl96nhpaKh3tclgmXv3H5LpU8a7kq60trbJDsx/70HNY91mmPkPGTxI8+VSyjr79SzE6JHDylqvYrYDKHc7k/5L8/F2SdWI1KRSYv4zcQqxqUfafLo4S6e55rS2i7R3iLSZeMmHb9bypb+8X+rNpaiuVqTBxDU0SRFCqhQKdxKLPSpOnXCHML7/seflnV37XUmWCaeNlJuvvThRHCZN2ziwQd577pmycN4MV9IVCNV7//1ZOX6iVb70mVtcaS4r39goL61Yp22iFFq3JMpZ55dWrJUXXlnncslcdN6ZZh5zXK44itkO3dnOpH9D4U4IIeVB4U5isUfFqRHuoSCce8ZkmTJxrAwYUC8tLW3y2trNWp4kkKPTnnH6RC0/dORYRmwvuvjsWFEZFeRxgtWLZS9Ohw2xlmys29Ydu2XNm9tKFu/lrvOvn/q9Lu/cs2bo9kli7OjhBa3mIcVsh+5sZ0Io3AkhpDwo3Eks9qg4NcI9FKSXX3S2K83i6+MsyV5Yx03rrcjgkx+5OiOsMc36t7bL/kNHVXRDjIKoYMX0P753mYr2cPqQp194TVa8vrEkK3c56wx+uPQ3us5JFvFSKXY7gHLXmRBA4U4IIeXByxvpc0CUg3Pmz9Q4yllnTtMYIjMEVuCVb2xSYX3heV1FM3y958ycrNbgNW9udaWiAhRiFWJ7yY1XuNKuwOcbYB5JYnTu7KkaR9ctiXLXuTcodjv0pXUmhBBC+hMU7qRPsXnruxqPHDY4sUOl75wKkRmy6e13VSxOn3xaorCeOX2Cxhs2ZoU1XDo+/YfXFLSQwx0G5HNLgWgthXLXGXjLeE9R7HbozjoTQgghpHwo3EmfYoARghDt04woTAKjmAC0Czl0pFnjsWNGaByHF/3eDQTAD7uYUVdGj7Ki/O3tuzWOY52zMI8bm7wOIeWus+eEEdAebBc8+CDAKl4qxW6H7q4zIYQQQsqDwp30KSD4Prbk/bG+7Z7X123ROCruvXuK7zCahBf88MUuBVjT4dMNMQpf9qg4hmD+3Yur1YXkovfMdaX5KXedfTzclONhAf7uSx96Rh54/EUNd979sPYFKEfAF6K3tzMhhBBC4qFwJxUFRjyBD3ySf3UpeJ/1UsADBXzA1761TcXx0gd/p6L5uz/6dxXMI4xYvf2WK7s9nnsSfp19fPDQUXnsqVfUwn/tovPkpmsuVJcXiGZsJ4z80hvivRTK2c6EEEII6QqFO6kYINqfev41TV9x4fxTMloJrMdwlYGPN/CuKo0DGjQ+YIQ0Xhh1sjje0ip33LpYrl70HjnzjCk67CNcXm67eVFmZJgXX1nrWhNCCCGkkqFwJ30eWIzh9gHRDks7rMoQqScbWI4xzCHEMKzaGCYRbj0+LLnhMrW4Yz0xXGJvAos+tsMN739vbIdYPNQsvnShpvHrACGEEEIqHwp30qeB3/j3f/5rdfuABRnjgie9TAj+3qVQqjvLC79fo5b2pBcLwT8fL17CwwWGVizGt7vcdYZYx3bwnUDjQBtsM6yz79DbE/T2diaEEEJIPBTupM8C1xj4jXuxjLHF87nHnDZmuMZ4e2c+/DCSpQ7duHnbLo1nTE1+CynWD8Mkgu3vFhbLvb3O3pWnpQf93Ht7nQkhhBASD4U76ZNg1BbvGgMf7mJenT9sSJPGW/MM1+gtz6dPGadxKXi/9kIW5GFD7Xq0tGSHakyi3HXGQw3ccQp1/Bxoth/oSat3b29nQgghhMRD4U76HBClK17fqKL91usvLdpiC793TLNp685EQeuHkpwyaazGpeCHNyzkduLHeR872lqm81HuOh8+3KzuOPk6wsLNCP74WO+etHr39nYmhBBCSDwU7qTP8dKKdRpffcW5JQvOhfNO1xj+6FEgZOErDyFbjAU/yuyZkzR+9uU3EodYxEOHF8uhLz4612LYyDi/93LWee7sqRpjW8XNE4J6+coNmj7bzT8k3/oUQ29uZ0IIIYTEU/t1g0sTUpB0GJuPhvqeffbDy4TWuRf87Nl7UFa9sSlvWBARhpPGj5GtO3bLpq27NG5r79CxzlesftMIbisyr150rowYNkTTcbzoHhyi48SPHTVcduzcq8J845Z35ERLq7S1tev8YWV/+sXVstpZmm+4+r0Z9xQIWSwbQzfW1dbI1MiLo8pZ50GNA9UN5q3N78ibm3fI/gOH5Ujzcdm554Bs2Lhdnnx+lQ5NiRdGvffc3O9RaH08SdsB9MR2Jv2XtvZOSaXEhFTX2NQjbT5dnCVtrjkdnSKdJu408S+W3qPlf7DkNjGHstSYgDg6HSGEVAuptMGlCclgj4q0xkjiMEG6M4zNjbOpsQ4Newz4bcMFpFgwJGMUWMOfMsIVVt8QWIAvu/CsxFFpPN+661cax80bYB1XmocG7/MeAp/ui8+fm/NLAdbnnvuf0s6aeElS3FCW5a4zHnRefnV9piOoB9PB0h5n8S5mfUCh7dDd7Uz6L83H2yUFoQ2hbkS2jwsJd4j11nYR85xoxL/Ikg/frOVLf3m/1JtLUV2tSIOJIeAJIaQaoXAnsdij4uQL954EwhIWcjCgoT7v0InlAF/3cLSWiWb+3X0pVLnrDNcY7/bSG981H729nUn1QeFOCCHlQeFOYrFHRWULd0JI34TCnRBCyqNqhTusgOs3bldfZIAh+mARnTXDdjD0vLtrn7yzc5/LJQN/5XDaV1bZjn/5mD1zsgxuanS5ysIeFRTuhJCeh8KdEELKoyqFOzrqLX3oGe08OGJYk447vXPPQc2PGztCPnTdJRmXBgjwZ15+Q9P5mDZprNxspvN4/998fOTGy2X8aaNcrrKwRwWFOyGk56FwJ4SQ8qg64X60+bj8+N5lmr7pmgtzhPPqtZvliWdX5ohwtE8aixocOXpcHlm2XBbMnS5XXrJQyzDN9376mJadOXOylsUxasTQbvs8nyrsUUHhTgjpeSjcCSGkPKru8vbamk1qWV98yYIu1u75c6araN+yfbeKbwBXFrRLCr7T3YK52bGwvdDHyCFx0/hQqaKdEEIIIYT0PapOuGNMaxD1ZfdMnjBG43xWdo/1k9+h7jUjRwx1pVlGj+xaRgghhBBCSG9QdcL9wKFmtaonEQ7fV4gt23ap9f7cs2a6EktcZ1ZY8CH0CSGEEEII6Q2qTrjjZTFhJ9IQCGtY5AcOaFBXlkK88MoabTst4c2SGGnmyedWakdV+LzfeffDcvfSXxc14gwhhBBCCCGl0K/GcYfIXrVms1x2wTw5b8EsVxoPhon8+YNP53RK9Ty//A15eeUGdaEZ2FAvM6ZNULcZWOJfX79Frf5x03UHvMr+ZOKPijS6ptp/21EVnVJNjFeOIz19El+2Qwgpjc3b92pH1BrtkOo6pSJGpcaa0rIQXHfa2lPaORXhC5/9hJZ/+7s/0I6pCPV1aZ0vIaR/M27MCJeqLvqFcIelfdmzK2Xdxu0qtm+7aZGrScaL/I99+H1d/Nvvf/Q57eAa9wCAZd1n6nfuPsDhIAkhJAaOKkP6PelWc0A3u7Q5mFPuXlrTZNINNk1IDFUv3DGm+0O/ebEkK7gf7jE6drsH84SvfJIof3vbLvnVY8/3uNX9ZGKPCgp3QkjPQ+FOqhNzgLbtNAfpVkm37TCxCe17TNm7Jt5t9PlelzZlnXZku1hqBonUDjdhhNHwk8xBPcWEqRqnXCz1E8wpwvtvf6Sqhbt/uRL81K+78j0yNcFXPYqf7gOLz08cnaYQ8HtPEv6VgD0qKNwJIT0PhTupWNr3m4Nwo6RbNom0vGXCZpPfIunWt028zdwsSx+k4mh6mJH89qAdlDoqdVLEPGCVHzhLUgPnmtiExvmSGnSuFfWkqqla4e5dXSCer7r8XB2vvVjQwfT4iTb5049d70pKh8KdEELioXAnfRqI85YNRpxvNPGbKtBt2gh11BXJ/vQY2ZKeLe90TpN305Nld3qi7DTxvvRYOWAC6ltlgGudy7DUPhmR2isjZY+MTe2QSTWbZWJqs0ypeUtmpt6QoamEfm91I62Ab7pIQ6rpQpHaYa6SVANVKdy9aC/HVWXDxu36ptQLFs6Si8+f50qzwIcdo8fkE+Xe1ebMGZPk2sXnu9LKwh4VFO6EkJ6Hwp2cctLmIGrdLOkTG0ROrDOifL1LrzUH2B7XqDDHpUm2dM6SrekzZGvnTNks8036THm7Y5ocTQ9yrXqe04yYn1nzupxd85IsqHlBzqpZLk2pI642xJwMjXOMgL/Y3LAvltRgI+gHnOHqSCVSdcLdC+9yRbPvePrHt1+baKW/54GntPNpUpvHzPLREfaWay8u2j0nL+37pPOt680F5XWR+okmjJeUxuPMXWqyxjY/3tbXFP/rQhL2qKBwJ4T0PBTu5KQB15UTEOVGkJ94wyjtNyTdAoH+pqlrcY0KkzbH4470dNmQfo+8WXOZvNW5QNZ3zJQdbSOlU4/a8hlsjtuaFO60KTmOYxvJEqkxazGzZq2cU/OMLKx9XhbWPCfjUttdbQT4yg95n4gJqSGLjW7oAZ1CThpVJ9y9qIZ/+pDByQJ21Iih0tBQ73KWQp1SPX6oyKjvPDqtvvzqehXtPdMx1e6a9MH7JL1piaaLonaEOREnSKphghPzk0ywIj9VZ9YVnV0Q5+nYYo8KCndCSM9D4U56HAh0uLQYYS4n1qhITx83MVxdSvQ7b60ZIRtqr5O1cpls6DxLNrRPk7daRsmxTnOAlcDg2rRMaUzL1IFpOa2h04S0jDfxyPq0jDJhhAmNCcfqgbaUHOioMbHRHC01st2EbSdSssWETcdq5IQ5FwoxrXarXFjziFxc85icX/s7GShxb4w3J1njPCPgrzJC/ioTX2FOoN77pYB0n6oT7vAtL4a4oRr9+OzFdErFyDGPPvl7fbNqFLjZvGfBrC4PBsURsztgcX/zapHjq1xBT2BOVjxlq/XeiPs6CHsj9L2wrx1j6iZKum6spFMDKNwJIT0GhTspGwyd2LJR0mo9dwIdabWgd70f56V+grQNmCdv1iySNzrfI2vbZsqaljGy8fgAaS9SGdWaY3TKwE4V51MbTTygU/PTGjtUnNvZBDPDDdSRbxH2FAhPAJvGGYL3GWxtqZX1zTWyxoj4147UylqTbskj5gek2uWChhVyufxcLq+5T8am3nE1EWoajXi/UmTY9ZIa9kGzjcoboIP0HlUl3OF/vu/AYZfLT5zFHZZ0UMrY6xDwe/cf0jTmN33KuJI6wuYS7gqXzuweE2PM19Zt5rq1y8TbzZ3LnHgYWsrEaY13mGDqSr14dSFllmYvEul0jRHvoyVdC8v9eOmsG2/y46TT5CHqB4+7VdsRQkixULiTgqgP+iZJH3/diPK1RqS/7txd1pV4jzMHw4Dpkho4RzpNeEsuktfa5sma1gmyprle3jqWKlqkw0I+a1CnhjMGdchMI9RnDmqXOhyn4b1aP8OZIh0czMXIrujBn8GW2zMFCcQp6TCzXNNcJ78/XCu/N0J+pYmPJwh5TDF/wHa5qv5BeV/nt2WCbLQVcQxaKKmhEPHXizTB/Zgn16mmKjunViaRkzznIuCCloXlUdyJ3L7b3NV2GzGPMWR3qrC3Av9dc70z6XakTZvYeYCIcDcnKvLpdK2mOzWu1XjIBWb+hBBSAhTuJIe2nU6grzYCfbVJmxgiPd9Y51FS2PmnS6pxjhHqs0UGzpMDdfPktdYz5LXmAbLqSErWHE3JMXPsFAKHHaznc5o6jEjvkNkQ643tzoKevQfblMtnbqfZ+2oo3rMpnw5LouQe+NnD2ZbnivYs7uzRcjyMvHqkTp4/aMKhOnnzWPxJgSnOajwg1zU+KVd3/G8Z1faCrYgDLrjDzTk3/BZJDb7CTFya6xDpGSjc+wx+N5g4R7SbO1VGsLuQI+CBT4cnsUvnnNg+bWL4/EHgGyGfRtwOcQ/Bj5dD7JZOFfq7TLO9ZnG4/FC4E0J6Bgr3fkrnUXVvscLcCHUv0vFioqIxB0XDVCPQ3fjlJsCa3m7E+vpjA+Q1I9A1GJG+/UTkAIoBLSYP7JR5gztkjhHpGpraZVBteJ8198Cc+zKmsvmsUdvm9TOnrY98WZZQ2Gex65xd8/A7mDSymRMjI9Uzac1pIsib8E5rrfzuQL08daBOVhypVQt9lBrT9L1DjskNg56T93XeKQ3HlpkveMLVRqgba0T8TUbEf8i61qTKcQ0m5UDh3mfwu8HEGWHuRTsuDSh3cU6bOOzpqgQneJaYspw7JES6jzuMeN+jobNtpwmw5EPY75d06y4ZfOYPdQpCCCmW4y3mumL+KNyrFPVDR0fR0Ipu0nhZUSB1C4I+WAPniUCkDzjTiPX5+tIhqWmS3a0Q6KKWdAj1Qj7ennEDOuVsI9LnOYGO0BSIdCuJcu+tPpcR2qZNVnT7ssyHq/eEqWy6eOxJ4GW44k6MTJnmXZlLByX6iTa+9HB7jfzmQIM8vq9eVhyujd0jGOnmA6Nb5OamZ2Vuyw8lffgxkY6DrjYC3vAKS/yIJXaUGr7RtVehcO8T+F1g4sxFA2kv1E2saR/7ehd3IXuiZvHpuDJDpq2N7VEB4Y5LjRXySGPYK6Rt59SUNDUN1vaEEFIsuIacMMpbpYS55ESFOz58XQiFex8Efawwksvx16xAhyVd3VwSLLVx1AyxY42rMDdCfaBLY4Q0cwdq6UwbYZ6S1U6krzpSI7uMcC8EBPm8pg6ZP7hdzjICfb4R7CPrzcGDA9BghbRP+/k5ea1twnY2pcmc6W0q+pnFzddN44m2ikOnjJ4EXXDnDD79+ePSNuXTKedy48pd2e7WWvn1/gYT6uX1o/GuL2cMSstNY9vlA43Py8ij90j60IPJv5LAEj/iVhP+0Kj/i0wBT8aehsK9T+B3gYkzF4uIWEdHnRzhjnQwXQZ7UiqZEz4oi0tn2gGbTqd9jEuTF+66VBu7fFPTcG13sjl+3Po+NjaW1xF43779MmrUSJfrWXpz3oQAHP/Hjh0v6TgrZ5reBNeW1nZcUSjcK4KOI0aQY4hFuLgYka5pE5fwJlG1xA44w4hyWNGNMB8w16TnquuLxdxUDLtbRFYagb7SCHRvTS/UgRRuHjMbO+QsI9LnG7F+1uA2OX0gHg4BJg5lusMcgzafrc+0wA1Ok5HyTD4gUpDJugO4S/tuoHPEuiGdc4KgzOaznziHXBuN9azStEs5TNqUbTtRK4/vHyAP7mnQ4Sej1JsJ3jeqUz5yWrucU/uMCIaqPvSQORl3uRYRGqZLauQfmnC7eRg70xRkl0jKh8K9T+B3gYl1d0CU44Zm44Pm5va5tQ3y1rEaHQd2dH2Hjgk7qr5TxjVgyKlOOc2UjTHpsaZ+QE32BAa5p4rPBaX+xA7KssLdhExsh6HCIdNp6hE3DR6t7U42/8/nvqTx/7nzWxqXwr333i//8n9/IN/+338vs2fP0jKImq1bt2m6WPy0ISteXSl/8Zdflz/9k0/IrbdaUdFbYJ2/8dd/q+kv//kX+bDQj/j2d74rDzz4qNzz038rer/jnHlr4xb5zeP3u5JTD64rbR0wSqRUeFG49wHUzWVDYEXHaC7ezQV3gyJpmOTcXGBBn2fTA2ebHet9ofVmosagTcfMtdOIdBXrh2tlR0tkx8cwrC6tIv3spnZZYEQ6LOqDajE3u45IZcCBZsptjf+0y7cgbWNfkps25GQs6eAAzVaH626NXpagPHpgx5Gd0JCbtpNHlujaazozf5R5ge4/TYyTTVMu1vbI4S5vP1850iD37xkgTxyol5ZOO23I7Ka0fGRch3xgdLs0Hn/Bvm/m4L3m5LSj83Wh6UIV8KkRt5kTtfiR+0hXKNz7BH4XmNhb1DMW9g757b6U/NmGgdqiGIbWdcoYI+bHGiE/GqLexeOc2IfIH2UEPsafxcnqCU/NrHDHSWz+sEqmhbe0ZyzuQ+zLp0423RHu27Ztl0/98Rfkphuvky98/rNa9uyzz8s3vvn3mi6WOAHkHwr+7m+/Luee0/UFXN7qmUSxIgxW/f/6V3+tQgyMGjVCvvP//eNJFe9YB9CbyzwZy8iH31+DBjUW/HXnf/zN/9Rjqxyuet+ioh/0sE433vyHcuWiS+W/fPXPXWlh+qJwB7iOtBtFbq84EBHmEyHIeyjce5i27UagrzEH1aqsQEe+lOEWa4cZQQ7LOYT5fO0oqmId5Yq7Wbg03F5WH62RV41QX3XYxkc7gp0cA2oxLvoCiPQhbSZuM3lzABjsnHGXMq2C5dgylzblmbRGPufyBk1ls1nMARgW63Iy2LS/X2YP1q5tcssM4YEdJfM9PD4fibWdTaf0zavA5TUZrK0uz9chbT+13IRsmc2DGpM+1F4jj+0fKL8yIn7Dsa6uNEPMA9TNYzpkyTiMX98u6SPLRA78QtKHHjYn7FHXKiA1QFLDbxAZeYekhl5j8g2ughQLhXufIDjhAku7F+4H2zrlT9YMlPUxJ0254L4GXz8I/NFG0MOKP9akR6rQ75QRtRD+Jq5PS725IOAwwcUJgh2r6V/E1DR0os6vN4AQSuLJp57VGOIlif/0pc8niq2//OrX5JUVqzIiBoLrjTVrNV0s117zfpfKgnXGuv3b974tkyd3fXGFF09JFCOq1q/fIF/7xt8YUXtAHxAArPwQ7//wd9+MXW5PASH9s3t+odZeD5b7iY/fnrM98MvDXXf9wOXyA/EZrnOxyygXL7Cjyw3B+i9d+is9RjznnbtAPvmJj8b+0gJOlnB/7PHfyD/+03dyHg6xvo899ltNJ1HMOQPONd+zJ7ZzKeBa0gFVDtngdIOVDjbvoXAvE7iz6IuKINKNQNe3i2I0l1LcXOrVYp7CSC7qi25Hc7FuLu7GAFRSuLSJ97aJvGbE+Qoj0uH6sra5tqDbS2NNWuYNbpdzBhuRrtb0NhWIfr66CCUoQ+wqsoIcZS7OfJrYJzzBQWarbD4q0nU6bevLI3F4sBrClpZszjbNrbVkV86uZ5APPzPFSPiAyKbt/LPlNhsK+TAKBLsJmg7LDDil1jbXyy/2NMqj+xq6WOFRf8mIDvn4+A45Hx60nceMeH/EiPifGzH/hFl2zNtr4Q8/6mMmfMIcT3SlKRYK9z6B3wUmdoI9HQh3H5rN3Wp3q2hv+j1t6FRSI3tM2N1WYy6OtbKrtVb2mbjNP/13Az8Hc4DIsDoIexPq2k0w4t7lR9R1yA1Tx7qWPY+3qsfhxe/MGdM0juOf/vFvcoS7F9VxeLcZiOJCjB49OscCHFrRP/8f/kwFNVwYQrzFFt/pwMFDct01V7kay6OP/1anKyTcvWiDkP3Kl7+YI9wg3sGf/efP94rwgqD23w/bfd68OXL48BF5bfUbWgZB6B+WSvkFI3RZKmUZ5eB/EQHhckP8NgZY3sQJ42Xtug0ZEf+1v/qKXHrpxZo+Ffhj6J6fft+V5K5zd7nj9j+Qj3/8oy538sBVsKOj0+gEJxv0ImTSweWMwr0AHQeNIMdwixDmEOnuraJtJQ7b2zAl182l8Sz1Tc+OFoJ7FfZYcO8yAZ+bj1nf9FeP1Gon0rdPFN4ZuJ8sHNImC41IP9sI9jmDWq03h5Mn4afGpjy3xtcZcqaxhOng7haU+3RYl03nxOEBach+u9x2Kf0CmsrUZNuUgl9L9y3d99N8ZPvoOmtZEFwba5F36UxsQL1bLRu59dXvifMPeZv2eYxKc/+egSri34nxhZ/b1Cl3jG+Xa0Z3Sh1Oxva9koaA3/cjfYCMZfAlRsB/Sju2Sg0GvbBrQ7pC4d4n8LvAxGZ3qNdfRLhjWEabNqI+FPZuGpDSvBE/Rsjva7dCHoJ+twl7TXqPSyPeb0K+He9PGQh3n9bXMJmJ/CWtxtS9cEXxLjw9iRf1pbjKQLSt3/CWy1m8IPQW8vdfU9jqGRU2hazowIvppPX280gS7rDkfufOu1Q8QtDGWYvRBg8nmA8E5x/dcVuPWt/9rxTR7+997VEXPjR4N5c49u7dK1/4j1/RB5BQgJa6jFIIH25AnHDHg5tfr+ivF+EvHaX4lvck2Mdw8ypHXBc6xvoCuJTh1zy95pgPJyEyWonC3dFx2AigtUaguxFcTsCKboQ6XrpXCrXDjTg/y1rRB5pYhbpJ1w5xDbL3F01n7hq2vNXsjzXNNeqfvgpv6zThYLvbWQmg9vRG+KVb3/Rzh7TKhAazM5VQgttl2FR2ub4kJlLCtF2aXZ/snSxOpNs4UxaI8zBl09k2tplv0bU+SzbtWxWHXbPwM25/6FYx5Vk5Z7eT5rQsCK5NVsjbLZNZK9SbjM/r+uK7hrELmPrpgwPl57sbZfnhes2HjGtIy+1GwH/4NAy5aU5KbBM8TO7/iRHy95gTeY9rGVA7zIj3j0hqzGfNsbnAFGBZJITCvU/gd4E/8RDMFdFb3zudeNcyiHhXbj5zT0rgYlPuL0dZbB7l7emUEfgpI+TrVOhD0EPMQ9TDau/j/abOHyGVLtyjQAx+8tOfkxHDh2XmA+HuXSKiNB87psIvKprQURBWYYgqCCMIay/4fJm30pYq3LGOS5feJz/+6S80j2UvWfKhRIsz2v/rv/0w42aC9h/84HXdFple0OK7xW3zQvVRvOU77MTb08sI8Zb8aVOnyKRJE3T7xAl3/6tM0sOBt2yXI5x7grvv/okeC0muWPmoFOHe0WmuL+YCo0FLjVRw9+5+J9w7DhhRvl4yr/rX4RZN3Pq2a1AkNYPUFcEK9DnOim7iehxDuMC7i3zmfgK6lh802xziHJb0FUfqjGivVfGeDwyWMM91IF04uFXObmqToXVmB7p566e/yWTKsnmfUlwmLMumcZD4gPLc2NfZ9q7MH1iGMGXFKZK+1B+D+HC1QV009infJpNXcnPJxH9Lu6lsPtxO4b7zsj3tfVtNyOymTDsXTD70j/f3d4stR17L9Pv474dtghPN5jedqJd7dg2Sh/YNMMeEbeHBsJxLjHj/owl4+6ydhzmLJX34N+bifLekjzxuFoVjIgI6tI75UyPk/8BMMsAU5M63v0Lh3ifInjRZ4W4CrOsuqFDXA9ukNZ/bFp827Q7tzG71MYgpw0mb0wZg3phPWt+uBvGu4r7VifvWGu2wgvgfF2Kc3Z4FAu3e+x50uXiK9de99UM3xrpDAC8eQ9cHCHfMM67TH8Tfbbd/KlG0eVEV+h57lxFfVopw9/MDEKtwj4C1t1gwDeYJwo645RAntEP8tgHFCMPbbv9kF8t1Ty/D4631W97eqh14H374Ud2uccI9br1C/DpEfyk4GfgHTTx8/O3ffMOVdqWYX43iOFW/InhwzYGrDC5g0dFlNGmiqhTuuK4bIZ5u2WB28ho7ossJk4ZYx1utS6HGPNAPmG1FOUS6vrTI+6FjO7prfebeAYJ0pHzrCbi91KrbC+LNx/P/UgtG1HVqB9JzVKS3ylwj2utS7p6FdYgsw6bCTwPauEzXkVt8PruTdb6une0oGkyj4tKSLfUpiM9s2seZMhPbVNc2+hlMa++jkXaZ+rAtCNNJZLaGwd6T7QcI49zt6reo1pkymzefuk2zbXwrX+aD/R5ZXZDRB6ZdZrPoR3bbaMqJeOiFn+8eJL/cM8joBDeBY4BpcsuYNvnEhDYZPxB1brq2nZLe/2MTfmSO/022cUjdKPWDT43+jJnJTFOQO9/+BoV7n8GfSObTn1wQ6CYdusZkYzTBB/LATmPxMdBTOqfE5+AGkyV3WluFdbAl+uRulo28XoLNB37SHjZiOhr2KFGXhu6QNLqLdzmAdT0UQRA9EGVRH3Rw5OhRtdQmCXcvvh+8/2cZi7gXo95CWqpwh++775TpLfsh+R5g8PCBB4ef/HSpXPje8zPrDAH4v771HRk6dEjRYh7f47dPPCUfvX1JrH93KdZwv3+jD0g9uYyQ6AOVz8cJdy96kx4MsO0wogvw+xnH0tvmoaAnmGpEeZIl3T8EFnIV8sfw2fPnuZL8eHexUync063vSOumPzEXlYNSUztYUnDVqB1qYgSTrhum6c7USGmVEdKRHiptMkyWfPTLOn2fF+7pFiNItphooxHpRpiceNPk33L5t7EBXMMigfVRO4qeaf3QIdC1o+g0K4QUXLNx9XZpd21PKm83ZWuP1sjKo7Wy4nCdvGbivUaEFWLawKw1HQEji/hl5N59bOzvdCBHfiBpxGC2JDdtAyKU23Q6HayfU5Wocy0NudPZVFBmYp1M67JlmdjNM5wjyORdvbbFZyRv5+vJpizRfEiwXcK0217ZbYh7tEtrbIPf7rYuCCavf77c5ZHGZzgPu/XDNNbYlZl/3Qv4vvo17Pf0nzgGT3Sm5IG9g+SnuwbJjhZzYgbUmSYfGN0mnzICfvoglGA6sy9TZj2OPCOy7wf2JU9dzosaSQ0192ZY4Ydeb9pjvpi2f0Hh3mdwJ46eFEiZvDuprFi3sd1dJo9m+MgERC52eX+yWXLrLNkyf+hnSjSBZdqMrotJI1YNb0KnWZ/hw6egYa8DSyhIsnJ63+hQNCfhLbBoH3U58MI6H3EjgHhLbPRBwLteeCFYinDHehb6LnHTFSLsyJj0YFMqflzxYlxI/L5K6hyaRCnL8HixG06TT7h7i3uSK4p/4AN++pPVMbTYYzzfr0Zx+GP0VAn3dOt2Ob7uRkkZQYt+Ogg1aqHV9zQbcYC0vZ51pmultaNB2jsbpM2E277xXq275+u/l/r6RqkzoaGhUWrrBtvhCBHQ0Q0B/ty1TSaNYB8MMBSdPhjAlQRiWOvc8HQps41rCrgCdjYbxbtfpGOfpBHDZxcdQSHGW7dJum27xtL2rpugRLA+eGHRwDOcBR3WdBM3TDfr58WQuyD7tAaQvxxDMMIv/VUj0F89UqdvzYTYygdGGIMFHZZ0+KafPbhFhtdmXRzsEsJlZnNYZibt7jhh3qb98k3sRLC9O7k0rOlRcaxk29h6L5h9GxNDRGvW1yB29W4aTeoncPMI6jJt7IxcHUp9PchtCzTlp4kQV5rdLobMPrRkt2IQZ7YtPu392mZd3mZceTbY+3qYd1rDfCquzvaf82mvF1xe2+DrmVL9t7UaGwGP6t8eaJTv7xwsG46Zp+oAPHZdNapNPjuxVWZCwOs2QqmJO/ZKet8PJb33e+b8iRmtq2GyWuBTo//YPAlgkAy73P4AhXufIThZkDbq2P9p3uwmu6u8eEcNDlSbticY8DHIlmUO6ZzdbdNxAt8fFoisxd+lzQdiWNsh4EcMm6DtehtvuY4Tml5MFesO4oUKLLdxnTzLIckaChEVivlShHsxlDMdLNfoZAnf/v/+zf/WbbHmhSusvN//1zvzikq/r0q1mpeyDA+W9eW/+KsuriX5hLuvSxLQ/uFB0276JIs7funAvoErlmfXrt16HONYXbhgvivNkmRxL+UYryTh3tmyTU6sv0XSLVuMWO8w1yKIdRMbsW4FghUM3gcXFtbWjgFGuNfnCvevvST1Na1SZ0JDbYvOq0dRUa+mQUvnYbMyPbUMc3Wun2DEuTkWB5igsRHrSDeEx4K7CPu0BlBc+fYWCPU6WXW0Tv3TNx6r1a2bD4wodrZa01vUmj630WxffcEfsHen7PJcyi1TP71YdU2yd6KIUNd2YZlJI+PKs/i0if28XXuby5Zbce1Ls+W+zJVaTJ1vGdbrZ5fRYXLrlcz0nmjeklmF2NoobqM5MrvSletnuK3106YQZ+/hvtwEk87uNZu3KRfDF82nc+qQtHm7l+z5afPAHkkQ8Zo328yWI0bKBPP/3KGB8r13hsjqZv8SLguk+vuNgP+TiS1WwGt7lCIY/XH4MREj4HVYSb8+HowLj7ezjv2i6DClttDF1QmFex8ic4IYsFs0II0/d9LYMt8OeZ/MlnmiL2RQYtpl09nLqu3UYpt74Y4PzZuPrHDvvRcwQWB69uzZq8IYggR+6yHwh4fwgEAaMyb7Jtc4a24ovDx4GJhz5mz5z3/2VVdSmKjV3Yuf0FLrXUJCMV+qcMc2wHdP4tt33qUW4lAcRhnUNKjLw04x1vxCYB6+IywE9Te+9tXYbR7ihXGxI8OUswyA6bA/0S8g+mKqfMIdv5z44Sgh3hcvvkJGjx4le/fuM+vw7/Lscy/qAw/2Vdz0IXH71Lv7FPv9Pf7BtdAyQaUI904jErCfRNok1XlUatJHTXzI3JRM6Dhi4sMmf1Dwqv1UxyEVy+n2I9La2ixtrcelre243Pb/jtF59bpw7y4YRrF+ktEYpxtRjjDDiPLTTR7xdKNP0PEO2OuuveBqwgVg4qRyjbJlrebijI6jrxmRDms64mLcXiYN6JAFRqCfY4Q6rOqnN5p9ozX+ruOXabH3JUvoi24yNtJPpJ0ozxHPgVA35f4lRv4zrLMg9rW55aGLSpgOY/3UOislc+rdNDnzcXEG30Y/QZcWSnYevUmw3TPJQBkEx4Itt+mws6qWu3au1vybe73mzKdri/u/XYjPI4oT8Nk22AJqideE3Zb+E2H5kQHyb+8OMXHuy5e8gP/TiSfkdBXwpkS3J2pM3LpZ0vv+VS3xOvRphNSQK0XG/kdJDbve5Nw0VQiFex/C7gj3aSKcIshpMCeRTbtPRC6HjMaKF9/ZkgymXfYw9vWmLFMYzEVPWkyCExkxApZrl21fwJSWkUOzQrmngQDpDlEh4kW777QJcQOfcT/EoLfoet9gXxdXFrXI+nUNhbtfXlhWqnD3oqo7lGrdLgZYgLFuWGf8ovDlP/9iQdEHkRb1D89HOcvw+G2fz6qeJIL9LxIQ7yH+wcE/KBYSunC7CUcsAuUK97h5JVHueXMyhTtE+7FjR801pFNqYJ0zF6Ea3ODNtQgBaSR83t/0ce2xnVNTtnPqktt0fkuX/kjq5YjUpQ5Lg0D8m30Hq7gK/qMmHLPpDqSbTTAPDB0HzfxaXd4EpNEGC0mb+s4TOu+8oDNordlmdSMl5WJYz9VSXjdBUogbppj0aWb1vXDGFRWRi4NrcXxdmEYULUvrOz1WH0UHUivS1zTXSasTwkngzdmzB7XKQiPQFxihfq4R7HgpnyW8pxjc+ugndohPg8xysmI7K+IRB/V+WjONL7XTuZzWZ9v7tBXDcXmf1pSLI/Uau7qgrU2GbZDw9ZbctQzx0xdHCU0LkrNfcnD7KGjg92JW5iFGKe7fSNp8tiybR50tywp3/EHca73NmXPNWtvNCWO+J/akrbN5i4p4Z4W3ZTb12tEG+d7OofL8If/gasGZcs2oVvncpBaZ0oh5QcB7IW7i9AlJH7xX0nvvEjm2wpRFMA/EqTFfsC920qFN/ZpUBxTufYhwR2C3IJ8J5sOdNoo9j7Jt/KclIZ3Z1XECPsAuzCW9cDfL0jLEWYv7qCHDUdgrwD0iiR/c/VON0XEziSsuvzQjDr1ohFjH+OZwO4D4xtCK8HfH8I8QVKEg937rhcoArOt//w//rGIPo6IsWnS5tov6vPcVi3t3CH26/TYsxnrvpyvG3aPcZYDQrx1DYUbxb2XFLy1Tp0zJvBwrBMfL755+Vnbt3K2dkmfOPD1zPHk/+HzuSf44iVq+yxHucb/c5AMPOx7f8RTHITojAzwQ+YchXwa682KrUjly9KB0drSrwIInQla821tsqqZGhZEG5J3oxTWotT1lR5Ux4n3JEnsOLl36E6k3SrSuNp3bOTVWXcWVeWLqIOCNUMgB/vEqL4C7WCpB2l4wkXAxQNrlu9QHcWydTZ8w3x3W9NVGoL9mBPrqo/Wyywj3QgyuhduLHekFFvWzmlpkYMbtxc09s1yD2XY+Z2O3bTLWdJfXdn67IY4p1xlk21gBn81n0q7c5rLlXohrlCk3M41pr2nbEAn7GeRz64Fro5+WaElO8wSKaNJrBHstwJb6XepyNtbCbInKQFdmW7gyBb+4B2WqCWw+XsSbvY5f+01bewR5qzzqTWw2FPZHdhunZHVzg/zfd4bKC4dzBTxGjrxlbIt8dmKLjK7HtDjOTdAdgtiE5pclvec7rjMrOkYH1A63w0kaES/140yBXWqlQ+Hehwh3hJ4fiG3Wpk1hWAZ8O4tNhfXaIIcwn5DGcjTrTlQtsmlkEdTibpT76ODGfzJJEsD5gKDDaCXFCvJShDtA3T/8z39Wi7wn6pNfqnAPgYCLCvCk6SB8582d0yP++yHeZaMUtxWPX9dC7h7dWQbwFvViKeUXCb//ow9kUZL6PJQj3LvjyhJ3fPiHomLcbnqLAwd2WZFugrW0G9Fu7sG2LBDtCLjZoo2ZDi64EOxqcTcCdslHPq7zW/rzu51wFyPc01a4Y+IuxJWBpPJiCK6disvjQpnBpyOxtkmug0TacrxWXleBDmt6vbx5vE6H6c0Hvs20gW1GnMOaDrHeItMb23IfNfz6ue2UnaWXWcDUeaGe2Z5ekLu8lttpbIlJ6wxMLjNv28aSLbdlgYhz8/L5jODWyKW13rXImU82zk7nW4K4FNJhTicpSBFNThnZfRfF39d9G5PXKMz5NubDJGyZF+4A9VkhHyfi8adHg2mT6VgeCHx/9Gg6sn9WHR0g/2IEPFxpQhrNA+ZHx52QT0xokSH4qUh3khPuOgcTt22X9N7/K+l937e/nIXAD37clyU1PhytLrvkSsOfx6QPgsMq99CyFyz/p3lzAOPGpzc/szsR7KdrpTfBMNRm0joqgQ8SBPhjajniOnPBrjUBsU/XZOLeAOIIAidfgO8yQlxdGGBd9MQNMRgFgg+uBggQaEllcUBUQczByuppGhR0aOsGELOwukIQFgLfGcIMPtk9iRfUELrwGy9F9GFfQEBi2mJEeznL8Fx44fkqjJMCRDfAwxfyGH7SAxcb7Oek7fzUU09rvCjYx3E8/YydHg9PxYIHDjycheBcgGjHMVWqaMe02Ob++/YpYBnTYG7o6OSJdCfycNWA1czXu7JMOsyjrUPnE7SR6DQIcWX5yosNCdPHrkNcsO3SJmw9npZH9tbJ/3y7UT61dqhc8vtRcsvqEfJXm4bI0t2Nsu5YvGjHC27eM6RFPj3+sPzzzD2ybMF2+eW8nfL1afvlllFH5PSBreZuYBbjAoAQ12u5qek0dwzzuGPWAeO02/tAWsz13oROc63vzFz/bcB9REUT7gOYRodltNN0Io609/cShFQk1ITpmloTzP3JxAhYhr1nod7UaXvkbcAyNe3aY1rMI3O/i/yhJCwN3bN8CEE2LvRlktcV39UGqxfctvDbym3rGp/W7Yjg9xH2hy2rqXH7zMcmZPev3S/Yd/YYQidoHAd2ehxn1gaP4w2PBeZYdA8CCwafkP8za4/cNXuPdob2HO9MyffeaZTrVw6VH7xTLy0dMecWOniP/2upmbdeUpP+yfYj8aRbJP3ufzeXlfC9COHZUFlQuPdRYi8g5kODz7vgydSbj1DQhyF7uvaQwO8F8IIcWCXzBfz8jxBXF4Yf/fgeN9figMjxAg8uL0llSUAswT3Bg3WAhbO7fOAD16gFGiOVFGLZst9p/P6rrtQ4CtxAIFAhFIsF3wuCGsD1o1QR6V+oddNN6DQUT7nL8N8Hoh9A7MOanRTmnGkfBiDwkQ8f6C6+2I5S8vC/P67zDcEDkf8lAG4zSeAhxYvtUn7xePGl5XLXXfb7e5b//hWNr72263sFCoHzCBR6yAjBgwPWv7fZdiIle9twQ4bFztyETbD2ZVjz8HZoG7oI8kxwN25Pl/poiLnR5y2PBrTL1zasKzzP1s4OWd+ckof21ss/bWuUz6wbKpe+MlpueG2kfHXjUPnRzkZ55Ui9HIsZnhE37ZmNbXLT6KPyX6ful5/PfVd+t2CbfPeMXfK58QflkmEnZEhdxg5qBBNuClagW9GEAAHlgxVXGptrOuqyQt2Lt+x1PyPEfMi0tcELOHt/sMLPhjDtRbopC0K0vReMKZSboMJR21qBriIf9yn3Zxrk/GVrcP8ztZEQgmw0VANJ3yfUCajLbCt330ewIt5td90fiLFv3X7Q/Zjdp7bOh3ptZ/epFfEYztUeIziO7LGI/g724dEer/g8xwj4783eJd+auVePdQ9e/Pi/tg2SG1YNk3t3m4dYfdiPBFjXR31aauaskNT0n+rbV5VB55tVMfeUjPXfU3nina4yfYzozsDeQVlYrvloQ0NMkZJUng8dFcpMaZdvY9shFcu2Pu5Ij27KHdapJ4Bw2Lwl/yu9i/FxB2PHjuniXgKBWIwLTLFlHgg9jGQCKyd8zjG0H4YjxAMG/Lo//amPZepDqzzwHVDzucp4a3To4hB1hcA6wJc/n/tH6D9e7DjuftkQrYVe7BMdzcRvM5CvU2q5yyj1+xTqnOrHS8cDG0Tv4KYmHcbxF/c+oPsyn4sJxL3f53HtcGzjYQ4PgXhoCIGlP+oTj/2LX5ZKfUur94vHtowOn5nPVca75RTTebg7XPbUMXNVgcXXZIxwgMV4kAlNRnBqbALEJ/JNtZ0yxNz/B5v04Jq0NJoLz4BUWgaa68+XP/UZnd/Pf/YvUm/adPFxd0RFiyW+NEuherMCOWTzPoV+QDtaa2TriVp5s7lO3VwQNpnQHp08gTH1HTKvqVXmDWqR+YNN3Nii2wjbDeTOxpf5dYcoQoS8CTaTrTflPh1+apmfv91JmkYcusxkAyKbtrlsOcShjfXTBUQ27WozsY1cTqOcmgA/jSUn3bVxF4poUtUkHX5eEuIzmwrKNVZVgIyNfdrnjTi25XjwdrErxxGHP3vk+XKfN2lzbmPf+P2DFo/ua5J/eWeYvNuKB4Es0wZ2yH+c3CzvG2HEvXk40Mdajc3UYb5tj7k4jDFZ6BU398xBEo37PhTufYzozvB7x5eH9Tnp6IQJ5GvWdX7m9DIxknpCIu1i7ZxqzqhRTeYueQqAoAGl+Lh78glyCCc/3GTzsWOZN3xGy6LCHYLNj4ASvrIf8/2vf/XXanmFIPNCO4l8wt2vY9i5MyrcvSjDg0OSa5AXjxB10aESk/CCrhii38EL8riHnZByl1Hq9ykk3PHwgzfLRtcF88/ncw/3mrCzcL63v4bHCMDx4ztM+23k2xbabiFY96VL79Pvl7S+/hiJPuRg2k9++nOaLvVBoVRUuJsbJ64teis398zMbdyVu9u44sUirj1opK6zHUbU/sOHtXz/V34pTfWdMrg+LcMGdKrgR2fMwXgIgNg38VCTRwx/2UHmXo4HAnR+w0MCyurRTmOdpb6efYDJ5wMdRQ+3p+Rge43sMqJiV1uNbDMifWtLrWwx4ny7iducj3gxjDIifc4giPRWE7fIXBOPasiuQzbl5+m2i8/rtnPpQHDb7We3p6s1uHq3vbU+s6655TYAW+7TWelsy31O6zRjyy2+Hp9B20w9kkHaxfGpSDrMxFCgut+TPa6y6Llmanydjd2n0wE2jzbI2zROUP2hzKXREL+kaZ23docCHkI9Uq5HXkTA4zy6d89g+d67Q/V8C3nP0Fb58pRmOXOQmU+OgE+K3Zz1wPFLAGG670Lh3geJ2yF+L/m6pJ2Wb2eWsqftaJA4Ge08/YmKGHXVKtyLJZzWd0QEUUEGIIgALJhJ6x0V4CC0JHeXqPgL16kY0P7YsVzXkSSiwtlPGzd6S0h3loH9V2j+Hr+cQgIf83x761Y51nwscWQezOuVV17NvGwJYvkrX/5iotXfC3S0C39VQDmmD8V03HCiScDC/vzzL2l7gF8LPv+5z8ROFx6v+GXGA8u+/3WomBeZdYfbXjgszZ01crTTvq0TejFzyzZpveZo3qB5e0PFNShOuO/58i/xi7yoq7W/L/cgDbDww8rtONpR466R5YHVmzigXWY3tspME2Y1tsncplYZ24AvlsV/7+wX8tsEeZfWOhOQMeW+DKU59Zq0aV+eEepx89OkT9u8zlXLLBkpjrJMeTZ2tfYzmC5bplFmivhUJB1mYihQTfJg939X9LwztYiybWweldl6++dPVI1Q74S7Di2p9S7GWe/aqlD3aVOHtB6RKuCzZwL83X+4c6j8ZNeQHFcynPY3jjkh/2FSs4xWwzpEOiz0YYxWPsa0JugB5eeTnV9fhsK9j5K0U8K9FW1Tyo7M1xbLsMvBiWbbhsIdsRfuI6pMuHv3iCSajzZ3sR7DMopxv79ghFKhDrC+82FU2MWVo+yxx37rct3jXPO9oq4ZpGfwrjU4JjD8ZKEHAvwC4d1uPBDy111zVeaYwgMBXJ5wPOYbvcbjf9VA+w9ef03B4xC/OsCnPgqE/skYFnLf3k3qc4yAey9uxsdUyNeooFdR35Eyca00G5GMNOqOtNfIoVaTbzPlbSk58U07jntvC/fuMLKuQ19kNH1gmwr0Mwa1qVhvxHoqdmX1khsKCCMowjIrXYL6nBgE8iYQI+F00XnEtbexTUfFdkaKY8e5dBhrykzjS7J1WpwhbOEJS3LSXZvmUKCalIk99nLBvV9jV+vbZPLuadbm8WliI9K9zPTCPSPgIdK1DgLelTmh7sU9yjUfEfAH2mvlX98dKvfuacr5VQtDnN4x7rh8evxxGVTnLggQ7qGIzwh35BH76bPz6ctQuPdhitkxSXsvadpid7adr7WuIxkKdy0zMTp2nyrhDgslKKXzXwisjtHXy8eVxRHXDkKrt8UO6ZvgoQ8UEuyl4C35xRyPHpwT5Z4PJ5u9+7ZKjbnB6q3TiEDcO7XzoLna1OgN2pbpbdTHBlyDWs11p70dw0Gm5PY7/oOW/+hH/1taTKMTpmWLCc1GwcMqruLfxHgwQIwHguNm+hN4CDD5NjO/Y66+3dz8UYZlnDDp1sCalwREAoQ5XFxG1nXqC4zGNXTIpAHtGqYasT5ELfV+XipBHK7MKVO7NSLtvKgwGaTstLYsp17JLc9xezHo/N2yfFuLLfc5X64lrr0V8L6Fr8dHkM7UW2xVXI0lLMtJxzUOKFBNehh7zHUF54nG+pdF8ygwH7bGxcjbimwcCHfkrauMLbNDSdrpVNibtFrkTTmOAXeky/aWOvnn7cPlyYO599/R9Z3yxcnNcsOYdtMWAt1olYxoD8W7CZnjG6HvQ+FeQZS7o/Lt4aQq++CME8y2wWGiafPhhTss7sMGnRrhTgipXPbu36EdSHG/RKg1H7gRe0OuxjaZEXKIO83Fp9WIcSvcRT76UeuT/5Of3Cn1RiDX1aWloaZTagv4plu6tvFiIKyDgIeQD4HvPG77Vmx7cttk6vwXyNT7pWTrw7x5btF0WOZ91C3ZtPf91/ahUM+ZZ265T/uUL7d5E2si285Gue2z6SzeOh9b52KQk45rHFCgmpxk7DGVC3SBxhqyLTSfqUSN/dMa5F2ZrcsKdxXxpswKeCPS9YTwdbbcCnikbQArjgyQfzICft2xBs17zh7cLv9l2jGZM9gcTWp1h2j3wt2Jdz0Qfej7ULhXEd3dkeGRYNM4uex8c0eUcbE5j4ZSuBNCSmTvgXeNOM8O0efjnGDa+VgxCVxzQuF+x+1/olU//ul3pcHci+tqO01sRDXuxxn0YhYhrgwklSeRWTtHmHeSAl/C4cV85tM2iLSLEd1aly3HtVjrcqbJpsNyT0Zuu3nZXESoZ6YLW0Tx01i61ueW5aTjGgcUqCZ9BHsc5mKPyWydinKNUecrban/1JQ5qTNtA+GusSm3LjTWEo+jHH/erca+odWW49hB6cN7m+TOd4bK3jaIdAsuB0tOa5W/nN5mpnHCXa3wTry7c6JSjsCcyxupbMJDL19IAsduUsCNNecmi/Z2MkIIKRGMAW7HdLZjgbs0fs42ebWKaRsfkA+Du+FmwLjQ5vat8wkDbsqmfRDCZeWGpPJ8ITqNn78NGYGQNrEJOha5rqtpa2KMl44XHOXOAz/p5+Yxbrode93kESL1COF31BHMEbuQGQ/dxBiLW8fnduOhQ7xkxu62U2rwf6aBqcuG8Prv7wFhHnslkzYfuGf4EAVFYSCVQdz+yuxnpDX4PxwH9tixxxqOIXuU4WjBcajj+SPtjtHsWPDBMS71eh7o+W/aaRnOK5wXyJvp8Xfj6ONy3/w98snxR6Xe/nylgv6eXQ2yptlKf4h+G4CPQZjuu2DLkX5G9sTKH/JiGuA8JISQkqkxN2AEc8MVc9O2wtPfqG2+09RrjHKX1nZO9FqB70GZr7OxFfg2nw32Rt814FYYV15MiE6bXcfMct33iL6BNEecuPZWpCNvyzTt22gwy/AByzZxVqRnBToeEiCQ7EuOIJBssOLJBPcHsePT+DMNTD2CSZmQT6jjmyNo2nzkE+rIRgOpbOL2Y9fjJvtnjw+T0uBe8KRHEI5RJ+D1+MUximM89xzR2J0f/hzQc8qdL/YhuFYGmQV9fuIx+cX8/XLpMPsG1jENnTJpgBXmoWyvROgqQ2KxR4V5NjWxHuT4icskvMuMxuYxtqnRnECEEFICew4dNjdt3Lzh147Y3IzcTd13WkWBxuYKhDoAV5m2NhPQQdWEP/rDP9LyH/3sh1Jn7uN4CVN9HVxl9AIWwc03Ci5o3cWtYHZOfkmR2LSzi/Ptc+tDn/UsLq11fptk22VamvrM5NlSJZsL2gf46WLrXAxy0nGNAwpUkyol9swzhb48G9uUPR9szspRaIxIjFpNmwuAc41B2r75wcSuLnPMmYR9oLQl77TWy8j6lAyqNQ8E5uEg+9BrH1oR60RKZi59FqwtIYQQctKAVazT3Cytdd0Etboj2Nfx+1fya5zKWqGtJRp11sLm8XlfF1rqbMDN2tRHg5t/ckB9NMS0c/Pxy8uujw32O5nYvfId87HW93C+TlAEIWpBz7oTwJJuQ8aanhH22T/IEoTsn2kBneJCxkquU1p8GsHLGU2bj3DaKCgKA+mfxO1/f6yhPHtMuT+Umw/8qQU+E3Ccu6DHPYS2O7988OeYlte7cnPu6HUge05NGCjSWIsFYdQou3QzQxPj2mHjSoLCnRBCyMnF3VDVBQai3dx8bZm7MatrTFbQZoK5ZeHncIh+xJ7cdlnxnw254j8jok0cH1CXVN+1LjrvnHXJWbdsyGwD/c5eoLjg82Z6HzJCHSJHRYcN4Z+pdX+2FlhhZEMo0hGAT0MMeFGVEfRBCEE2GggJiTsuMseTSccLeJu3At6k9HhHsOdKmM4KeCPYM+ddVNjbc1+FvJkG14/81vVovm+Cb0AIIYScNHDzzXZIM8HcoOHrnhWz9sac9Rt3wd2YbX329pUrioMbt4awzgWzXMT2ISAa/MNBoZBtF84zG+zyM9/Fx+Z7xIvzbNqOuAMxg4AyCApImuyfKdUbuK1xwXz4kBHf0TYuYFo/fT6RDlAUBkKKJe6YyRxrJp09ht2fOwDDPK4PGX94d57olHpO2XMN55Z/kMY5pg/XWo7z0rZHDAu7tbLb5eiKVBj4NoQQQshJI3OjNjfSnD9zE8Xt2f9ZC3NcsKI3gymzwd7IcwIEfzS4G31WUIchrCsUItPkLMMLcy/IbZxxB8D3w/fVgDJT4oKZOPizN2oEW2MDQFMfoiIdhPlwegp1crKJO5Yyx6BJ5x6f5rhH0Lz7wzkCAY9rgjm/MtcADfahGWn/axjOR32Y1jxEvFuCjzOh8txl8A0IIYSQk4YVqIijAeWhoI0P/s/jZL/7M20yLcyfiuJ8ISsComIgKXSdJjpPux7ZP5P36x4EMzMN2ZS9KSOEZQDNfSjVmp5PqCMbDYT0FnHHWObYRDoImfNF/1DmPs0BbQW8C+4c1KPepK1l3afNuWzS6TTyWYu7+rpHT4YKAd+UEEIIOWnY26+9Afl0UojD3+Q92hZlGsyN3QQvlAuFuD8rB5L/wrb4i51vTgjWMwj4/tFtANDehziRDsJ8OI98Ih2gKAyEnArijj9/vPs6H+wxjTPNnFtaZmtCAe9/5YoL1hrvxLybNnty+LhywLcghBBCThru1pkYcoRoJIT1Ht82KeTDC9zuhBBkoyFc9zCAcD4QKFHhAsJ8znYwH1bUZEMUFIWBkL5E3HHpzwVf54M91uMEvHmcNpWZX7xMbcYCnxMwAxNnThQfgzDdt8E3IYQQQk4acYKzmJAPX404GsJbd1yIm6bYUMy8QPS7ZLYB6lwAYT46z+g2i4KiaCCkEog7Xv154ut8sOeBF/BWvqMm8wuXEfJWoEPAa+NM3nwEwROm+z74FoQQQsgpIbyNFgpe7PrgydybiwxRUFRu8MQtB8Gvq/nvMl2Yx804lBU6TSSEIBsNhFQ6cceyHv+IXQCaRjmC+bMCPvzzbX3O54Gv9aGyoHAnxHH8+HHZt2+/yxHSNyjnuOzrx7IXqdEQvZ3mCyFx9fmCF9OFQkYYBCGuHYL5jw0gWhb9vjp9JERBURgIqWaix3jm3DDpLtcKlCPklGf/NKcnlQ+VDb4/IX0KCI6eCKWydOl9ctvtn5L16ze4ku4DAfWXX/2ahnLWyfM//uZ/yre/812dX5R7771f3n/NzfLY479xJaSa+Nd/+6Eel6UcP//5z76q0/RV/D00jBGiYtgH898lhDcvL4S73NCLDEkU0zauDULcOmW+TyREQVE0ENLfiDv2M+eNSUfP97hzC8FfQwBiDb6gAkmlDS5NSAZ7VKQ1RhKHCdKdYdwp0tSIsYt7DoiTnhIc3/7ffy+zZ89yucLcffdP5Mc//UXJ0yWB7/Jf/+qv5a2NWzQ/atQI+c7/948mHqn5YoFYv/HmP5SZM6bJ/7nzW67UEm6vcudfDl5E9uayTsYyQkpZHh6ktm3b7nKlcdX7Fsmtt97scvnx+/7KRZfKf/nqn7vSwvw/n/uSHne/efx+V9K3aD7ebl1Ozd3T3lhtrHlTj7T5dHGWTnPNaW0Xae8QaTPxRz5st+PPf3m/1JtLUV2tSIOJ4eIaBdetk0F0nfNRQlNC+j1xpzDOa19e6BTXa4tNdjlPK+lcpHAnsdij4tQJd4jQ6665ypWWxosvLVfRciqFO6z2X/vG35jvc0D+7m+/rmV/8Zdf1+/1D3/3TZk8eZKWFQMs6f/4T9+RP/2TT3QRfBCQTz71rC4D87/pxuvkC5//rKvtWbBvfnbPL+SBBx91JfZh4RMfv12uveb9rkRkxasr5a67fuBy+YEYDbdFscsoFuyHb/3znXnFMn6x+MW9D+i+Av7YW7LkQ9LY2KhlUU6WcPf7Hvv33HMWahm272OP/VbTSeCYABD8+Tj33AVlbdfu4oW7F+zlCvclJQj3U0nkaxBCuoHKkwjQJRrbKBZ/HkavK5V2flatcG9tbZP1G7fLxi3vaH7Y0CaZOG60zJqRK5je3bVP3tm5z+WSGTJ4UJdp45Zx5szJMv60UZqvZOxRceqEe6kWxpAkAQ7Bs3v3HpfrygMP/LsK/jtu/wM5bdxYV5rL4KYmufTSi10uHi+2IAC/8uUv5gguiGvwZ//580ULJm89veen/2bmmbUGP/vs8/KNb/59RtD7713KvIsF++Xz/+HPVNzC8j9v3hw5fPiIvLb6DS3D/vpPX/q8Cl2/XsUQ7qNSllEMfj8A7NOPf/yjmg7xDz5+eeDZ517U5Z1nRO3f/s03tOxUgX1/4OAhs++/70pyv1d3SdouvU1PC/elvSDc9RKYQGS1CCGngLhzFPokiej1xFNp53NVCvf9Bw7L0oeekRMtrTJiWJMMG9IkO/cc1Py4sSPkQ9ddIg0N9dr2lVUb5JmX39B0PqZNGis3m+k8EO33Pfqc7Nx9QAYOaJBxY4bLlu27tW7B3Oly5SVWrFUq9qg49cIdInDjxs2uNj8zZkxXUZ0k3L1I6w5x7ioeWGC/c+dd8sqKVdouak0GaIP1gBDHd/yjO27La333IjhqSYcl+Qv/8StdxCV86bH80ELbE/j5RoUeXDm+8dd/q3XhAwP2YxJ79+7VdceDTShIS11GEhC2/iHMEydQYWn/l//7gy7bttTl9RY4Vj71x18oS1xXiqtMXxbuhJDKQOVKmUQuMRVB1V3ejjYfV9EOPnLj5fLxJVer4P7Tj10v77t0oQrtR554WevB7JmTtV1S+MDi87UdrOkhmAfmhXli3ljG5z7+QRXtq9ZsltVrixObJD8Q7RDhxYRiBT4EfVwAEJNxdQhJQOjhYQEiywvPf/rHv4kV5ChDHcQiHiIwDaZNEro/+elSjWfOPF1jgLZww8G6fvnPv+hKLcijHJZ9iP6eAA8J+F54GIkKSFi/P/kJWwax7MEvA0nh9dfXaJs/uPUmjUE5y4gD2wbWaP/LCX6NiAP7DKId2+rTn/qYK7WUsrzeZNmy32m8ePEVGhNCCOmKPuzbZElUomgHVSfcX1uzSS3riy9Z0MVlZf6c6Wo5h2UcAh8MbmrUdklhx8692m7B3KxwgnsN5gGRjnl6YMWHpR1W/ueWr1GrPOke8DOGi0gYIO5AtBxtiwFW+GgYNGiQ1p09f15sPUIcEN3oPIgHB6wXhCDSKMNIL3EBdd6HG9OgPX5lwKgxIbAIh1ZjAIHr3Um+8bWvqhAOQd52UB2hlnrMo7t4oQ3f7DhGjx6tcXRdk4A/OVi06HKNQU8tY9CgRhXs//a9b+sDQNPg3Aduz++etr+8wHc+zvUG+xsPa1/64udcyckFDxaPPv5b/UUl3y8ycccXgt9OcXUI+X4RIYSQSsQL+GgASeWVSNUJ9zc379A46o/umTxhjMZHjh7TOB/Wh32HuteMHDHUlYqse2ubxucvnK1xlHPnn6EPD+/u4s2xu0BUhdbaUKhGy+MEWLG8/fZWlyodiGS4VMCF5tJLLlQXmDB4ouUImOZrf/UVFfBDBg92La2bBCzCnuajzWpBh4sJRDtcYfbs2dulgyTya9euU1GPeWIe4VCUEIRw1Yk+JBQC8zrttHi/f7i+ALQpBPz8vb96uC9BTywDxwAEez6xC1asWKXxvLnWrx3bB+uGbYyHIxD3wIbtizY9EaL7LuSVV17V7bTIbKdC4PiLHldJAW0JIaQ/UckiPY6q83H/1l2/6uKPHvL88jfk5ZUb1A0GFvV8bNi4XR5ZtlzdZcIHgbuX/lqOn2hTF5k44GP/w18+IRcsnCUXnz/PlVYW9qg49T7ucRTy3y3k4x43Xej/nuRTDEslhGPo4w4hXOiBoVR/Y8wT43BjGmwHrBdcPn77xFPaURGiHBZobKfouvqOi/juU6ZMlv/1re9o587v/+udup5hx8ae8oPHQwB+QUjabiHejz26bwpRyjJC/PeNTuf3yYP3/0zH78fxEoL9HNdH4WR1DPXbCeuX7/jCMZnvXInij3P8QhV9cDqZ0MedEELKo+oub1/6zC2Joh0WdFjk0Zm0kGgHL7yyRttOm3yaK7EcONSsnVGT8Nb53XsPaky6B0RW+DN/nBsA2pQLLJ8QMxBr8D2HiIP4L4buWPmTOHbsuH5HWOIxXB+AywfcNuAGU6zgxbpB0GEav57Tp01Vqyu+69QpU7SsO2hHUCOoMc9CrkrYzt6PvRTRXsoyisUfQ3i5EfY3RDS2NwKEMOq//Bd/1cUqDgu9bxcGfCcQlnn/ehxTYbkPSb7rfjthut44vgghhFQuVTmqTBJPPrdSO45edsE8OW9BfuEAP/afP/h0lxFiIP7vvPvhvFZ9UMjyXyo799gxpk8W/qhIw95u/631HZZ2E3e69PRJ1ve4p4izuMNP++jRZk0D+P7CjQBiCxw5elTGjzstMzZ2qRZ3b92Eu8sVl1+qVmpv5Q7H28YDAgRadFQZuFbAbSWJb995l64vxFoSg5oG5Vi/4UqBEXK8hTc6uonfTlGrrW+fz6JdzK8EhcA8IHq9oMavAIXEuN8vxY7UUs4yovjtEd1O2Jce+MNHLet+XYu1Zsf9qoLjAq5NpY5M40e7KeZXCXyPSrS4b96+V63r2TcdOos7KjXWVFeLu7nutLWn1OKO8IXP2oejb3/3B2ptR6ivS+t8CSH9m3FjqtM1sF8Id4jtZc+ulHUbt6u/+m03xXeAC/Ei/2Mffl+Of7sX9CdbuJ9s7FFhRTqSWcEexH3AVQYuFG+8sVZHavFitBTh7kVSOKxi6KoSiq4k4R662ZRL3HxBknCHVTZuqMBihHt3wbLxnbF9sN3sSDb5RSC2KTrlgkLuH6CcZcRRSLhHyz3h+hYjcm+7/ZMyYviwnH1YrnCPm1cS4QNIKdBVhhBCKpOqv7zB3/xn9z+poh3W82JEO0acgWiH8A5FOyjGxYacPN5/1ZUq7h555HFXUjwQdX5IwHBYRYjK//7N/6blEH2+s2ISt37oxi5uEGHAfEBcnQ+f+Uz8sIVJHDtmO1cnvSiqt8A2wwMDtjlE79f+218WJQD9KC7FuH+Uu4xS8K4t88+O74OCdcQDA/AdY5PAwyZ+USnUIbYYfOfdm26K7z8TBQ+4PvjjDOvty/z3DMsQMPoOIYSQyqOqhTteroROouhIesu1Fxf9UqT1btSYubOmakxOHVYU5QYP0uikCSGy/PcYhaO4UXxgTYWlHqLcu2BEhSHyKAcYMz3fvGHZhlsLAlxefNoHWE9BtPxoc7NMnTpF06V2Em12wv1kgl8n/DaDRR+W6kIi3OPHQ8eDVj66s4xS6AmR7cEoPsD3R+gOjz32W43Pf895GhcCv0r54I8z/HLky/wDAMalD9v2xjYlhBDS+1StcIerC96ICqv5HbculqmRDqb5eH39Fu2UmjSkJNxt8CbWJOBOA/zQk6Q8IJbhNhMNsMQCn4ebCnzUkS5kHffgtfawRv7D330z0aUE5XBzwBCPxVh8ITqLffERXEEgUB94sLwX/GzeZLcBOpueDLxLEbZZKR1kAfYJ9hmmzTddd5ZRKl5kr34t/q3JeLjDMQUwOk8+nn7G7m8/tGQxwJ0L1vUQHO84lvEgWuovDJgW29j/SkAIIaQ6qUrh7v3T4RoDH3O8ZKlYMAQkRo05e07yeNFTjCC347RbgR5l89adGo8eaS1gpDz8y3RKCf5FPfmAtRHCMOntpiHwTQ5fh5+PD3zgGrUU+7ed5sO/FbOQBToJ/MIAConKKP7XBgjHYoEohKAGsNaWKirvve9BjfO5f5S7DP99IPpLAZ2Qsa/QFwIPUVEwRCQo5NqDhxIvtkux4r/40nK5667sOP1g+e9f0fjaa6/SuBQefti+0KuYcd89eHAo9kGXEEJI36DqhDuEN0T7mTMmFe0aE7Jmw9sanx28KTWKr3tphf2JPAQdYV9bu0XfnlqKlZ90BYIJrhKlhGIFH9r1tLsA5ofX+MPymU8QQWxCMBayQCcBoQlrMMRiqd8BvuYYpQXLj1p8k3jqqac1htD90Y/v0U6jSSGKtyIDiOUkyl2G/z4Q/cV+H4Dt9oXPfUbT8KeH8PcvRsIoQ9g+WJdPf+pj2iYO7Ae4UQH0cygFHCNRoQ93IiyzVLcpfG+/vvm2cRS45aDzLI5HQgghlUHVCfcVr7+l8elTx6tFPClAYEdBp9Qt23ere00+Kz3q8HIltH1s2fLMvDDf+x59Tq3xiy7iT9bdAaLIC6lSw4533nVzOfn41/j/5rdPahyH76j50duXaFwq3r2mHMusd62ByCt2HPf1G+w5hU6TEOH5QhQvyPFrSL6HjHKXUc738aBvAV5ChWkh/L/xzb/X4B+KwvHvo+A4wzjvfpjPfA9geOttiLfwT5wwXmPg3Ymuu6b4fQrBjV9O4J6F74A+GUnrG+0TgWnxYi5MV+rDHyGEkFNHVb45tRji3pzq36oafVNqHBDrz5n2sO5HKWb6vo49Kk7dcJA9QTHDQSYBcQXLLxg6dIgcPnxEp4UPsR82EvjhBnsCiFv8ahDi5++HFPT56Hp4fH2+4SC9hbVYwYb2eClUMUR/8fDTwu0p3/K6swwcM0nzL3b5EM5+LH50GI5ze8G8XnnlVXWFgsiG6P3Kl7+YaCHHMQRrPtqdPT87eg3KMX345lr/Zti4MeWjwML+/PMvaXuAY+Hzn/tM7HR4wMDDCMAvPB68gVdHr7nxuqJdwXoSDgdJCCHlUVXCHWJ634HDLpefUSOGSkNDvctZYDEHpQz5iOEmvU875jd9yriSfOr7KvaoOHXCHSKjXIs0OgtCZHdHuEOk+XG8Pd6qGc4TIsqPBNJd0GEyOt53VLjjO8BSCmtwnFtQMcKdlI9/WRcesj74weti90EIXHB+ce8DKpI9OI5gWfcPaf5YS3oYi+I78aL9B6+/Rn85yAes8vCpjwKh/5++9PlTYnGncCeEkPLoV29OJcVjj4qTL9wBrJ+DBg0qaHlMAuIfY2+j42YoSmDpxPjnxQpazCekkEjrDfx3Qadbv3yUJa2LtzCfinXtD/hjoie3r7fkJ1n648CxXO750RegcCeEkPKgcCex2KPi1Ah3Qkh1Q+FOCCHlwcsbIYQQQgghFQCFOyGEEEIIIRUAhTshhBBCCCEVAIU7IYQQQgghFQCFOyGEEEIIIRUAhTshhBBCCCEVAIU7IYQQQgghFQCFOyGEEEIIIRUAhTshhBBCCCEVAIU7IYQQQgghFQCFOyGEEEIIIRUAhTshhBBCCCEVAIU7IYQQQgghFQCFOyGEEEIIIRUAhTshhBBCCCEVAIU7IYQQQgghFQCFOyGEkJNG2sWEEEJKh8KddIuOTt6GCSHF08lrBiGElA2FO+kW7e2dLkUIIYXhNYMQQsqHwp2UT0qk1dyEaXUnhBQDrhW4ZuDaQQghpHQo3ElJpNwdN7zvHjvRTvFOCMkLrhG4Vnj8NcRfUwghhBSGwp2UTPQ2m06n5cixNjnW0i7tHWmTdxWEkH4NrgW4JuDagGsErhUhlOyEEFIaKXMhpcwiXbBHhRHhLo3DxMY2DQN7p5aZdGc2bf412DzmYdtrrJ+EkGokY0FP2RSiGvOB2IaU5mtqgrQrN/+ZNhrbOWg6BNea1nY8DIi0mXjJh2/W8qW/vF/q60TqakUaTIxlEEJINcLLGykSdwdFFCQVk7A32EyJuwn7dPZmzMDAUKXBfOBcBzatSYcT4a4sUxWUBQlCCCEJULiToglvxP4mbG7HervVgLwPvhwfir1xMzAwVGcwn/ZMR15zkXPetdArgyvzhGlCCCHJULiTPNi7KT79jTW8v2rafNg6L9RRYMtyfwK3efPJwMBQZSHuXEeMak2bJDKZMs1bfFqnc+ncFoQQQjwU7iQWvcEmgNuwrfaxbe9vvFqayVtf1kydL2dgYKie4M5tPdfxpwX4x59rg4Qt1k8td7k4fHtCCCFZ2DmVJGKPDNfh1OW1A6qmbbnvhIq8jgjp80Eb4GNCSPWSEecm9pZ2LTMfYUfUbKdV38blfXtIeo1zYedUQkh/h5c3Uhh3A9UbK4IWuRtuEOPGjAaZNpp2eXOkaUCagYGhukLO+e3Oe3s50A8V7foXE6MOCeBjQgghsdDiThKxR4a1sOMDsbei56QzMcqzaZvz6SzRPCGkcsmIbocKcZvKiHIV6T5tPjKxb+/S+NDYtY9CizshpL9D4U4SyR4ZTowjpWVxQj1sg09fj0Tmw30SQqqJrMa2yht5L7y7CHStd7G28WXZvH7aKAcKd0JIf4fCneTFHh2Q5xppnClDXsty0+7fxZpy0xBCqhkvtlWq238XZ4V6mNaclvl22bI4KNwJIf0dCneSl+zR4SS4+UBsy+MFe6bORjYO6VJACKlYIiI7ToRnywqLdv20URco3Akh/R0Kd1KQJCGeKfdlGtuM5lHkE64kkyWEVA1ZnZ0rvDUyHxDs7j9HtNuU/bDZbHkcFO6EkP4OhTspSCi+NWk+fFEo3m3KfriWlrCOEFKVZPR2ILxDwa55TSSLdv10dXFQuBNC+ju8vJGCZG+kehvW+ytiDa4AsQZXZsdpzg3ZsZsZGBiqKeQ93901AQG5TJnN2th/2ogQQkgCtLiToulieQcmER5A3gLv4dFFSP8jV4Bbse7RpMvbyH0GbZKgxZ0Q0t/h5Y0UTfbGam7E+mk/MmnEyJsPBJTatC9nYGCo/mA+Mue+Pf+BLbUfmbT/tBEhhJAC0OJOSib3iLGZTFFQxwOLkP5LjhZ3mWxZkCpBtNPiTgjp7/DyRkoGN9rszRaJlPt05S74dhqyxQwMDNUazIcPvjA8/3NSKLdJQgghRULhTsom98aLhA3Zv6DUfDAwMFR5CM9595dbaj5NhEAIIaR0KNxJt8m5cefclJFgYGDon8F8mij+2kAIIaQcKNxJrxB3w2ZgYOhfgRBCSM9StZ1TW1vbZP3G7bJxyzuaHza0SSaOGy2zZkzSfBLv7ton697aJocON2t+xrQJMn3KOBnc1Kj5kA1m/keOHnO5eCaMGyXjTxvlcoQQQsqFnVMJIf2dqhTu+w8clqUPPSMnWlplxLAmGTakSXbuOaj5cWNHyIeuu0QaGupd6yxPPrdSVq3ZLAMHNMi4McPl0JFmOXCoWfNLbrhMRo4Y6lpa7n/0OdmyfbfLxXPZBfPkvAWzXI4QQki5ULgTQvo7VSfcjzYflx/fu0zTN11zYY61e/XazfLEsytl2qSxcrMR7yGvrNogz7z8hiyYO12uvGShK7UW+Acef1HTn7rt6hzBf/fSX8sAI+qvuHC+K+nKkMGDYq31hBBCSoPCnRDS36m6y9trazapZX3xJQu6uKjMnzNdRTus5BD4HqSXr3pTzpwxKUe0A8zj/AVn6Dy3bNvlSi2wxg8f0qRtkgJFOyGEEEII6QmqTri/uXmHxkm+7JMnjNE49E3fvHWnCvMLzpntSnKZPXOyurwMGdxVhI8ZNcylCCGEEEII6T2qTrjDCg6rehItrW0ulQUdWOELH/Vh98BqDj91WNA9cKGJgg6xoSWfEEIIIYSQnqJqR5WJA8L6Z/c/KcdPtMmffux6Vyryrbt+lfF7f3vbLnlhxVrZufuA1qH8nLNmytTJp2neA+H+8weflluuvVj27j+krjaw2gN0Zp09Y6Jccv682E6whBBCSoc+7oSQ/k6/Eu5+1JjoSC8Q7vBvHzpkkLy2douK7qGDB8nho8dk/cYd1o1m4Sy52AhxDwT+rx57XkepaTH1Z82epkM/7t1/WC348KPPN4JNqezcYx8kCCGkv9Jp7lZt7SkV7ghf+OwntPzb3/2BinaE+rq01HAMeUL6PePGjHCp6qJfCHdY2pc9u1LWbdyuYvq2mxa5Glt3590PZwT4rddfltOhFPX3PfqcWuA/cuPlGXcZPwoNBP/iSxd2EedJDwmEEELKgxZ3Qkh/p+ovbxjTHe4xEO0Y6jEU7cALbgjzRRct6DIKDOr9cI94MZMHHVYh5ONEO4CbDHh9/RaNCSGEEEII6Q5VLdxhFf/hL59Qn3b4okeHeowS9WP3eCu7f5sqgMBHeZIbDMrhH4/OsoQQQgghhHSXqhXucFWBKwvE8x23Lk4U5QAjyhBCCCGEENKXqUrh7v3L4RqDkWIKvQRpykQ7fGTSUI6+HKPFeO554Cnt1JqPQ0eac6YhhBBCCCGkXKpOuG/YuF1Fe9xbUJM4c+ZkjfHW1TiWr1yv8elTx2sMprgXOWF5caxeu1ndZM6eM82VEEIIIYQQUj5VJ9xXvP6WxhDZGGs9KWC0GA981WGdf3nlBnl++RsZCzti5PEgAJeb8G2s71kwS63pjyxbriLdzw/TwLf+iWdX6kg1aEcIIYQQQkh3qbrhIAu5r3jCoR1BOGRklKQhHzFiza+fXpF5WVMIhP5Vl59b0E2HEEJIcXA4SEJIf6eqhDvE9z4jpoth1IihsSPCQIxv3rrT5USmTxknI03bfJQzDSGEkNKgcCeE9Hf61ZtTCSGEVC4U7oSQ/g4vb4QQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAFO6EEEIIIYRUABTuhBBCCCGEVAAU7oQQQgghhFQAqbTBpauK1tY2Wb9xu2zc8o7mhw1tkonjRsusGZM0n8S7u/bJure2yaHDzZqfMW2CTJ8yTgY3NWo+jg1mOZvefldOtLTKwAENcvrU8QWXQwghpDQ6O821vV2kvUOkzcRLPnyzli/95f1SXydSVyvSYOIamqQIIVVKVQr3/QcOy9KHnlEhPWJYkwwb0iQ79xzU/LixI+RD110iDQ31rnWWJ59bKavWbFbxPW7McDl0pFkOHGrW/JIbLpORI4a6lln8NGDapLFFLYcQQkjpULgTQvo7VSfcjzYflx/fu0zTN11zoYw/bZSmweq1m+WJZ1eqwL7ZiOqQV1ZtkGdefkMWzJ0uV16y0JVaC/wDj7+o6U/ddnWOEA+nueT8eZk6v5wzZ0ySaxefr2WEEEK6B4U7IaS/U3WXt9fWbFKL9+JLFuSIdjB/znQV7Vu271aB70F6+ao3VWiHoh1gHucvOEPnuWXbLldqXXEwDSz6mCYU9FgOxPy6jdtV+BNCCCGEENJdqk64v7l5h8ZJPuaTJ4zR+MjRYxqDzVt3qjC/4JzZriSX2TMny2UXzJMhg7N+7hDxmOai8+a6klwWzD1dY8ybEEIIIYSQ7lJ1wh0+6bCqJ9HS2uZSWdCBFZbzOB92gI6p5y2YlWPB37Fzr8ajR8ZPg3nBN37rO3tcCSGEEEIIIeVTdcL9S5+5pYv/ugfuLbDIQ1CHIhyuM+jACt7etkvueeAp+dZdv9Jw/6PPaVkUP+pMktgH6OC6c/cBlyOEEEIIIaR8qnY4yDj8CDBwe4EF3QOBDv/2oUMGyWtrt8jsGRNl6OBBcvjoMVm/cYd1o1k4Sy4+f56bQlTcQ5TjQSEJiH48FORrUyw79/ABgBDSv+k0d6u29pR2TtUOqh0pLa+vTWvHVIT6urTU2GJCSD9m3JgRLlVd9AvhDkv7smdXamdRDNN4202LXI2tu/Puh7W8xQj0W6+/LGfMdtTfZwQ4RPpHbrw8Y6mH2AcnS7gTQkh/JzqqDNIAI8lwVBlCSH+g6i9vGNP9Z/c/qaIdI72Eoh340WAgzBddtKDLi5ZQf8WF8zWNFzN58vnRE0IIIYQQ0tNUtXDHOOs//OUTcvxEm9xy7cVdhnqMMnXyaS6Vi7eye792QgghhBBCTjZVK9zhz46XI8EyfsetixNFOcCIMqUydvRwjWHRTwJuMrTME0JIz5Fy/uuI4cuOEJYRQkg1U5XC3XdChWsMRpiJur9EmTLRiuvwpUwhvhyj0XgmjhutcdI47X4kGi/wCSGEdB9ocy/Q4cvu/dlRRt1OCKl2qk64b9i4XUV73FtQkzhz5mSN8dbVOJavXK/x6VPHawxgwYelHm9PRQfWKK++/pbGZ7sXMRFCCOkmEOcmwMpeC9EeSaugRyCEkCql6oT7CieYIbLf3bUvMYRiGz7ssM6/vHKDPL/8jYyFHTHyeBCAy0v0bazozIqhIjHqjHeZwTSPLVuubjIYQrKQtZ8QQkgJOIFe64Z/RECaop0Q0h+ouuEg/TCNhQiHdgThkJFRYL1ffOnCzAg0IavXbpYnzHRR8CBQrMWfEEJIYXC3wljuGjptDNTyHljgVcQTQkgVUlXCHeJ7X57OoiGjRgyNFeKwnId+69OnjMv7dlQAKzum8Vb8YqYhhBBSGrhb4YalsQsAQj0TXJ4QQqqRfvXmVEIIIZWNF+/2I4CinRDSD6BwJ4QQUlEk3bUo2gkh1Q6FOyGEEEIIIRVA1Y0qQwghhBBCSDVC4U4IIYQQQkgFQOFOCCGEEEJIBUDhTgghhBBCSAVA4U4IIYQQQkgFQOFOCCGEEEJIBcDhIEkX8PbYltY2GdBQ32feABu+FXfI4EEyuKlR04QQQgg5efRFjdCfoHAnGXAyPvSbF+XAoWZXIjJiWJPc8P4Lizo53921T37+4NMybdJYufm6S1xpYSDK73v0Odm5+4B86TO3uNIsr6zaIM+8/IbLWbCMyy+cz4sGIYQQchIoRSPE3bejxN3v48inEb51169cKp7LLpgn5y2Y5XLVAYU7UXBi/Ns9v5bh5iS86Nw5MmrkUNm3/7C8sGKtHDQn6aduu1oazNN1En76Ey2tJQn3DRu3y7LnVul0IHpSPr/8DXl55QY9+aZPGaeW9i3bdskLr6yR4yfaCq4XIYQQQrpHqRrBC3fcu5MoRlAX0ggQ7tAckyeMcSW5TBg3SsafNsrlqoParxtcmvRjnn5xtezYuU9uu2mRjB09XE/A4cMGy7TJpxnhvF5a29pUOCfx0K9flLr6WhntnrrPPGOKxvn4lx/+u6x9a5vMmz1FGgc0yMHDzXLheXNcrcjR5uPm6f4lWTB3ulx8/jxpbBwgtbW1esGYYk7SV1a/pa84nzxxrJuCEEIIIT1NqRrhtTWbZHDTQFl86TkqnuNCIQppBPwCsGrNZrlm0Xkya8bk2GXAtbbaYOdUouDgh0CO+o4jj3LUJ4En6517DsoHr3qvKykMnt7xlPyRGy+XKy9Z6EpzWW9OWLBg7ukah/if5XbvPagxIYQQQnqHUjWCt5CXSzEaAX72/REKd6K+6WDiuNEaR/Hlvl0IyvBz2HVXvqfLCZ0PPK1fu/j8vD9h+ZMynx/7QPMUTgghhJDeoVyNMGxok0vZujgNkUQxGsHjrer4lR7LQFzNULgT2bvfj9YSL7x9uW/nwRPxA4+/qD5sUyef5kp7DrjHJHVegd8bmDg+/kJCCCGEkO5TjkbYsn23DDWCGv3U4IeOgSsQ4P6CX+l7gnd22gcBaJG7l/5avvfTx3QZiO9/9Dl1palGKNyJHvTFEG33yBMvy7gxw096j22cjOisMm7sCJk/Z7orJYQQQkhPU65GWL7qTTl85Jjccu3F6vKCAPcX/EoPQd9TYKSbc+efkVnGBxafr+67Sx96piqt7xTupCAYqzUKTjqcGB943wWu5OQASztOxsaB9XL15ee6UkIIIYScCqIaAQIeAv3sOdPU3QW/yMPlBQF51GG0uO6KarjTYF4YjhJGPL+MWTMmyU3XXKh+9stXrnetqwcKd1KQaAeQt7ft0pMOJwZOnJMBLgSPLVsuj5gAK/8f3nwlx3AnhBBCTjFRjQBdgCGh4e4axzlnzdTYu7qUC8Q6lhOnBSDg8av81h27XUn1QOFOdMgk0NrarnEUX+7bPfrk7+VM80QLfIcTH06YExjB53sCuMb87P4nZd3G7fK+SxfqiXqyHhgIIYSQ/kypGqEQDQ11Gh85ekzj3mKg0Qnhy6KqBQp3IqPc0+qOnXs1juLLfTv8/AQR7TubhAFvNkPw+e4C0Q7XGLxs6WMffh992gkhhJCTSKkaAUY7dEAt5ArTXQMcluEHqkiiGkeeo3AnevLAT+y1tVvUJSUEeZSj3p9kGOklKaAdgs+HROddCLRHpxNwx62L6RpDCCGEnGRK1QjweUcHVP8uliir3tikcfSljqVqhD37DulAFXHTweiHkW1mz5joSqoHCneiXH7hfLWkY6QYfxIgRh7lqO8O9zzwlL4uuZTOKOvNkzR+5kIHF/yk5t1vwlCtwz0RQgghfYVSNAKMbBDyGFUmtIijPQa2wC/2FyyclfPul3I0woJ5p+uylz27Mmc6aAMY/WBtP3/hbFdaPaTSBpcm/RycYHh6xYngwYG/+JIF2ku7GDB2KoAfegjGbsV84e4SZznHdHg6Dq306IyKEzwfuDhEl0UIIYSQnqUUjeBFPe7rAO38dBDt0Y6r5WgEEK5TuIwRw5p0tJlq/KWewp3kgJMNlm7E+NlrtjkZ/c9fxeAt4NGTBU/DmGfSSYTp0DMdPcE9viwf+EmuGk9MQgghpK9RqkbAfXzz1p2aRju4x8S9Zb0cjeDBdFu27cp0dh09clivvBSyr0DhTgghhBBCSAVAH3dCCCGEEEL6PCL/P1TzXQC5ZUu9AAAAAElFTkSuQmCC',
      tableList: [{ time: '近一周', percent: '－0.10%', fontColor: '#45C04D', color: '#fefeff' }, { time: '近一月', percent: '＋0.47%', fontColor: '#FF6961', color: '#fafafc' }, { time: '近三月', percent: '－0.10%', fontColor: '#45C04D', color: '#fefeff' }, { time: '近六月', percent: '＋0.10%', fontColor: '#FF6961', color: '#fafafc' }, { time: '近一年', percent: '＋0.10%', fontColor: '#FF6961', color: '#fefeff' }, { time: '今年来', percent: '－0.10%', fontColor: '#45C04D', color: '#fafafc' }],
      detailArrow: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAUCAYAAACEYr13AAABtklEQVQ4T6WTz0obYRTFz/liIoio64IgSMAYLBITmVHxAURcuXAhPoJiSkERbKAglhbcddFs+hYuXMSFMZJ/gpo/+ARKENyoJI3fFRM7mUYzSXGWwz2/+5177iXe+fGdelgAEVGJbHFOa9HTQd8+SekEbgESqdyCkNt1EWOo9G5PTQ0+tINYgONMYR4iEZvgUlUf1w1j7NoJYrfARLawBsGyTXCjqD4bEyNnrSCvhvhsBeSmAO6aSFAh5KsZ8u+/BXkzhXgyN07yB4iBF4hA4bcZ8P1sHm7LGI+yxQ9K6z0Aw43OjHlw9yUYDN7//ee4B+l0uqeCnh0AM/bhdmkJT076r2p5tYspnrpYJNWGVUeUqbllhnyHjoDaYmXyYYBLllhYUi79yQj4844WYrlcb/c9d0EYVqGg8MflDs8GvCX7q19ZSCbPBx9dak+EQ41CObjt90TmvN5ys+V/APHkRUgpfhOwrxEfo2ZgJNrqNizASab4UYv+BaCrfg4oK42IERo96GiVj1P5FRCr9c4sUVXD5sRYoV1K1gtip6cDHt29SQ1WXe7vzcPq+BbadXQc4v+Kn+ufADMClBVv8AF9AAAAAElFTkSuQmCC'
    };
  },

  methods: {
    timeClick: function timeClick(index) {
      this.i = index;
    }
  }
};

/***/ }),

/***/ 38:
>>>>>>> c4674d27ce2d0d102339398a4112b4b7b2d91c6f
/***/ (function(module, exports) {

throw new Error("Module build failed: Error: ENOENT: no such file or directory, open 'E:\\hzBankDiamond\\src\\view\\gold.vue'");

/***/ })

/******/ });