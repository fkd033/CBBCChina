// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 81);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(82)
)

/* script */
__vue_exports__ = __webpack_require__(83)

/* template */
var __vue_template__ = __webpack_require__(84)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\pwdset\\faceFind.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-adacbce6"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 82:
/***/ (function(module, exports) {

module.exports = {
  "top-box": {
    "marginTop": "16",
    "flexDirection": "row",
    "paddingTop": 0,
    "paddingRight": "35",
    "paddingBottom": 0,
    "paddingLeft": "35"
  },
  "note-box": {
    "width": "162",
    "height": "78",
    "flexDirection": "row",
    "position": "relative"
  },
  "bg-img": {
    "position": "absolute",
    "width": "162",
    "height": "78"
  },
  "line-top": {
    "marginTop": "30",
    "width": "83",
    "height": "3",
    "backgroundColor": "rgba(56,148,252,1)",
    "opacity": 0.3
  },
  "face-box": {
    "width": "161",
    "height": "68",
    "flexDirection": "row",
    "backgroundColor": "rgba(235,244,254,1)",
    "borderRadius": "6"
  },
  "icon": {
    "width": "36",
    "height": "36",
    "marginTop": "15",
    "marginLeft": "15"
  },
  "text": {
    "fontSize": "22",
    "marginTop": "15",
    "lineHeight": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(56,148,252,1)"
  },
  "line": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginLeft": "36",
    "marginTop": "27"
  },
  "title": {
    "marginTop": "76",
    "marginLeft": "52"
  },
  "identity": {
    "width": "500",
    "marginLeft": "53",
    "height": "60",
    "marginTop": "86",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "line-bottom": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginLeft": "36",
    "marginTop": "15"
  },
  "note": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "marginTop": "86",
    "paddingRight": "53"
  },
  "note-input": {
    "width": "400",
    "height": "60",
    "marginLeft": "53",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular"
  },
  "note-text": {
    "fontSize": "26",
    "fontFamily": "PingFangSC-Regular",
    "lineHeight": "60"
  }
}

/***/ }),

/***/ 83:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  data: function data() {
    return {
      identity: '',
      id: 'faceFind',
      note: '',
      tip: '',
      noteText: '',
      pass: false,
      color: '#8F9AAE',
      bgImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPMAAAB1CAYAAACbH9f6AAAF/klEQVR4Xu2aPWyVZRiG7/s754TEAwkNFOtEwgYMhIFYw4CzW5cOxBjqKIkmLDqQiIkxahzFUYxx0IaEjRkGUKkJkohLJ1xoKETIoYbanu8xbcRwiH/0/HznPOfqVuj3ve993c+V8/BjPfU1Px+1l1/RZFG2p0sXx21NW5oKqfH0z/I9BCDQfwIhPbL0i+Qfy0LfuNTVSxe1PDvr9pOn+8lvFhdj286p9oyL4s0ITUvq+P3+X5sTIACB/yAQtr6LKM/ubtbO2159/PMdsi4/jFNWnA5pAqQQgMAQEwg9kP3+5HZ/0iHzn6v1q1ac49N4iAvkahDoJBAhz126qK82Vu7NT+Y7K78fdtQvSNoLLQhAYKQI3KrbMxNNX3dE1O/9pjMR8Y6k2kjF4LIQgEDb9oe7ntMZL7ViT92aD8UxuEAAAqNHwPLl9dCs7z+KfWvrcUXS1OjF4MYQgICkpUbdR323FfvDcUP8OzJTAYFRJbDm8CHfbq0erLvx06im4N4QgIBUNHwQmZkECCQggMwJSiQCBDYIIDNzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBJA5SZHEgAAyMwMQSEIAmZMUSQwIIDMzAIEkBDZlvtuK/eG4IamRJBcxIDBuBNYcPuT7j2Lf2npckTQ1bgTIC4EkBJYadR/1Uiv21Kx5KY4lCUYMCIwZAV9uh2YdEfXlh+V7tt+WVBszCsSFwKgTaEfER5Pbi3e9keTXlTi8rrig0N5RT8b9ITBmBG7V7ZmJpq9vyhwRxXJLr7mIzyVt/hpfEIDA0BOIKP365A59abvsEHf5YZyy4nRIE0MfgwtCYJwJhB6Ujg+e3177+DGGDpkXF2Pbzqn2TKh4y9aLfEqP87SQfUgJRIS+t8tPdzdr522v/q3Mf63cK9pTlJouHcdtvSTpBf5ybEir5VrjQKAt6XaEvi1q/rosdXWyqTsbq/WT4fnz8YiMwt2VOBJlfCHrwKCubPmmrLndTS8M6kzO2ToBZN46u4E/uSl0xFlJRwZw+ILtk4g8ANI9OgKZewRyUK+53Vo9WHfjXJ+FXigaPrFrm38eVC7O6Z4AMnfPcOBv6PPKfVP2icmmfxh4MA7sigAyd4Wvuof7snLb1ySdROTqeu3mZGTuhl7Fz/Z05bavFaXmdu1gta641i0fj8xbRjccD/Zo5Wa1Ho46u7oFMneFbzge7mrlZrUejhJ7cAtk7gHEYXjFllZuVuthqK5nd0DmnqGs/kXPuHKzWldfWU9vgMw9xVn9y/7Xys1qXX1RfbgBMvcBatWv/NeVm9W66nr6dj4y9w1ttS/+h5Wb1braWvp6OjL3FW+1L+9YuVmtqy1jAKcj8wAgV3nEvdU40F7XG7VSn/EfQqpsov9n/wFJM5aChpRb4gAAAABJRU5ErkJggg==',
      noteIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAACFUlEQVRoQ+2aTUvcYBDH/5NkBd96Ugptz73rguz6sn4Boaf12F4Ewa4Xzy2uglcvG0TBS3t0T4JfQNvtroL23nMrSHtSsNC8jEQWlLBmN8mTasLkmDzPZP4z8xvCkyFk9KJuuvJVHsiNOHMAvwbD6LY+0ecEG6Af1h/94LRK10HvChRW3OR+t8/ZIeANgGEAXQORqDCAAVwxsK/90xebK/T3ofcFC9uyy+xiF8CzhB0Oa/6SGAvNZaMeSVjBtD4AtP4EMuX3n0H0sfVe34gmrGZVQbQaNpz/ZT3zWms5V1UmrFUxHoWzgml7fN1dIqxDTgsdSlEyphg0KcV2QAMbgZSi4rLrZE5KUUrR9+Uh7V4xd8KYMCaMKYbKZ04YE8aEMWEsUgSkeUjzkOYRCZ2eNwljwpgw1jMukRYmzhiY1yJ5FneT/+eI6rP7uP4p2y/CevwpoSzicQ1JxnrMmJwrtgNV3mP9/AJjtmbPercM1zh88Rzf6/PkhKnGxNt92IxNbfGMw+5nML+6FUL0UyftbWOJvqRaWMG0vwKY8olotCrGdKqFFU3bGzgZui+CgevjijGYamGZzdikySWX3E/3GdNYe/etQkepzpjXFX/9tsYd6CVPiA7n6OVo7iz1XTFMVoLWJt7uVTka206sT6qsDolN1OyyRk9zrM9lLJxEHevL7/CAYTnbmRvE9DjI5OhsbMAf0cANJkJVVXU6X3QAAAAASUVORK5CYII=',
      faceIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAACFUlEQVRoQ+2aTUvcYBDH/5NkBd96Ugptz73rguz6sn4Boaf12F4Ewa4Xzy2uglcvG0TBS3t0T4JfQNvtroL23nMrSHtSsNC8jEQWlLBmN8mTasLkmDzPZP4z8xvCkyFk9KJuuvJVHsiNOHMAvwbD6LY+0ecEG6Af1h/94LRK10HvChRW3OR+t8/ZIeANgGEAXQORqDCAAVwxsK/90xebK/T3ofcFC9uyy+xiF8CzhB0Oa/6SGAvNZaMeSVjBtD4AtP4EMuX3n0H0sfVe34gmrGZVQbQaNpz/ZT3zWms5V1UmrFUxHoWzgml7fN1dIqxDTgsdSlEyphg0KcV2QAMbgZSi4rLrZE5KUUrR9+Uh7V4xd8KYMCaMKYbKZ04YE8aEMWEsUgSkeUjzkOYRCZ2eNwljwpgw1jMukRYmzhiY1yJ5FneT/+eI6rP7uP4p2y/CevwpoSzicQ1JxnrMmJwrtgNV3mP9/AJjtmbPercM1zh88Rzf6/PkhKnGxNt92IxNbfGMw+5nML+6FUL0UyftbWOJvqRaWMG0vwKY8olotCrGdKqFFU3bGzgZui+CgevjijGYamGZzdikySWX3E/3GdNYe/etQkepzpjXFX/9tsYd6CVPiA7n6OVo7iz1XTFMVoLWJt7uVTka206sT6qsDolN1OyyRk9zrM9lLJxEHevL7/CAYTnbmRvE9DjI5OhsbMAf0cANJkJVVXU6X3QAAAAASUVORK5CYII=',
      pwdIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAYAAACMRWrdAAACFUlEQVRoQ+2aTUvcYBDH/5NkBd96Ugptz73rguz6sn4Boaf12F4Ewa4Xzy2uglcvG0TBS3t0T4JfQNvtroL23nMrSHtSsNC8jEQWlLBmN8mTasLkmDzPZP4z8xvCkyFk9KJuuvJVHsiNOHMAvwbD6LY+0ecEG6Af1h/94LRK10HvChRW3OR+t8/ZIeANgGEAXQORqDCAAVwxsK/90xebK/T3ofcFC9uyy+xiF8CzhB0Oa/6SGAvNZaMeSVjBtD4AtP4EMuX3n0H0sfVe34gmrGZVQbQaNpz/ZT3zWms5V1UmrFUxHoWzgml7fN1dIqxDTgsdSlEyphg0KcV2QAMbgZSi4rLrZE5KUUrR9+Uh7V4xd8KYMCaMKYbKZ04YE8aEMWEsUgSkeUjzkOYRCZ2eNwljwpgw1jMukRYmzhiY1yJ5FneT/+eI6rP7uP4p2y/CevwpoSzicQ1JxnrMmJwrtgNV3mP9/AJjtmbPercM1zh88Rzf6/PkhKnGxNt92IxNbfGMw+5nML+6FUL0UyftbWOJvqRaWMG0vwKY8olotCrGdKqFFU3bGzgZui+CgevjijGYamGZzdikySWX3E/3GdNYe/etQkepzpjXFX/9tsYd6CVPiA7n6OVo7iz1XTFMVoLWJt7uVTka206sT6qsDolN1OyyRk9zrM9lLJxEHevL7/CAYTnbmRvE9DjI5OhsbMAf0cANJkJVVXU6X3QAAAAASUVORK5CYII='
    };
  },
  timeChange: function timeChange() {
    var _this = this;

    if (this.noteText == '重发') {
      this.noteText = 60;
      this.color = '#8F9AAE';
      var timer = setInterval(function () {
        _this.noteText--;
        if (_this.noteText == 0) {
          clearInterval(timer);
          _this.noteText = '重发';
          _this.color = '#4BA0FF';
        }
      }, 1000);
    }
  }
};

/***/ }),

/***/ 84:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "title": "忘记交易密码"
    }
  }), _c('div', {
    staticClass: ["top-box"]
  }, [_c('div', {
    staticClass: ["note-box"]
  }, [_c('image', {
    staticClass: ["bg-img"],
    attrs: {
      "src": _vm.bgImg
    }
  }), _c('image', {
    staticClass: ["icon"],
    attrs: {
      "src": _vm.noteIcon
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("短信验证")])]), _c('div', {
    staticClass: ["line-top"]
  }), _c('div', {
    staticClass: ["face-box"]
  }, [_c('image', {
    staticClass: ["icon"],
    attrs: {
      "src": _vm.noteIcon
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("人脸识别")])]), _c('div', {
    staticClass: ["line-top"]
  }), _c('div', {
    staticClass: ["face-box"]
  }, [_c('image', {
    staticClass: ["icon"],
    attrs: {
      "src": _vm.noteIcon
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("设置密码")])])]), _c('div', {
    staticClass: ["line"]
  }), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.pass ? '短信验证码已发送至150****0000' : '验证身份信息'))]), (!_vm.pass) ? _c('input', {
    staticClass: ["identity"],
    attrs: {
      "type": "text",
      "placeholder": "请填写您的身份证",
      "value": (_vm.identity)
    },
    on: {
      "input": function($event) {
        _vm.identity = $event.target.attr.value
      }
    }
  }) : _c('div', {
    staticClass: ["note"]
  }, [_c('input', {
    staticClass: ["note-input"],
    attrs: {
      "type": "number",
      "maxlength": "6",
      "placeholder": "请输入短信验证码",
      "value": (_vm.note)
    },
    on: {
      "input": function($event) {
        _vm.note = $event.target.attr.value
      }
    }
  }), _c('text', {
    staticClass: ["note-text"],
    style: {
      color: _vm.color
    },
    on: {
      "click": _vm.timeChange
    }
  }, [_vm._v(_vm._s(_vm.noteText))])]), _c('div', {
    staticClass: ["line-bottom"]
  }), _c('pwd-next', {
    attrs: {
      "tips": _vm.tip,
      "open": ((!_vm.pass && _vm.identity != '' > 0) || (_vm.pass && _vm.note.length == 6)) ? true : false,
      "text": _vm.pass ? '下一步' : '获取验证码'
    }
  })], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });