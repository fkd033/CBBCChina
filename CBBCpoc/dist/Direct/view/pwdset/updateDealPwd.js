// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 113);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 113:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(114)
)

/* script */
__vue_exports__ = __webpack_require__(115)

/* template */
var __vue_template__ = __webpack_require__(116)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\pwdset\\updateDealPwd.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-569a2fe1"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 114:
/***/ (function(module, exports) {

module.exports = {
  "text": {
    "marginTop": "128",
    "marginLeft": "53",
    "fontSize": "38",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "text-image": {
    "width": "38",
    "height": "36"
  },
  "old-pwd": {
    "marginLeft": "53",
    "marginTop": "39",
    "height": "80",
    "width": "400"
  },
  "note-box": {
    "marginTop": "39",
    "paddingTop": 0,
    "paddingRight": "52",
    "paddingBottom": 0,
    "paddingLeft": "53",
    "flexDirection": "row",
    "lineHeight": "80",
    "justifyContent": "space-between"
  },
  "note": {
    "fontSize": "30",
    "width": "400",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)"
  },
  "getting": {
    "fontSize": "26",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(75,160,255,1)",
    "lineHeight": "80"
  },
  "old-check": {
    "marginTop": "20",
    "fontSize": "24",
    "marginLeft": "53",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(250,86,101,1)"
  },
  "new-text": {
    "marginTop": "83",
    "fontSize": "38",
    "marginLeft": "53",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "pwd-box": {
    "marginTop": "38"
  },
  "pwd": {
    "fontSize": "30",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "marginLeft": "53"
  },
  "repwd": {
    "fontSize": "30",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "marginLeft": 53
  },
  "repwd-box": {
    "marginTop": "39"
  },
  "line": {
    "marginTop": "10",
    "width": "678",
    "marginLeft": "36",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)"
  },
  "forget": {
    "marginTop": "48",
    "marginBottom": "48",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(68,70,79,1)",
    "marginLeft": "292"
  }
}

/***/ }),

/***/ 115:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      oldValue: '',
      oldpwd: true,
      note: true,
      pwd: false,
      repwd: false,
      pwdValue: '',
      repwdValue: '',
      noteValue: '',
      textImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAASCAYAAAC5DOVpAAABsklEQVQ4T+XTQUhUQRgH8P9/npobhRRBZ/EQy3N1qce+fKtpl9pTdCi6FyESeYogQhCiQxCBl9pT1MGDdOhYx2rdt+32VtHaVczqKh2SPVTqezOf7EKQaGrgzTnON/Obme/7htjDwT20sJ+w/IeZTkiTk07Zz/43h365ekOiaCrtdhUaOfODyi2AD4zWbq+bCHYL+uVP5yHqFSC3Pcd+2MAmSxWb5AzJz1gzac+zf+wEFsoLcUj0ToBDoEl4pzoXOVmqXlFKnjY2Cwli0WriJTcZX/gXmA/mzgEyTkADcri+zhhepR/MXxbRmUjMqNLoUM3WC9ZPE3luKBOhFU4PJJO1XG76WHNry2mjeI3gBYKzsHDRCiMdUo2S1utNfVYIqo8FuA5BCCK2+XbyHYKjpHrS48SH/45vwIrF+XatdP152baDuLP8i2dIZgi5KcAYDV+2qJ/+msQegRwMdXii3+3+9gfcgAXBl7YQKyPRb32/r69ruVHpqbl+GHkDxQHvZPxtfS6Xmz2iYtbdA2i95zgdtS2xrRL+/uPX42Z1JbtqZOhsyl7arsr76W/u1O27ja8DGVGgjRwMwWoAAAAASUVORK5CYII='
    };
  },

  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'forgetDealPwd.js',
        animated: true
      });
    },
    pwdInput: function pwdInput(event) {
      if (/^[a-zA-Z0-9.]{8,20}$/.test(event.value)) {
        this.pwd = true;
      } else {
        this.pwd = false;
      }
    },
    repwdInput: function repwdInput(event) {
      if (event.value == this.pwdValue) {
        this.repwd = true;
      } else {
        this.repwd = false;
      }
    }
  }
};

/***/ }),

/***/ 116:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "title": "修改交易密码"
    }
  }), _c('scroller', {
    staticClass: ["container"]
  }, [_c('text', {
    staticClass: ["text"]
  }, [_vm._v("验证信息")]), _c('input', {
    staticClass: ["old-pwd"],
    attrs: {
      "type": "text",
      "placeholder": "请输入原交易密码",
      "value": (_vm.oldValue)
    },
    on: {
      "input": function($event) {
        _vm.oldValue = $event.target.attr.value
      }
    }
  }), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["note-box"]
  }, [_c('input', {
    staticClass: ["note"],
    attrs: {
      "maxlength": "6",
      "type": "number",
      "placeholder": "请输入短信验证码",
      "value": (_vm.noteValue)
    },
    on: {
      "input": function($event) {
        _vm.noteValue = $event.target.attr.value
      }
    }
  }), _c('text', {
    staticClass: ["getting"]
  }, [_vm._v(_vm._s(_vm.getNote))])]), _c('div', {
    staticClass: ["line"]
  }), _c('text', {
    staticClass: ["old-check"]
  }, [_vm._v("原交易密码错误，请重新输入")]), _c('text', {
    staticClass: ["new-text"]
  }, [_vm._v("设置新交易密码")]), _c('div', {
    staticClass: ["pwd-box"]
  }, [_c('input', {
    staticClass: ["pwd"],
    attrs: {
      "type": "password",
      "placeholder": "请输入8-20位的登录密码",
      "value": (_vm.pwdValue)
    },
    on: {
      "input": [function($event) {
        _vm.pwdValue = $event.target.attr.value
      }, function($event) {
        _vm.pwdInput($event)
      }]
    }
  })]), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["repwd-box"]
  }, [_c('input', {
    staticClass: ["repwd"],
    attrs: {
      "type": "password",
      "placeholder": "请再次输入",
      "value": (_vm.repwdValue)
    },
    on: {
      "input": [function($event) {
        _vm.repwdValue = $event.target.attr.value
      }, function($event) {
        _vm.repwdInput($event)
      }]
    }
  })]), _c('div', {
    staticClass: ["line"]
  }), _c('pwd-next', {
    attrs: {
      "tips": _vm.pwdValue == _vm.repwdValue ? '' : '两次密码输入不一致，请重新输入',
      "text": "确认",
      "open": (_vm.pwd && _vm.repwd && _vm.oldpwd && _vm.note) ? true : false,
      "router": "passwordSetup.js"
    }
  }), _c('text', {
    staticClass: ["forget"],
    on: {
      "click": _vm.jump
    }
  }, [_vm._v("忘记交易密码")])], 1)], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });