// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 121);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 121:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(122)
)

/* script */
__vue_exports__ = __webpack_require__(123)

/* template */
var __vue_template__ = __webpack_require__(124)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\photoIdCard.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-3c6363b4"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 122:
/***/ (function(module, exports) {

module.exports = {
  "top-box": {
    "marginTop": "16",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingTop": 0,
    "paddingRight": "36",
    "paddingBottom": 0,
    "paddingLeft": "36"
  },
  "top-left": {
    "width": "175",
    "height": "78",
    "borderRadius": "6",
    "position": "relative",
    "flexDirection": "row",
    "alignItems": "center"
  },
  "bg-img": {
    "width": "175",
    "height": "78",
    "background": "rgba(235,244,254,1)",
    "position": "absolute",
    "top": 0,
    "left": 0
  },
  "first-icon": {
    "width": "36",
    "height": "36",
    "marginLeft": "10",
    "lineHeight": "78"
  },
  "first-text": {
    "fontSize": "22",
    "marginLeft": "5",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(56,148,252,1)",
    "lineHeight": "78"
  },
  "top-item": {
    "height": "78",
    "flexDirection": "row",
    "justifyContent": "space-between"
  },
  "line": {
    "width": "84",
    "height": "3",
    "backgroundColor": "rgba(56,148,252,1)",
    "opacity": 0.3,
    "marginTop": "33"
  },
  "item": {
    "width": "68",
    "height": "68",
    "marginLeft": "8",
    "backgroundColor": "rgba(235,244,254,1)",
    "borderRadius": "34",
    "alignItems": "center"
  },
  "icon": {
    "width": "29",
    "height": "34",
    "marginTop": "17"
  },
  "line-top": {
    "width": "678",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)",
    "marginTop": "27",
    "marginRight": "36",
    "marginBottom": 0,
    "marginLeft": "36"
  },
  "title": {
    "fontSize": "30",
    "marginLeft": "226",
    "marginTop": "76",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "front": {
    "width": "534",
    "height": "330",
    "marginLeft": "108",
    "marginTop": "35",
    "position": "relative",
    "flexDirection": "row"
  },
  "front-img": {
    "position": "absolute",
    "width": "534",
    "height": "330"
  },
  "camera": {
    "width": "36",
    "height": "33",
    "marginTop": "265",
    "marginLeft": "176"
  },
  "text": {
    "marginTop": "265",
    "fontSize": "30",
    "marginLeft": "29",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(75,160,255,1)",
    "lineHeight": "33"
  },
  "rear": {
    "width": "534",
    "height": "330",
    "marginLeft": "108",
    "marginTop": "68",
    "position": "relative",
    "flexDirection": "row"
  },
  "rear-img": {
    "width": "534",
    "height": "330",
    "position": "absolute"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "48",
    "marginBottom": "80",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 123:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var navigator = weex.requireModule('navigator'); //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

exports.default = {
  components: {
    PwdHeader: _pwdHeader2.default
  },
  data: function data() {
    return {
      frontOpen: false,
      rearOpen: false,
      bgImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAAAnCAYAAAAfBybSAAADwklEQVRoQ+2bTWsTURSGz5k008TpxE5ajN+tpRYUBbWIIKg/QHSlC12K/0HwF4hLdwrSnYu4UsSFK60rRa1g1aKo1eJH2iaTZjImnST3yJlkYtpGLWicC72zmsm9Oee973POncVwMZ2myLHj4nwE8CIRbAMABHV12gECgJlqlS49vKddw1mnegpRu9nprCp+eweIxGnMFMQtTYOTyqRwHBACbuOsI+4jwrFwJKisRPBAQQi5DnwIc464D6oTwkPR6IQHiHA0PBVrOzMRjPN2pCCEWAcKQojmB6kVBAVBAgckkKA6QUGQwAEJJPidkClUxzVNOyKBnjUpQQjxUEEIGb2CEDIATq8gKAgSOCCBBNUJCoIEDkggQXWCgiCBAxJIUJ2gIEjggAQSVCcoCBI4IIEE1QkKggQOSCDB7wT1oT9cEurLWrj++9kVBFkgzBXFbQA4IYGeNSmBhLiDn3OLZ3Q9emNNOiDBoj2vchavXn2y7uTp/eciUbyACFvUIZH/QoYPiXyuVejyrfTEdUynZ+Llck5zYl0YcyLY3R1BV9dQdwuo6xEsRSP+yZ0EJDqmrlQqYjzew8KaF//GD+Wo2zg5ZMLePX39O3ckHwH+RgxB4e2H3KEXk9l5AKcZL1YxaHmOji2oEbgABf8uXqmR59XIMxJkeIIWF2tUNmtklqsUiyUFjo19iBlGF7YDwQEYBkBvR/X2toT//t1pgm9NGhTBwEisf2i47zEArF8himDh3cz8wU+Ti1keC0xonVc3xPSBc958/s9LW+289pHywObzWDsArlslvHL3bbeZ6ULPy2rxeBad2Fa/8rgrXDeCySQAJJPgFvO/PMtm9PTSL8dtAGNb67gNANYSvUaPILeo+fEZOvuyFHv9SdfrgPbt2mINDCaeAraAIFj4OD03+vz1vN1qNMBKlwNT/mz/389g43M5AMOoVz9H3B4viS9feD19wklVCdPpl/q3Hh0ZBHeEbWeQYQBsBN6i6kDsf3aY0DQtcpbFM80RcpxcM0cAn8UvL4IA0uEDm6yhoQ0TPgiChffvZ/e/evaVCTcrL6g+jhNcbAbAfOOxH37e/87w1c5bGaNsWr7xvPVYVoXYfMtKEXcAA9hY9IhfzFHLiiODyGW6cAAABgcH4c2bp1oQcvPmv6+IIAKLWB5vaiqLqVQwg+EzkA3+D9yRgVGum2qCYjg7dvdbwzsT4zOfFo5MTWTzS83ONN8xgRGtZvC9bWfRsvqWvIvarXS189r9l9cbXCMjo2J6eho+8ubSAGDbJfoBFwA9rW15iqIAAAAASUVORK5CYII=',
      firstIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABLUlEQVQ4T7WTQS8DQRiG33d3S6UJcfEvOEhoxQFXIhLhNwjaxMXZOrtV7Z/ARRwc6yC2h178CwcnFM3sfDLdanZjV9K1ndPMN/M9877fzEfkNJgTB1yoq03b4iUg49mg/Aq07LJyEZxAxAXpZgL1cwcgv+pksllpKDEiRgea9WS6pPWDiO5+fDrLT8d8/8tyqqJFTzYsHdz2kkXW/FqhaaaDWpoF6fqH9mkvnmatXH+ZhDXVBhiogjXf3mPHJCx5siKiV0OO1Xw84H0qaO7sdWaiWPQArocJuBNl77eO+BwF/Vg1qhIVlc/VNYntaE1EcNOqOVsxa/0D5qWTQQ31RqAUAwGdVtWJxaL7o3/+YX/3L0X/bhHTtBZxRWJsWDXhV0NXC3Yy9VfShbmBvgHMlcufirupUgAAAABJRU5ErkJggg==',
      Icon: [{ img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAACD0lEQVQ4T41TXUtVQRRda86UQU/2YhJWiEJFVC96ruFvqMQ+ofoB5jWoqIw+fO2xzrm+Bj2Wff6BCoR7bxFkFBaZDz4FIUIEcTozs+J4FbKucPeGWcNm1rD2Zi9iJfoq2hr5fAhkBwCREAIgU5wAQgNloCIFfmJR6KuoK5I7IyKjxwyIQVHfI3E2RDDLJAcTwUJ0DNAuMhIPptoR5E4L+gltuLcFyJboxgl+rJbtw1Vlf2P/3ewUwQ6WEl+WCZvh7XMAPwx/twtmmLIvqmN81Ywcp9lJynaylOQT/z8wi7VFU8EEG/3+E6XEnQC1jaVKfgs+zFm7sRocmAn52/NcaEZarZXS7LhktjNO85sU39XKy7JbioHEHfPSTsZJfgPk+/qofTY4qXbn3f71frCRnZke4VJccUcZ1M1Sml8PCh9el9ue9k2qO/Lu7HpkH9n7b0Y4HydumFQP+9P8WgTOVkftk5Y0A4iTbBgwvYxTP87gPtfG2h63Sh5I3ZAP2l30fBXgl3rZPmomm/m329ULXb/WLEmaDRmZPUXPVxT4tT5mp5oNbNOinX45QbeGnGRHDM3eYkkuy3C+fs5OtSo7vpMdhrX7CtmXyLBQG2170Cq5lLhDog4wrviLFHLBzAEehlEAHOjt8mp648TChg4wbNjSSb2G7GQxOQT1wBhIgULDglQDwZUsrmHFnoQpLPsHohn/7BEnwk0AAAAASUVORK5CYII=' }, { img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAn0lEQVQ4T2NkoBJgpJI5DIxmU/6rMf7/E87IwMBMjqH/GRj+/mdkWcloMfm3AwMDAwgfIMcgmF64QSdyWRvIMchi8m+QvgPILiLHHJgeFIMGmddAYWTR/12BgYVFgSg//vnz4EQh5wOMMAIbhIhBYsw6cCKX9QBWg4jRja6GXBeBXYFsGHaDCIURNFwIGkQVr1GcRaiWacnxDjY9VCuPAJU+bzlXBKIWAAAAAElFTkSuQmCC' }, { img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAABOUlEQVQ4T2NkoBJgxGWO4ZTv8mz/mJ0ZGBmlwWr+/3/6i+nv3vM5nA+x6cFqENiQ/yxxjIxMH//++3cNpJGZiUnr//9//L8Y/yzCZhhWg8wn/UpiZGDgecPwcOadPNWfIIO0V/1n433xO+M/A8OXk3ls89Bdhd2gyb9r//1nOH46j3UPsgbTSb9dmBgZLE/msjYTZZDF5N8NDAwMB07ksh5A1mAx+bcDAwODw4lcVpA8CoC7yGzKfzXG/3/CGRkYmImJyP8MDH//M7KsPJXDeAukHm4QzDaQS4gxCOQyZFdjGITN2dgMRvc+/QwChR3IRbCwgLmOZBeZTfpdBTYoj7UNLQZRYpag10wm/FAHGXCmgOMmRQbhikGSvUa2QRSnI1BYMDMzh5GSsv/+/bsKFnY4yyMiUzdcGQDwx6kTtXj9QAAAAABJRU5ErkJggg==' }],
      camera: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAARCAYAAADQWvz5AAAB/klEQVQ4T52SMWhTURSGv/OSSEnE0UEsVikFlyo46CROxSS1Q6V1aZtnoQ6C0A6Ci5o6iUsLgoNF85LqYIsdapOIk7gpOKigUApWqktHsVVs8n55xcBrSEP0Tpfz/+fjnHt/o8FJ5XUTMWGGhWUJYUyVMjZZ37bDGIjJRzroVFhB3Jd4H24woxvjkh+lszxkX3do2ayct4foqkJbIBiMY5zeWKXrZdYqYfOZrKKJDpYRrwTTgRaBXye+sGwpT3mDkR2j+rjFUcs3WjudVwbh1a08a2lPPjAWjzNbE+cH7XcjSK02MKc9tfvmJsPATAASDueLI7YQbh6Y096NTcYMrgR1wd1EnJn5QfuRyum4GQsS/RbhCD5PG4J6CkrEfD4IfgKTJhwZ1w3athy6Y1VGzZiWGCfCt11BaU/3DNqXXDsXnrLX0zPBWtG1y8HDB5+RLqh/V1DK07pj9C5l7E0YlMzplBmLJdf21+pNQWlPlS2fwy9GbS0M6nmo9pjD56Jr0ZZAKU/vBHfKrj0Og9J5DUlcLbl2rCVQ0tOIwVQ0wsnFYVsJmvpm1Vmp8lowUXat0BIIyVJ5rhncALZBQKfgVinDbczUGuiv62xOHeZwdDtHPp+eX7TV+qA2fexmqW4G+gjsM1j/F0DNKwii8N36HuhAJcIFIPE/IGAjWuXJH2zoB3JFrkclAAAAAElFTkSuQmCC',
      front: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQsAAAClCAYAAABLNNh4AAARmklEQVR4Xu2d+W9c13XHz7l33pDUFu2St1gWN0u0kya2nNhQYlFcZC1G0P7AX1rYUAIkQBKg6H+g/6BogRZIkViw+5t+aGDU2ijSVG3XaxwXSrQMh7Ls1Eski7KijeK89+4pRgpdhZLImTdv7n135ssfJItz71k+587xvHnfdy/Tn3+2/IusXdhG65lpKRHpkqZ3hp/jc+WXd70oT5ChNTNjZ/7GGPDB2mjY98VKEbp4dYo+PPoz/mP5Pc/lP3a+JBs4pg4iuhwLnWNNUcT06fBzfPXPzeI+Q7RwdrPAGPDZ9aJgbcx6YzTC+yKO6WtKaA0rWiSaJvY/xydvNosXZL0haj34QzpJxDK7KeDfIAACzUhAeOcvqUc03X+2RK/daBb4AQEQAIG7EejZJ/njQ1zix34hCxZ/TqWjezgCLhAAARC4GwHetVeeNYoKB57ncWACARAAgdkEtr8gq3KKVqNZYG2AAAjMSWDHi9KlDHWjWWChgAAIoFlgDYAACNROAJ8samcICyDQFAS+ahY7fyW9saIzh3bzR02ROZIEARCoisBXzaKqWRgMAiDQdAS27JWl+YiWQZTVdKVHwiCQjAAPviQLJ6eo9P5POExmArNAAASagQBunTZDlZEjCNRAAKKsGuBhKgg0EwHcOm2maiNXEKiBAJpFDfAwFQSaiQCaRTNVG7mCQA0EIMqqAR6mgkAzEYAoq5mqjVxBoAYCEGXVAA9TQaAZCUCU1YxVR84gkIAARFkJoGEKCDQTAYiymqnayBUEaiCAW6c1wMNUEGgmAmgWzVRt5AoCNRBAs6gBHqaCQDMRgCirmaqNXEGgBgIQZdUAD1Obi8A+Eb1qgu6JTXgPMX9NaW4xocSx0KW8yl2YUjT5bgdN7mE2jUgGoqxGrCpySpXAL34jQdcKao/jaB0LB3MaZ4qM6D9yRH/o38CTqQaSEWMQZWWkEAgjGwTKnyIWTdDyvJpeJiX9kMpxvtrIjJEv4wW5E888wBeqnZvl8RBlZbk6iM0agcMTslrH4YMqp1aJIV2rYxESMvEfLhzLHx8a4rhWey7nQ5Tlkj58Z4bAoeOyPAiiR8rfRdQlKJE/TXPunR2dPF0X+xaM4tapBchwkW0CYwV5OOa4g5nqusu9xHKlpHNv+tow0CyyvY4RXV0JCI8Ww28Rqfvq6uYW48z6Qm87vcnMYstnWn7QLNIiCTveEThycvpRldPrbAfOWo9vXc8F235r9QdRVq0EMd9LAsOn5F6t48dcBG+EjJnSR7d9k6+68J/UJ0RZSclhnrcExsYkZ+6PthJxi6skDJlPBjrzH7jyn8QvRFlJqGGO1wRGTksXm7jbZRLlTxch6xEfv+x0J8oS4cO/n7rf5Liqe9oqknjbI22fUIVfFL1yTJa1BqRcLpBKfF8Pyez6Bn9ZyViMqZ7Anj2ivve3Ub/LTxUzUecC/fun1/GZ6rNwO8OZKGukUNokxDuTpM8k+/u78+/NN/fgyal1+Vzw6HzjsvJ6KQp/t31D20dZiaeR4hg7I2tNFG/KQk5Ccr6/M3grC7FUEoNzUdaRk+EWUrSlkmBvG2Po6MCG4Oh8c7PwsXO+GG99XZQu9LfzeDVzMLYyAkeKpW8pUvdXNrq+o1hR/Nq/60N79vjx4JnzW6doFrcvSDSL+r1JR8bDAWZurZ+H6iwbpV8baOc/VTfLzWg0Czfc5/SKZlGfoux7U9pWrIr762M9oVWjf9vXzZ8mnG11GpqFVdyVOUOzqIxTtaPKD4nlJP5OtfPqOV6xPtnbwRP19JGWbeeiLFyG4DIkrcU8n53Dp6YeyungkfnG2Xw9MnFxW3fLKZs+k/pyLspCs0CzSLp4q513ZHx6g2LdUe28eo43Ek8MdLWcrKePtGw7F2WhWaBZpLWY57NzqFD6ZqDU1+cbZ/N1nz5ZzHBxJsoaLoZPs6HeJAUSRWODncF/zTd35OT1Ls7lnCr25ovx1tcligr9G1px67QaaBWMHS2Wvm3zCdMKQqI4jE4NbmwtVjI2K2PcibI+lDUSxttITK4qGKwiDvTh/vV8dr55B4qyJB9FPYazr+BUQqaUyx3f0cmX5ssLr1dHYPhU6XGt1T3Vzarz6FAf69vIH9fZSyrmnYuyUskCRkCgAgJltTArtbaCodaGiNbvVvI/PGsBzeHI+a3TLEBADI1N4OVTsniRjp7MwvMgs0mHJf3fz/T4saEvmkVjv0+QHREdOj7dE+T1+izCUEYXe7vZr1unu/bKs0ZR4cDzeCYhi4sKMSUnMFooPUFKrUluoX4z49h8Pvhw/jf185CeZeeirPRSgSUQuDOB0Ql5iiRekUU+ivQXvZ38dhZjmx2Tc1GWD5AQo98EjhTCzUrxsixmIaTP93eyF4+pOxdlZbGAiKmxCGT6MkSZzwfb/bgMmVkVzkRZjbUskU0WCWR5P5NwOjr5zCOtXjxI9lWzwBecWVzmiCkNAmVRXgvFT6dhK00b5aMNL4QXjg71rL6Spt162YIoq15kYTdTBEaK8iRTvDJLQUVizm3ryr+TpZjmigU6C18qhThrIvDrD2Tp4oXx5nofUVhpkCxkrrN+3SdZP5pFpdXFOO8JZOu7C32ir5NP+wQVzcKnaiHWmgm8WpzuMaIfcvUJo/w9hRY94Ytq81bgEGXVvPxgwDcCo0VpJ4o3Oolb62N96/14ynQ2nwyIsoRHC1P35nRbVQcARfGU6etu+4yostOof31GlrZdu1SVDyeLKWNOpxYsMX/9EF/MWFg1hXPj+MIH4gESqm5bhJq8EhmSUn9H7gizH1v/z07XuSjrYKG0KZfwkKGIZP/2BjxkqMY1mfr0Rjz06NWi9AjFVh8u83FXrDstJmeiLGyrl/p7O3WDjbjb+NjYmdbw3ge2akVVHZuZGK6haDLSo0M9XEpsIyMTne2UhWaRkRUwRxiN2CzK6R4uTD+cU7rTRgV83D5vNhfnoiw0CxtLtTYfjdos9ono5cWol5nbaiM0z2yWq6+3547u8fS7ipnsnN86RbOo6zJNxXijNosynP3Hr6xtzbfW9aDkiPU72zr4XCrFcGgEzcIhfF9cN3KzKNfg1fHSd4TV6nrUQ7GEvR3BoXrYtm0TzcI2cQ/9NXqzuLH7u8TfT1uoVRZgySX9+sDjfhx8PN/SdC7KwmXIfCVy/3qjN4sy4bHT8rgxcarHBCilP+9tZy+2zKtklTkXZeGQoUrK5HZMMxx6NFaQlUbFT6ZJWhn9Vm83n0/TpktbzkVZIx/KGhOGg2SqvN+tKFZBMFzJmQsH3p5cEqxYvJEpYJewffQtFEo4efnEju+uaOhDj/btE738r+LtaV2K3Nir4n/0waEhjn2s+1wxOxNlNRpI5OMvgdFiOJje2SIy3dcZDPtL4+6ROxNlNSJM5OQngdGJcCsJL0wleparfR3Bq6nYyogR56KsjHBAGCBAIxNhLwsvSgOFsFzp7wjG0rCVFRvOb51mBQTiaG4CY8dlUZiPntLELWmQiEmmg1Luzd4e9mJ/zUpyRrOohBLGNDSBA0VpaaGwR4i+K7GcV1pP1ZKwieM21rySid6epuD4jk6ersVeVuaiWWSlEojDGYHhE9c7gxa9JjJSln0Lk7pMhiZZUVV3MqR8R0/RCiGzmIg4p/i9cDo+O7ixtegsuRQdOxdlpZgLTIFA1QRePybLwgXh+lDUvYpMx4wBForJyCRrXdEtY4njJaR4hfD/SwAMqYmAzWdBEHz4vQf5y6qDy9gE56KsjPFAOE1EYN++fXrNYz/YaIhb4pgeE6YFs9NXSk+ZWL5QTHfch8II5ZXmVcbEtz25ykLXtKb3Fcn02fdfPjE0NFTVJ5WslcK5KCtrQBBP8xA4PHHtgbzk7otYdZGYuQ5OFiV0kVldECIpE+Lyfo5ilhumpTf/eZcfVpM5MeMljj7d1rHgfxuBLkRZjVBF5PAVARFh5r/cn3WPiHpigoLpKxfb2hYvXRlwvElis+rWS4c5ETKFTOqL8hghs4qEgkqQly9pWKsvQtHvTV2+eL5l0dKpdzsonL2/xZ1irsS+7TEQZdkmDn+pEnj5lCxenKOumOMHWXhl+bKAmbzaoFmETPmyR1jOa9EfX45o/AcP8+VUQdVgDKKsGuBhqnsCY+dkUXwp2myMdPvWHOajd7N5cEEvyb3Ru9q9XgO3TuerGF7PLIHyCWMmCgdYUT6zQaYQmBgqqVxwpL+dx1Mwl9gEmkVidJjoksBosfRtQ/T9Ob9cdBlg+r5FEb3W15n/bfqmK7OYgWYhPFak+65H16s68KU11xr1dtKnlR4y9MoxWdYa+HUNW1kJ6zvqekhm1zeypRE4eEq6czrc3kSNYqbIonVwcOt6LtS36ne27lyUNVIobZKEhwwxyf5+HDJU93WTpUOG3jgli6/r8Dmhxr70uFtRmajUujR4afMq+198OhdlYVu9ur/Xa3aQpW31hgvXd7BS3TUn5bEBMaYw2N16wHYKzkVZaBa2S169v6w0ixsb66pwt5jmvpxkRaZkgr07OrkiKXr1FZ97hjNRFppF2qVM315WmsWNS1ZFm9PP0D+LbOiNSi7B65GZM1EWmkU9ypmuzaw0iyOno78hYx5MNztPrSn18UB77j9sRu9clIVmYbPcyXxlpVmMjJd+LEzpbHuXDEVmZrHQ1f6u/L/ZDMj5rVM0C5vlTuYrK81ieLz0942m0kxWEaKyunOwK/9PSecnmYdmkYRak83JSrM4Uiz9Q5OhnzPdgc78P9rk4bxZ4JAhm+VO5isrhwyhWfxl/Zw1i52/kt5Y0ZlDu/mjZEsq2ayR38kak094yFCpNNz/6KKz83kuHzLUunz5BuPZU4jz5WXjdSVk8oZObM7A049oFhlpFjYWHnyAQC0EhsdLP+E77GZVi01f5zLrq/0d2uoXnM5FWb4WC3HbJzBalHaRsP9O29/Zj8adRylv18fBSF8nn3YRhTNRlotk4RMEQCA5AWeirOQhYyYIgIBNAs5FWTaThS8QAIHkBJzfOk0eOmaCAAjYJIBmYZM2fIGAxwTQLDwuHkIHAZsEnO+UZTNZ+AIBEEhOwPlOWclDx0wQAAGbBCDKskkbvkCgAQhAlNUARUQKIGCDAERZNijDBwh4TACiLI+Lh9BBwCYB3Dq1SRu+QMBjAmgWHhcPoYOATQJoFjZpwxcIeEwAoiyPi4fQQcAmAYiybNKGLxDwmABEWR4XD6GDgAsCEGW5oA6fIOAhAYiyPCwaQgYBmwQgyrJJG75AwGMCuHXqcfEQOgjYJIBmYZM2fIGAxwTQLDwuHkIHAZsEIMqySRu+QMBjAhBleVw8hA4CNglAlGWTNnyBQAMQgCirAYqIFEDABgGIsmxQhg8Q8JgARFkeFw+hg4BNArh1apM2fIGAxwTQLDwuHkIHAZsE0Cxs0oYvEPCYAERZHhcPoYOATQIQZdmkDV8g4DEBiLI8Lh5CBwEXBCDKckEdPkHAQwIQZXlYNIQMAjYJQJRlkzZ8gYDHBHDr1OPiIXQQsEkAzcImbfgCAY8JoFl4XDyEDgI2CUCUZZM2fIGAxwQgyvK4eAgdBGwSgCjLJm34AoEGIABRVgMUESmAgA0CEGXZoAwfIOAxAYiyPC4eQgcBmwRw69QmbfgCAY8JoFl4XDyEDgI2CaBZ2KQNXyDgMQGIsjwuHkIHAZsEIMqySRu+QMBjAhBleVw8hA4CLghAlOWCOnyCgIcEIMrysGgIGQRsEoAoyyZt+AIBjwnceut0Ryz00cEf8gmP80HoIAACdSKw/QXZqJnW8c4X5Clmyr+ym4/WyRfMggAIeExg117ZIkIl3rJX1i4Qenz6Gr01+nOe9DgnhA4CIJAygZlLkCtE73HZ9s5/lWX7f8pflv+7/yX5el6odbbPA8/z+MzvygbuFBPG3KQCPrevDqwNv9bGtSv02dGf8ZXt/yxLZCHdc+hHXLjRLG79eeaX0pfTtGD271/Zzf8587tde+XZOzULjLlJBXxuXx1YG36tjTigDw7+HX9yayX/D2AQGKThAwpfAAAAAElFTkSuQmCC',
      rear: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQsAAAClCAYAAABLNNh4AAAR7klEQVR4Xu2d23Mcx3XGv9O9iytJkZRIUaYuvADgLZaccmyXqSqFIEhKIqlXvySWSs5DquL8EfwbUpVUpcoWS8qbHyOZoijwElcUlS2mbMkWKQCUSIq6mXfzBgK73Sc1C0LBLhbY3ZlZYAb97YNAYfucnvM7vQc70193Cx68dv2rruvtxiYRrARgJy1+e+xluRy9ffB1/SE8Hp1uO/2TbciHY2PJfi4eUcXNu+P47NTP5ZvoMy/Rfw68odvEoQ/Abae4LBblsuDLYy/L3QfFYr0HemuLBduQz8HXlWOj5oOxFD4XzuEho3hUDJapxblfvyxnp4rFa7rJA11v/wxnAdHaosD/JwESCJGAyoFfYIdaPP7nSfymUiz4IgESIIG5COz4lXZ8/BOZlO//u/Ys/xqTpw5JmbhIgARIYC4CcvCwvuQNRo68IqPERAIkQAK1BF58TdcUDNayWHBskAAJzEtg/+s6YDy2sFhwoJAACbBYcAyQAAkkJ8BvFskZ0gMJBEHg22Jx4Jc66AzOH31VLgQROYMkARJoicC3xaIlKzYmARIIjsCuw7qyo4xVFGUFl3oGTALxCMi+N7T32jgm//cfpRTPBa1IgARCIMCp0xCyzBhJIAEBirISwKMpCYREgFOnIWWbsZJAAgIsFgng0ZQEQiLAYhFSthkrCSQgQFFWAng0JYGQCFCUFVK2GSsJJCBAUVYCeDQlgRAJUJQVYtYZMwnEIEBRVgxoNCGBkAhQlBVSthkrCSQgwKnTBPBoSgIhEWCxCCnbjJUEEhBgsUgAj6YkEBIBirJCyjZjJYEEBCjKSgCPpiQQEgGKskLKNmMlgRQIUJSVAkS6IIEQCFCUFUKWGSMJJCBAUVYCeDQlgZAIcOo0pGwzVhJIQIDFIgE8mpJASARYLELKNmMlgQQEKMpKAI+mJBASgeBEWaoqp86g13WjV+6jq9QxUSyUYMpF+OJkZ0m7cN+O4+6u7bgrIhrSYGCsJDAfgSBEWSc/1mUlmXhMxa4RIyutgW00LJyHU683Rd2VonZ+PbhD7jSy4fskEAKBJSfKir5BnBjFdzzKG0VkpQhin+eqClXVmwaF87sH8BW/cYTwkWCMcxFYUqKs4yP31sMWt0ClN/WUi96FK40Mben5MnXfdEgCGSawpERZ73yovegoP1MoyMNtZ17Wa+XJwofPPyN3294XOyCBDBBYMlOnxy/peky4p6EoNOIqgHFAp1UUVGC9QIxCReGcoGyBCQV8Iz8QlNFpPxp6QvgtoyEsNsg7gSVRLN4d1W0Ct3m+5xIK1ylqlznxvRYoesz9DMMA6oCSVXNXxd0R2Im5El15ngH76d4BOZv3wcDrJ4H5COS+WJz4TJ9R556c88Ms6FH1qwF0JRgK90XMdVHcm/Ohj7Wf794kHybog6YkkGkCuRZlDX8y8bRY+1Q9wl5dUSFrRNCTVgZUcU+gV4zYUj2f6tzFPVs7P0qrP/ohgSwRyK0o6/iYbgbc9rowDZY559eKwKQNWxXeWnMZHnPoLspnhvq7Pk27X/ojgcUmkEtR1vBZfRjW/bjeMwoVrBb1q+Z7JlELXVVWieiNZpMRPdNQMTdEcX22LyicfX/PNrnWrD+2I4E8EciNKOvkSS249eVdItI9C7DgYa9+VSvgBdIFj50q+A1Ey63YGjE3oJhVFFR13H5ZODU4KC35a6VvtiWBxSKQG1HWiTHdoXCbakE571eKwSMtA1QMCGSjQEa9+POt2qvHVWvMzVo7gf1sd7983Ko/tieBrBLIlSgrEl0Vu90unf0sosvDr8c806EzE6BejKnMkqBHBDsAdAgw4eFHRMw4PO6r0ftNJk0NTKSzqGovCl8at6eyKto6eV67fLn8HCCdTcbJZgtEQESv7+4rvrdA3TXdTa6mTo+N6vesuCdmRieAOPFPQlFsJmqBdKviRwLM/SERGfdePxCj4834hFQ0GZ8rULVK1am9tG9A/tCUjwVuFD33kYLbucDdsrsmCQz1F95ssumCNctNsTgypp1FdXtMzbeK6IHmAx1F09CMSI86/ABSR3uhuA+D3ymaLBQPen2gw6h64Omjbxdih/f3y5yirqYvOuWGLBYpA03ZHYtFAqBHz93vK2phW/XthLMQearObUnDntRLtxFEf1n/Xx4eqTHFvwdpfb1HdNsB1YtirJvZuRF7drBPzjW8oAVuwGKxwMBb7C7TxeLAL3XQGZw/+qpcaDGuBWl+bKz0txayYmZnXrAK6uddNCYaFRO9WHuRAhF47MXspevvqegsDcVcfqr8irlmFFVTsA56a19/8b8WBFILnbBYtABrEZpmulgsAo+mu/zV/2j36kfcUK2uwot/at5nFWoGRHUDRC6q+JGZHYrKMgDPRovHYORL9bq2clui+ic1qF4YNo+f6mKBklFTVZiitSPXr9rjP9kpzT3/aJpKsoYsFsn4tds6i8UiF6Ksk2P6uIf766pbEKBT4aseds5KoMpqUVnn4a6IkStVxQLyKLw85uFGxMg4VKyIbFD1FoLRKl/z+Jn9jcVcimZWqm5FYH8/2C9ftHuAteKfxaIVWgvfNovFYppCpkVZJ89N/JVXu7EmZSs9fOu6imknKhYSLS6tfkW3J5WFpDFfBuYqgCrdhRF3frCv808xXbbFjMWiLVhTc5rpYnHwsL7kDUaOvCLVf1VTCz++o+Gx0o8FUlUYvPh1UES3Etl6Ce4YNd9UfwvSq3v6i+9n6UJZLLKUjdnXksVikQtR1vFzpd21W+Q5758Ug45YKVeJZkC03jeLir9G78/TqXpMWmM+r2oieneor3gi1rW2yYiirDaBTcEtRVkJIJ48V3rBq1SJrlT9xmiXq1bdRlOmItIn6iM915jWCK8avd+ov+iBqYipko0b0dJgX/FoI1u+TwJZJpALUdbwaPngrJkQ+M3NyrtrEyAqO1VhYPS/6yWn0fsNEhrJv6uWqEczInsGCm9leSDw2kigEYEwiwVkOSoCrNl6igiYNHifxaLRsOL7S5FALnbKSvM2pN1J5G1IuwnT/2IRyMVOWak/4Gwj7bw84GwjArpeogRyIcri1OkSHX0MK5cElqwoy3izUZF8014B7nnTeHOcvIiycjlKedGZIJDpnbISyL2tqGxNi7A3OibQyfn8CfIh906LCf2EQyAXoqy4C8kqy9Ahs7bgi5teFf0aorM26f3Wn+RnIVlcBrQLl0Aupk6j9MRaou7lIYE8nlZ6VfQ2RKvVmTOd52iJelpM6CccArkpFifPaZ9XN2vzGzUSLUEvVM44VRRMdHapoiACq5Vl6L5X1EwvDKv8FANVX3V8oVRSbhBtYTP17+nXzP0utCIRvxpt9g+g7AFnDEqqKEO0JF4v5GXzm3CGOCNNi0BuisWc2+p5GQC0qoikBacVPx561prqRXhZ3lavldjYlgQiArkQZU2nqt6GvdDorBD8QKYWhpWjv/oarc+wKEc/IXDq4W30U+A94KMt8MRE2+BBo1+ImVqSrl7ERGeYCUQ9TLRdnwFMdLJZdPoxPGy0HkWAwvS3l2jRmRhEnj8QY6qeZ2R5w14OfxJolUAuRFnTQaV1FECrkBq0z+VRACkzoLsACORClDUzD6kfMpQwyTxkKCFAmueOQKZFWTNppn18YZJM8fjCJPRom1cCmRZl1UJNejCyVzgDc1sU9wBMiKk814j2xuhUQY+HXx7NqsyVTB6MnNdhzutOQiAXoqx6AR4f082A2143eINlzvm10YPJme9XdvIWve5hb9ma08NmtnOV2VW3AiqrazfYUYW31lyGx6zjAqZ8lM8M9XdV7WeRJEG0JYGsEMjN1Gk9YMOfTDwt1j5V7z2vrqiQNSJT60LEY1JgvoJB8yebexTU+O8oprbvU8U9gV4xYkv1+lTnLu7Z2vlRVpI733VwW73sZonb6rUpN8dGJ79nxcx5JEB0WxEdbygwV2q36G/mknTqyIE1D44njG5b6r6c+kv7Bjoyea5p3ULLs06bSf+itcnihr25/mYxnckTo5M/UjFrFyuzzun1fVuzd+r1fDy4u/dijZbm+s10scj68YXzIT42UhqyRiq3G4vy8ijvHrDviIhflP5jdMpiEQPaAppkulgsIIdUu3r3U33IePdcqk5jOFNrf7dnk/w5humimLBYLAr2pjvNYrHInSirlvY7IxNbC8b2N52FNjU0ai8NDgifWbSJb2hus1gspnOQG1FW7aAZ/qQ0KLZyyPGivtRpaWhLIboViX304UIGwG8WC0m79b4yXSyyfHzhXKhPfqzLfIcbbD0V7bEoy/hvn+9bfrk93tP1ymKRLs+0vWWxWORWlBUl59in2m+9S23bvKQJL3n/+QtbOj5M6mch7FksFoJy/D6yWCxyPXV6fLT0HEQeip+SdC19WSf3bi0cqxzEnvEXRVnZTRBFWSnn5s3T2tO9wu2uPdYw5W5adme8fX9wi1xt2ZAGJJBxArn9ZnF87P5moFB/bcgiQvdld2Hvts4/LuIlsGsSaAuBXO2UNZNAvYOH2kKoRaeqOr5noDjcohmbk0DmCeRqp6yZNE+c02dV3eqsEXYebt+WwpGsXRevhwSSEsitKGt4VDeJuB1JAaRt74z/et/mjtNp+6U/EsgKgVyKsobHdLsxbsODjWsWnaWIvX5fcXp/v0ws+sXwAkigTQRytVPWTAaHDqn54d9hGTC+oqjdyy1KKzxkhYh0tYlVtK+Fitd7rqi3CiV/u9DVeWu8C7eeXyd329Un/ZLAYhPItShrLnhRAXn2p3jcwn032tI/LcjR8QEO9o/v/Qe+OHQoPytM04qffsImkNup02bS9u7Zie9GO2mlpcXw0Mm9/cV3mumbbUhgqRFY0sUiStbbZ8c3dJjiNhgUkiQvmuVQLZ19fmv3+SR+aEsCeSWw5ItFlJjTp7V4c8XEE2LsOu9llanZyHeu5EUnlznVv7iS+8Z3Xrq0v7+fDy7zOtJ53YkJ5FaUFTdyVZX/HMGy3g70dCk6JzyK0bkAnVMnHUcHApSNYOLKtRvjZ76/6vahHO1+FZcJ7UigGQK5FWU1ExzbkAAJpEcgt6Ks9BDQEwmQQCsEcinKaiVAtiUBEkiHQG5FWemETy8kQAKNCCxJUVajoPk+CZBA6wSCmDptHQstSIAEagmwWHBMkAAJNEWAxaIpTGxEAiQQnCiLKScBEohHgKKseNxoRQLBEaAoK7iUM2ASSEaAoqxk/GhNAsEQoCgrmFQzUBKIR4CirHjcaEUCwRHg1GlwKWfAJBCPAItFPG60IoHgCLBYBJdyBkwC8QhQlBWPG61IIDgCFGUFl3IGTALxCFCUFY8brUggWAIUZQWbegZOAq0RoCirNV5sTQLBEaAoK7iUM2ASiEeAU6fxuNGKBIIjwGIRXMoZMAnEI8BiEY8brUggOAIUZQWXcgZMAvEIUJQVjxutSCA4AhRlBZdyBkwCyQhQlJWMH61JIBgCFGUFk2oGSgLxCFCUFY8brUggOAKcOg0u5QyYBOIRYLGIx41WJBAcARaL4FLOgEkgHgGKsuJxoxUJBEeAoqzgUs6ASSAeAYqy4nGjFQkES4CirGBTz8BJoDUCFGW1xoutSSA4AhRlBZdyBkwC8Qhw6jQeN1qRQHAEWCyCSzkDJoF4BFgs4nGjFQkER4CirOBSzoBJIB4BirLicaMVCQRHgKKs4FLOgEkgGQGKspLxozUJBEOAoqxgUs1ASSAeAYqy4nGjFQkER4BTp8GlnAGTQDwCLBbxuNGKBIIjwGIRXMoZMAnEI0BRVjxutCKB4AhQlBVcyhkwCcQjQFFWPG60IoFgCVCUFWzqGTgJtEaAoqzWeLE1CQRHgKKs4FLOgEkgHoGZU6f7neLC2z+TM/Fc0YoESGApE3jxNd1uBRvkwGu6UwQdb70qp5ZywIyNBEggHoGDh3WXKiZl12Fd16P4m4l7eP/4P8u1eO5oRQIksBQJTN+C3AE+kCjAA/+mq379T3Ij+veeN/TJDkVXbeBHXpHR6d9FDuqBYZspKuQze3RwbORrbNy7g69O/VzuvPgvukJ78djRf5CRSrGY+XrhFzpUsOip/f1br8qb0787eFhfqlcs2GaKCvnMHh0cG/kaG66I37/99/LFzEz+HzJFQ6TRWeYNAAAAAElFTkSuQmCC'
    };
  },

  methods: {
    jump: function jump(router) {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + router,
        animated: true
      });
    }
  }
};

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('pwd-header', {
    attrs: {
      "backButton": "取消",
      "title": "宝石山开户"
    }
  }), _c('scroller', [_c('div', {
    staticClass: ["top-box"]
  }, [_c('div', {
    staticClass: ["top-left"]
  }, [_c('image', {
    staticClass: ["bg-img"],
    attrs: {
      "src": _vm.bgImg
    }
  }), _c('image', {
    staticClass: ["first-icon"],
    attrs: {
      "src": _vm.firstIcon
    }
  }), _c('text', {
    staticClass: ["first-text"]
  }, [_vm._v("拍摄身份证")])]), _vm._l((_vm.Icon), function(icon) {
    return _c('div', {
      staticClass: ["top-item"]
    }, [_c('div', {
      staticClass: ["line"]
    }), _c('div', {
      staticClass: ["item"]
    }, [_c('image', {
      staticClass: ["icon"],
      attrs: {
        "src": icon.img
      }
    })])])
  })], 2), _c('div', {
    staticClass: ["line-top"]
  }), _c('text', {
    staticClass: ["title"]
  }, [_vm._v("请拍摄你的身份证原件")]), _c('div', {
    staticClass: ["front"]
  }, [_c('image', {
    staticClass: ["front-img"],
    attrs: {
      "src": _vm.front
    }
  }), _c('image', {
    staticClass: ["camera"],
    attrs: {
      "src": _vm.camera
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("拍摄正面")])]), _c('div', {
    staticClass: ["rear"]
  }, [_c('image', {
    staticClass: ["rear-img"],
    attrs: {
      "src": _vm.rear
    }
  }), _c('image', {
    staticClass: ["camera"],
    attrs: {
      "src": _vm.camera
    }
  }), _c('text', {
    staticClass: ["text"]
  }, [_vm._v("拍摄背面")])]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.frontOpen && _vm.rearOpen ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump('confirmName.js')
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v("上传")])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });