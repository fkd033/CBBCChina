// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 133);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = {
  "header": {
    "position": "relative",
    "alignItems": "center",
    "backgroundColor": "#FFFFFF",
    "justifyContent": "center"
  },
  "back-box": {
    "width": "58",
    "height": "88",
    "paddingTop": "25",
    "paddingRight": "29.5",
    "paddingBottom": "25",
    "paddingLeft": "29.5",
    "position": "absolute",
    "left": "16",
    "bottom": 0
  },
  "back-icon": {
    "width": "18",
    "height": "29"
  },
  "back-text": {
    "fontSize": "32",
    "position": "absolute",
    "bottom": "22",
    "left": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  },
  "title": {
    "fontSize": "32",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(68,70,79,1)"
  }
}

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      Env: '',
      backIcon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAA+klEQVRYR93YsQ3CMBAF0LMbp2aNNBmAhgXYhZ0YA+oUyQI0tBmB0shFJIQIln3/HxZuT5aev66wvpNGj2vUJWawvu93IYSz9/40z/MtF4gJLKG6rruKyCAii3PukMPRYW+oNajFe7+fpum+lRwVtoGSGOMlhHAcx/FhDtOgEpaSmBZFgSFQcBgKBYUhUTAYGgWBMVBqGAulgjFR1TA2qgpmgSqGWaGKYJao/4ClV1imVvy7sMIVw6ySq4JZ4KphbJwKxsSpYSwcBMbAwWBoHBSGxMFhKBwFhsDRYFocFfYF99tSZS1MmqyhXnHNFXe59vDTnL5jNah05wl5lh42UQjcCwAAAABJRU5ErkJggg=='
    };
  },

  props: ['backButton', 'title'],
  methods: {
    jump: function jump() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      navigator.push({
        url: urlFront + 'passwordSetup.js',
        animated: "true"
      });
    },
    back: function back() {
      navigator.pop({
        animated: true
      });
    }
  },
  mounted: function mounted() {
    var _this = this;

    weex.requireModule('event').getPhoneType(function (res) {
      _this.Env = res.phoneType;
    });
  }
};

/***/ }),

/***/ 133:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(134)
)

/* script */
__vue_exports__ = __webpack_require__(135)

/* template */
var __vue_template__ = __webpack_require__(136)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\view\\register\\registerPwd.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-09894f31"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 134:
/***/ (function(module, exports) {

module.exports = {
  "text-box": {
    "marginTop": "128",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingRight": "51"
  },
  "text": {
    "marginLeft": "53",
    "fontSize": "38",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "text-image": {
    "width": "38",
    "height": "36"
  },
  "pwd-box": {
    "marginTop": "38"
  },
  "pwd": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "marginLeft": "53",
    "height": "80"
  },
  "repwd": {
    "fontSize": "30",
    "fontFamily": "PingFangSC-Regular",
    "marginLeft": 53,
    "height": "80"
  },
  "repwd-box": {
    "marginTop": "39"
  },
  "line": {
    "marginTop": "10",
    "width": "678",
    "marginLeft": "36",
    "height": "1",
    "backgroundColor": "rgba(231,234,238,1)"
  },
  "pwd-check": {
    "marginTop": "20",
    "marginLeft": "53",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(250,86,101,1)"
  },
  "referrer": {
    "marginTop": "82",
    "fontSize": "38",
    "marginLeft": "53",
    "fontFamily": "PingFangSC-Semibold",
    "color": "rgba(68,70,79,1)"
  },
  "referrer-input": {
    "marginTop": "38",
    "marginLeft": "53",
    "fontSize": "30",
    "height": "80",
    "fontFamily": "PingFangSC-Regular",
    "color": "rgba(203,205,215,1)"
  },
  "register-bottom": {
    "position": "fixed",
    "bottom": "40",
    "justifyContent": "center",
    "flexDirection": "row",
    "left": 0,
    "right": 0
  },
  "bottom-logo": {
    "width": "21",
    "height": "30"
  },
  "bottom-text": {
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(203,205,215,1)",
    "marginLeft": "11"
  }
}

/***/ }),

/***/ 135:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _pwdHeader = __webpack_require__(3);

var _pwdHeader2 = _interopRequireDefault(_pwdHeader);

var _pwdNext = __webpack_require__(7);

var _pwdNext2 = _interopRequireDefault(_pwdNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
exports.default = {
  data: function data() {
    return {
      pwd: false,
      pwdCheck: 'hidden',
      id: 'forgetThird',
      repwd: false,
      pwdValue: '',
      repwdValue: '',
      textImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAASCAYAAAC5DOVpAAABsklEQVQ4T+XTQUhUQRgH8P9/npobhRRBZ/EQy3N1qce+fKtpl9pTdCi6FyESeYogQhCiQxCBl9pT1MGDdOhYx2rdt+32VtHaVczqKh2SPVTqezOf7EKQaGrgzTnON/Obme/7htjDwT20sJ+w/IeZTkiTk07Zz/43h365ekOiaCrtdhUaOfODyi2AD4zWbq+bCHYL+uVP5yHqFSC3Pcd+2MAmSxWb5AzJz1gzac+zf+wEFsoLcUj0ToBDoEl4pzoXOVmqXlFKnjY2Cwli0WriJTcZX/gXmA/mzgEyTkADcri+zhhepR/MXxbRmUjMqNLoUM3WC9ZPE3luKBOhFU4PJJO1XG76WHNry2mjeI3gBYKzsHDRCiMdUo2S1utNfVYIqo8FuA5BCCK2+XbyHYKjpHrS48SH/45vwIrF+XatdP152baDuLP8i2dIZgi5KcAYDV+2qJ/+msQegRwMdXii3+3+9gfcgAXBl7YQKyPRb32/r69ruVHpqbl+GHkDxQHvZPxtfS6Xmz2iYtbdA2i95zgdtS2xrRL+/uPX42Z1JbtqZOhsyl7arsr76W/u1O27ja8DGVGgjRwMwWoAAAAASUVORK5CYII=',
      successImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAABECAYAAADdjVbeAAAJRElEQVR4Xt2bC/Bu5RTGn+Wea41LMkMZJBrJjMSQGBFDF7oY5XZyLyq6SBldMJ1GSTJObil0ZIjjEuGQSyNkhAyKESEyI3JJbj3mx3rPvGe39/ft/X37+5//OWvmP2fmO+/e7/vs/b5rPetZa4eW2GzfWtJLJB0u6Z2SVkXEv5ZqGbFUEzGP7V0lnSHp4dW8V0o6JiLWLMValgSw7ftJOlXSvpK65vyKpNdExOWLBL5QwLbvKOno/NusB5D/SPqgpNdHxG96jB88ZGGAbT9b0imStm5ZlSV9TdJjJd2m5f//Jukt/EXEjYNRTbhgdMC2HyHpbZIe3zHvZZIOjYhv2n5obvWndYz9taQ3SDo3Im4eA/hogG3fQ9KbJb1IEp64ab+VdJykcyKCN7zObD81d8MOHaA415xvzvlcNjdg27eVdLCkEyRt3rKaf0h6u6Q3RcSfu1ab4YqHdZKkLTuOwackvTYi8Owz2VyAbT9F0umS2Jpt9pl8Mz/tuzrbdwWUpFdLanN0xGzi90kRcX3f+5ZxMwG2/UBJp0nas2PCn7DgiLho6ILK+AxlJ0t6TkcoAyxH6B0R8c++8wwCbPvOhIxkSbdvmeRPuSVZxCjsyfaj02M/rgPUzyQd1Ze49AJsm3HPk7RS0lYtExM/z874+fu+T3vIONv7pWO7f8d1hLkjIuI7k+47FbDtnZMO8m+bXZJhZqEMiYlts6teld6+zUESuj4k6dgu4tIJ2DZvkjPEm71VC9Jfpcf88JA3NcbYDIFEhZd1EBfIylvZERHx13rOWwDOp0gmQ8y8S8sC/55kYeXYLGjow7D9kNzme3RcW2L/OuKyHmDbeF28L164aZCFC9JB/GLo4hY53vZuue4u4vKDPN9r/wc4KR7xlLjaZlxw2BhMZ1HAk7i8UNIbOxwrU386bB8A3ZMEY2raH5LLvisi8MTL3jJ0QlyOaCEuNwOYJ0Jsre3fks6SdPwsbGY5PBXb901igtMtdhOA8cTHVD9+mwQgIn64HBY+zxpsQ03r9PJGAJN3HlndGNaCOrHRm+27SYL9FbsBwMQriHoxODD57EZvtreQVCcY1wOY1A32Uozk/MyNHu3/o8+9JF1XYbkOwKRar6h+PDgiVm0igGGL11ZYrgXwu1MnLr+/NCLes4kARi39ZYXlGgC/T9JB1Y8HRcT7NxHAZFY/r7BcDeAPZIJQfn9+RCCVLkvLpGZbSd+KiJsmLdL2gyRdVY25CsDnSYJtFTswIlYvR7S2D5TEcSO+/kjSoyICSbfVMrlgXLEfA/gjkvavftw/Ij66nACnAIG4RwZXJzx7RAS6WRfgh0kiDyh2BYA/Jmmf6sd9IuLjywVwsqVzJaF41Aa3336Sgml7R0m1MHE5gD/ZEOP2igjk0A1ueV5Z306NxZCT41zPn7RI24+UhPBf7DIAsyWeXv34jIi4cEOjzbfDgycJqA0isTeVi2lrTAHw0mrcpQD+fCMP3j0ivjDtZs3/t/0ESQ+QtCYiSCtnNtt7pTaFSlob53HPiKhja+c8tlE6v14NuATAX5SEYlBst4j40pDV2qbADYHB0LrYJbWz6H0720elOtrU0diJB0TEX/reLF/CxdX4iwH81Ubha9eIQPLsbbYR3HevLqCkst+QnWL7dnQDNEhQuSVqzJFDC2q2nyRpbbWutQDmldci9y4RgfTa22wfm8l2fQ1CPLz8vdNuZPvuqZfRIdC8xyGzUt0s0n2uuuFFAObw15rzzhGBCNDbbLP9SDMPa1yE8IfAQIF7vYphGWd7O7SmFuGQtI5d8uXeC2lObuOM6zh9IYABV7v9naap910LsH1oAm+WS9GuV0QElcR1lmojJKcpqkMHIRU1LRyM2/bekj5RXbgGwN9rNJnsGBHfH3z3vCA9LHT1To174BeeWTQy2y/PMmpTPMTJ7DuGlmb7WXlUylIuADDeFApWbIeIuGJWwFxnmx3DNm3WeanrIpq/kvJMyxycd879WIU42i5qcnI+gCltPriafLt5Cs7V2dxG0mclUR2oDUW02dcBTTw6IvADo1lK0Oy2YqsBzFMn3Sq2bUT0LmBPWl1qSvBySEmXEVfJ0NgRo5rtF6TmXu57DoCvlsTbKLZNXybTZ3UZXxEZntsy/pp0TjORlGnz216RZdwy9GwAQ9OQQoptHREsZFSzTXqH4F/SO8IhTux3o05U3cw21UUKCsXOAjAVtntXP261qEVkUZv6D07xhGmKxbwPwjbiJCJlsVUA5gnX3nTLiFhIFX9eAEOvtw0RqjX2MwD8x0bg3yIiarV+6DzLZrxt6tzw8GKnAxhwlCSKbR4RNyybVc+xENv0edL+WOwUAFNsqvuhNlv02ZoDQ+9LU8AjOjymumglgJFL7lD9iCZ9XETgzDY6sw0vP17SIS017xMBTOdLs9djYd2si3qCmbG9mBZHSfdsmQeGtwLA0L9J3azkuucNTb4XBaztvrZ3ydYqOnnbDAXncGrepceDblZqwtt3XDBaN+uYDyLbE3FKJAltLViwSJrV1qWI6wbZhtCzJeh/6upm5bsESD7tfhvMUqtG+8ILN9NQ1sWRRHg4tZmDt/Vp0c3KNiZo186sAJyrm3Xep5Rsja6Frk57ykS0GLd+QjCpE4/KGw5gUjfriXDVId2sswK2zZcwsKauzIseS1qrvjFpjj69lnSzkqfW8ay+56Bu1qGAs82QTiOk4LZOe4R5diSd9lM/E5gKmAVmMYvaDt20Xd2stOfjIL47FFSH58WnlE57ejWaRo80rRk0ind22jcv6gW4XFT1YfJEOetNm9rN2udhpLjH9u2KGoRSvoEY/CnAIMAVcJpF+Nqkq5u1EJfTmt2skwDbplRDeERtbDMAAhTAM9lMgCvg0z7DuUU3a8f2pYbErqF9qi0ysGUREM6c10HOBbgC/uR8M5M+w6FUsp6onr4B6QffcJ+Wh4G4R234dWPl6KMATseGB530GQ7DEOro9LsypVw+vOzy/oQXwszElv6h+3o0wNXbpqkcBtTWzcowiAui/BM7Ou35Go3ez9Vd5ZmhIOvxowOugCMMTvoMp7luOnJoTj95UqPKPGC5dmGAK+BUISAuXZ/hUGSDoxPDIfsLtYUDroC3fYZDizJp26AC/DxPZMkAp2PjMxyUCP4Q1+DhJOZLZv8F7zZurKSp5d4AAAAASUVORK5CYII='
    };
  },

  components: {
    PwdHeader: _pwdHeader2.default,
    PwdNext: _pwdNext2.default
  },
  methods: {
    pwdInput: function pwdInput(event) {
      if (/^[a-zA-Z0-9.]{8,20}$/.test(event.value)) {
        this.pwd = true;
      } else {
        this.pwd = false;
      }
      if (event.value == this.repwdValue) {
        this.repwd = true;
      } else {
        this.repwd = false;
      }
    },
    pwdChange: function pwdChange(event) {
      if (/^[a-zA-Z0-9.]{8,20}$/.test(event.value)) {
        this.pwdCheck = 'hidden';
      } else {
        this.pwdCheck = 'visible';
      }
    },
    repwdInput: function repwdInput(event) {
      if (event.value == this.pwdValue) {
        this.repwd = true;
      } else {
        this.repwd = false;
      }
    }
  }
};

/***/ }),

/***/ 136:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["container"]
  }, [_c('pwd-header', {
    attrs: {
      "title": "宝石山注册"
    }
  }), _c('div', {
    staticClass: ["text-box"]
  }, [_c('text', {
    staticClass: ["text"]
  }, [_vm._v("设置新登录密码")]), _c('image', {
    staticClass: ["text-image"],
    attrs: {
      "src": _vm.textImage
    }
  })]), _c('div', {
    staticClass: ["pwd-box"]
  }, [_c('input', {
    staticClass: ["pwd"],
    attrs: {
      "type": "password",
      "placeholder": "请输入8-20位的登录密码",
      "value": (_vm.pwdValue)
    },
    on: {
      "input": [function($event) {
        _vm.pwdValue = $event.target.attr.value
      }, function($event) {
        _vm.pwdInput($event)
      }],
      "change": function($event) {
        _vm.pwdChange($event)
      }
    }
  })]), _c('div', {
    staticClass: ["line"]
  }), _c('div', {
    staticClass: ["repwd-box"]
  }, [_c('input', {
    staticClass: ["repwd"],
    attrs: {
      "type": "password",
      "placeholder": "请再次输入",
      "value": (_vm.repwdValue)
    },
    on: {
      "input": [function($event) {
        _vm.repwdValue = $event.target.attr.value
      }, function($event) {
        _vm.repwdInput($event)
      }]
    }
  })]), _c('div', {
    staticClass: ["line"]
  }), _c('text', {
    staticClass: ["pwd-check"],
    style: {
      visibility: _vm.pwdCheck
    }
  }, [_vm._v("密码位数不足，请输入8-20位的输入密码")]), _c('text', {
    staticClass: ["referrer"]
  }, [_vm._v("推荐人编号(选填)")]), _c('input', {
    staticClass: ["referrer-input"],
    attrs: {
      "type": "text",
      "placeholder": "推荐人员工编号或手机号"
    }
  }), _c('div', {
    staticClass: ["line"]
  }), _c('pwd-next', {
    attrs: {
      "text": "注册",
      "open": (_vm.pwd && _vm.repwd) ? true : false,
      "router": "registerSuccess.js"
    }
  }), _vm._m(0)], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["register-bottom"]
  }, [_c('image', {
    staticClass: ["bottom-logo"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAPCAYAAAAyPTUwAAAB6klEQVQoU32RPWtTcRjFz/nnxiRNKVahg4MvoG2aWFNtaiyIoA6Ck4ij30DwAzhaRAVHFYpTHdqlWAeRDiIqcrU3vXExL2JRFESsLtpK0t7e50gKAQfxzM/h/M5zWAnr8yZM92aGHxcK3MB/xKBSf0PHfhDPRXd7fHSwSjL+l4dB2HhFYgcMAhEBmOtJ2/V8Ph+R1N8mBmH9BcF+ABGJl4p5h0ntUazROEpPl8v7VromVsLGM5A/nXRX+v20VCpFQbV2mnJXBXwjOdOTihcKhcIag7B20clbKJWGfnQjF6uNCUqTpNuAWQLkeznMsBLWrmwiOX/syIFmN65areVjuRsAJKhN4AvBvVxcqj8kkEjQe5DN2lwul1v1/bf7k+nEpEx10q0CyoE412GehdS3VdC5T0zY7C/LNLPWPuXEvMF2kTwKIMdK2LwvWIpw66RWKAsib5vvRVERjiMyDIO60OnDYKl5j842nfAOCa8FxUMwPhLtZGxcdsQEwPOAZTsY10i2JOsDXBKydTjvCWRlSoMiz0L6DGInX4fNQ0ngTCwNAGrBuc5yocXxiCMvAxgQ8FGmm+yw+L6fcantRY8cl+QJXAZwiUARwFRLmjpRyn/dOu6qVlvpXWt/P0xgN8XjRHxrbOzgh+7//wD0B+l1sq60ZwAAAABJRU5ErkJggg=="
    }
  }), _c('text', {
    staticClass: ["bottom-text"]
  }, [_vm._v("杭州银行宝石山")])])
}]}
module.exports.render._withStripped = true

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env === 'X' ? '88px' : '40px',
      'height': _vm.Env === 'X' ? '176px' : '128px'
    }
  }, [(!_vm.backButton) ? _c('div', {
    staticClass: ["back-box"],
    on: {
      "click": _vm.back
    }
  }, [_c('image', {
    staticClass: ["back-icon"],
    attrs: {
      "src": _vm.backIcon
    }
  })]) : _c('text', {
    staticClass: ["back-text"],
    on: {
      "click": function($event) {}
    }
  }, [_vm._v(_vm._s(_vm.backButton))]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v(_vm._s(_vm.title))])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(0)
)

/* script */
__vue_exports__ = __webpack_require__(1)

/* template */
var __vue_template__ = __webpack_require__(2)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdHeader.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-14ac54fb"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 4:
/***/ (function(module, exports) {

module.exports = {
  "check": {
    "marginTop": "20",
    "color": "#FA5665",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "53"
  },
  "next": {
    "marginLeft": "36",
    "width": "678",
    "height": "88",
    "backgroundImage": "linear-gradient(to right,#4AD0E0,#4BA0FF)",
    "borderRadius": "44",
    "boxShadow": "5px 5px 5px 0 rgba(70,146,255,0.51)",
    "marginTop": "31",
    "alignItems": "center"
  },
  "next-text": {
    "lineHeight": "88",
    "fontSize": "36",
    "fontFamily": "PingFangSC-Medium",
    "color": "rgba(255,255,255,1)"
  }
}

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
var modal = weex.requireModule('modal');
exports.default = {
  props: ['router', 'open', 'tips', 'text'],
  methods: {
    jump: function jump(router) {
      var _this = this;

      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/';
      if (this.$parent.id == "forgetThird") {
        if (this.open) {
          this.$parent.isOK = true;
          setTimeout(function () {
            _this.$parent.isOK = false;
            navigator.push({
              url: urlFront + router,
              animated: "true"
            });
          }, 1000);
        }
      } else if (this.$parent.id == "faceFind") {
        if (!this.$parent.pass) {
          if (!/[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(this.$parent.identity)) {
            this.$parent.tip = '身份证信息错误，请重新填写';
          } else {
            this.$parent.tip = '';
            this.$parent.pass = true;
            this.$parent.noteText = 60;
            this.$parent.color = '#8F9AAE';
            this.$parent.tip = '短信验证码错误，请核对或重新获取';
            var timer = setInterval(function () {
              _this.$parent.noteText--;
              if (_this.$parent.noteText == 0) {
                clearInterval(timer);
                _this.$parent.noteText = '重发';
                _this.$parent.color = '#4BA0FF';
              }
            }, 1000);
          }
        } else {
          if (this.open) {
            navigator.push({
              url: urlFront + 'prepareFace.js',
              animated: true
            });
          }
        }
      } else {
        if (this.open) {
          navigator.push({
            url: urlFront + router,
            animated: "true"
          });
        }
      }
    }
  }
};

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('text', {
    staticClass: ["check"]
  }, [_vm._v(_vm._s(_vm.tips))]), _c('div', {
    staticClass: ["next"],
    style: {
      opacity: _vm.open ? 1 : 0.4
    },
    on: {
      "click": function($event) {
        _vm.jump(_vm.router)
      }
    }
  }, [_c('text', {
    staticClass: ["next-text"]
  }, [_vm._v(_vm._s(_vm.text ? _vm.text : '下一步'))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(4)
)

/* script */
__vue_exports__ = __webpack_require__(5)

/* template */
var __vue_template__ = __webpack_require__(6)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\hzBankDiamond\\src\\components\\pwdNext.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-86c0103e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ })

/******/ });