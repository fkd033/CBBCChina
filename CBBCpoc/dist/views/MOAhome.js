// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 298);
/******/ })
/************************************************************************/
/******/ ({

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * navigator 跳转页面公共方法
 */

//  引入navigator模块
var navigator = weex.requireModule('navigator');

// 封装navigator跳转页面公共方法
var navigatorEvent = function navigatorEvent(jumpPageName) {
  // const navigatorEvent = (cdnUrl) => {
  var url = '';

  // 判断设备类型
  if (weex.config.env.platform === 'iOS') {
    // iOS设备路径
    url = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/' + jumpPageName + '.js';
  } else {
    // Android设备路径
    url = 'LocalAssets_' + jumpPageName + '.js';
  }

  // cdn路径
  // const url = cdnUrl

  // 获取url，开始执行跳转
  navigator.push({
    url: url,
    animated: "true"
  }, function (event) {
    //  JScallBack
  });
};

exports.navigatorEvent = navigatorEvent;

/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(299)
)

/* script */
__vue_exports__ = __webpack_require__(300)

/* template */
var __vue_template__ = __webpack_require__(301)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA-weex\\src\\views\\home.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-61c7d83e"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 299:
/***/ (function(module, exports) {

module.exports = {
  "home": {
    "paddingBottom": "15",
    "backgroundColor": "rgb(245,245,249)",
    "fontFamily": "PingFangSC-Regular"
  },
  "fixed": {
    "position": "fixed",
    "backgroundColor": "rgb(249,249,249)",
    "height": "50",
    "width": "750",
    "top": 0,
    "left": 0
  },
  "header": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "backgroundColor": "rgba(249,249,249,0.9)",
    "paddingBottom": "20",
    "paddingLeft": "32",
    "paddingRight": "32",
    "borderBottomWidth": "1",
    "borderColor": "rgb(230,230,230)",
    "borderStyle": "solid"
  },
  "headerIcon": {
    "position": "fixed",
    "top": "0",
    "flexDirection": "row",
    "width": "750",
    "paddingTop": "40",
    "height": "126",
    "backgroundColor": "#ffffff"
  },
  "scanCell": {
    "width": "40",
    "height": "40",
    "marginLeft": "32",
    "marginTop": "23"
  },
  "scanImgCell": {
    "width": "40",
    "height": "40"
  },
  "scanImg": {
    "width": "60",
    "height": "40",
    "justifyContent": "flex-end"
  },
  "right": {
    "width": "60",
    "height": "40",
    "justifyContent": "flex-end"
  },
  "scan": {
    "width": "38",
    "height": "32"
  },
  "title": {
    "fontSize": "34",
    "fontWeight": "500",
    "color": "rgb(17,17,17)",
    "textAlign": "center",
    "fontFamily": "PingFangSC-Medium"
  },
  "menu": {
    "backgroundColor": "#f5f5f9"
  },
  "menuBar": {
    "flexDirection": "row"
  },
  "tabActive": {
    "width": "187.5",
    "height": "236",
    "backgroundColor": "rgb(255,255,255)",
    "position": "absolute",
    "top": 0,
    "transitionProperty": "left",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out"
  },
  "@TRANSITION": {
    "tabActive": {
      "property": "left",
      "duration": 400,
      "timingFunction": "ease-in-out"
    },
    "menuTxt": {
      "property": "transform",
      "duration": 400,
      "timingFunction": "ease-in-out"
    },
    "menuTxtActive": {
      "property": "transform",
      "duration": 400,
      "timingFunction": "ease-in-out"
    },
    "menuPanel": {
      "property": "left",
      "duration": 400,
      "timingFunction": "ease-in-out"
    },
    "contentList": {
      "property": "height",
      "duration": 400,
      "timingFunction": "ease-in-out"
    },
    "todoContent": {
      "property": "height",
      "duration": 400,
      "timingFunction": "ease-in-out"
    }
  },
  "menuBtn": {
    "flex": 1,
    "alignItems": "center",
    "paddingTop": "32",
    "paddingBottom": "32"
  },
  "menuIcon": {
    "width": "140",
    "height": "100",
    "marginBottom": "32"
  },
  "menuTxt": {
    "color": "rgb(51,51,51)",
    "fontSize": "26",
    "transitionProperty": "transform",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out",
    "fontFamily": "PingFangSC-Regular"
  },
  "menuTxtActive": {
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular",
    "transitionProperty": "transform",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out"
  },
  "menuPanel": {
    "width": "3000",
    "flex": 1,
    "flexDirection": "row",
    "transitionProperty": "left",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out"
  },
  "menuContent": {
    "width": "750",
    "backgroundColor": "rgb(255,255,255)"
  },
  "contentList": {
    "paddingTop": "16",
    "height": "196",
    "transitionProperty": "height",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out"
  },
  "listPanel": {
    "flexDirection": "row",
    "height": "60",
    "width": "750",
    "alignItems": "center",
    "justifyContent": "space-between"
  },
  "circle_dot": {
    "width": "8",
    "height": "8",
    "borderRadius": "4",
    "backgroundColor": "rgb(0,164,234)",
    "marginRight": "24",
    "marginLeft": "24"
  },
  "listTitle": {
    "color": "rgb(102,102,102)",
    "fontSize": "28",
    "flex": 4,
    "lines": 1,
    "textOverflow": "ellipsis",
    "marginRight": "16",
    "fontFamily": "PingFangSC-Regular"
  },
  "listTime": {
    "color": "rgb(153,153,153)",
    "fontSize": "24",
    "marginRight": "32",
    "textAlign": "right",
    "flex": 1,
    "lines": 1,
    "fontFamily": "PingFangSC-Regular"
  },
  "loadMore": {
    "flexDirection": "row",
    "justifyContent": "center",
    "alignItems": "center",
    "height": "80"
  },
  "loadMoreImg": {
    "width": "36",
    "height": "36",
    "marginRight": "16"
  },
  "loadMoreTxt": {
    "fontSize": "28",
    "color": "rgb(153,153,153)",
    "fontFamily": "PingFangSC-Regular",
    "textAlign": "center"
  },
  "loading": {
    "paddingTop": 0,
    "paddingRight": "16",
    "paddingBottom": 0,
    "paddingLeft": "16",
    "width": "750",
    "alignItems": "center"
  },
  "indicator": {
    "marginTop": "16",
    "height": "40",
    "width": "40",
    "color": "rgb(153,153,153)"
  },
  "todo": {
    "marginTop": "16",
    "backgroundColor": "rgb(255,255,255)"
  },
  "todoTitle": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingTop": "32",
    "paddingRight": "32",
    "paddingBottom": "32",
    "paddingLeft": 0
  },
  "cellLeft": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "cellRight": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "leftBar": {
    "width": "6",
    "height": "34",
    "backgroundColor": "rgb(245,133,63)"
  },
  "leftBarColor": {
    "backgroundColor": "rgb(44,188,144)"
  },
  "cellTxt": {
    "color": "rgb(51,51,51)",
    "fontSize": "30",
    "fontFamily": "PingFangSC-Medium",
    "marginLeft": "32"
  },
  "more": {
    "color": "rgb(102,102,102)",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Regular",
    "marginRight": "16"
  },
  "moreImg": {
    "width": "10",
    "height": "18"
  },
  "todoContent": {
    "height": "134",
    "transitionProperty": "height",
    "transitionDuration": 400,
    "transitionTimingFunction": "ease-in-out"
  },
  "todolistPanel": {
    "height": "134",
    "paddingLeft": "32",
    "paddingRight": "32",
    "flexDirection": "row",
    "alignItems": "center"
  },
  "clock": {
    "width": "67",
    "height": "58",
    "marginRight": "12"
  },
  "square_dot": {
    "width": "4",
    "height": "4",
    "backgroundColor": "rgb(0,164,234)",
    "marginRight": "8"
  },
  "todolistTitle": {
    "flex": 4
  },
  "todolistTxt": {
    "lines": 1,
    "color": "rgb(102,102,102)",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular"
  },
  "todolistTime": {
    "lines": 1,
    "textAlign": "right",
    "color": "rgb(237,78,78)",
    "fontSize": "24",
    "fontFamily": "PingFangSC-Regular",
    "marginLeft": "4"
  },
  "recentlyUsed": {
    "marginTop": "16",
    "backgroundColor": "rgb(255,255,255)"
  },
  "cellIcon": {
    "flexDirection": "row",
    "flexWrap": "wrap"
  },
  "nearList": {
    "width": "187.5",
    "height": "196",
    "alignItems": "center",
    "justifyContent": "center"
  },
  "img": {
    "width": "187.5",
    "height": "98",
    "justifyContent": "flex-end",
    "alignItems": "center",
    "marginBottom": "24"
  },
  "newsCount": {
    "width": "36",
    "height": "36",
    "borderRadius": "18",
    "backgroundColor": "rgb(255,0,0)",
    "color": "rgb(255,255,255)",
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "textAlign": "center",
    "lineHeight": "36",
    "position": "absolute",
    "top": 0,
    "left": "115.75"
  },
  "newsCountMore": {
    "width": "60",
    "left": "103.75"
  },
  "nearImg": {
    "width": "80",
    "height": "80"
  },
  "nearText": {
    "color": "rgb(51,51,51)",
    "fontSize": "28",
    "fontFamily": "PingFangSC-Regular"
  }
}

/***/ }),

/***/ 300:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _navigator = __webpack_require__(23);

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } } //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
var dom = weex.requireModule('dom');
// const animation = weex.requireModule('animation')
// const navigator = weex.requireModule('navigator')

exports.default = {
  data: function data() {
    return {
      Env: WXEnvironment, // 获取设备环境变量
      show: true,
      activeTab: 0, // 点击第几个菜单栏
      loadMore1: 1, // 控制加载更多是否显示，消失数据显示十条
      loadMore2: 1,
      loadMore3: 1,
      loadMore4: 1,
      todo: 1,
      menuBtnList: [{
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAACWCAYAAACrUNY4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjRENjY3NEUxNTNGRjExRTg4RUU0QTc5NUZGRTI5NTJCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjRENjY3NEUyNTNGRjExRTg4RUU0QTc5NUZGRTI5NTJCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEQ2Njc0REY1M0ZGMTFFODhFRTRBNzk1RkZFMjk1MkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEQ2Njc0RTA1M0ZGMTFFODhFRTRBNzk1RkZFMjk1MkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7J5d6vAAAJcElEQVR42uydW4xcdR3Hf+ecue2le2n3AluobenSVlLaApHGaLC8aNJ4CYGAFC+J0YZEEjTyIj4QjZr4IgkhRh9srC2YqNEQH3xQK09SAQ0FoTdbSqS62y3d2+zu7Nz8/c6cgdltuztnz0Cncz6f5EubmTND9z/ns7/f+Z/zP+PIwVFpAAOa3Zpdmi2aDZp+TYcmLQBXn5wmqzmvOaM5pnlBc1gTWQIngkhrNF/UPKi53X8vgGuPsuZlzTOaA5oLH5RI12u+pdkXVByAVmFG83PNDzXnwrzQDbFtQvOY5oTmm0gELUi75uua48G+nmy0SMOav2t+pOlkvKHF6Qz2ddvnP9wokT6reUmzk/GFmLFDc0TzuagifU3zW00XYwoxrk6/CVxYkUgPa36q8RhLiDle4MLDYUX6jOYpxg9gAU8FbtQl0lbNISoRwGUr06HAkSVFsum+Z4WZOYCljpmelUVT44tFelSznbECWBJz5Bu1D9Re2TAklRNRVCOA5ZnWbJbgCojaivRtJAII1eI9vrgi9Wne0rQxPgB1M6tZpxmrVqSHkAggNG2BO1IrEgCEZ29VJFuUdxvjAbAibC3ekIl0t7AoD2ClmDu7TaQ7GQuASNxhIm1hHAAisdlE2sQ4AERi2EQaZBwAIjFoIq1iHAAiscplDACig0gAiASASACIBACIBIBIAIgEgEgAgEgAiASASACIBACIBIBIAIgEgEgAgEgAiASASACIBADhSTAEzcWatCtbuzz50saMfLQ/KTcF96c5PVWSv43lZf+/5+SNyaJcyJUYLCoSXPJBOCIdCUd29SXk8+vTsnN1QvozjiScSuzvO3orz9k2tq3LHdupSLCQpMoy1ObK3YMp2bshI50qilcjSp9Wqt6UqxXKk7miyMmpopydLkmuXGbwqEhQZVXSkbsGk7Ktx7tEoir2mD13a68nnxhI+q8BRIIa0p7IJq0212lV8pbww567LuPKsB5H2WsAkaC2x9bWbo22bnbssxy2zWrd1l4DiAQ1FPVYZ65Ylvk6JuPyuk2uVJYSx0eIBJfK8Wa2vmntd9S2t7IlyeMRIsHC4x7LdKEsueLy25trM7qtdYEJurvmaM0ZgqtPV9LxJxr2rE3LLT3LzyDc0u1Jbm1KXpsoyKyKx8lZRIr5BINId8qVjw8kZc9QSrb3JPxzRXasdFZbN2vhZguVbVPqV48KZ+eRupOuypSQvesz+ti8PD+alwndtkCrh0hxlMgq0a1agT6t1eXedWl/Nm6+VJZzsyU5PDIvZ6ZLMh7MPthzN3Z4knIduaHd9afJ7TXGRd3mlYtlmdSDJmRCpFhhlcgkenRLu9y+unLJjx0nnZ4uyp//l5cDp+fkbRWqEHRtbnD+yKrVp7R67eytvOaT16f8qx6ePDYjR8e5Bu+qHefKPY89wTB8sJWoV3f8u7Sds4pibd2AClIol+X4VFH+eC4vz/0np8c/RbmYK8tMsZKslhqbjJiYL4vNL9hVDdbqdWmb161/VqqZyKi+Jm9T4ww1FSkO7dyey7RzVon+8HZOnh/Jy+U6NJPJrgCvVqi2G9OyjjYPkWjn3mvn/vTfvPxC27lT+vfl9v+j4wWZCiyhzaO1o52rbee0Ev1L27npOs6y2snbLG0eFelaxXZQLSr+zFmYy9wybvU80XvtnLVmk7qjv6qV48ULeTk2WfQf7whxhvVMtigvvVPwW7u1mr60s6DNs/bulIo6V6q/x7OrjqzVnA9kBURqONt6ErK+w5UPdXihRLLjol19SblZZaq2c7aj2slUqxjbtTXb2OnJSnbb6pIL2+m7kmXJeM67bd4Gfc8X9JhqMh9OJLtU6c1sSY4Ex2OASA3lq5sy0q8tWb/+5g9zZU5ad26Tr3adkf25yp94SKhg0X7z2/v3pGz5hfPue1vr2K7/v96UI7liCJE0o3NlDSKFwZGDo9TvOpl9oE+SrrPkeqFWwDo6q5TtvxrjQ6ciNZ60F48rRO3HdLkhBCK9f+U7Pj8nawbDwTIKAEQCQCQARAIARAJAJIBmg+nvBnE+V5Lzc9HPbdvVD90p54p3WwVEaml+fTYnT5+Yjfw+H+tP+jdBsdsXd3NLYkSKX0Uqy+sTxcjvs67Dk4l8SYpcuIVIccRaMVvCEBW7yLRNezoOXhEpluzo9eTLGzOR32dzl+ffciuFSYgUR7Z0JfyqFBVbDrEm7UiKmQZEiiNr210/EE/45AGoSM2D3Tb4ryPNu6LU2k47jrMWlMqJSE2LSfTE0WzT/vtsRtEmQ0woRKK1A6AitTJ2uy27WWOzYuenbGq9h3l1RGpm7NvIH1yfbtp/n53kNdltah0QqWkZ1p30hvbm/Zpx0ydtN7fk/BQiNTP2G7/NYxwQCSLRqGUUYSqM3enHblNsN84HRGoJGrWMol5cp1IB9w23yVduyvABIFKrVKTGLKOoFzvUsVsSj/HVLYjUSjRqGUXdFSk4Lutg8gCRWolGLaOov7WrfL3Mtl4+QkRqIRq1jCJce+fIEJf7IFIrwTKKeMMnD4BIAIgEgEgAgEgAiASASAAtCOeRQvDaeEEGMq70a1r1why7fn10ruQHqEjvj0gTRRnRHazcwvfltp/NfsZXx4t84CFw5OAot2sHoCIBIBIAIgEAIgEgEgAiASASACASACIBIBIAIgEAIgEgEgAiASASACASACIBIBIAIgEAIgEgEsA1yLyJNMU4AERiykQaYRwAInHRRDrFOABE4qSJdIxxAIjEcRPpCOMAEImXTaTDUrl3OgCEx9z5S3Wy4R+MB8CKMHfOVc8jHWQ8AFbEM/afWpFmGROAUJgzB2pFGtPsZ1wAQrE/cMf/fqTqg0Oa45pOxgdgWaY1m+34qLYiSfDAdxkfgLr4XlWixSIZT2r+yRgBLIk58uPaBxaLlNfcr5lkrAAui13k/UDgyhVFMk5qvqDh23gBFmJOPKQ5sfiJK61Hek7zCOMGsIBHAjekXpGMn2j2UZkAfAf2BU5IWJGMn2nu5ZgJYoxNc98XuCArFcn4veYOYTYP4scrmjs1v1tuw3rv2WATELs0jwuXEkHrY/v4dzQf0bxezwvC3PxkXvMDzbBU5tCzjDe0GDOapzU3a74f7PN1UXuJUFj6pDJNvldzm/9eANcetp7IlkIc0vxSgmvnwhJFpFoGNbuDfnKrZoOmXyrX7SX5rKAJyAcTB+c1ZzRvSGV1uC1sjXwDoP8LMACPmHBcOhCeZAAAAABJRU5ErkJggg==',
        txt: '杭务公告'
      }, {
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAACWCAYAAACrUNY4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjVCNEVCNTdFNTNGRjExRTg4RUE3OTlGRTlEQkFBNjdDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjVCNEVCNTdGNTNGRjExRTg4RUE3OTlGRTlEQkFBNjdDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUI0RUI1N0M1M0ZGMTFFODhFQTc5OUZFOURCQUE2N0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUI0RUI1N0Q1M0ZGMTFFODhFQTc5OUZFOURCQUE2N0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7NBVplAAAMMklEQVR42uydWWycVxXHz+ye3buzp1mcpW3SlqZteAGCeOgDYpFAIChI8EBVqZVoJV4o8ACCByQgUoUQvFSqaPsCYpF4opACJZAmaWhLm5U4dpzEsT2L7ZmxZ+f873wOY2M7883YcWbm/5NOvXRmYt/5fj73nu8ujvtfOyqrQL/GEY3DGvs0dmj0aQQ1fELI+pPVSGtMaAxpnNP4p8YxjfFGX9zdwHN7NL6s8QWNhzUcfK/IXYzPim6NvRqPW98va5zWeEXjJY1YPS/urOM5GzV+pDGs8WONQ5SINDEO6xrGtTyi8YLGprUUCdnrGxoXNJ6zum2EtBIBjac1zlvXume1RRrUeFPjhxohtjdpcULWtY5r/t7VEumTGqc0HmL7kjbjQY0TGp9qVKSvafxaI8I2JW2cnX5luVCXSE9p/FzDxbYkbY7LcuEpuyJ9QirVC0LI/3jBcqMmkfZrvMxMRMiSmelly5EVRUK571VhZY6QlcZMr8qi0vhikb6u8QDbipAVgSPPLicS7uZ+h21ESE18W6pmQFSL9E126Qix1cV7frFIvRpfZdsQYouvWO7cEukJDT/bhRBb+C13FohECLHPF+dFwqK8D7A9CKkLrMXbBJE+KlxPREi9wJ0jEOkxtgUhDXEIIu1jOxDSEHsh0m62AyENMQiRBtgOhDTEAEQKsx0IaYiwk21ASONQJEIoEiEUiRCKRAihSIRQJEIoEiEUiRBCkQihSIRQJEIoEiGEIhFCkQihSIRQJEIIRSKEIhFCkQihSIQQikQIRSKEIhFCkQghjeBmE9TO0YMfl+3+Ttka6Gzp33Mkk5DhTFKeffcPfNMp0urjd3nE43S2/GFSHqdLOvR3JRRpTdgd7JaAy2sutFam1xuQDicvDYq0RrwRG5aop0Oi7g7zddjjky6PX7q9fnE7mnO4WSyXZDqflVguI8n8nPleMj9763NCkVad3904a/5a92iArYGo7A/1S8jtFberOUXKl0pyIzsj/56+KZdSMfM9SDWZS/MNp0hrw7mZcZN5XM6KNAcjG8TrcMmuELp8zTmmyJeLMjo7JScTo3IicdV8r6ByIVMRirQmZIr5BV+nCjkp4IIrN+/vhB8dvwN+t0Rulm8yRbrzuByOdRkb4eIvl8tS0s/wcSmPUVl06M+HGiM+rlRpxO/g5MH2FKndKGkGmSsVTEZEF6y8hEnqjorulKB2OX1Od8tXGikSsSFQ2YxpLqfjcnZmQoYzCSPUciJBoC3+qOwL98lgsMd87XQw81CkNgdjGVTUTievyx/HL8o5lWnxuK2aDpdbBeqVj/XvkojbJ5s6IqY4QihSW4Ps887UmLyVvGYkwtcrkSsV5VI6JuG418zKeLx/j3i9fjYkRWrzjKRi3MymZFKz0kqZqLorOKuPm9DH35ibMd1CsjZw9ncTUVQxZgpZmSsWbD0vq4/H84rlMhuRIhFqQJHIKlBvvQ2FOgfvE1EkQigSIRSJEEKRWoyyecM42qFIpCEwSRZz5uxOlHWa5zk5PYgiEQCJNndEpMcXqGnenMN6Tr83JNv9XZwetIZwZsMadsMwSxvz44pmqYP9u0AucVrZxGWkgAg7gl2yO9gjVzNTEstnzCK85ZZRIHNFPD4ZDPXI3nCv+Fx8uylSkwGJpguVvRCwYK5sW8SyBF1es0fEgC8kXpUJsSPQLUf6dkqfLyjvT4+bfwOyVq8nwnMhYNjtk72hXjkY3Sh79CM3NKFITQfmwp1KVCaXjs5N2Z6WABnQfdvsj8inN90nfd6gkQMzurcHuoxk+IjpP1iT5Kjq5mGxHx6L50NCSBfg9loUqRnBZNF3p8fkeGxYLqXj9b052jXbrd2yj/TuNBuuzGcdLIlA7Ax2s6EpUmuD1andevEjotq9Q1fPTuEaGSls1hCFTZeO9TaK1JZg/c/ByID4tXu1P9xnCg52ZEBP0IikXbtOHSfx7hFFakswsL83PCBb/Z06XsppRqpMHq1ZJH08qmzYcwFC8R4QRWpLcOGjS9bl9Uuk3FHfa1iv46BEFKldwepUVO6woC5dyNm+j4RHo+oWdHtN165Zt0SmSKQhsF8CdjDF8Shj2Zk6VqeWJaRdOpSvH+rcZLp3zEsUqe1AJjo2eVlOJUblispUD5hbh3tFmBYUDHpMJZBQpJZkpfEL7vvky6W6u3Z+3kSlSO1CeZkuGwoNOK1iV7Z72d1QVxxjqUpdOjYaDPVKwO1h+ZsitXBBQSr7by8FigSPdG4xR2Vivl1RbN5HKldeA2cvdXsCa1a5wx+CysRanj5BkdYJnC2EqUC4EKGTY8H4xmkkQPdsY0dYSiK2b8h69DVQuVvLmQ34yTOFvOS45x1FWi9Q3o7nZiVbKpgpQNXFAIclEw4h047e3dktNX8MiiZjQiZSPywDNQAuwKFMQhL5WVPubr6MWjTHXmIzfvwuhCKtC6jGXZudMsdGYkvgZmM8m5L3ZsbNH4MpnhnLrt16gS7deDYtJ+JXzbIGzEDAmKiefRXuFCgqmLFdKW/WSp1MXJWr+scgXczxDaVI65iV9AJ8MzFq7hcl9K/6gciAOZMIp53f7qS8Ow2mLWFFLbLne5pFj8dH5HTymjmwjFCkdb84MYsBf91xQZ7VrlKnSoQVqfPHTy4HZi7gsVgOvivYY74G2CT/YjomI5mkTFini3fr4+YPDZtf7YpiwVA6bh47qZkxf5sSdtk6nSKpwkOmkdmk6dKVuLk+RbqbxhsInF1UK1guvj/SZ4TbrJKgO4iLfVzleSN2xayuPZ+aNI/Fpie4L4XuI8rpqAiidI3xzZ/G/2P+XQhNJShSW4HlESiNH+7aJvdpd7CyeA9z9HKSViEwbklWFQDwOb6HCmG/LyRRT6WkjgyFyhuOw8xY97TIOryfbIL1ARNRH+3aIg93bjZHUpoMo9kIXTWMXa6kEybDzIP7PJhNfjw2Yk7hQ7kdI7ANKtWB6IAc7t4q2wKdnEjEjNQ+mQirZ5FJPtS7Q/aEe03FD/d0cLLe29Nj8pfJIZN9slVHW1YqhCn5q3b5MHUIyyvQHcRSC2zR9WF9LWSkCR0rpc2KXHbymJFamIpE/fKIZiOsM5ovHKT04seY6ASykY57ljqmEllobG5aTiVHjWzT2t2rzBJ3a1baoFlpmzwY3WjGXsxMzEgtC6RB5Q3Z4wG94FGJQ5cOWQTVvr9ptrmYii17tGXZZKai6fbN73l3ILLBFB+i7g4da/XfqspBxuquIaFILQNWud4T6JIP6ngG3TG302VukKLyhnHRmanrZlfW24HDmAvTN82mkbgBjC2/MMF1m7/TjJtwjyiZn6VI7Nq1Jth+GFkkqpkIM7oL1oTRM8nr8vrkZTPlqFZQ3ft7fFhOJkZNEQL3kDCjIqzjJ2Q9/FuEGaklQcEApWpkCmyzhY//0Ez0L81E6N7ZKV1jtjlmnr8zNWZu6h7Bbqy+gCk44CbrXKnABqdIrQkyDipvcc1CKDrEcmnTpbuQitmePY7xErqFlzNx8cZdZnaE2+k0oo7nUpz2c4dx3P/aUdZJCeEYiRCKRAhFIoRQJEIoEiEUiRCKRAihSIRQJEIoEiEUiRBCkQihSIRQJEIoEiGEIhFCkQihSIRQJEIIRSKEIhFCkQihSIQQikTI+pGDSDNsB0IaYgYi3WQ7ENIQCYh0ie1ASENchEjn2A6ENMR5iHSC7UBIQ5yGSMekckoIIcQ+cOfP88WGt9gehNQF3Lk+fx/pl2wPQuriFfynWqRZtgkhtoAzL1WLNKnxItuFEFu8aLmzYIrQ9zVSbBtCaiJlOSOLRbqu8V22DyE18T3Lmf8TCRzVOMM2ImRF4MhPqr+xWKS8xuc0ptlWhCwJJnl/3nJlWZHARY0vaRTZZoQsAE48oXFh8f9Ybj3S7zWeYbsRsoBnLDekVpHAzzSeZGYixDjwpOWE2BUJ/ELjMxwzkTYGZe7PWi5IvSKB32ocElbzSPvxtsZjGr+53QNr3bMBBYjDGs8LpxKR1gfX+Lc0HtV4v5Yn2Nn8JKfxA41BqdTQ02xv0mJkNH6qsUcqsxZytT6xnl2Ermk8p3GP9fG0cD0TaV7K1jWMa3m7xtMao3ZfxN3ADzBpZSbEgMYRqz+5X2OHRp9GSMPD94rcBeStwsGExpDGWamsDsfC1oY3APqvAAMApBJIjg+QjesAAAAASUVORK5CYII=',
        txt: '会议通知'
      }, {
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAACWCAYAAACrUNY4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjY5OTZBNzQwNTNGRjExRTg5M0Q1RTRBRTE1NTI1QzFFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjY5OTZBNzQxNTNGRjExRTg5M0Q1RTRBRTE1NTI1QzFFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Njk5NkE3M0U1M0ZGMTFFODkzRDVFNEFFMTU1MjVDMUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Njk5NkE3M0Y1M0ZGMTFFODkzRDVFNEFFMTU1MjVDMUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7cTRBIAAAObElEQVR42uydW4xdVRnH17nMlCkt5VYKLbSU3qhpscpESoAETCA+VR80GkUTfZCQQKIkvoj6oNEHEy8JMUZfSIjAi0ZjeJaEhEQuLYE20iulXKb0MoXeaDsz52y//7fXPj1zmJmefc5MZ/ac3y/5Os05e+9zZs367+9b3/rW2qXhbdvCNHCD2QNmW81uN1ttttTsSrMFAWD2uWB21uyY2UGz3Wb/NXvR7Gi3F692ce51Zt8z+7bZnWYl/lYwh1kQ7VqzDWZfia8nZtvNnjN7xmy4k4uXOzjnJrPfmR0y+73ZICKCAlOKfVh9+T2zp8yWz6SQ5L1+YrbX7IkYtgHMJxaaPWa2J/b1vukW0jqzV81+a7aI9oZ5zqLY19XnPzddQvqq2etmX6B9ocfYYvaK2de6FdIPzf5hdhVtCj3snf4etdCRkB41+4tZhbaEHqcStfBoXiFpcukp2g9gHE9FbbQlpI1mz+KJACb0TM9GjUwpJKX7ng9k5gCmGjM9H1pS461C+pHZ52krgCmRRn48mZA0m/sL2gigLX4emiogmoX0U0I6gFwh3pOtQrre7Ae0DUAuvh+10xDSw2YDtAtALgaidsYJCQDy851MSFqU90XaA6AjtBZvuYT05cB6IoBOkXYekJDuoi0AumJQQrqddgDoig0S0lraAaAr1klIy2gHgK5YJiEtph0AumJxmTYA6B6EBICQABASAEICAIQEgJAAEBIAQgIAhASAkAAQEgBCAgCEBICQABASAEICAIQEgJAAEBIAQgIAhAQwK1Rpgum8LZVD6YorQt+dd4b+e+8NfZs2hdKVVxbn+ydJSEZHw+ibb4Yxs/MvvMDfFI80C5RKqZgWLAilgQH/fxF/B90MSldfzd8TIc2iR1q0KFRWrgzV9eu9Q7q4imL6/n19oXrbbWHBfffx9yS0m62bud3Nq9X0jj4wUNRfIpQWLnSvCnik2aNSSe/uRQ9Rq9xjERIAQgJgjASXIkkmD/30XhZaTfT6RCHYTH4fQEhzUkCZZWnxrAM3v5e9pp8Tvd4qgMzq9fHntoqw+fjW7zSdwkRIMGPUaiH59NNQP306JOfOeVasfNVVjcna5MKFkJw5E+onT4ZSf7+n0MtmOrZ+6pS/70Jp8UaN6wwMhPrHH4cwNubJjtKSJZ7G9gnWs2dTs/fK116bZuOUELH36p98EhL7TiVdQ5k6+2xASDNPlpFr9SStnbzFc6haoH78eKgdOhRq9rN8zTU+V6P5Jnkniaj2/vuh9s473qkrK1aE0s03h9qxY/6aOrx/hjxZ/Eyl2cs33BDC6tX2ctnPlyiCiaG6bp2LKTEB144cCfWhIRdyddOmUL7++lDSZ46MhNoHH4S6nVex4yu6lsSHV0JIM56Z0Wx/c3pbndru9H7HN7FMKiTzKGPvvhtGX3stjO3bF8rq5PffH0rmIeR5JLKxXbvCyKuvekfvu+MOryyo7d8fLrz0UqiboNwDyZtE4coLVdasCaXFi92zjb39totJaWv3UnasvJCuq5IfiVWezue47HdIzIONvvFGGHvrrXBF5tnMACHNOAseeigNf7IQyO748haqS9OdP9hdfkLkTew9D99MNBJe7cCBUJPn2bDB30/Onw+JhXaJBKBQTuGghXY6Vp28smpVqJiXkjhcSOY95JEkvPQvWU0FbefW7XPKErYdVx8ebnxm/ejRkNxyS0hMUPJ22etBFRiEdQjpsgnpwQfdE3jpj5yNiaN++LDf3evqxCdOTOHOyo2kgERT+/DDMLZ3b6hYx86E0Dgu83oxhKvcemvov/vu0Ldli39mI5RUFUUUkMJFhWaJwkAL41zUdg0XXgwHJSqJOZgndFHpOPtdsnEWYR1CunyhXVOmTF7BhWWvS1xJOxeJNW0az0iEShIoLPTkwGRFrhKfHdNIOmSZtuz/SjAoxJOQ5MVMIC4S8zJle12hZF3eU4K3zy3rBiAPateUiIJCxjJTigjpsimpPPFde7LXJ2xxG8Ncd12julpeycPFiTqyPJP0Yt5DYyCJRCGfmx2vsK6yfLmPbcox8+bHnzrloaZXoZuVly1LvZV5IzfziD7usuuU9F2UKURICOmy0jKnk7TOxVziXA/VNLZZutS9mYTkni7LyLUKVEMxE0USvYiHahr/mMiqa9e6UCpKd8ekg67vqXB5KXkajaVuuin9rD17GilvhXn6PIWEJbJ1CKlwqIObcJT6VueuvfxyqJkHkVeYNPOnTq7sYJbMaE67x/GSZ+RMTO55Tp4MNfM6CjerNr7S/JESDKO7drkn0vhMXksC1ns+PkNICKloKP0sT+AhntLfH30U6hrXtGb9FMJJe1rfZIJQ1s4TCMraaYym9Ll5N3mhoLGaJlUzj6SwTRO+69e790vkrUysEtKYtKiJ2BtvTL+DQkuEhJAKFx1KINbxJYKKhV21oaFQO3z4swfGjJuO0WRqdePGix4phn4Spf+My9xDFJLGU/I4nia3n4mEZmLydPiJE+m8kjJ2JiSfiAWEVAiylLV+xuybPEhFFQ6qajh06GKolmXj4rjJwzETm7xXc+2dhKGsnCcZJCQlDRTaKRWvxISOj/Ne8jeqdgjyfhKSeT+Fli42QjuEVJyWrvqd3wf20YuogyvrprkkiUkdPEQxeMeWCOz/nrXbudN/ushi1s7PW7PGf7pHylLx8Rqe0FC4J3EpRLRQTmFkUE2fQj2Nq4q6twRC6sExkTq5vIeNixLVuqmzKzWtVLhS2KtXh6qFXGMHD6ZZNHkZvac5IBsHKTmg0M+FppIjeZNYlOqhmYV+Pjlrx0uYOk5i0nV1DX9PQjLB+TUkRAlN5zI+QkjFUFHJqwfUwcOWLZ4wKGuQryLRWMHtRaaqnVu1Ks3o2TEuvBUrQt/goId/45ZJxALWSkwWZFk3eaDq5s0NgUikXvEdvaCuIwHXdT2lzfV5pL4RUlGE5N5FHVopbolE4ZbGL+rAWTpcHV+paE2gKuyS+BSaqWLi/PmLY6xsnVK2jELeK5YU+Q5G5nX8OrquClTj+MkncOXtdH3NK8lL6TvMhz0mEFKPoE6t8Yg67kRaixUIQfVyza+bt6jkqMie7DqN9zWGKuruRnMcRpkAeKSCoCURWiahSVelvtvNksUUeFnhmbxJXOrQuJZCvg73XPCCW4WQVH0jpELQvEJ2aMgnRNuat8lKgJR9iwmKhpBUlKoyn1h4mhW35hGnj6fsup4EyVN0CwhpVtASCPMeYwcOhNHt29Nq73Y3X1SRq3X4/nvuCX0a+8SK8drRo2F0x44wtnt36uFyCsmTHybM/q1b07kmUuAIqUjhXYiV214i1I5HEjpeBavN+0JoUxSV+mhxnioY8oogprx9dWy7leuAkGYbpad9zZB5gUrOMZKnuOMEbuPlWAnupuvl/T7yktq5SNXmCAkhFQKJxgb0VVUZqHh08+bP7i83lVeK1Q/l5sesxCLVjsOxuEqX0iCEVCB3lC5JD0uWhIp5F/cCeYQUdw8qtVZoM6ZBSD0pJoVmEgStMT8DD5oAAI8094mbSGppt++XoO2wlHxo69QkXU2rOr2mzU0AIfWmli5cSFfB7t/vE6m+LqmN9LcvKde+Cxs3erICISGk3iXbEPK998Lozp2+ZXHbiQKtN5InipXhvq0WIKSeRaFcnA/ytUJ5hKRlEqocZ18FhNTTxIydNjDRNsK+Ub3mgS4hpiROlPq5WsM0yRIMQEi9gUQTxaDwzHcCyln93dhbARBSz4tJyyD0pInmJePtOrVssxRASL0e3nmFdptpbyjgvZImAMAjzX3i9lnZsgftgtr2wr5YtKpEhT+yUltrAULqWSFpHkmPvty5M31O7FTPQmo+T/t3L1wY+gcHQ1XbaWVCyvMEjEsJFRBSUYSktLeWmte10aMJKs88krYZrq1c6U/ta6BJ3uxJfJ18JSU8dH7cpB8QUiESDV6ZoA3z165NtyTOsYxCG5T4w8Sa5pE8jb5hQ1oypIV9eZdUxKdh+NwW+9ohpKIIyRf2mUeRGKpr1uTquI2HLjct7FOpkJ4n616qnWXrrcR0fCV7JAwgpEJ4JI2J5EVU1dDBwj73Yk0d3suGtBOrPEom1rwoFZ+N1fBICKlQYpJ1WqHQ3Nm13bBCsvh09a68JSCkwoV4c/l60BVMyAIgJACEND+ZLxOdTNYyRpqtsY6vIVI6uuidMJvwBYQ0I/1rePjic2CFthPWg4+1ocnISNoBz50LtSNHfGm5b3xftJWt9jv4YzYneso6IKRp6WPHj6cL7GIK27cOjkJSYarXx2lf7kxIenJekYSkG4HdHLRRix7+DDkCleFt2wiGAUg2ACAkAIQEAAgJACEBICQAhAQACAkAIQEgJACEBAAICQAhASAkAIQEAAgJACEBICQAhAQACAkAIQEUkBEJ6TTtANAVpyWkI7QDQFd8LCHtpx0AumKfhLSbdgDoij0S0iu0A0BXbJeQXjRj22KAzpB2/pMlG3bQHgAdIe0MZfNIf6M9ADriOf3TLKRztAlALqSZZ5qFdNzsadoFIBdPR+2MKxH6tdkZ2gagLc5EzYRWIQ2Z/ZL2AWiLX0XNfEZI4o9mb9BGAFMijfyh+YVWIY2afdPsFG0FMCEq8v5W1MqkQhL7zL5rVqPNAMYhTTxstrf1jcnWI/3b7HHaDWAcj0dthHaFJP5s9gieCcA18EjURMgrJPFXs68zZoIeRmnub0QthE6FJP5lNhjI5kHv8abZXWb/vNSB7e7ZoATEVrMnA6VEMP9RH/+Z2ZfM/tfOCXk2Pxkx+43ZupDm0M/S3jDP+NTsT2brQ1q1MNLuiZ3sIvSh2RNmt8af2wPrmaC4JLEPqy+vMnvM7IO8FykNb9s2HV9mmdkDMZ7caLbabKnZIrM+/lYwBxiNiYNjZgfN3g7p6nAtbO16A6D/CzAAcW8PUvZEksUAAAAASUVORK5CYII=',
        txt: '工作动态'
      }, {
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAACWCAYAAACrUNY4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjdBNEFEREUyNTNGRjExRTg5Q0VFQjJERDlFMjBEQTY1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjdBNEFEREUzNTNGRjExRTg5Q0VFQjJERDlFMjBEQTY1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6N0E0QURERTA1M0ZGMTFFODlDRUVCMkREOUUyMERBNjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6N0E0QURERTE1M0ZGMTFFODlDRUVCMkREOUUyMERBNjUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz58vD6QAAAIvklEQVR42uydT2wcVx3Hf2/W6/9OGidpaAJtKjVt3B5oSqC9mcCFU0EIBIJSAQeqSq3UVuqFAgdQOCAVkCqE6KVSRdsLCAQHbpQjRYQKgWiaVErbpG3+2aljb9Ze2/P4/d7Metfr2c3uTrqJ15+P9E3s9cyu9dv5+M2+efOeKx2dlmvAzZojmgc0BzW3a3ZrxjRDAnD9WdKUNBc0pzTHNX/XvKo5n/fJB3Lsu1PzsOYbmk9pHO8V3MAMpZnU3KX5Qvq41xzTvKx5UTPTzZNHXexzi+ZZzTuan2sOIxFsYlx6DNux/K7mOc3ej1Ika72e1pzQPJWetgH0E6OaxzRvpsd68VqLdEDzD83PNOPUG/qc8fRYt2P+7msl0hc1/9Qcor6wxbhX85rmS3lF+p7m95pt1BS2cOv0u9SFrkR6VPMbTYFawhankLrwaKciPShJ7wUA1HgudaMtkaY0L9ESAWS2TC+ljrQUybr7XhF65gBafWZ6RRq6xhtFekLzSWoF0BJz5MlmItnV3B9RI4C2+KHUjYCoF+n7nNIBdHSK90yjSLs036U2AB3xndSdNZEe0oxQF4COGEndWScSAHTON6si2U1591EPgK6we/H2mkifE+4nAugWc+eIiXQ/tQDIxWET6SB1AMjFXSbSHdQBIBcHTKQ91AEgF3tMpAnqAJCLiYgaAOQHkQAQCQCRABAJABAJAJEAEAkAkQAAkQAQCQCRABAJABAJAJEAEAkAkQAAkQAQCQCRABAJADpngBL085/JAXGjN0lh6rMycM/nxe28VVxxKHNTv7wk8dmTUvnLs+JnT1M7RII1nBMpFMQNDOo7PSQuKuhj2Schto0bHNZdnHgqh0jQ2CqpPKkk0kIkKejjxeHmPwc+I4Gdu1ECRAJAJABEAgBEAugN9Nr1HOuSHgg9aKE7ulnfgI9F4lWR1RX7ht4CRIJ1qERufKe4sR3ihsdr3c0+FcWu/djX8Yr4hRnx8xfFV8q1nwMigXpSHJZo390S7blD3Lab1ZsMkezb5UWJz56Q+PR/xM+eEVlZoniIBGuoSIW9UxLtv0+iyU+sibNRpKVwIdVfPi9u7qx4REIkqCPSFmh4QtzgaPJZqSpSY8tln6GGxkQGRxhtgEiQ2dlg8pgcLQXxybZ1rRTcwH8fKUGv8UlvnPXKtdrGTvVsG+/paEAkAEQCAEQCQCSATQW9dpvmnRoM3eFuYnfoEs8cXuS9+NVlkfKc+NKH4aIuIBLUESTasU8Kt94rbnwyiCVxY8+fD/L4C29L/MHxZEQEIBLUiTQ2KYV990hhalpF2tX8GtRyWeLJj4uvlEQQCZGggUJRZGhU3Mh2caPbW4g0FGYOcsURakZnA+RsvygBIgEgEgAiAQAiASASwGaG7u9c1RtKJqVvMYlJuBVidSUZZRDH1AyRYENzvmNvmHtBRraJi7JL6RcXxM+fl/jcW+FrQCRowO3aL4WD0+KGJ5q2Sr58WeKZd8LYN1mpUDREgg0t0k23SHTbIXEt5lUwkWzCk/jUsdAyAZ0N0C12q3iYd4ERB4gE2YJcbT6F+llSmcQEkSDTkqtMYpJuE7YTJjFBJABAJABEAkAkAEQCAEQCQCQARAJAJGgf18ZohYyhQe2McHCulo5eT7KHIzGo4iOFQau5PHKt70VaEyKqk6KdtZFc6p8TJ269VFfdV2qvV/3afkdX4P1CpBsTH6+I2A17Nudc1EQi28Zun/CrtfWObFnL1Urr4UWryX5hCuLqCuf2PBZ7zmajzW27cBPhSvJAup8tnemq4/6yhipVn99z8yEi9ZqlkviF2eSgNpnqD9C0BQk39i1eFl9ZTA5Wjbe5ua/MiYxkTPQYVjWPg2y+PK+vcSV5XhXPLy0kt2XYbRuNr5doFLb3pUv6etX9lsN+cuVD8SPb9PUzpjq217SpjvV3CiICIvW0RZqfCSuPO5XFDWQc2Oliyn7mdCKdtjLOqRCz70k8tkMiE2aDSFEQIgg4e0YP7kthvyDI3HmJL5wSVyknt7hnHPQmTXzx7eRGwqqQ8xfDY5GdJtq6tY376WuG/fS5WT0dkXpOPPOuyBt/q00hvKFFisIc3NZq+dJsMneDbhOfPR6+j+unHl5b1TwVyU7HbDXzhZnkNG0xEcQkcMPjqRBxQ0tmq1FUgjh+7lzyvYoRBNEWMR77b7IY9IYWKUpWTVdpQwsInX9cLh2dZmw/QE7o/gZAJABEAkAkAKhBr123hTv0oP5TbDox5GbE23Uou5jrvay8/mfeZETqQeE+/eVkVbzisKzNErSpcWH0hV8shS52REKk3pwTb/9YOoatn86OfVj0mdmOEKl3h1ylHEYuuEIflTBc0F1GJETq4TFXngtTFft+WvQ4HVIUBtgCIvWCxee/TRGgdqpPCQAQCQCRABAJABAJAJEAEAkAkQAAkQAQCQCRABAJABAJAJEAEAkAkQAAkQAQCQCRABAJABAJ4DpQMZHmqQNALuZNpHPUASAXl0ykt6gDQC5OmkjHqQNALt40kV6jDgC5OGYivSr9sS4JwPXA3PlrtbPhX9QDoCvMnfer15F+Sz0AuuJl+6depDI1AegIc+bFepEual6gLgAd8ULqzrohQkc1C9QGoC0WUmekUaT3NT+mPgBt8ZPUmQ0iGb/UvE6NAFpijvyi/oFGkZY1X9NcplYAmdgg76+nrjQVyTip+ZaGFXkB1mNOPKQ50fiDZvcj/UnzOHUDWMfjqRvSrkjGrzWP0DIBBAceSZ2QTkUyntd8hc9MsIWxbu6vpi5ItyIZf9QcFnrzYOvxb839mj9cbcN252ywDogHNM8IQ4mg/7Fj/Aeaz2j+184OnUx+UtH8VHNAkj70EvWGPuOK5leaOyUZtVBpd8duZhF6T/OUZn/6/zHhfibYvPj0GLZj+TbNY5oznT6JKx2dvha/zB7NkfR8ckpzu2a3ZlxT5L2CG4DltOPgguaU5g1J7g63G1tzTwD0fwEGADUlk9mowskbAAAAAElFTkSuQmCC',
        txt: '杭银研究'
      }],
      listsInfo: [{
        title: "关于零售信用类贷款产品定价调整的通知",
        time: "2018.5.8"
      }, {
        title: "关于明确总行资产保全机构联系人及一些其他的东西的东西的的",
        time: "2018.5.8"
      }, {
        title: "关于开展异地授信业务专项检查的通知",
        time: "2018.5.8"
      }, {
        title: "关于零售信用类贷款产品定价调整的通知",
        time: "2018.5.8"
      }, {
        title: "关于明确总行资产保全机构联系人及一些其他的东西的东西的的",
        time: "2018.5.8"
      }, {
        title: "关于开展异地授信业务专项检查的通知",
        time: "2018.5.8"
      }, {
        title: "关于零售信用类贷款产品定价调整的通知",
        time: "2018.5.8"
      }, {
        title: "关于开展异地授信业务专项检查的通知",
        time: "2018.5.8"
      }, {
        title: "关于零售信用类贷款产品定价调整的通知",
        time: "2018.5.8"
      }, {
        title: "关于明确总行资产保全机构联系人及一些其他的东西的东西的的",
        time: "2018.5.8"
      }, {
        title: "关于开展异地授信业务专项检查的通知",
        time: "2018.5.8"
      }],
      lists1: [],
      lists2: [],
      lists3: [],
      lists4: [], // 菜单栏内容集合
      loadinging: false, // 显示上滑加载效果
      currentRotate: '-90', // 向下箭头旋转角度
      todolists: [{
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }, {
        topTxt: "任旭东「108097669」",
        bottomTxt: "贷款合同「182P466201700012」",
        time: "于2018.5.4到期"
      }],
      todolist: [],
      loadingingMore: false, // 显示上滑加载效果
      nears: [{
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFGNTZENEY2NTQwMDExRTg5MkFEODJDQTU4QjY1RDIwIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFGNTZENEY3NTQwMDExRTg5MkFEODJDQTU4QjY1RDIwIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QUY1NkQ0RjQ1NDAwMTFFODkyQUQ4MkNBNThCNjVEMjAiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QUY1NkQ0RjU1NDAwMTFFODkyQUQ4MkNBNThCNjVEMjAiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4BOP1rAAALAklEQVR42uxdWWxdVxXd9973PA+J4yFxBmdyJkrTpmmTqoKSSkh8MUhFoFKQ4IOqUitBJX4o8AEKH0gMUoUQ/FSqaPsDYpDgB9GoRQwpSWhEkzijMzpxbCdOPPu9ew9r73Nf4jgk8fCme95e0kqiF79nZ6+799ln731OvLG9T1Me0A7uAXeDW8B1YBtYD1aT4n6YAsfAAbAX7AH/Be4Dry72w1OLeO8y8Cvgc+BjoKdaLQjVMVvAzeCn4tcNeBB8C3wDHFrIh/sLeM8K8MfgOfAn4E4VtyDwYtuyjc+Dr4GdhRSYvf1b4AnwlTj8KoqDOvAl8HisQTrfAneD74M/AhvU3iVDQ6wBa7EtXwJ/BjwAPqr2LRs8Au4HP7tYgb8O/hZsUpuWpTf/JtZoQQK/CP4SDNSWZYsg1ujF+Qr86ThrUyQDr8WazUngreCb6rmJ8+Q3Y+3uKzCn329rppzYNfnt2Vuo2QJ/A9yutkosWLtv3ktgrpJ8T22UeHyXZlS8Zgr8bQ3NzoTqV2cL3Ap+TW3jDL4aa3pL4OfBWrWLM6iNNb1DYIVb+FJOYG7W71B7OAfu0XeywM+Q9nNdBGu6hwXepbZwFjtZ4C1qB2exmQXeqHZwFt0scIfawVl0sMCNagdn0eirDdyGCqwCK1RghQqsUIEVKrBCBVaowCqwQgVWqMCKMkCqVN842PoJCtY9TsHGXeTVLcGjtoAfJTtF0Y1+CnvepfDYPoquXZLXbj++KfIallGw5ePkNbaS5xf4NA5fumAiMmPX8LNcoOjih2QmRuS1ihM4tRlGb1lFVFVHsPwC4w8ErG0mf+VHiMIM0fH3RHCanogNHong5uZV8pesIL9jo32QvAJNKBlRmAy+v9e2Vh7c6NIRigbOVqYHC7xFrBLwSK+umYJVD5FX3wLPuS5GFk+OQutNMLaBNxmI69UvJa+pHf/qKir0GJqZHie/eQVls9MQ+Fzs3pW0BrMXeXn69kEKxuyg1GOfo2DDbhGdcuEYns2Chz3vUebQH8SbKcwW/p+XriFveTd5zcvl56s4D86rB/GDkq4mv2UlmQ27KIDnhif+TmZkwIZuDtPDfRRFGcpkMwjrTbcfgAKDQ3RFrsH5f14gcqqagpXbyKtpIDM6BONG1mMRts3UGJmBsZKuh7pNyssjW0Xe0pWUfvxZhOsnyKuuL1xSpduk0niyV1VL1Nol67ENk0fFg22WWyRwkodEyyDJkmVCBc6zyPBcn7NrJFxhVT2Z8eGiCmyw3hOyes4DzNgwmckRFTjvOkNkr30DtkYd1qOKCd6icQZ/7gMKz4NH31GB859hBHavjC1LafYJRoT26peowI7GEPKR8FFT6c4WqMCFBhc5/EAFdtmLS7lNqxCBjc2gc4kWF0V8n+6qpnHFib/Oy4mS/P1zqiLEjSIp/kuXiQXEPln2yhw6Z9bDM1NkMpO2WMINiSCtApcluE88fJnMjX4pVcoeeGoc25ZpEVjEY5Frm6S75DW2YUvVQFH/KYoGz5Lfsoq8jm7yl61WgcvKU8MsmclRETW6+F+K+noounrK9oizmdtrIXtxYIcB/I4N5Letl4GAsPegNOmDNdsp4D61ClxOXpuRqlF46p8UnvwHPPgKvHbUlgrZa2sbidLxTVGZCTLs0SODFE3cgKhH7CAAh3Fu2LPHZyacMIsTAkun6PolaRFGFw7bjhFPeyxbQwHInko1DTY089ez6OMQdqBXQrJBOL+FdI2NApkpFbhsEqgbVyg8e4iyH/4FwsH7ahrJb19HftejCLePQOjVtpqVS6hC6+3ZI38lAw++Q2Bk2gaef8dslwpcKn1tnzfsPUDh4T9JD5gTI3/tDkpt3SPTFJIts+fOyJbZQ6PB8/I+M3Th7q0SRwTOplXgEuuLbQ8nRZxMmdFr5De3W3E3fYz81rWSKf/f9w1B3DPvI6z32e3TXQ8NXpO1O8rfWJEKPF91YXx4onjhwBk7l9W5TUZx/dUfvf9bEcY5LHtV3IRoiQscUTyoZ27Pi8mfTaILHskVmLdEPH/cd0yyYa+pTeafZTT2AeA+cRqJF++ViddgDsl4WHKh2YO4sgd2oJqVWIHZCyNkzgbZsBQtOFteutJuhx4AmbpE0hU0ttm1lhMqrmLx7zxxCWG52JH08Jx4gc21iyKOFCyw5vKw3ZxOSPAWqhpfxwMB5DYS+4hKWOVBd6yb7LVecwfW4SpSOLMGZ2zliZOjVI3UkvMTUuOS580BRInr9kxTrQ3p6sHFBBc4Zma9+Wqq8+dOjkjyJpWx/tN2nVcPLjK4WSDVKc9WpqQVuPgTBJx0RZePU3h6P4UQOeDEq6qOguYO9eBiQiYm65dKwmQmbiKk9sP78nDmKDst1a3oxmVpVsgIrp/c/wQuuQLXNNpx2FRaQqgZuhg39BfhxfzezARFyM55lpkb/nyojWobVeCio6EFW6MuhOlaW/AY7JWCh5meWJz3IjOP+k8SjQ7ZE4Kt67ANa1WBi+7BbHyI7LWvlyzXjAzJumkGz9LCzuIaPCTnKDzzb3lQpL3YbgcBbElTBS6ywr60BYPVD8uxUd4ySWLE1yZAbHseyMxJWEnSRq/JFAgPDPBn+Us75bPtWePk5qKJ7iZxohVsfNJOZgzYxn14/G8SalMPfVJmrR64fYrsnRrcG464w8TVsVzjAp8tpxO10FHCrRKvxV3bKZi8Ce9DiB6+EnvhhDQepEbNYZaFyk1JxtsqORgGQXnYjs8PSeO/qpaC9bvIX/OwvK+Up/NVYK4kc6a7fDN5QZVtQFw5JaM43Ov1l2+S1qHPB9DwIMhMFu+bsdeVxOzqGdtPxr6XX5NJEDwUwbZnZBhPx2bLZsvUQAQR008+J9UnHrpjb5QbbiAipeM5Zz8WDPtlmcviIkY8K8217KD7KQo2PSW3/3g8VekA3JiqlPPAdQjHXUTdRrpL4sXDfbfO50ZIom6dbJATh9VyzZFsg/iKJZ7hWrHFToLg71xoFbojcC6rxvrpd26FWOsp4mrU1dNCXmelnixzVkbu8uCtlYfsm0OyEGs1v+4a3DzZwOsyT1I2tVOwdoedmY7vzRKBvcCWH1NpK7Yjx1QqR+D4xh1vhkdW6jUsehmpClwg5MKlwk2BZWJiatx9C3NRha9wqjSBubggt9DJ1sVFT7YHzrmgYhsgFSZw9vCfpbjPzXquB7unb3ys5twHlD3wuwoM0fxky+1zjnpw7soInv4cHSzdhmJs79Oa6WiSpVCBFSqwQgVWqMAKFVihAqvAChVYoQIrSotpFnhE7eAsRljgfrWDs7jOAp9SOziLkyxwj9rBWRxngferHZzFQRZ4H+n0m4tgTd/JJVmH1B7OgTXty+2Df632cA5v8S8zBZ5QmzgD1vKNmQLzVNjrahdn8Hqs6R2lyr3gqNom8RiNtaTZAveB31f7JB4/iLW8S2DGz8D/qI0SC9bupzNfmC0w3z30BfCm2ipx4KbRF2MN7ykw4yT4ZTBUmyUGrNXz4InZf3GvfvAfwZfVbonBy7FmNFeBGb8AX1BPLnvPfSHWiuYrMONX4LO6JpftdujzsUa0UIEZvwd3anZdVjgM7gIfeC51rjNZnHjtBl8lLWmWEmz774BPgEfn8ob5DN1Ngz8Eu+O91pjau2jguy5+Dm4iW6WanusbFzJVeQl8BVwb/36QtJ9cCJjYtmzjLvAl8OJ8P4QPgOfjh+H/sWJPvC5sBdeBbWADmFat7otMnDANgL3gMbJTNjyIseiByP8JMAAN00IZvBeKuAAAAABJRU5ErkJggg==",
        text: "公文流转",
        newsCount: '999'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU3QThBRjM4NTNGRjExRThCRkFBRkQzM0JBM0IyMjdDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU3QThBRjM5NTNGRjExRThCRkFBRkQzM0JBM0IyMjdDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTdBOEFGMzY1M0ZGMTFFOEJGQUFGRDMzQkEzQjIyN0MiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTdBOEFGMzc1M0ZGMTFFOEJGQUFGRDMzQkEzQjIyN0MiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4Vmpc6AAAL+0lEQVR42uxdW2xcVxXd5z5mPH7HseM4LWkT8g5V0zSQAAWUIvjkIYFAtPBJVIlKgOCH8gUCJH5AqhCCDyKVpP0BgfilUPUDtYkIqJQ2L9LQtHUS20kcP8b2zH2w1znnJsM0Nn7Mndx72UvdtuvYM+O9zn6cvfc549GxMWoBNrAcZjnEsotlC8sQSxdLmQRLYYFllmWc5SLLGZaXWV5gWTM5ag0Er2f5CsuXWB7WjyVoJWKWUyzPsjzDcq1dBI+wfIvliLVQQfqosvyK5Ucsoyv5RWcFP+uxfJvlHMs3hdy2opPlayxnLQd+qwneznKS5ccs3aLvu4ZuywG42NMqgj/N8leWh0S/mcE+lhMsn1krwV9l+S1Lr+g0k9b8G8vRqgh+guUXLK7oMrNwLUdPrJTgT7E8LfrLDZ62nC2L4N0sx8Vyc2fJxy13SxKM9Ps5yZRzG5Ofa95CNRP8dZYHRVe5Bbj7RuM3GitZm+xGWqw335hh2Um24tVowd8Rcgvjqp9qtuBBlkssFdFPITDHspllIrHgx4XcQqFiOaVGggXFwmMJwWjW7xd9FA7o0W8CwY+SNOuLCHB6GAQfFF0UFgdA8C7RQ2GxEwRvEz0UFttB8LDoobAYBsE9oofCoscRHRQbQrAQLBCCBUKwQAgWpACvaH8QCrAlR9Fgh6L7u1za1uPSe7td2tDhUBf/tVFMNFWPaXwhojdnI7o4E9K/Z0Mam49pIYz1iS8hOIuuiJntcBWNMJHv6XJoM5N7b6dD91Qc2sTSV3Ko4hIxhzTHH0ZqDg13RPxzDm2rujQ6F9FllqvzEV1j8uuREJwZ+BxoOpncYSZy/zqPDqz3aWu3o622v6So21OafJ9XQRTHFDB582zKs0Gsrfn6QqwJfv1mQK9OhvrzzbqxaFh8LATfXawvsxWyGz6w3qODgz7tY5KHyoZU/o+tW2kLT5x4bEkLY0MgrLXKZO7oddmtBzTCC+U0kwz3jQUQxELwXYm1sNwBJvfAgEePbPD1561M9DBbLsh11BK/3PAF+OuJlX68Pl/RPezaQfJL43W26IBmgvxacm4JhmX2MCN7+lz62LBPnxwp0XZOqCquWtViwa8N8WIZKJn43cVuvcasIi7XoogW+Os4FoLbhk4m4D5OkD57b5k+OOTrjLnkrH0wBQ+BmI3FcrPu0/mpUBN9ZT6+5dqF4JSz5TJ/gCs+xPF2P7vl+7scbXFuCwaPEtePBG0ve4ePsOvnpJom63WqMsNhzhjOXaFD9zc5Tu62ygfR2AK5LZwqU/Y57usy7h9EIzbjORQJwem6HLbeAd767Onz6P2cNWMb5KagdTwkvMJefp69/R7vqV2duCklBKdcpTLuE0UMFDUQd9PSuUnklH4uxGRsx8qOEoLTs16TXKEyNcjKxtdp6xsPv4GfC3tkLKyKKxacGrosuVu1NbXPkgb4uVDPxt642xcLTg3Y48JNguTeNiq6z7ppVMcqrhCcGhBvQSwKErDmdgFWa+rapkImBKcVg9ExsnHYb2M6i4WFBcWJNHmSRaeY8LByXVa2p4jamcwmpUxXKXKE4HRxN8uFeWw25Ipg09qLdW24nS0801ok3UuOpFSZHmq2ST9Zi2m+jUVhNP7RMqyGlLvecK4InmcFX2dyMVYz00ZNY1GN83NO1qK2Lqz/O4IxSzXGir4wHeq5qXZhfCGmM1MhXZqNaLouBKfqKm+wBb8xE9GVOWNNacdEGCya/v+cDOhS1YzwCMEpAV4ZrvmduVAPycFdIy6nofLYPh+8xttMLCwYRFfFRaedSXPCw1aEmWZMP+qhuBS8NbZj8BggFa4ZC2ohpNxl0bmb6AitFWPqcWOHGY5DlclvcQUCawaL5+UJM3iHJCvI4VCWlz8LNgSfZZdZduu0b8CjXt/RBGPUZq00x8lzMLlvVSN68Wqd/sEEY2sW5HAYPncE69gYkT568hor/vnLNQqZkf0DPm2sOHogYK0eApZ7diqgv4zX6SW2YISDtGK9ELwIyRhWn+Dty4tjdTOI5yrqL/lrnqxEpQxJ1YmJgP50pa5j8HyOzyzl+nQhMtxXbgR06nqgXXYrihBYOKPsmvG4J9l6se+NcnyyoRDHR+GyW+VCI7s1mg5MeTLM+XHDXBOM5jumHnEWaUeP25KBOCRqyMwf6PfoYY7rGLpzcnzRYy5jcDKcPlhW9KEhT89HP8AkV1rw1yCG45TEh4dwkgGVs0hn03NhPl21l0dyQcJwRWkr+8RISZ9uwMxUK46uYHITc18P8oKBh0AGra7V6eKMyaSl0JF2TFEY2SF9ch9HV+Cah1o4r5x4hz7+sLmT2JJ9vW2amK/rOnjetsJOPgk2Z5MeYisb1rPKrQ+SeEgM+ME7YDF15zQW545g6NhT5tT+upLS+980FA+CEdMxYA+iTZVMCcHtKXLE+ooFFDpiSm8AL2R/PKYb/THVQjy31KJTB5IctOze4uTn9FSgj5SwDbf05F9Sj77OGTTq0LiFpypZdPsInra1YlguXBBO5CduVBO0SiKUXSRJx+pt22x4VTcbolwWPTJHsLKKbo4fzbpFs/+1mwFVmQgcIe20N+koWn3P1rEEIwRgwO76QkSXmGR8xmM2hwLV8NpiokzeAJA9gpU5PeDbAfeE7MQqY7ptZbj+aJy3L0bZSidFCcGrUXTi5jHulYzJ4oYez2buiy1EPB+mP2qhKW3GQvBSGbLZg64rK+rxzIFrTVj87jgJpWLiEZMX7EGbL89Z1eJKiHYdU1DB/Vv47NwhxrvWxSABm6xHvE82rydLcwGZITi5+mhLt7nlBtMa+vwv0X8VF1A+VErpz8mtdbA4uFX8f6sSLWzFSq45surdgVy8hiSEoMJ1zV6m9uas6WphhksIbgCyYRzT/OgGn3b2erea96op/sYNBpqcdIAlG5fa2n2wZ634TtswRbdfG54XNWskZWhdIkETgptweNjXxzM/vtHX538rDQWMpVxe3JjoxLTmOzSSx1DL8PaN+UEYu7SrN9aeCG766IV5IbgRaM2hOoXLQ/tbfGtOuwCXDk+0EGbnNWWGYMReNAzace9GaspUKJ86+g5MIbgJcGuhSzobRuzNowUnBZJaJAS/Cxh9BbG+yu87Zbr2GsRQtkl3iF+O6cNCOcnFn2ELkqa0Ece3K2B4rWZsVwnBzXhjJtQxbLJslj/2ttjjZv0wAUj1ldlOYYFmbSAgMwT/8XJd7zvRNEgmG2eD7HdwlCUXF6QhScxaQyIzBD9/pXbrfReAgJnFjHJM2b4bA6/WsfVzXNISZszlZMpFJwpDeRA3uWOQDorLapvOta8Nh9HR3ZqqR7qyJs2GRbZJSRYKYt/X7+mZ57KbXYIduzXCYMC5KbyZR6gXapauecgMwdAJkixUsXYxsY8Mmcu+Yc1ZjcNJpwsDCCc76noPj1sAgkAs+M4WYS0YA+14YwxIpgm2C3PWx4F0vDeT0nE4S1lDpgiObH8X70KGgfP+UqSz0yDDLhrbIkx8XGDXfHUu0smhJFmLumnzRlXvsJs7O6V424ECvtKJSzaTLJM141onxN9R/Q4tWfMyx8aK9nZ9giYvIxCCBUKwQAgWCMECIViwAtRA8LToobCYBsFXRQ+FxQ0Q/C/RQ2FxHgSfET0UFmdB8AnRQ2FxCgS/QERSjy4ewOmfkyTrb6KPwgGcjib74GOij8LhWXxoJHhOdFIYgMtnGgmeYDkqeikMjlpOdcM/+eYmpNUs3aKfXGOGZSfib6MFk/3G90Q/ucf3E3KbCQZ+yvJ30VFuAe5+0viNZoJxJ9EXWKZEV7kDmkZftBwuSjBwnuXLLKHoLDcAV4+znGv+h8X6wX9geVL0lhs8aTmj5RIM/JzliFhy5i33iOWKVkow8EuWz0lMzux26POWI1otwcDvWQ5Idp0pvMJykOV3/+sHlzuThcTrEMtTJCXNuwno/rssH2B5fTm/sJKhuxrLD1m2273WrOi7baiy/IxlB8sPLBfLQmOpcqUYtNupx1j2U35vP8oq0M9Fy+84y6/J1pZXirUQ3IhhlsM2Luxm2cIyRKau7QtXS6JuE6Zxlossp8lM2WAQY80Dkf8RYAAJI43DzehLPQAAAABJRU5ErkJggg==",
        text: "用印审批",
        newsCount: '19'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjE0MDREQ0M4NTQwMDExRThCRDg4RjY2RkNFRjg4QTgxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjE0MDREQ0M5NTQwMDExRThCRDg4RjY2RkNFRjg4QTgxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MTQwNERDQzY1NDAwMTFFOEJEODhGNjZGQ0VGODhBODEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MTQwNERDQzc1NDAwMTFFOEJEODhGNjZGQ0VGODhBODEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4DNJ3nAAAOeElEQVR42uydWWxcZxXHz8x4PJ7xvjtOnK1JnTZdSFuaUCqqVCCxCIpQUREUJHigqtRKUIkXCjyA4AEJqFQhBC+VKtq+gApIpaytKCpdaFoSSNosTeI4i/fxOl7G4+H8vrnjOq7tOHNnfO/MfEc6haT29J7vf89+vjOBG/72mOSB2pQPKh9Q3qO8Q7lVuVo5IpbWolnlKeVB5TPK7yi/qvyi8oDbD69w8bvNyl9R/qLyrcoBi1VOFHG4Sblb+ePO36eVDyk/rfyk8nAuHx7M4Xc2Kf9EuUf5p8q3WXALQgHnbDnjc8qPK3cWEmC0/VvKJ5QfccyvpY2hmPJDyscdDML5Bni38uvKP1auseftGdU4GIDF9fkC+B7lN5T32fP1DX1A+TXlz7oF+OvKv1Wus2fqS23+jYNRTgA/qPxL5ZA9S99SyMHowasF+DNO1GapOOhxB7N1AXyd8lNWc4tOk59ysFsTYMLvZ2ykXLQ++ZnlKdRygL+hfLM9q6IlsPvmagBTJfmePaOip+/KkorXUoC/bU1zyZjqR5cD3KL8NXs2JUNfdTBdBPh+5ag9l5KhqIPpZQBbKi36UhZgmvW32PMoOaJH3wnAd4vt55YigelBAN5vz6Jk6TYA3mPPoWSpG4B32XMoWdoNwO32HEqW2gG41p5DyVJt0J5BaZMF2AJsyQJsyQJsyQJsqQBUURJvaSAgsVCl7Iw1yq6aZtlZ3SRbovXSHqmRlsqY1FRUSlUoLBWBzPs8n16QmVRSJufnZGguIf2zk3J+ekxOT43IqclhOZ2ISyI1JwvptAXYK4oEK6Q+XCXbYw2yWcHsUDDblJsjMWkKR6WuokqqFdiYAlsZDKmpCiwxWwH9uwoFPqAvR1Cq9eXgRdiqn3VjXYcMKOB9yhcU9LOJURlLzsjswrwFeCM0NayA1FREFMxq2aYa+8HGLXJ9bZvsqG6UaDBsfmY9n1MZCBng0W6JvHePDq2dXkjKmam4HJsYkH/Hz0uPavTA7JRq/KwkVfuLSbOLCuAq1bpNVbVyV8tO2dewSa6pbpYa1b5oVksDgby8RLwou/SzN1fVyYeatsq7U8Py1ugl+cfQabk0M6HmO2kBzrc57lIzfH1dm9xQ1240Fh/bpGY1UCBLURWqMNwgUWPmG9Xsd1TVyP/G++XY+ID0qvkuBrPta4ABDz/aoVp7oLFLPtKyQ25p2GzMdD60db3P0KwvEgDv0Rdru7qFOnUREhfpU22e0kAtbQHO7WDDanb31XfK3a3XyK0KbFtV9YaC+z7/r+HZTfUdGtDVGvfwwuC78lq8V5ILKd+C7EuAQwpig0bIBFB3qA9Ea/G9+Fmvgzwi7s3RjL8P8We1MARioxpppzQAswCvQ3MBt7umRT7V0S17a9ulNeKvbRG8aNs0pTK+uTIq4wru8ckhGdGcOm0BvrJZRnMBl5yUXNevBLg8o3SJPNd33Jhsv5lrXwGMucPnYpbRXMDNVp986d/02XhGnjU+N20CrrfGLpoKmQV4hVSIaJmACp/rN7O8Fsg8K8+cSqfl0uyE9Cb8k0L5BmDyXFKhW52AqtiIZ+bZKYpgo09NDVuAF4sKqr0UMe7SPJdUyOtoOdfAi2dHhvH5Wbk4My4zqsVelzU9d3Dktbz9VKj2qQZQJixW4tmRAVmQKeyD+MHzJ6BxQG2Z8qNXRYy8FkNUBmRBJmQra4AJrGjx0TigtuwGXNMFSiVNwYGIdr1FB36PgGjQdIvm8gIysiATsiFj2fpgUgwKBpT9aBy4Idp4F9TvDSlQ8woahRI+fy1/DrijyWmn9zsum6N1sjPW5LozhSzIhGzDcwnTXy5LgGnWU9Sg5efWMNOU/1P/CfnP2CWZW0jJ5zr3aurSKVujDauCm0yn5OWRc/LHvnfMy3Fn03b5ZEe3aRXSSXJTsEEmZBspV4DRECYx9qq/op/rlkJOnXg2Na8pyoj8pf+kpBVEAh8qTksLJsS1ExrpUkN+ebhHjk4MSGI+aaJeKmn5CAOQCdloLwZHL3gWTXvmg5mhYsxme3VjXtIiNG5Pbasxi+DzerxXtbNH3lbwAHPpAVNxOpuIy58HTsqbevj8uSUSM/1e2oKhPES/yIRsyIisZafBDMgRhKx3zGY9KQp1YQIsAq03Ry/KofgFDbzmzVjObvXJtU5Ue3SiX/46cEoOqznHhDYoqJ/uuE4ONG1V/5kfgLOTIciIrEfG+8oLYKYfGZDLV1rE5wDkXs1BiYopNvQkRuX4xKA8r755Rv0y6Uvv9Ki8OtJrzDPgtkdqTcR7oKnL+Ot81r55JmRE1rIDmNFWph/zTUxGRtVcn9eomGCL0uHfB981U5QQJvl1BRgTTbtvb12bHGy9Rq5douH5JGRE1rIz0eSKjLbmmzL95Kjc05nZy0naNDY/Iy8qyEfUJJMS0b/FfN6kJv3Dzdvldo12qwvkJ5ERWcsOYIbSaQ8WgoiEN6npvb1pi0yl5owGj6hvjmvOy0QkKQwB0EfbdsnN9R0F0dwsISOylh3ADJrHXKZHxMVpk88umMpVelmrnQG5O5u3aSQ9KCfnh0w0jYa3RKqNP6b3zHPwErxnATLlxlCeyqbI2OKyiFOUABMQuU2PAHdSwaHMSMC0sAxgUiPKjztVWzHLAEkQhcmkmEHvdjiZuDy9CYRM5NtQWZUXs704XF9uAHNXKOiyfoVGvjR8Vk5ODpn7RekVigloN5Uk/DD/noEarqS8MnIuU9RYfiD6AlBqRPP3a9rkVof5vGAoXH4A5yMdmVJ/SlR8SLlvZnIVM542kxYZE575M0PrF2cmVq2IZa7G1Gjw1SWUtdyAjJkPerhnrqhvF0Y19emuac0UM0IrB0rUmyl+oO2UIunuUMxoCsdWDdA6q2qlK1Zf1K1LzwHmCidvtptDjFXQYO80zXWqVysRF8aoalGyxIxzS2G/aia/t7JlCTi3FhvzIidxwIKHc5aeAcz9XIoPBDW5UsRocIvh1Yh23YxqeJ8GVNwFptYMuPdsum7DXuQ5DwfwPGs2EN1SaSp1QkYvx2g902C0KWju+uaeQmD+OED8KzfyVzKE+N+R5LT5uez0BtUtSpUrB1lBU/jIXhx3SxRWkLXsAMYfmjzTxfwzARSrF4iKSYVW8nW0AnsTo5mWoaSNT2akNTaycuoSVpfBfPYO9cFMeLiO9PW/31+ODX+AcVvhIbB69tJROTzWt+ohLqgPTKgPRnPnVYsHVZv+qbnzG5pareyzMl2p+7bcJPd23qBZkrs0aVzzb2QtO4BZeELnxw1l7zKZYsLi3yzPQ4NSpT9DHpwKLJifMNH7KrCFTeUpYgI4t+BCuAdkLTuA2WZDgx6/mGuqBAh7NA8GrL6Z2hU/hwG8uGptz/SoKYbgFtjn0bVKh6fSXKGpWZwMcZsiDc8mjKzlp8FmscmkWXiS61QHwdAdzdvMvaD5dGpFDSb4OjE5JM/3nTApE8tbuP/0sbbda1qFapflxewyF2Q8vUpAV9IAc/D0Zs9Oxc2IaS5TjLwUrFOoW6PdR4MBYBkCCDjrk+gXdxb4/hNRO7IhYyLlXZrkWR7MG07Rn4nG6SLaWrNeQiZkQ0Yv7yd5erOBJWPMRk2m/L3I5GoJWZAJ2ZDRS/IUYIbVGYxjbmqkQMUAdtzhq2lPUsSgfh0p8O1FZEGmHmdLXtkCTG5KEMKSsfMFMmW0/xodn0tkTAGjMVy4b/FDBmRBJmTz+iK45+1CKktskCM1YQ8VYwD5bNNVqLZSUPlEe7e5StJSWW3ahYUClwEDViAiE7J5TZ4DzIGwHpArHmjXjfUdeZ1wzKY9lB9pFRKthwp0b5e06L9jfUYWZEr6YK2S5wDz1lOQZz0g6Q6D6Oyhyvctf/xuIX0vadHAzJRq7hkji1/2WfpmooOGAesByYkx0dtcljE3mtBYRodejfdmZPEJ+QZgghF2P7JrKmQWfL//VqAfiYY+LUlmw3h2ZPDTklJfzWTRWmP3I8Pi2SVjft6VBbikQVxm+9fIucW9lX4iXwFMksQBUSBgjpkNcn5cZZglNBdwn+k9bFYZ+nEpqe+mKjmgUWf3I+sBOUS/LCNdGlDhczHLaC7PyjP7sRrny7FZerdUg/BpmG3mmrPrhPN1nzjXiN90iDRaPuT4XLtO2KW5ZvcjV0wo/S0uBJeNX7eULWIc0Tz3JU2FiJYJqPwMrq8BzoI8aWaqxswfuNTNsFyhV/ovfwasCeVHKlR2pX+BUigG5VgPeFQPuJBfypHVVvwsLT+6QvZLOTaIuHqCJv3+0jF5ZaQn56/VWY+fXetrdYqJigpgM9ecTsmsmkxM9zAzz/r/MZtX+mKsiiX3ffmczI2DlNFGAjmmHxmQY4bKfjGWT8w2QMDsobrSV9sFl1xXZT6a6yT2q+2KSLMxn2yy8WqbjV/JfvuoBdiSBdiSBdiSBdiSBdjSVdIcAE/YcyhZmgDgfnsOJUtxAD5lz6Fk6SQAv2PPoWTpOAC/Zs+hZOkQAL8oUlKX+yxlCExfyAZZb9rzKDkC04vZPPjX9jxKjp7mH0sBnrZnUjIElk8uBXhI+Ql7LiVDTziYXlaq/KHypD2boqdJB0tZDvBF5e/b8yl6+oGD5fsAhh5TfsueUdES2P1s6V8sB5iB3/uUx+1ZFR3RNPqCg+GqAEMnlb+snLJnVjQEVvcrn1j+L1brB/9B+WF7bkVDDzuYyXoBhn6h/IDVZN9r7gMOVnK1AEO/Ur7X+mTfpkOfdzCSXAGGfqd8m42ufUWHlfcrP3ulH1zvTBaB1wHlR8WWNL0kzv47yrcrH1vPL1zN0B07cX+kvNvJtabseW8Yscjz58rXSqZKte79xLlMVfJlB48ob3f+95DYfnIhKO2cLWe8Tfkh5fNX+yFuLp8NOZoMtysfdPwC3zi1Q7lVmS/ODVus1qSkEzANKp9RflsyUzYMYrgeiPy/AAMA3fZa+5AKvs0AAAAASUVORK5CYII=",
        text: "业务审批",
        newsCount: '66'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjFGRENFMzk2NTQwMDExRTg5N0YxRDE4NzkzMzE3NkRDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjFGRENFMzk3NTQwMDExRTg5N0YxRDE4NzkzMzE3NkRDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MUZEQ0UzOTQ1NDAwMTFFODk3RjFEMTg3OTMzMTc2REMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MUZEQ0UzOTU1NDAwMTFFODk3RjFEMTg3OTMzMTc2REMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4QG52tAAALpElEQVR42uxdCZBU1RW9v38vMywissoOMuCgCEQgRFSWUDBmMYtEiQhVaAolgQqLppKQpRJFKSpGDagxS5kgW6UwBEMSQZZohIAKqOwMQUBmWIZNhoHp5f+fe1//6e7fy3RPTzP//c89VUfKnu6/3POWe9+77z2lZt4IKADaI0chhyFvRvZEtkM2RwaAUR+CyBpkFfIT5H7kVuQm5OnGXtzbiN+2QU5GPoi8HamwVnkhYPIGZF9kmfm5gdyOXIZcjDybz8U9efzmRuSzyKPIXyMHs7hXBYppW7LxMeRCZKerKTDV9ieQB5GzzeaX0TRohpyOPGBq4Cu0wCXI95ALkC3Y3rahhakBadGvUAJ/DfkBchDbVxoMRG5Dfr2xAk9Fvo68jm0qZW1eaWqUl8DTkK8gVbaltFBNjaY1VOB7Ta+N4QwsNDXLSeBS5FKuuY6ryUtN7eoVmNzv5ewpO7ZPXp4cQiULPBM5gG3lWJB2szIJTKMkP2MbOR4/hYQRr0SBf8xNs2ua6rnJArdFPsy2cQ2mmJrGBH4IWcx2cQ2KTU0tAjPchYl1AtNk/efYHq4DzdF3IoFHA8/nuhGk6SgS+PNsC9diMAl8M9vBtehLAvdmO7gWJSRwB7aDa9GBBG7JdnAtWnrYBu4GC8wCM1hgBgvMYIEZVwFe172RrwjUTqWgdCwBT7teoLTqCJ6WbQECzfFt/dHvREIAwRrQq8+A8dlJ0KsOg3GyHLTKfQDhWhZYNijNW4NaOhLUvneD2uVWADXL0h0Sm35zQxfr51oYtOO7QTvwDmj7/g1GzXnn26Zm3gjDsf1LtwHgG3IfqH3uxDcpcG9j6KAdfBfC778O+rGPWOCmFtY/4hHwdL2tSe6nf/oxhN7+oyOFdpTASos24B/zPVD7jbbl/trejRBa/yIYl846RmDH9MFq6Sjw3zMHlCL7Ej+pYBX1Ggqhfz2LffQmFrgwVvWBf+wM8A66V45WBAtY4Bs/h0iPQRBat1A4ZhwH52tM9HaLvv0racS11Ax8Jno2ekYWOB9xm10PgcmLhEMls7MXmLRQPCsL3MCaG5j4HHja9ZTfo2/fSzyrrDVZPoGxzw3cP98R4saMiM8auP+Z7AMsLDCAv2wmxrf9HRdvUkzuH/d9Frhex+XWseAd8GVwKrwDvyLegQVO1+9e1x58EtaAhoLeQWnZlgVOaZrHzZQ+5MjVQfSXzWKBLX5Vz8GgltwBboFaMly8Ewtc16yNfgzcBt+oqSywKO29hoKng/sWV3g69hHvZnuXYftsktcPSl2mhctgUOYI0U7z2m4FNIBhsxHcDE66Y4EZToY888H+YvD2Hwdq72HCQVGKW2HxU0Xim37mCOhHdkBk11owLqY/xkBp1gp8X5yW272CV0C/UInX3A766cMZnSTvkG+m/qG2BkJvLcwYA/vGzkj7t9Df51+7Aqt9hoP/nsdFdmSK0Sj7kdh9EPiGT4LwliUQfvc1iB5pkBiXUAEpa/C9KQ0n+I8FKemyNLKW6XrhrcvBqD6TWii69s/4G7sEtr2JponzwPh5acVN53H77n4Y/F/9IRRqWxFKwwnc92SDsjIzJfs1VRKgY2qwyI4sm5laq45+CPrxXcLDpqk4kRabEEpRU64f3wORnW/Ue/20WZCBZuBpf5NFULXXEJFXTbW5nqAnVqjU7gPTfle1CGyADHvb2CcwGtg/bpa15qCgwVW/AK18c1Jz2QECExaAp233eIs88jsQ2b2u3pUItUvST14orTtD0aQXQGkRnxTw3jKmXoH1qiOxOWpPlzQ11RsAT6fodidGsEasnKBm/pr1omms1tOuh7WfWr8oRVxhsIunIPTGPKtIxdeBelN+GwQZ5yuw9q+xXi95lUOywKcOxQoTPTc5dZb36VyKf4jWF/34bryJHNnI9gnc9y6r0aurUoxuMfDJgxD56J+i2a2j4m+W/yjTpXPWD65czPIDDTTsFmKG69I/Y/9LifLgk+PAN9uaaE9n66kw2sHNYrlIfQiRt1uo+3e5JaUAZQP5BWrP26MFFP0HWtoSFzieHKh9ugu8X3jw2hY4sf+L9nGHC1+IkicxVJ9oWslpI0ctrkgYItv/lpPAiQ5i/H9UUOsKDF5LP7GfvWil2Lq5z9VYyVf0yB+yfykSFHGwfvZY9pi5Ym+0lUHHUBQefzE2K1fEoAgtWxWF4MQB2ycY5IiDk+NOW5wSAx27l0DbsyG3r6OYwtkyn181++HE8Eij/lci2CawUVtt1duWNUeKSK/x3TEx51/oCQJ6ut2WxsHaxQMd0dCnCkWNN9NKLnnQ2AwqavyRs823Xn56pFXOQHMRA1MGidojvoOy787JYh1wLqv7aYG4d8h409EaCGEsJPE0XwP0ij1cg0VJr9htLWnZcrKwSSx+7DUonr0mRm8Dl5HSAAR5y+G1z6cOUrTplttzH0uowTf2xf63RMTkUUfxKBjZwq1rRWDt8PtJAw1d680pprhZadnO0n9qR3bkV7jIodI16/2TBi4yFpKac2BcqIx55b5hE9I23yxw+RYwzldaPvN/aU503Dn5IbGm+MtmJ8XNWzJOHeYkVHJz7M19YEJL6GdpDFtmge0bi8YaFFr3Gwg8MN9i5MD4p0Cv3AcaTRRgSEKLu0TymmXMOgihjS830glI8to9uZ/kJxypujg64bk0yRwsewUmg/xvK4Q3/jYlbdbTqVQwvTA6BFfPA+Pc8cbd/PIFDJRbJDhguXvxiQMecafxtBgz5yY6CeGtKyC0+qmcnBPq/4IrfiC2OWp0A3LhhLWPJ4ctx9WB+pljKc+rS1h7ba/BsRZ3z3rQDv0X1P5jxQoHmpaLpeygIXXapKx8M0Q+fjNzKBO8BOH//Cn3e25bkRLSUNhGhUgUJnTEEq8XG+AwHbzwhpfEJmux1ujIduv1tyy1zGHbNp7k5H2yGA5oohksMMPpfXDdoIEiySR5Y2GEg9JsrySNwDSSVDT1z45fI0xOYe3vp0izG540TTTlGYfffM7xtTe09nmptjqUqg+mcCny4RrHihvZsTpL6i07WVgDXpByTDcbKJc79NYi9qKzWyoMwb/8CPSqTxwjLg2ChFbOlXLfSinDJJq3DS6d5QiR9VPlEFz+eDTZnePgBoh8+QIEF0+XehNu7ehOCC6ZKZ5VVkg90EG1onbZHIi8t1I+h+qDVVhzn5C25koXB2duAyPRJS0Vu8Wkf116jJ1xrtgQfP/bjvAPHLPjO52CUoueqm/Md23aLtCAyK51EN74SmzGyQlw5qEcnfuB764pYtlnU/W14Q0v57S8hQUutNBDxkcXshV6K1/sGiJ7N2Ff+1eRQuRUuGI+mCbqKSOD9vegrR7qlpHkE4NTpqa2/x3QDm3hg7GkBNZkWojtad8b2ROU1l3ElvtK8+vjmZPhK+j9XhY5VMZnp0W8rVfuFZkjsh+y4VonqyG1kPKjZM2R4jiYwQIzWGAGC8wCM1hghuQIkcDVbAfXopoEPsV2cC3Ok8CH2A6uRTkJvJ/t4FocIIG3sR1ci+0kMJ1VzisM3QfSdGOdk7WD7eE6kKaVdXHwEraH67CM/pMo8BW2iWtAWi5OFJhOmHiV7eIavGpqahmqpC3VL7FtHI9LppaQLDDtSvZLto/j8aSpZYrABNrEcSfbyLEg7SyLrJMFpoyzB5AX2VaOA00aTTA1zCgwoRw5CamxzRwD0uohZEpmfqb5YDpxagbbzTGYYWoGuQpMoN0+H+WaLH3NfdTUChoqMOF3yPHcJ0sbDn3L1AjyFZhA580MZu9aKtCqeDr2bVW2L+aak0WO1zDkXOAhTTtBtv8Jcihyby4/aEjSHZ1+8TSyxIy1atjeTYbLyBeRfSA6SpXzwUz5ZFVWIGl//R7mv7SPLs8nFx6GaVuyMR27Oh3Z4F3QaXVhIR6mA3KU2S/QVu10Rg6doEHbqPtYq3oRNh2mKiRtK0SLkSnLhhIxGp0Q+X8BBgBHdrCYoorZqQAAAABJRU5ErkJggg==",
        text: "CRM",
        newsCount: '999'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjMyMjdFM0QwNTQwMDExRThBMEVFQzIzMjQxNzA2OEQ5IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjMyMjdFM0QxNTQwMDExRThBMEVFQzIzMjQxNzA2OEQ5Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzIyN0UzQ0U1NDAwMTFFOEEwRUVDMjMyNDE3MDY4RDkiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzIyN0UzQ0Y1NDAwMTFFOEEwRUVDMjMyNDE3MDY4RDkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4P5h08AAAPx0lEQVR42uxdaWxcVxU+bxbPPl7HazZnc9wktLRpG0RbSAFRChQEVCAoleAHVaVWgkr8oYAQCH4gAZUqhOBPpYq2PwBR+FGQ6EaBkLRN053EabM4iRPv23j2hfOduW86du3YM44z7z3fE53a9TLzcr97vvudc+69MfY8/RBdBmtnP8C+n30Xey97jD3E7iNtl7I0+xz7KPsp9mPsh9ifYx9Z7Yt7VvG7rex3s3+V/Tp2Q2NVk/mUt7D3sd+mvl5kP8L+OPuj7OO1vLirht/pYv8F+xn2X7Lv0+CuiRlqbDHGg+wPs3evJcCI9u+yD7A/oOhX25WxIPt97McVBt7LDfAO9hfZf84e1uNdNwsrDIDFVZcL4M+xv8z+QT2+lrFr2A+zf361AH+L/U/sUT2mlozmPyqMagL4Xvbfsrv1WFrW3Aqje6sF+A6l2rTZwx5WmK0I4H72x3Tk2i6SH1PYXRJgyO8ntFK27Zr8xMIUaiHA32a/Wo+VbQ3YfWcpgFEl+aEeI9vbD6ii4lUJ8Pc0NTuGqh9cCHAb+zf12DjGvqEwLQN8F3tAj4tjLKAwnQewNmfZ10yA0ay/Vo+H4ww9+m4AfCvpfq4TDZgeAMA36rFwrO0DwLv0ODjW+gDwdj0OjrUdALhDj4NjrQMAR/Q4ONYiLj0GzjYNsAZYmwZYmwZYmwZY2xqYZz3+pbv9Edrb2Elf7N5Ds7k0vTI1RE8NH6fJTFID7ASLev0McpTaGoLU7gszjRk0kUnQGzPDdC45rQG2PcAeH/UIwCFqbghQzBeigpzWJIloeKFY1ADb1dyGi7wuNxlGqUsadjfQ/pZN8rnBf/4zcYamsykNsN0s4PbSjnArXd+8ga6KtPP/l/76LgYaUb0n2ln+2ddnLjqCrj3rCVzQ8kfaeumm1s3UF4lRtpCnXLFAHsNVFl9hT4NEcZH/zHAUx/MZW9P1ugF4e6hVgP14bLsIrAyDO5yepRDTc7M3IHRtlOl6owDsBLr2rIfI3RpqEXBv4egFuGOZOToRH6fTiUm6KtpO+5p6yFC7lkDXEaHrUhcVQNtZXXucDi5oF+De3LaF+pmWZ3MZepMB++/EIMX58w5Ok4qL5spRpmtfWXhBWePn80zpGmCL2A6mZUTtJ9q3UxcDDYD+OXaKXpw8S+Oc96LQgUiFql5s1yHo+kOsrvFdN0f2C+OnbUfXHqdGrgkuItek5bdmRuglBtfn8ojYgpJubQguuaXUpOvdiq4hyOxG144D2M+pD2j5FqWWd4VjooQBLgQTIhffu61jp6RGbmP5cjyiP8TquiArMtlKXTsOYKjlDzOwoGVELtZOAHto4iyNpOP0he7dQssA12WsvNcyvxhCdJDXcDvQtWMABi0DXFAyIreSlgFug8sttAxwUaJ0G9U10t4rhnQQAjeVz9HbsyOcasU1wFeqiAFwb259Ty2btDycmqWPxrbSpzr6VOTWfpADEwdr+PnUNE1kkxrgK2E7w20SnR+LbRMASmr5JKvlcyW13FNSyxFUqVYBLtZcsyDiMdyirDVFr3Hkbgu1SNQupGWAa6rlWmnZtEQ+S6NpvO4wJQtZmstl6WV+/YupuAZ4rYsYABeiqi8cE0FVqZY/UqVaXmjQyLlCni4wxb86PURPXTxOUyysUOw4n5qRcqcGeA3VMqL2kwxgj6mWx8+IuoVavrNnr+Sv1arlSgO4mCj/Gj8t4J7n/DctoBYpyyDrNGmtihjhVrqltVcit0fRMhQtyo8+VssHWFDtjrZTrCFYMy1DJSNyXxg/JRPnTGKSUoWc7TpLlgcYAAG0iKJZ7MA40La1TMsoOADcfzMIULQA93LR8lFFy2cSU7IO29EsD3DU66PeYIso5FYGN8hKuI9Vc4s3KLSMqAUtX2RA7ujqp73RzlXT8ijTMiIX4A4mp0RY2dUsDzD6tRBT1zR2UbsvRB6O5iavX5r0aaZMCJ3NgSYBHeB2+sM10zLWV0yUZ0bfoYPjgxK5AFc3/NfQSjmnS2rMTd6AfCw/vMtFbQz6lmAz9QSi1KiAr9WyPGGwnmPNPTY7SnNM/3Y3y298x7qKtRBtPjTo5wkul5c+wFEL0QVw3cbq/jpezpuxu6M/0k4dfmecqnW3333bj6z8gHmmR+ydmlMiJ8gqGoIL0YuqEmrM2CHpMoxV3yRT6vu6Sq/PH7OFgqzzOZs1+S1F0QDGa2ALa6kUCDCLi1SSXpu+UF4LAXKn1IRrv/G4KJSclzUcTUCs9XgWMAF2XSIdw8TBGjyUnNEqumZa5EjZGGyUiEnksrIRLr1IhQjgnoiPcX6aFTBuaNkgxY7a19u8FETOJqdpLpfh19so6ruykIL3Qbnz+bGTdHRqSFN0LdGLUwWf6dwlUbM51CSNAhQUoJDfl8IwVSbzOZrNp4WeQxxl2OZarbBCNJ5LTdOzo+/SIU6x3klMkJ+BDCj6NwVc0OOVYy6IZEyw6VxKJoaO4CrWPOyU2BvtoN2RjnLaY0wYNMDRml5QOcJnULYvTZ6TtdklwshNnb7IPHW9XBFjKDUjB87+NjxAg5wK4TXc2FrHkwYTxqRrRC/SL7AMJgAmBipaWJeLOoJXlgNhoIBhc4OftjIt9nLKA6EzlU1KYX8pgYPdFCj4Q1qhDYhDZCstYjzDkfvk0Ft0jtdWsAXE1AVeGvBeWN/BKgDXtKB6/TZfkBkkK+9rl9y47ioaAxVnysV4YVC75HSBTyIbg4koXkzgZBVd43cRbUGOOlS9lqLrhAIG4B7kPHdgbrxcW8Yfkz2wxoOmASrAJjMP52eLevzlM02IYjs0HOpK0UWpHuXo1NykqiTlKMDr3kZ/I90a2yZtOQxmghX0YoV+TIA3pi9KxHOixIB4WF1H5kVfZW35lanzTMul2nJykUmD3ZIi4hhcNPPDjd2ShonS548opuw3NgmNp3lyHedlBIURK4NsmTwYAz6eTUgzHcJma7CFtoSaBeBJpmNQdvYSdA1FjHUYShhbYRfSMgTVXy68TWcT0zJZiktMOXwPyhrAotgRVTm3aX5VDOkKRMp0DWovaoCXL2hkCiWFnOM1EVGzMdAoYGOQM8W8pE9Q2Yur66zsw0I+DXqNKHUN2oUwOszC7GWO4OVafoAKr5Xl98PPIi/GxDFFnHS3+HPQuJ8pHJ9PZlLy7HkLRrKlatEAEHQNULDGoamAc0XoJJUKIQVpwOeWKIaAghGFLlkzOQJZXacl350TKq2mWDEQH2dmSJdOG/J773THRE1j4mHi4HaAGzl3BlvEZYPfsJRVFyvU6Ah+H9A5msgkaSQTl3pzL4O8KdAkqvqUNN7zS0YhJgYARaQhrwXVHouP8no5KnRajUJAFA+n4rKm4wxTaEHOjQpcmIVdJ38PkT/KkyhlsUi2JMBC18V8uQ6cK6KkWBCgkB+DQpda9d6j67Ssy5gQRzjnPS35a3XdIbxHQtE11n9ELSaNuSYbqswK1Y+vg8bxvmlVAtUUvUwpEcr38ORZob5eFlzvzk2ICLuUpJG0i4FEaXFgdowa3G6aYaqt5VQgXks2uM+MSDmzTbYAGbSBtYFLHVgDZWMpubqxSz7iuY/wUnGSnzVjAbq2fDcJ0TCWTkgkXkjOVtWAZyKXQS6sshuUV8JrkNU1IhXiz6fSp3KkoAvFdL093CoMhMmJ36k3XVsSYBEyPIAoGUJBQ+iAIrE2V9O6kyrZZYihoqJ+RLHJHhBXSJnMHrShnhnPCmWNIgkmJzxZx05U3SgaM750242r3Ic1CwqIDkQKUpRWb1CEEiLiTHJKttTUq+KWUIUVFDlaGgKyLTfWECpHsqHSqP5wjFo4Vwadu/iL+Pl67Q6pG8BYrxAFqPuaZUYAigHD13ASAT8DBYvIxc7Jvw8P1A1g02ZYREHo/eH8GxKZqLjhGefRNU/IGCvr2zv7eHKWovzwxNn1BfBnu/oFSKQfGCCzXRdUxQOPimiInMlsUoCFQq2/wi/t8sCerUaOUpxRur65R/aLVUYyWAcNimuaukRdrzuAcda2naO01ReUyhXWOKQjoDNcKwglDFrD/ZHo9OBIyvnkDFnB8Kxj/Iw4nwR2aWrwywE4lDBNMzcLojvWVlE6XTcAPz96UiIYtIx8FUoZwKKpDuoz23iImKwIlcU3AdTTcHz0dV6TEa23d/TJOamFR1NFZxju9Qcw6sKgY1DzjNxgk5aNdSkpLJSALRaLlm6sY+KhCYL9Yo0ev4hD3GIL9W8aGKied3rUDWDsr6rGDJWKWBFkLB2HJgdlUqLRgWOsqFuDylEixbWIupK1TF7slXzTggeujdJJQ+y8fCF/WgThrkhMthHhTPGrHN1vaoCXjtpNLFKwsR0F/QaX23LPCK0wmJiW0iSaDViTcTC8kdM+LD34HB0wDfBiAHPk7mRwsesSJUB0lszKklUM1S1QMHJ0bOZD3dxK93ZYGmCILOzR2qNODFburLCK4bmQtx+bHaGDE4bltu9YDmBDqlwBqXLhLDA2oCOVMpvtltMHLoPzXz9tDDRJzmte7WAVoC0FMAoDKE9uYzrG4e4Ngajc6wxwDYveaGOoHR5oZ2KHx9GpC1J1Qz5vBZAtAzCiExH76Y4+Gagd4TZqMEpNB+MyHCxbS8ZB7Rlnk5Ee7WvaQP8YOSGnIa1w5YPHKoOEXZTXNnXTTW1b5CLRljqW92p5frQJUXPGhEyptibqz/U+BeGxwuCgRYiLuXG3Bj5WVoLsZKBq3EJwnVwwTrJJIZ3ILXqY7ooxY90HhcFFtG7jqAW4ZipkZ8PtPjhrtYc9toIjNY6OYDT7w+qYCD5PWayhUDsruaibRWLT3Pj6rEVXiiszaku7JckRAKMTFlW7Lev6LHuefsgZ/8SXNmuuwdo0wNo0wNo0wBpgbRpgbXazDACe1ePgWJsFwMN6HBxrkwD4HT0OjrUTAPiYHgfH2nEAfFiPg2PtCAB+joh0Pdp5BkyfNUXWK3o8HGfAdMjMg3+vx8Nx9jj+UwlwUo+JYwxYPloJME6CPaLHxTH2iMJ0Xqnyp+xxPTa2t7jCkhYCjDvrf6zHx/b2E4Xl+wCGPcR+VI+RbQ3Y/aryCwsBxoVOX2af0WNlO0PT6CsKwyUBhp1g/zrhgjdtdjFgdRf7wMJvLNUP/iv7/XrcbGP3K8xopQDDfsN+j45ky0fuPQorqhZg2O/Yv6TXZMumQ3cqjKhWgGFPsu/T6tpS9hr7jex/Xu4HV7onC8JrP/uDpEua9TSM/ffZb2B/eyW/UM2mO1yX+jP2HSrXmtPjfcUM1/T8mn0nlapUK766tpZdlfiXLx5g36I+HiHdT14LK6qxxRhvZr+P/Vy1L7Ka04VjKpLhHewH1LrQz97LHmPH4VivxuqSllWCaZT9FPv/qLTLBhsxVr0h8v8CDACTlD6SMXaLQgAAAABJRU5ErkJggg==",
        text: "法规会签",
        newsCount: '1'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjUzODlFOTY3NTQwMDExRThBODg3Q0QyRkFCNjI1NDE3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjUzODlFOTY4NTQwMDExRThBODg3Q0QyRkFCNjI1NDE3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NTM4OUU5NjU1NDAwMTFFOEE4ODdDRDJGQUI2MjU0MTciIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NTM4OUU5NjY1NDAwMTFFOEE4ODdDRDJGQUI2MjU0MTciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4yzv1vAAALcklEQVR42uxdCXAT5xV+q9VKNtjc2BwJtzlLuByuEK7QFlomNJSUqw2FpqEZyExCJp1OaDuTtGQaIJCLITCTMqXlahJoSDgKqSkQGgyYI1zGmBsTzA22sbXSavverwUfWIdlHavlfTMfMpJ2V3rf/v///vfe/0sqmTMEIoA05DBkf2RnZFtkU2RdpBMYgeBCliCvIs8gc5G7kduQV2p7cnstjm2MfA45CdkHKbFWYcFpsBGyE3Kk8byOzEGuRC5HXg/n5LYwjmmOfAd5DrkAmcniRgWSYVuy8XnkB8gW0RSYWvtryDzkLKP7ZcQGdZAzkScMDZRIC5yB3IOci0xhe8cNKYYGpEXXSAk8BrkP2Yvtaxr0RGYjf1JbgV9AfoasxzY1ZWv+1NAoLIFfRC5BymxL00I2NHqxpgI/bXhtjMTAB4ZmIQncBbmCW27CteQVhnYBBSb3exV7ygk7Jq+qOoWqKvDLyB5sq4QFafeKP4EpSvJHtlHC4w9QIeJVUeDXuWu2TFc9u6rATZDT2DaWwVRD0/sC/xyZzHaxDJINTSsJzLAWJt8TmJL1vdkelgPl6FuQwMOB87lWBGk6jATux7awLDJJ4M5sB8uiEwncge1gWWSQwOlsB8sinQROZTtYFqk2toG1wQKzwAwWmMECM1hgBgvMYIEZLDALzGCBGYkBu1W/mFQvHeQuQ0Bu1RNsae1ASm2KtzMtANBBL7kF3mvnwHvhEGgndoK3MN+yAkslc4boluqS0juAMngayBkDINRCFe93ueDe+TfQ8r9hgU0LWQFl6POg9PsZhFuBpOV9DeqGuaCX3uEx2FR3qbMuJE2cj+KOh9qUl8kdB0HStKUgNXqUBTaPF+EE54R5YGsVmSVVUv1mkDR5oXhkgU0Ax8hXwNaya2R7hNQm4Pzpm6LbZ4HjOey27w/2x0ZW84oOrnVvgGvVq6DfLKj2WL3kJrhW/xZcK2eBfvfWg4Zp1hGUAZNY4DiOvKAMe8Hva3LrXqCdyQE1a0m173Dv+Ctop/dgV9wcpDoNqn2PMmAivlafBY5L623bW8xv/Q7NvUaDrUkbnOfuAG/B0cqt98YF8BzaiAomgTIkwJo7fN3eczQLHBeBuw4P0sBtoAz/jfhTzfqo0kvqjmU4+dVA6T8BpJTGga/TZRgLHBeBWwfftkvu0B/kNr3Be+EwaCf/5wtqFOaDdmwbCttECBw8cNIepKQUFjimwK5TahDato3KiBmiNbu3LcW+2Qvu7R8LJ0x0zXieUMZ6Cbt6FjiW7pUfp6jaL5jWHuzdfwjea2dB3TBPhCPFcz1GReV6pgsTJKTAdkfQ92h5u5A7fU5V8Q3x6Pl2k9F3K6B++fb96ZA9c2zg6zmSWeBYQldLg77HW3gSBd1c/Wvf5QoKrcuKgwqsu0pY4JgKXHwdm6g7YKRJGTgZ7H2fvf9/z/7PxThMgRHH6N/V7Hq3C1ng2CrsFd6wrUUX/130iZ3gOZZVfsitS77nT+0B14qXy8foR7qjw/Ur/9fyuETumAWOMbRT2QEF9t6+DN7zB8sF1jy+x7KiSgl+UQgQ6DrnDuDJPCxwrOE5shWUJ6eAv/QgxZErxpI9hzeD+sVfwN5nDDhGzAz9Ooe3cKAjLr30zQIRsIjuNS6BlrudBY4X1G1LANxl0Tv/v98VIU0WOF6tGL1bddOCkN4rZzwBSc9/HFJ4UnTN+9aKbFOiI+GrKj1HtoBUPz2wJ0wjdVKqYEgOXN4uULd+CFaAJcpm3bv+jt5xMTi+P9Moja3FDXNgPXbN74mpGAtsppacsw68l46B40evidLZmgdProG65X10qnaAlWC5umjKHMmdh4CS+QzYHn0s6NspCeHZv95XABBFh40FDvZBnXVBwpYpxlHNDd4rp0Evuhr4mNQmIm8spbUHG47TQEkDtyqOI2EpEOK9fgGsDFMLLCXXA7nrU9giB4NMrbHK+CpWJGT/E7Tj/7XMmPlQCGx75Hug9HlGCBtK6ar3yilwf7UItLP7I+SZOMHebQTI7TLFTaXlZwtvXSQ4lCSQ22biZ+zmyxOjc0fdu/fqGRY4sFEdYMfWaqexs1nHsKdMbpzehL30BMdvKgSgKZdUt1Hlm6gwHzx7PwNl+PQHCwA8LihbNh1FPuu7AWhYaNgCoLQItFO747oUJu4C0ypAe++nRfViJEpUhTf85dwaBymoVVJ5j61p2zCnV1+IqJfc/QcgOeqUfx5XCaifzgbt3MGHS2DycJW+48R6IGo5EZ827f8c1P8sDuoZU0tzPDUDP8cTUfuu+u3LULpoQlwEjts82Dnuz8KJitoX6z0Gx9DHfXPb/N3VvydzLDiG/trnXUezFQVJSVqyBYulngNj81MRlNP1ZK8Ryf5qvW105GgaBhTOxJtOsH4zkFv18Dl6texhtOPbxFKah0pgqW5DSJ6xRjhXsQKtQdJO7gLvxaM4xToh5sPBHCC5bR9wjn8bx5TwOju6jlj/FKe6rrg6WY6Rs4SDFW8I46t38fGu7xFFpzm2Z88nIsZNKyRCzUJVarnHssC1cR6eszRu3y2usWh39mqxhigaTlaN7nLqnilSViHZJLfvB1KD5qCuf0vUd9VEYFq5qG55zxeAiXdMIa4thyomTGAE/85Rmu+PstDnsZ6jX0HZ0imm+V5xzya5v1kVfCFZKGPd9fPgPb0XbDhm2iKx1ERzgydnrU/oEAIvYv69cb5fj/2hFZgK1LUze9GZebx2XVHjVgA4lro+eV082lp2ExExqV5TEXqUlCpTIYprP1COo4tVEN4r+aJbpq5WLDEdMDlwqz24AdxZi8V4bbpeyAyhSloB6Jy0IEJdQhm4dywD9761vthxbW4avEEco2aBrXlnv0OMuml+5GLgVhWYkDR1CRqyU2SnRLnbwVtwHFviDRRb9dVGU2RL1ytPW2y2++FFMQdOaydquOTWPaHaslxapbh7tdhbi+LQZoZpBKYkvXPsG2B2eC/nib20EmV3PNOU7Igx72YBSA1bVrGoR5TRaJeOi+CInYL5KU1i7/Hj/JjWFnv2rUuo3LN5arKMbs8x6tVy0dH5ogI4/cbF8iEWx1eRVhwwEb3l1rG5+fAGU7e+D3rRNUg0mCvhb3dA8ozV+KlkkcAXSXa/n9wGcpehoAyaEjWhRd315oUip5uoMF1FB6UR9WtnQ0+Sk9AZA0F58pdhVVNWP9Bq4M5eA+6vlyd8IZ6FqiolkdNVBj0XdkWI0PbiEZz6vGPaEpzEHYNr36GK3WKJcru+oAyeGnB56QNHlxWBO2uJCFrQuawCS24ITuU6RCH0wMlBNirVxRJRd9ZHvsiVxWDZHd8rCk3jur3nj33FcPV8CQT9TiG+tldMe6gq06qwtMD3x9UL34KKfBjBP8rBAjNYYAYLzGCBGSwwgwVmsMAsMIMFZiQEVBK4iO1gWRSRwIVsB8viJgmcz3awLE6SwLlsB8viBAmczXawLHJIYNp0WWdbWA6kadY9J2s/28NyIE0v3ZsH/4PtYTmspH8qClzKNrEMSMvlFQWmNRnL2C6WwTJD00qhyjnIYrZNwqPY0BKqCky/HPUm2yfh8SdDywcEJryLPMA2SliQdgsrPlFVYNrzYDzyDtsq4UBJowmGhn4FJpxE/gKpsc0SBqQV7QuZV/UFf/ng9ciX2G4Jg5cMzSBUgQmLkdO5JZu+5U43tIKaCkxYihzHY7Jpp0PPGhpBuAIT/oXMZO/aVDiE7IdcF+yNodZkkePVHzkbOKQZT5Dtf4/sizwWygE1KbpTkW8hM4y5VgnbO2a4i1yEpL0p5hhaQKQFvocC5CxkG+MxBzifHA3ohm3JxrSNEP2q9cWanoQ2YYnEh0lHDjPGBdoYg366hH6oIAWpsFYB4TYcJvoZN9r55Tj4qmyoEKPWBZH/F2AAo/DeTzUw7vsAAAAASUVORK5CYII=",
        text: "财务共享",
        newsCount: '999'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjYxMzJCRTUyNTQwMDExRThBODczODBDNDlBQjlDQkFGIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjYxMzJCRTUzNTQwMDExRThBODczODBDNDlBQjlDQkFGIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NjEzMkJFNTA1NDAwMTFFOEE4NzM4MEM0OUFCOUNCQUYiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NjEzMkJFNTE1NDAwMTFFOEE4NzM4MEM0OUFCOUNCQUYiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6AvTTNAAAI9ElEQVR42uydXWwUVRTHz+zMthQKKRQKlM9S+ZKv8mUh4QUfiL5USTQaRRN9kJBAoiS+iPqg0QcTPxJijL6QEIEXjQYSHuGBEARaiJRvLIUCLQgFSim0uzs7nv/du5t2tsW2tOzM3fNP/lrrzoY9v733nnPuncFqramhIVAZey17FXseu4I9gT2KXUiiJ6mL3cG+zW5kn2f/xT7I/vdp39x5imtL2e+y32IvZ1vCalAq1B7Hnst+Sf/eY9exd7N3slsH8+aRQVwzmf0t+yr7O/YKgTsssnRsEeMm9nZ2+XACxmj/mH2RvVVPv6Jno5HszewLmkF0qAHPZh9jf8MulnjnTMWaAVg8P1SAX2HXspdKfAOjKvZR9qtPC/gD9u/sMRLTQI7m3zSjQQHexP6ZbUssAytbM9o0UMA1OmsThUPbNbN+AZ7P3iUjN3QjeZdm90TASL/3SKYc2jV5j7+E8gP+kL1EYhVagd1HfQFGl+RziVHo9Rl163h1B/yJTM3GTNXb/IDHs9+X2Bij9zTTDOAN7CKJizEq0kx7ABaZpbfTgLFZv0ziYZywR18OwC+S7OeaKDBdC8DVEgtjtQKA50kcjNVcAH5O4mCsZgPwRImDsZoIwKMlDsZqdERiYLYEsAAWCWCRABYJYNEwyAnSH8YqKiJnzhwqXLeOCtasCVUguw4coK79+8m9do28zk4ZwSIBLBLAIgEsSVaA5HnkxePkPXpEXltbqALpPX5MlEyqzyCA+xIHCGATFy4QJRKhAuxeukTJ9nbyXFcA9zkKODhJDTh582aoACfv3yfv4cPUKBbAfQ0DVwXJhbmeFEmSJRLAAlgkgEVSBw9Q9owZFBk3jiJjxxJZhp6755o4efeustvUlF+Ao0uXkj1tGtnTpxsN2L16VcHNO8AjXn6ZqKiIrMJCowFHJkwgZ8EC6ty7N78A44NTJGIuXC31BR6Tu8eM5a7RYefJQ3zwBc7hZ5UsWsokkQAOWGIzqIZ/ALf6BHBvfLGf3NU1MMjYpuRrcK1pcoz5JID06JHaanSvXye7spLs8nLVTHniZXfuqJ0r1KuoyZ25c9XpTpXhC+CAwb1yhWJHjlDi9GmKLl9OtGwZWSNGKGcBwzWdnZRoaKD4sWMUr6+naFWVgosum4JsQAlnBGBMr+6NG9S1bx8lcLLiwQPyDh1KHaPBh5w/n6yRI3teA7jnzlH86FGKHT+uXhs/eZK8jg4asX492VOnpr4YAjgImURE1ZoejvnAvJYCcuL8+Uwdqnrf6Htj8KI33NhIscOH1Wu89vZUgoVreWRbqFtlig5QLyEapcjo0eTMnKnOdGE0ImFym5vViFSjl0GrrhLLxVTOozZeW6u+COo9CgooUlaWmp75vfCeAjgwhC2VTOGWF8DswggG3FhMHYSLYbrmn4nt8QgFWEzNSZyh4pGr4HJCVlBdrW6ZiZSUGNNCNQYwYRSPH09OVZX6FaZft6VFJV84EJc4e5Y8jFbs8HCWndQ/W8XFZE+eTAWrV5OzZAlFSkuNaqMak0WnGxX2lClqKna5/MGodXVNrEohBptphkAMUn0pOAmL8ujFz5mGB9ZgA9ZhM7JozohxGtPTSRKSLOw3Y0pWx1l5FOPEZo9OFcO1Ro1SdzOq12Ja5y8FoFqOo0a2P/MWwDkSRidq3+S9e6luFEAyUHX0Nv3fWd8KT30BsBEfS49YJGKY6jnbdhYuVE0PARwEwFwDo8ExoHtz040OLpNUOZVezrn2xUkTC5ANACy7SYbLjDoYayavl2rN9GfAerpW63P6viGsv3yNeq2vHEKCpt7LcQRwYACPGaNai8T1LPl2hADW40Qrc+8QXs8Asc5GcJTGD5LXYLusTL2nAA7Kh+BMWAEGTN82IaDGT5ygeF0dJS5eTA3g6dOpYOVKchYtysqULSRbekYQwEEZwenpubcSqq2NXB6N6Fb1mIZLSsieNEmVQ7IGh6EOxs4R1llfSYRWJVqS3Tfz0QDBBkPy9m2yUCP3+LZYqRGMI72ymxSQMqmxkeKnTlGytTV7DUYD49YtBTMDvaUltW/MU3bWpgLq4NJSii5erDpcAjgAQoPDbWhI7R6hNenLolVt3O33GL0uj3YFvZcsGidBUAvLFB2gKVrdA8TA+tPoyOws9bae6xMgQXqYmTQ6RGaPYHvixMzNbFknI7EG88jGRkJSP7lH1cBc66oDeb462NLbjnhPARyUaYjXzCivnWpa9dfBOIxXX0/xM2eI0oCxRYjNBK6fs8or7CbxFK32hQVwQACjzgUolEi+Mgmb/MiircuXe9TNqIGdysrsOhhJF3aVpFUZpDnaTh2U602cTGHatbpv3uO1+B1q3SKz/05OSbIMlwCWLDrkQuOiooLQr7JnzUrN0FOnphoZhhyNzWvAlgYMqOkSSm08sC0BbAJhfeAdSZWvHDL98RH5AThd+uTLIyMkyRLAIgE8BDLwcQl9fs4cftacrcHqPiEcnTH8QWjq0RA53HrM2QiO1daqA+umPvwkc3dFU5O6kzHvAKsR7D99YegITt+DnJMCorWmJk8WQ0myRAJYJIBFAlgkgEUCWCSARTEAbpc4GKt2AL4lcTBW9wD4H4mDsboEwOclDsbqAgAflTgYqzoAPsiWDQfzBKYH0knWCYmHcQLT5nQd/KvEwzjtxj+6A34sMTFGYLmzO+A77B0SF2O0QzPt0ar8iv1QYhN6PdQsyQ+4mf2FxCf0+lKzzAIM/cA+KTEKrcDu++6/8APG7XdvsB9IrEInbBq9qRn2CRi6xH6H7UrMQiOw2sC+6P8ffe0H4+8j3yJxC422aGbUX8DQT+yNMpIDP3I3alY0UMDQL+zXZE0ObDn0umZEgwUM/cleIdl1oPQ3u5r9x/+9sL9nspB4rWJvI2lp5lKI/afsF9hn+3PBQA7dxdhfs2frWqtD4v3MhKeW/8ieQ6kuVay/Fw7mVOUN9lb2TP3vOpL95OGQp2OLGM9gb2ZfH+ib4O7CofjD4NGsa/W6gMekV7AnsPEgyKiweqLiOmHCI+kb2ecodcoGBzGe+kDkfwIMAPxgRHX2ObYwAAAAAElFTkSuQmCC",
        text: "管理会计",
        newsCount: '99'
      }, {
        url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkI4REMyMzY0NTQwMDExRTg5NjlEQTI1QzE5ODRCMDUxIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkI4REMyMzY1NTQwMDExRTg5NjlEQTI1QzE5ODRCMDUxIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QjhEQzIzNjI1NDAwMTFFODk2OURBMjVDMTk4NEIwNTEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QjhEQzIzNjM1NDAwMTFFODk2OURBMjVDMTk4NEIwNTEiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5ucTGeAAAH00lEQVR42uyde2xTVRzHf2u77r3BxhibEYTAYBgUBjIiaASRqAT0D1EUfCUqwYiiQjAiGjHgK0QTgkZMRGED8REQSTRRGREijIyBCIzx2BiPwdh4dOu6vuvv192S2q3d7XpH7zn+PsmXP0rHmt+n595zzj3nYILSS6AB/TGTMRMwIzCDMbmYNEwSMJFwYNowTZg6zDHMXkw5JmY5CTEIzsE8hXkCM9b/bzFa4sPsx2zErMdcvlGC8zGLMPOUFsr0PjbMV5j3MQ3R/KAhiveaMIsxxzGvsdwbSirmJUyN4iBRa8HDMPswH2HSud5xI11xQC5GaiX4IUwlZgzXVzeMxlRgHo5V8AuYHzGZXFNdtuYfFEc9Ejwf8wXGyLXULUbF0fxoBc/ErOb6CcNqxZkqwUWYMm65wrXkMsVdRMHU/d7EPWVh78mbQodQoYIXYm7nWgkLuXs1+IXgmawCZSDNrVdsrJjhoMx4BbfgN1muNJfqpaEtuB/mDCaF6yMF7ZiBmOZAC57LcqUiRXEKwYIZuZgTEEwP64u5HtJBz+gLSPAU4If1MkJOJ5PgEq6FtIwjwSO4DtIynAQP5TpIyzASnMd1kJY8EpzBdZCWDAPXQG5YMAtmWDDDghkWzLBghgUzLPj/hClevzg/xQBflmTAtHwzJEb4mtVZPfDZcTusqrb5N8wyggjecGcG3DvA3O37Bqcb4ePiNDAbAVYetrExUS7RU1TIDWZxUWrEls7oTHCbO7oLbh9zAkzol8jGRBF88Ko76p8pzjaxMVEEr+jB/bQwg/fDCSP41wYnPLunFRraveoFZ7LgaKGdDbobfZDImhnZnV4/a/PCwC2X2ZoIw6RInGz1gBU7Yemm/67mvTnVAONzTOD06r+wNo8PzrZ5od3jY8GheH0dnbBJuZ17zRX39xWm9dhR7uZ6B7xSaQWLKz6idTuyPHDFDaKTbEyAp4ckQ+nE+J1ho1vBVRIIDvBggZkFyyz4Shw7DboVfNTiFqIzpYY/L7lYcCg0k3noqhyt+Pt6BwvusqMlgWCac//pnJMFy3of3opy4zkW5hbcy5TW2eP6+3UtmO7BHoGXcTQ5vPD7BScLDgdd2o61iNuKv8POlTvOX1Ddr5EQ+T5cWueI+2fQveCDgt6HT1k9sLfZxYK77WgJ2oLLdNB6xRAsaAvedNrOgtVwzemD020eoeQevubGzqGHBct6mf623qGbzyKEYNE6WhtPs2BpW/AvDU7/dhsWHM1YWJAWTBMzr1dZdfWZhBB83uaFZoe+Hw7Tis8Hdlig2qKvDqEwWwXoMn1ffuelL9/U2mFnY3wnFOqxl7+7yQUuHX4HhRFMl+muBF+0e+HrWjswAl+i/YLDdLRG9eH9SlIIDteTvjWLt7NIITiw2yGUQWnGTjsgGAHvwaS28rIb7snrvNvh8VuS4ERrfHqvLS6ff1rS5vax4FjZ0ejsUvDakvgemEvz5TN2Wvw9ab5Ex8D2c05dfi46fWDZqFS+B8fc0cKh0p5ml04lG1iwFrxcadXljoeqKy4WrAXU0ZpebtHVM+IzbV5YVd2uy3rpcoe/GowJHUOkzMT4DpHoAQMN4fS6vFfYaSAqaK3VA2k4BqbDWcxo/BQWurceSqTi78nBzlSTw+ff2M3j4F5mQIoBPhidBrNxDJxkSLg+Vi6/6IIlB63+S7kWPDooyX8I27gc0/Uv1q5LLlh5xAa/XXDqvk5CXqKHpBth97Q+/vMuu4I6YbN2WWBbjMOqNXekw4uF4f9T1ncOtcHyf/R9vKJwnSxqq5snZYaVS9CIpWxi5Pd0x3NDkyPKJd69LQ2m32RmwVpyd//E65fLSND89PMoqadfordHpal6L0lmwRpyV3/151V2dUqPGoqyjP4jm9QwNtsE2eYEFqwVWVEUs6ezS7lJ0f1cTpKBBWtFfZs3ivf2bDLkfBTHK9KZXrSqhAVrxM/YM1Y7DN1ytmfrk2ni4qjKxXN/XHRCq8vHgrVrwR5YXdP9tOA+HAdvjmGHwZID3S9/pUfAyw7xMElzFlVZYd0pe0S5M3daYpo+3H7eCQv3W/2X4HBj7Wf+aoEKnT7dEnqiI8DUAWaYX5gMY/qa/McGHrG4/dtGNtTaNdtZT0OyN0amwtR8M2QlJkAj3m9p98KHR2y62WAmrWBG0ks0w4IZFsyCGRbMsGCGBTMsmGHBTBc4SXAr10FaWklwI9dBWq6S4JNcB2k5QYKPcR2kpYYEV3AdpGU/CS6Hjk0BjFyQ0x2BTlYV10M6yGlDYBxcyvWQjo30R7Dgdq6JNJDL9cGCmzHruC7SsE5x6l+TFXixgLrVmHSuj9DQet/hdP8NbsGgvLCc6yM87wXkhgomPsUc4BoJC7n7JPiFUMG0ivsxTAvXSjjoodFsxWFYwcQJzJMYD9dMGMjVXMzx0L8I9zx4G2YB100YFijOQK1g4nPMPG7Jum+58xRXEK1gYi3mEb4n63Y4NEtxBD0VTGzFjOPeta74G1OC2dLdG9WuyaKO1wTMUuApzXhCtX8LMx5zVM0PRLPojg6dWokZpoy12rjeNwzaZb4GU4hZobhQRfBUZbT0U4ZTczDF0HH6EKMd9DyXHvmVYTaAMrccLbEIDiYPM1m5LxRhBmNyoWNeO5FdRcSldJiaMHWYauhYZUMLMWJeEPmvAAMAnyly6g25FEwAAAAASUVORK5CYII=",
        text: "采购系统",
        newsCount: '9'
      }],
      scanImglist: ['data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDZBRUVBQTE1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDZBRUVBQTA1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4ZGQ1ZmUyYS1kZjFlLTQxNDYtOTIxMC1iYmZlMDQ2ZjE1ODkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoyNzVlZDEwNi04YjBlLTExN2ItYmZiZi1mYTcxYWMxMTAzNGMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4qLiJtAAADNElEQVR42uybz0tUURTH73MkBbEokpTApKGC+gcCc9eiNklk25R+LVr0k2aiqHFaFP3cKPSDclEtQ1y1yoXRplpEQdDGFoFYDBpTYlbS9D28+/B5eb/uG4N7x3PgK/Ou943n8865991fOpVKRSwnqxPLzBiYgRnYbqunH4VCIa7eWugItAfaAq2OqU9d/0uoG/qm4c9maBjalqBuCXoHjUBDxWLx51JE2IFOQePQFWh7Aljvvi7omGYALiaEJWuBdkKD0CcEbV+1wOT0A+g2tDJlBv3SrP8n5d9phZ4C+lw1wBeggyG/o/QpR2hapuZdTccvQWMx3036HXL/1bhIOzTSCmjDG6CPUKOv7Ad0GXoCfTGg/8lCR6EzUMZXPgFtCmvTYRE+ocB+h3ZANw2BFbJfycuO9K+vfD10QDel9wak93tD3zTPoDtKWbcOMEW2Qyl7bPjrVfVvqw7wOuXa6yhMtnHluk0H2LFwADWvXK/goSUDMzADM3CtAE8rQ7XPFnDMyAmNfzydGJjGzTloDvoKnTadFhMFeg+f9M3ijuum9C2oWc4zn9uQqoC+L+fta/B5OHKJJ+HoRVgSae60wiLcIueS1lnMIuQEIl9Sgc9C1yydOMRZBQ8kD+gb/pTur1FYb/bXr7bhyRpvupNqSu8X7rpzW43CnleB30K7bSVC++SxNAMnAM7Y9kDw+qmDMmmA+8TClkmPJbA90t8yPvfpANMToh25JmgVNGBJgAekv+T3YFikg4Cb5U2etVoC7PezSXJwp8XADMzADGw98LxtD4ZGWAk5AkFojXdWKeswPHCqf7OYQc3opPQH5brXcODeGP9jgUeUa1qY7zQUtlP6F+V/LPA9sfiYA537GBXuOap2Q0DbpT+jYvGJo7L0P9DCFuKn5FPz39hAiwtSdMJuLsIZ2pt6JdzjQyUNCDrT+Ui4RxyjOspG6U+Q5dB+p3SByWjrIhuQLh58Q4zzu4S7/JvTAKa61Sw1XZdbLqnfw3Tw65CMeBqr/8/1/Rl5GLD5pRh4DEEbhXuqdkwjRV8Ld1NOx6j+Gw3IF8Ld3cwC9mGSmxz+rxYGZmAGZmCD7Z8AAwDtVcE2N5xeuQAAAABJRU5ErkJggg==', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDZBRUVBQTk1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDZBRUVBQTg1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4ZGQ1ZmUyYS1kZjFlLTQxNDYtOTIxMC1iYmZlMDQ2ZjE1ODkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoyNzVlZDEwNi04YjBlLTExN2ItYmZiZi1mYTcxYWMxMTAzNGMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6qkuj5AAACvUlEQVR42uybTWjUQBTHs7ZU7MFLWQTBS2sVQby0p/UDFRUUxC8UVPCgPZYqVgRFhNJDkaLevIgHQUH04BdF0dJSBUGwgkeFLvRgrdSDolSpaPwP/AOPYbvdxGRJJu/Bj2Rmdrbzz3vzXhK2Bd/3vTzZIi9nljvBjUl+eeH2jN21DvSCLWAZ+AxGwRXwTn7QP1rMvIdPg7fgGFgBmng07XGOOxPSh8Fl0DDPeAPHj7ggeAm4KtovQAdYzOOYGDOh3Zx1wdu5X41Ngp0M7Tked7Hf4+e2ZV3wSnH+AMxa47PsD6w9dVm6QvatZr61V+fbw4H9rfVvRMnk9fDwV3F+ENirLLI/sA9ZDmlTaq6DL2KPjoDdoJXHEbHHjZ1MMnEleeNxBgzyvIWhai7wWvBogST3EOypsN9T6+GzQqzHu6njVQSY/mHRNpn6cRKeTkLwOXBJtIcZujfBKjAAXoMyjwPs3wGuiXlbwVDcouMO6QugX7SfgX3Csx/B+Srzuxn63WxvBk9Zq3+kTfBF0CfaZqH7wc9qpcQqP6aE9VB0D/s2gicU/T0tId1niTUL3GuLDVG3T1m3oxv4nUv/+wnOvPEIeSNhWz9DObAhenYuhgs5yGwf2Ct6+lvoq8jIahTFv5e1MYyZErNGtKfAL3Arxq3yHqzmeQlMgJfgd43zy3wSm5GC74P1MSxuOTiQ8M1MC7dLGDMXapPcwyXH3+yU7CxdEIOHwuYBXrg/dVh4MQjNGu2u/XBSqSzdc9nV+ppWBatgFayCVXCKLOrj4fMIc+6AG1kVHOVl+RsN6Qx5uDPCnOksCx7XLO14SKchAXXWU3CHhrTjId2WN8FlDWnHPTyR8Lra0ia4VUNas7RmaQ1pFayCVbAKVsEqeOE67OfBw58cd+y0LbjLS8lr1ATM/PrvRNAo6H+mOW7/BBgAw3eEuLFv/voAAAAASUVORK5CYII=', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDcxN0UzNEU1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDcxN0UzNEQ1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4ZGQ1ZmUyYS1kZjFlLTQxNDYtOTIxMC1iYmZlMDQ2ZjE1ODkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoyNzVlZDEwNi04YjBlLTExN2ItYmZiZi1mYTcxYWMxMTAzNGMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4kAlakAAACuUlEQVR42mL8//8/w0gCTAwjDIx6eNTDox4e9fCQAiwgQnfvxMNAymYY+/PIZed8W+QYthnmEWuDEsNAMA+IHdAUcQGxxBD03DcgfoEmdgDdw8lYNHoD8RYoeysQ+wxiTyK7dT8+t4I9DEzfKILAPD3k0zC6n0arpRFVLQ0SEA/EBUDMR6BAqgXiDUPdw+JAPBuIWYlQuwyIpYH4/VBO0ipEehYEOIFYZqjnYcbRQmvUw6MeHvXwUPLwcxLUgoZZ3w11D98F4l4g/ktAHUi+CYifDoeWVgm0FcWBR813IP4xXJqWMA99H+xtaX4ys8YHaH4cUp2HCUCcT6befUDsPNRKacMB0jtgMTwX2pgnJ+C2DEUPL4Li0YbHYAWUxrAkA/lj2peB+MZQ8/AsBvKHbx8AseJQS9JsFOhlH4pJuhCIfcnUe2IoevgaFI+W0qMeHvXwqIdHPUxvD39BYmsAsdAg9oMVEv8TudXSaSD+CO3gKwPx2yESibvJjWHQTF3pQIxKUACOA/ESSvIwaEYvEBrbvwaxR0GjmN1A7ArEvykttDYCsRm07ctIAgaZvRStVcaKQ20OkrrXQKxKol2g2cQyIP5KyDOMlCwfJrAWpAWIq5E8ASpY7oD7hUjrL5DMmMOAWFxzB6r+NdZ+pXM+RSUcLUA0EFchlQW+MM/iAVkMkIE9EADNF2+isDdGNw87MUDWfYGSGmimIBKITxKhD1RGBEEHBkDAAsmcQethXSBehxQzxdCYIhaAqkEvIH6ClFLaBquHRYF4G7TeBoHpQEzOgi+QZ/2gWQEEKoA4c7B5GLRMcTMDYu0FKFZzKTDvPDQrwCbXJgOxy2DyMCivmUPZJ9EcSy5ADjRmIF4DxHIDPeKBXHf+ghY4yMmRUgDKFiLQZP2SGu5lHN3GM+rhUQ+PengoAYAAAwCI4oGBOBnR0QAAAABJRU5ErkJggg==', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDc4NDZCMEI1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDc4NDZCMEE1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4ZGQ1ZmUyYS1kZjFlLTQxNDYtOTIxMC1iYmZlMDQ2ZjE1ODkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoyNzVlZDEwNi04YjBlLTExN2ItYmZiZi1mYTcxYWMxMTAzNGMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6njtdRAAADFUlEQVR42uxbPWwTMRR2IoRgQYEBtWUgYmFgATHx2zAWhsBQNiAIxFIERLAxpFn5EVQdYKlCGWEIWWDkEBQJMZCBARhKhARlKgcjAoVn9TvJMueLc3fJOXd+0qez3/k9+bOfn32XS67b7bIsybo4nKyWy/wyRVggjBvGcYVwbkur9YxX8jE6NpEsQ58WvEo+ZsemynisIe0jOUOI/peg8ixjYglbwpawJTy6Jy2cmLSETi6pOVoeIEwo2n8jLKXpLH2LcLWHzW3CtbSs4RkNm5k0zfAGofxYujft0yZVj4cne51L7baUccLPCZWAe7tRrqDuhzuEEsqyXCE0ffQ1oO+QjiolkGoD8r0CyttRrvr4cAkdyZcnZ6Arog0TBrCeBGGGDjYIe3q048ScHn7KAuECyD7BYDyAvgg4Sa3hOsjUIvrxiHlyHKRaGAhR35ZmfKgzzOUs4R0611a0KUiEPOkA3HZW0E9C52Cdi3on7LYUl3Qw002EtuvTpqiIgkWEaxt2JRDi1zn4doX1zfUnkibM5S5Gv6ZITryzRzTCelIYsLagF8PaMYGwF9qfCS9C2nO7y4SfEimVPnHCLkg3IiSuBvwsSjPaRB6YM4mwGH4VzTXMhD3VVaxTV1jXic7wrGJ7qArZl/UZ4lUkKNdnUFpB29EwCNcDQrsuhaSj6VPVVtuHfXiwhC3h0ZZBJq2HbO0NaBjhb0ZPEw4R7hE2ath8xMPD76QIn4pguwOEjxJ29WEzRviSFGE+4jtD2n6Q6jcJ9wPav2Tqd+lDI8xnZlNI219SfZWwHND+jwlr+C/hh83SKSbMM203JJZGkfD+hGwTW8NPCftC2r4ZRcLH7NEy5UfLzTj9hJHlQW1pgyT8Xvf04yP8S4NtQp2X9wa0X28C4YkYbL2faC8CRs/wfITt5TWu/G3kYab3I/wnREZihC/F4OMt4aDN0pawJWwJ6ySt6SwQ5vue9/n+owh+u6MS0jrPoa8U+hWDJ/a7aob5x2jXCVsVhl8JNxT3zrO1v8uMGUaW9/mCV8ll7Z9pmcvS/wQYAAQ1tsrLeaQjAAAAAElFTkSuQmCC', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDdDQzJFMkE1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDc4NDZCMTI1MTI1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4ZGQ1ZmUyYS1kZjFlLTQxNDYtOTIxMC1iYmZlMDQ2ZjE1ODkiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDoyNzVlZDEwNi04YjBlLTExN2ItYmZiZi1mYTcxYWMxMTAzNGMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5NAheJAAABGklEQVR42uybQQ6CMBBFgbjyOC5clcTjuPECHsAjeB4TUG/kttakGFwgBRraad9PJpCUJjxm2plpQqm1LnJSVWSm7IA3kl72dTl8Lo0x9eex1li9Pd+S8bBaMk5IC1PZu3dKN+zSAAMMMMAAp5aHbcnnpKGST6KHG5v8h6xJLaTVwnHWsKRaenKNi4cBBhjgqA8AQhUroT28erESGnj1YoU1HFCrFCukpexCWlJv69PDInpbn8AielvWsKe0FG1vi4cBBhhggAEGGGCAAQYY4KDd0pwOKdo5hLRVOzLvLmzOaEjXMz5ezHO+KvnnIYddespZtFR1Z+jdGr4a2yfM+zR26gMfi9/TydS064Arl608AT3YpXPRW4ABAHcXSYuTVSZlAAAAAElFTkSuQmCC']
    };
  },
  mounted: function mounted() {
    var _lists, _lists2, _lists3, _lists4, _todolist;

    (_lists = this.lists1).push.apply(_lists, _toConsumableArray(this.listsInfo.slice(0, 3)));
    (_lists2 = this.lists2).push.apply(_lists2, _toConsumableArray(this.listsInfo.slice(0, 3)));
    (_lists3 = this.lists3).push.apply(_lists3, _toConsumableArray(this.listsInfo.slice(0, 3)));
    (_lists4 = this.lists4).push.apply(_lists4, _toConsumableArray(this.listsInfo.slice(0, 3)));
    (_todolist = this.todolist).push.apply(_todolist, _toConsumableArray(this.todolists.slice(0, 1)));
  },

  watch: {
    activeTab: function activeTab() {
      this.loadMore1 = 1;
      this.lists1.length = 3;
      this.loadMore2 = 1;
      this.lists2.length = 3;
      this.loadMore3 = 1;
      this.lists3.length = 3;
      this.loadMore4 = 1;
      this.lists4.length = 3;
    }
  },
  methods: {
    //  上拉header消失
    showIcon: function showIcon() {
      this.show = false;
    },

    // 下拉header出现
    hideIcon: function hideIcon() {
      this.show = true;
    },

    // 打开扫描摄像头
    scan: function scan() {
      modal.toast({ message: 'scan' });
      weex.requireModule('commonEvent').openScan('hello Weex');
    },
    openMenu: function openMenu(index) {
      if (index === 0) this.scan();
    },

    // 菜单栏内容手势滑动事件
    swipeEvent: function swipeEvent(e) {
      var direction = e.direction;
      if (direction === 'down' || direction === 'up') return;
      if (direction === 'right') {
        if (this.activeTab === 0) {
          this.activeTab = 3;
        } else {
          this.activeTab -= 1;
        }
      }
      if (direction === 'left') {
        if (this.activeTab === 3) {
          this.activeTab = 0;
        } else {
          this.activeTab += 1;
        }
      }
    },

    // 上滑加载新的信息
    onloading: function onloading(list, loadMore) {
      var _this = this;

      this.loadinging = true;
      // this.loadMore += 1
      setTimeout(function () {
        _this.loadinging = false;
      }, 400);
      list.push.apply(list, _toConsumableArray(this.listsInfo.slice(0, 10)));
    },

    // 上滑加载新的信息
    onloadingMore: function onloadingMore(list) {
      var _this2 = this;

      this.loadingingMore = true;
      // this.loadMore += 1
      setTimeout(function () {
        _this2.loadingingMore = false;
      }, 400);
      list.push.apply(list, _toConsumableArray(this.todolists.slice(0, 10)));
    },

    // 点击加载更多列表信息
    loadMoreInfo: function loadMoreInfo(list, loadMore) {
      switch (loadMore) {
        case 'loadMore1':
          if (this.loadMore1 === 1) {
            this.loadMore1 = 0;
            list.push.apply(list, _toConsumableArray(this.listsInfo.slice(0, 8)));
          } else {
            this.loadMore1 = 1;
            // list.length = 3
            setTimeout(function () {}, 400);
            list.length = 3;
          }
          break;
        case 'loadMore2':
          if (this.loadMore2 === 1) {
            this.loadMore2 = 0;
            list.push.apply(list, _toConsumableArray(this.listsInfo.slice(0, 8)));
          } else {
            this.loadMore2 = 1;
            // list.length = 3
            setTimeout(function () {}, 400);
            list.length = 3;
          }
          break;
        case 'loadMore3':
          if (this.loadMore3 === 1) {
            this.loadMore3 = 0;
            list.push.apply(list, _toConsumableArray(this.listsInfo.slice(0, 8)));
          } else {
            this.loadMore3 = 1;
            // list.length = 3
            setTimeout(function () {}, 400);
            list.length = 3;
          }
          break;
        default:
          if (this.loadMore4 === 1) {
            this.loadMore4 = 0;
            list.push.apply(list, _toConsumableArray(this.listsInfo.slice(0, 8)));
          } else {
            this.loadMore4 = 1;
            // list.length = 3
            setTimeout(function () {}, 400);
            list.length = 3;
          }
          break;
      }
      // if (loadMore === 1) {
      //   loadMore = 0
      //   list.push(...this.listsInfo.slice(0,8))
      // } else {
      //   loadMore = 1
      //   // list.length = 3
      //   const close = setTimeout(() => {
      //   }, 400)
      //   list.length = 3
      // }
    },
    scrollTo: function scrollTo() {
      if (this.todo === 1) {
        var _todolist2;

        this.todo = 0;
        (_todolist2 = this.todolist).push.apply(_todolist2, _toConsumableArray(this.todolists.slice(0, 10)));
        dom.scrollToElement(this.$refs.todo, {});
      } else {
        this.todo = 1;
        this.todolist.length = 1;
      }
      // this.currentRotate = this.currentRotate === '45' ? '-90' : '45'
      // animation.transition(this.$refs.moreImg, {
      //   styles: {
      //     transform:'rotate(' + this.currentRotate + 'deg)',
      //     transformOrigin: 'center center'
      //   },
      //   duration: 300, //ms
      //   timingFunction: 'linear',
      //   delay: 0 //ms
      // }, function () {
      //   // modal.toast({ message: 'animation finished.' })
      // })
    },
    nearMake: function nearMake(txt) {
      // modal.toast({
      //   message: '你点击了' + txt + '菜单'
      // })
      // var urlFront = weex.config.bundleUrl.split('/').slice(0,-1).join('/')
      var urlBack = '';
      switch (txt) {
        case '公文流转':
          // urlBack = '/home/documentRouting.js'
          urlBack = 'documentRouting';
          // urlBack = '/template.js'
          break;
        case '用印审批':
          urlBack = 'approving';
          // urlBack = '/workspace/approving.js'
          break;
        default:
          urlBack = 'template';
          // urlBack = '/template.js'
          // urlBack = '/home/documentRouting.js'
          break;
      }

      (0, _navigator.navigatorEvent)(urlBack);
      // navigator.push({
      //   url: urlFront + urlBack,
      //   animated: "true"
      // }, event => {
      //   // modal.toast({ message: 'callback: ' + event })
      // })
    }
  }
};

/***/ }),

/***/ 301:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["home"]
  }, [_c('div', {
    staticClass: ["fixed"],
    style: {
      'height': _vm.Env.deviceModel === 'iPhone10,3' ? '68px' : _vm.Env.platform === 'iOS' ? '40px' : '50px'
    }
  }), _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '68px'
    },
    on: {
      "disappear": function($event) {
        _vm.showIcon()
      }
    }
  }, [_c('div', {
    staticClass: ["scanImg"],
    on: {
      "click": _vm.scan
    }
  }, [_c('image', {
    staticClass: ["scan"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAwCAYAAACrF9JNAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkQ5RDZCNzlCNTQwMDExRThCMkM1RkZDNEYwNzczQzA0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkQ5RDZCNzlDNTQwMDExRThCMkM1RkZDNEYwNzczQzA0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RDlENkI3OTk1NDAwMTFFOEIyQzVGRkM0RjA3NzNDMDQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RDlENkI3OUE1NDAwMTFFOEIyQzVGRkM0RjA3NzNDMDQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4We/GhAAADD0lEQVR42uyavWsUQRiH38sFEwhRFIMJQgweKug/IEQ7C20MYmxN8Kuw8BPvRPE2Z6EoapOAH2gKtZSQysoUERu1EAXBJhZCiHIkchpi1MP4e9lZ3Bv2dnZWNDubeeGB27m55H12ZndnZidTLBZJEavBYbAbbAIrFfUXwDPQAz5T9NgIRsCWCHXL4DUYBcOlUulbWOWGkO8y4CSYAJfA1giC3u+2g6OkFxciCnK0gR1gCLx3HGdvHElO9C64AZZTvPiuWf9nzP/TDh5B9Kyu5HlwoM533DUqIcyIbndLM1m+bsYVf5v5Uef3l+u1aCbgmlwH3oFmX9lXcBE8BB9p8SMHjoDTIOsrnwQb5Gs0qCWPS4JfwDZwLSGCJO4TBXEz/OUrXwv2R+muewK67htKZjwGN6WyHpUkt2CXVPaAkh1yfptVkmukY+9iT3JMSMcdKskMmRdV6XiZzmAgNWElraSVtJKLLjkjDZM+GOAwKyYN/vFrqCSPU/NgHnwCp5JuiME4PydP+GZHx6J01+ugVczTnpjQHSF6R8x7V+HziPx9Y8RRBBnSokv3xuNNmtvEXCxtMYkWLnN3PQOuGDo4V8WC4zgF7q4DKRX0ZlUDLDmV8ktyirvrPnLXVTvSKAjOseQrsMtUC9xY7Nh1yUtmTTsBeFQ0gGxUyX76s9zfa4hgr8i3gs/9Kkk+E/ymqAWsAIOGNOSgyJfzHpJbVJZsFRW9aDdE0p9ni/CwNx4raSWtpJX8r5JV004Ej3QCiqthAryGOSeVdSW8oeT85jAzmVW10lvpuC/hkn2K/AMlR6VjXmzuTqhgt8gvLP9AydtU+wqd9xGMkbvPpjMhcp0inzGq3alSEfnXRNDi8rQ4O/7KTTwJF/BOq/mQBPhdynNyt5qUNRLnPXz3yd3eFnazaxb5BEUe1+N0FEkOXnbPBXQFT7hJkfBOcpc68xqSXPdvlmGuitcFWo8H3gx0ULRsnGj8x/X9Pe8QBAtxBwPDYD25uyXHNbrfC3JfHOkE13+pIfaU3LduOQjeC6v8W4ABAA/BtgxqYZcQAAAAAElFTkSuQmCC"
    }
  })]), _c('text', {
    staticClass: ["title"]
  }, [_vm._v("移动OA")]), _c('text', {
    staticClass: ["right"]
  })]), _c('div', [_c('div', {
    staticClass: ["headerIcon"],
    style: {
      'top': _vm.Env.deviceModel === 'iPhone10,3' ? '58px' : '0px',
      'opacity': !_vm.show ? 1 : 0,
      'transtion': 'opacity 0.5s ease-in-out'
    }
  }, _vm._l((_vm.scanImglist), function(scanImg, index) {
    return _c('div', {
      key: index,
      staticClass: ["scanCell"],
      on: {
        "click": function($event) {
          _vm.openMenu(index)
        }
      }
    }, [_c('image', {
      staticClass: ["scanImgCell"],
      attrs: {
        "src": scanImg
      }
    })])
  })), _c('div', {
    on: {
      "appear": function($event) {
        _vm.hideIcon()
      }
    }
  })]), _c('div', {
    staticClass: ["menu"]
  }, [_c('div', {
    staticClass: ["menuBar"]
  }, [_c('div', {
    staticClass: ["tabActive"],
    style: {
      left: _vm.activeTab * 187.5 + 'px'
    }
  }), _vm._l((_vm.menuBtnList), function(menuBtn, index) {
    return _c('div', {
      key: index,
      staticClass: ["menuBtn"],
      on: {
        "click": function($event) {
          _vm.activeTab = index
        },
        "swipe": _vm.swipeEvent
      }
    }, [_c('image', {
      staticClass: ["menuIcon"],
      attrs: {
        "src": menuBtn.img
      }
    }), _c('text', {
      staticClass: ["menuTxt"],
      class: [_vm.activeTab === index ? 'menuTxtActive' : '']
    }, [_vm._v(_vm._s(menuBtn.txt))])])
  })], 2), _c('div', {
    staticClass: ["menuPanel"],
    style: {
      left: _vm.activeTab * -750 + 'px'
    },
    on: {
      "swipe": _vm.swipeEvent
    }
  }, [_c('div', {
    staticClass: ["menuContent"]
  }, [_c('div', {
    staticClass: ["contentList"],
    style: {
      height: _vm.loadMore1 === 1 ? 3 * 60 + 16 + 'px' : 10 * 60 + 16 + 'px'
    }
  }, [_c('scroller', {
    staticClass: ["list"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_vm._l((_vm.lists1), function(list, index) {
    return _c('div', {
      key: index,
      staticClass: ["listPanel"]
    }, [_c('div', {
      staticClass: ["circle_dot"]
    }), _c('text', {
      staticClass: ["listTitle"]
    }, [_vm._v(_vm._s(list.title))]), _c('text', {
      staticClass: ["listTime"]
    }, [_vm._v(_vm._s(list.time))])])
  }), _c('loading', {
    staticClass: ["loading"],
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": function($event) {
        _vm.onloading(_vm.lists1, _vm.loadMore1)
      }
    }
  }, [_c('loading-indicator', {
    staticClass: ["indicator"]
  })])], 2)]), _c('div', {
    staticClass: ["loadMore"],
    on: {
      "click": function($event) {
        _vm.loadMoreInfo(_vm.lists1, 'loadMore1')
      }
    }
  }, [_c('text', {
    staticClass: ["loadMoreTxt"]
  }, [_vm._v(_vm._s(_vm.loadMore1 === 1 ? '加载更多' : '点击收起'))])])]), _c('div', {
    staticClass: ["menuContent"]
  }, [_c('div', {
    staticClass: ["contentList"],
    style: {
      height: _vm.loadMore2 === 1 ? 3 * 60 + 16 + 'px' : 10 * 60 + 16 + 'px'
    }
  }, [_c('scroller', {
    staticClass: ["list"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_vm._l((_vm.lists2), function(list, index) {
    return _c('div', {
      key: index,
      staticClass: ["listPanel"]
    }, [_c('div', {
      staticClass: ["circle_dot"]
    }), _c('text', {
      staticClass: ["listTitle"]
    }, [_vm._v(_vm._s(list.title))]), _c('text', {
      staticClass: ["listTime"]
    }, [_vm._v(_vm._s(list.time))])])
  }), _c('loading', {
    staticClass: ["loading"],
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": function($event) {
        _vm.onloading(_vm.lists2, _vm.loadMore2)
      }
    }
  }, [_c('loading-indicator', {
    staticClass: ["indicator"]
  })])], 2)]), _c('div', {
    staticClass: ["loadMore"],
    on: {
      "click": function($event) {
        _vm.loadMoreInfo(_vm.lists2, 'loadMore2')
      }
    }
  }, [_c('text', {
    staticClass: ["loadMoreTxt"]
  }, [_vm._v(_vm._s(_vm.loadMore2 === 1 ? '加载更多' : '点击收起'))])])]), _c('div', {
    staticClass: ["menuContent"]
  }, [_c('div', {
    staticClass: ["contentList"],
    style: {
      height: _vm.loadMore3 === 1 ? 3 * 60 + 16 + 'px' : 10 * 60 + 16 + 'px'
    }
  }, [_c('scroller', {
    staticClass: ["list"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_vm._l((_vm.lists3), function(list, index) {
    return _c('div', {
      key: index,
      staticClass: ["listPanel"]
    }, [_c('div', {
      staticClass: ["circle_dot"]
    }), _c('text', {
      staticClass: ["listTitle"]
    }, [_vm._v(_vm._s(list.title))]), _c('text', {
      staticClass: ["listTime"]
    }, [_vm._v(_vm._s(list.time))])])
  }), _c('loading', {
    staticClass: ["loading"],
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": function($event) {
        _vm.onloading(_vm.lists3, _vm.loadMore3)
      }
    }
  }, [_c('loading-indicator', {
    staticClass: ["indicator"]
  })])], 2)]), _c('div', {
    staticClass: ["loadMore"],
    on: {
      "click": function($event) {
        _vm.loadMoreInfo(_vm.lists3, 'loadMore3')
      }
    }
  }, [_c('text', {
    staticClass: ["loadMoreTxt"]
  }, [_vm._v(_vm._s(_vm.loadMore3 === 1 ? '加载更多' : '点击收起'))])])]), _c('div', {
    staticClass: ["menuContent"]
  }, [_c('div', {
    staticClass: ["contentList"],
    style: {
      height: _vm.loadMore4 === 1 ? 3 * 60 + 16 + 'px' : 10 * 60 + 16 + 'px'
    }
  }, [_c('scroller', {
    staticClass: ["list"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_vm._l((_vm.lists4), function(list, index) {
    return _c('div', {
      key: index,
      staticClass: ["listPanel"]
    }, [_c('div', {
      staticClass: ["circle_dot"]
    }), _c('text', {
      staticClass: ["listTitle"]
    }, [_vm._v(_vm._s(list.title))]), _c('text', {
      staticClass: ["listTime"]
    }, [_vm._v(_vm._s(list.time))])])
  }), _c('loading', {
    staticClass: ["loading"],
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": function($event) {
        _vm.onloading(_vm.lists4, _vm.loadMore4)
      }
    }
  }, [_c('loading-indicator', {
    staticClass: ["indicator"]
  })])], 2)]), _c('div', {
    staticClass: ["loadMore"],
    on: {
      "click": function($event) {
        _vm.loadMoreInfo(_vm.lists4, 'loadMore4')
      }
    }
  }, [_c('text', {
    staticClass: ["loadMoreTxt"]
  }, [_vm._v(_vm._s(_vm.loadMore4 === 1 ? '加载更多' : '点击收起'))])])])])]), _c('div', {
    ref: "todo",
    staticClass: ["todo"]
  }, [_c('div', {
    staticClass: ["todoTitle"]
  }, [_vm._m(0), _c('div', {
    staticClass: ["cellRight"],
    on: {
      "click": _vm.scrollTo
    }
  }, [_c('text', {
    staticClass: ["more"]
  }, [_vm._v("更多")]), _c('image', {
    ref: "moreImg",
    staticClass: ["moreImg"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAkCAYAAABmMXGeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NUExMURDNDI0RDFCMTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NkM2RDI2NUU0RDFGMTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo1QTExREM0MDREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo1QTExREM0MTREMUIxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Prhhnl0AAAIUSURBVHjapJbLK8RRFMd/w1DYiKyIHRFL8ohCjKIsWLDwimZDyTv/whBbMwkzHiVRkkcJ5RV/gNcaO0xZUCS+p85Pp58h995Tn8U9M326v3vPufe6/H6/z7KsHrAIusGbpRBer/dHLgr0gTjQBTZAgmUYJF0W42pwAFJMpW1gVuTywTHIMJF+gE7gE/lMcALydKUUn2AE9IvfUsEhKNGV2jEJWsA7jxPBLqgzkVIsgHrwymOqjHXQYSKl2AYV4FH8bwYMmUgpzkApuBM52swJ4NKVUlyBInApctQsQRCrK7V4pmU8cztaeJ3jdaUWr20lr7UdNWAvEAgk60opXrgqQiJXCI4gTtOVWly/7VzPdmTT0kCcoyu1u486b9jRfTTjQl2pHWPcEB88TgL7EHtMpBRzEbpvyVRKEWOy+5GCjsxVnqFdIU0m0lEwLSb2AMpxZ+26NWQuLqlekbsFVRDe0EBVGsub0yxyF9RdEH4fPCpS6vM14BG5UzrAIQzrbFQK37JSuMmfHNbZ/XTqFlAgckGuzxedUyqXPzFL5MYdnaQkLebbNFXkBvhK+fxrJr9tVC1YEUX9zs+i0H82IJK0nYs6msfU141gS/c2HeQnkC184hN/S6WY3aJLxh0vlHt+sF2qtpybTxn63FaRv6YadFzPSi+UeYfwPMJ9ryxtEOMdfpk8mL5P6cXxDKb+6hKV+BJgAJ0QdkktcujgAAAAAElFTkSuQmCC"
    }
  })])]), _c('div', {
    staticClass: ["todoContent"],
    style: {
      height: _vm.todo === 1 ? 1 * 134 + 'px' : 10 * 134 + 'px'
    }
  }, [_c('scroller', {
    staticClass: ["todoContentList"],
    attrs: {
      "showScrollbar": "false"
    }
  }, [_vm._l((_vm.todolist), function(todo, index) {
    return _c('div', {
      key: index,
      staticClass: ["todolistPanel"]
    }, [(index === 0) ? _c('image', {
      staticClass: ["clock"],
      attrs: {
        "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGUAAABXCAYAAAAdzotpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkEwNTY4RTlBNTNGRjExRThBRkEzQjNDNUU2QjFEMTFCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkEwNTY4RTlCNTNGRjExRThBRkEzQjNDNUU2QjFEMTFCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTA1NjhFOTg1M0ZGMTFFOEFGQTNCM0M1RTZCMUQxMUIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6QTA1NjhFOTk1M0ZGMTFFOEFGQTNCM0M1RTZCMUQxMUIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5sqF9vAAARkElEQVR42uxdCXgURRauyWQyuchJyAWE+wgkBAIqika5RG65FUEFRBZZBD4VRQVUWBBZAQ9Q1N115RB1BUGQQ+5DBWLCEQgKJIAkIQm5yDFHktn3Mm9CpelOpudimPC+7309Xd3TXVV/1av3Xr2qVhgMBuYKpFjdhz8NBp4JPAy4BfAN4F+APwTexd9omLzL6crizlyPYoG3AUdyaWrgQcQrCDCnbY1uLgZIAPBWAuQA8EPAnnQ+G1gD/CLw3525EK4GynTgxsCHgVGeHQTWAmcALwF+ku57G9j7LiiOoaF0nAesE7m+kcYWf+CEu6A4hqLomFLLPafp2PguKI6ha3RsWss9pmvZd0FxDO2g4ysS17sA9ybRdvAuKI6h98kmGQ78BXAoV85BpCorgVcC590FxTF0BfgJ4FLgCcBXgdOBc4E3E0jbSD2+qxI7kNBO6Uaalp4G/0Dg82SfDJbQzJzTolcoFGb9KT4oqDUchpBxFgMcAexBVjIOoBeAf6VWuS8xL6/C7iX5tDd/doZcLGjJh1HPyRH7m1SZoYz430eB+xLILZnRfcMI1L9Iy0MjdSOU8YI52TTHraXgb6oLFMhoPzjMAX5QRnVlAn8E/CFk/IaDQDGfnv9ZWEas+FnAU4CDZDxpP/BiKON2h4ACGW0Gh0+o5Vijrs6ETK93VrEB5ZxEln+glRrgZCjnZbuBQr0DKzJAeK3Cq8ENbUjUZV1AWB78LnPTlqrdS4t81DnpER4F18KZoVIM5a8o0xonAgNdLl8CjxCRbwYoX5a2YdOMCi+/0gq1t1apLfH0yM8IUudeaaIsu9FA5JH56NIR6zVWgwKZHUuZVd68yc1Q3KzTyZyHnjqWFz/gqtSDPfIzPcO3r+wUcGLnfe6lhUJA0UboD5kudgJA0GG5G/h+Pr3c2z+/ILb30cx+U5N1QZGSDSjo2ObGjQ6u7eZz6VSMoBGWAz8DZVxrM1Ags/3h8AOvDECLybw8ev6mwg4Pm20NKzXFyqj1bzwYmLwjgdXM0AzI8AonAAVV6HVcJTBobHsujX77SKXa22wFJeDU7tAm374zxKMgK1wAzCC+x5gDiptERtEVsZYHpKR53MmU17d9IQeQKhHn6VuhKNe5s1sz86eTSK90gXxhKIblAIJUENPr2pk5P35RAlJEoN2uo/q02k5ZzY8h+KLUGes2ys0oUsvPpvYOOLXnAUHya9B6tlnRupXAAaiA0FFp6bMgH+g1fq1mq9/zAOZb7rOwAabOXL8RGzCXHEj1aTbdIr6ggI+RfVEtsrAF4AttCMhiMyu/FRweAe4A3JYYxYOnyO0aUr/PEaMNsRfedd7Md70Kh0U1W3/PwxeeW/mzbItcW6rssLD/RIEoewzFmEVjCmTuEPx8wDSoX5j04afYNR0BCLV4NNZGMeMkVaQNxBMqIzgR/w3wztoMWVsC45+yr1Gr1VOncIP/IXj3g7JB6RocjJb6HzXE1qwNG+0NCFRGc3KBPMk5Ee1B12hQR0M2zd7AtF32xFDftKROXFKb49ev1zmWCgMnhvAn2Q+NO2pPQKACopnRzf5UDbX7ViqmxoJi6SLWE6VpSJT50hjYgkRcG0oTEgKOQRPT4d1r0FCEfJ0RjDGL4RrjgaExhskFBs0GAShYv0vl9hT0pA4yGYbJ7x573x6AQKFx8FsA/DfMg8ijyknk7AHGijgJz6isvromW0EgKjhlpZJ8bxXx09thOka14GDdk0ShWOQO3r8K+A14fr49ekzc7G6zOANzC/SUwXJBwVaIooSVNok+e/bl77+xEpBpUNiPBYUdT60lROQxvwP/Bz0I8L9cAkBNnt5m1NIbMuMcu6R7CbiQGd31Wajytn9veIn3lRScY3mWGSe6bmnUwC/BO/8ryOs0ZowVsxgYePdIeHc0nV4EUFrKBQW9nyp6+SF4+W4rAJkKhVwl6B048fS4yCPQ07qo2shak40iKZpae1Nm/RRDBdkjJ+Jejm+q1Ja8xMQDJ3D8nMj3Gsg39uaVlgLT8vNpvQJO/tyDTvUAiodcO0Vl+lHp4aWzISBYuYkigFwkVTGhCpA12UHAKD6x0gZT77DFnA+KOmyhw5LfS4xN/CD1XW1wE/RzCd3tmL9Eyq9pjMFyTLXUjgFTQiNWv5YYj8xNV+ZhI0DQoXnIJBa5lvsW2h8Ehj8wzn+guIhn9o3cxHLde3reruiTCw7Ohsa3iPJjIsznIcq31cAoNcVq2XaOiGw1qmWlRT42AGQkM07DNhDYDQ/DffOhxeoBjB4ERixz7EyoUu8XEpO0NKkop8cYFFF/cdcwv5sp/1YB415S6CNWv3JAqbZ+1Tnpja0EBAPjvhZ0WdSm4uC+Q1WiirFJpCGp2O0jz8uj5kemzPlxiUHpfkAgar6GcgyxBhh17qVIsfqVA0q1XaIqyg3xyjjnayEgCWRB889fT+NHLgCCbpPnyWXiFKQJaxV8YvFv28t9ArYI6udbKo9sYDyvXfRRFeWEitWvHFB28CfhO1bFWwAI+qs2CVo/qrnj4D4dAIIFRLGgZk5GFWofNYwzSbrA8C2CHrOJyiULmPAdK7sIvOPbLQEFDbZq17zfmYPdVIVVdoK5gHiRWslPan2HYgrGj0oApB85GJ2WDEqV4fTcnUm6gLD9fJ1juah8ZgHjXlKg8j+9rxvvICFD2CKH5Bvw8x1T2o023Y9VqL10dQFCoKwkK91EvyEIcF8ZAIKOxvvZHUJghbvHzO/1uLKsKJpLXgVlmSoos6gdozBUKgAUvrzoNVhoqZcYreVUZgzNkSIxQFAs7eOS8mlQvwyAdGfWBV3cFvK5fMq/7ftjJigqK/y4ZNQc99cFjIDQs9AW/ldk0cwj/BFdFDNkAqIiHxJPTxMg6CTsazMjoyBLjfP/yNia7QlKSdOYwowBL24RJK+i8rLaRJmApiMgltopppdsYOKzZbcAQoSe1/bc+Vdw3xYABLW3YRJOR4uowzuPzoyZ98hs5Kiv3+xh796S1Wfy+dKmHXmtqT2Vl5kJzGq49q01xmMNZyLwT3UBAq0GK/5VXqSSm8TkqvZldziljVtyAGyYEi5pNpVbChiTR3sr1SOzCSjwAozDHUEGn1QPMYHHB6/NhXuzyRZpzVyANKEtSnK7j+DHkSCpyqZ66siM0wVDqB5lUa0yGR6IMbi9pK5TzNRMwYD2OQDicScO7LXR1YGzkoKP/tDDTVdmGvRnQPmXiwUVQtpZOJy19F3W+prQldKIO3+vSv1l7B40c1wJlApvv/K8Lv1/4ZJC2c01ljYla0EZz/3GXvUZ9BLUTLozF6SMgTN+N7gpdRLlv/2gQNcNE4iojRRV3xlVfFcERe8XogNNLJVLepTqwWl6Sj/B/01TqZ2ZC1Ne/MCTgvrr50yg8M63kiotbU02ji/hrgxK7v2j0gxKlU6iHm47KLxjEVdrlZMq6NJUqVJXaho1u8S7XZwCFJCjGF0SwYNCxxasHlBJVCwfyBcpN4DbXj1FaBQmkm0SUS9AadYpU5DUxpbPt9Sh115wjpGLYcw1VxuL9ZTrIvVhXixYYqTdekor7vcNGE8ymLxFm3c0lUW2u2Fw99BJ1EcNMhyPiAWe7AjxFSBwrSAFsnpEFWoffmlgbRGbzYA/BWDWAvvZExQ+E8VmZMwVtTCdRCMVkskNhSsKjgPH2QsUPo7LNHmjqk+gGFQeWon6EJJJ+dlNChL6zybbY6AXm7RySHSKm15b/Z7ApO0J8UntrNpMrbhFlxPnZqzbZGW2FGb0FJxnwemMf6E4Y8bdOqZwksbqnsJPbZomezzqU09RlOs9JOpDSCbfWJaiawZG+nQhMYbL4Y+JGdy2BEVXn0CBHisHlDIAxHQPGp44jY1bo7RjxoifZ2whvvg9VkIdCYomrOUFRUV5Vb71DYIL9P6NCqx5Xmnj6CyLQNGW+JgJCtaP0NjE8QiXE2KY7OfA/2bGpRkvYJYsBYWPicUl040SHQRKypyta253L8FwVOgpnhL1ISR00J6SuIYBFcnMGMCIvaUr8EhLxdc5wTmuMyysL6LL9+LvDQVJf0gYjtibkDNqMcJf5oxPFHUDLO0pqYJzXGx5uL6A4n3ldGgd9SFUh4VL3tFWwQggDEzBBU24DOOfDGduGSuxtKfgYJXLnWNwRV696Slpybw3PJfqQ2o8qQYFeg7uk4YbR+DaztHMuJJsIjOuMlvOjPNSlmlfiXl5GHu5l0tK8ElLxgDmSpdXhSsrFOrs9CguaS/VR22gtAZADtLAjjt6JDHjyoP2ZLfobKESI+3hfge2WzYmvhbZ6TIU/NvGpm56jadEPYgN8oxsElSD9zFjXAPW1XdSjdgaUDB6km8h42vpxq4DytEfYvmxnNWMIhWz5vEe3KKrO9gqOFu7s05129LMQZe9RN3RRKP9zh3505UBUZYWuftcOtGBSzpA9SBFqO7GABhDgX812wayMp9fcr/9Wn88ATduy3JVUCK2rYhTlOvUEuW/dfzpmvE9cIrc91i7lADl4jJ2023/intp4dhyb/8wVwMEjEW34ONb+Cj/Qiq/PIq/at+eQsF3/DYZzWPmJqCDrdTlesmPy2NBfPFzRh/Ya0tfW8yprzDp11UP1Gne9rl08oQrAYILlRoe+UYY5/aB3Xpl3pAhzMQW9pZcEmEmCmu7/EncwuOGq4AStXZOL2VNB+Sy6g197GELXR9cvdMRRsrj3iSNyQLdGrx5816zxKRxX1/cN8tkVFXmdRv8bNq4JXd8HBj0kKioDfOeZgaDaSILta1oWiYim+SsecQVwTj79jS5THA+eS0AtgJYaUZvwQxO558bdGzzP7yvns2+kwFBb3DjTUtGcIAgTbcUEDljCjrFpBZRoivgeTPFGO7BsoFLimy3dORwRYW+7I50p1ToFS0/e2G4UlPML6PbQOVk9gbluTrumSTjeTjnnH6zYOU9Y99MiGNO/E1FKWrz0YSBntlp/M5L6VQ+5ghQ2tZxTxiIMD8zewvOAuJq4Ore4V6cNyZ60aA7au0j7hrhe+EYv4MelmcYlc8hoOTXcY+er2QzgEEPKG7UWe1s88r8c2z0u0M7MYOTO5Ehf61XTuwn2F0DM/0UlYs5CpQdddyzG7QwWStcoQDfM8HqWa+rqUNBlA3BPUucEQ/o0aoOC/uP90s9fK/g0jQqD3MkKDjjJeVyx0Dmtyy0X3Dp8gI+TVWUE9dxfq+x/mf2hzgTIJgfELHPeWanNxdcWlDLUnX7KRlkp6CvCj/dikvF0B+G2/fhuvG50EsuWvJgsF3ws1A419Dw1re6aXK7j9h5edS8ZIOb8rYpAThh1eR/C7uCLdIHlBKxHowGYk8A5pTNJKQ5G+ZwxiOSL1ViHoBRZOmLJQBBoGvYPHq/kPQrw+fsyu/8mMMnxwKTfooAG2SAR36mcE2NMJ82BUY2KACE1S+VAAQ/DTsOGPcoFm4vZSht0iEls++UwwWd+tjd7R9wYldY+M5PHvT+60y0yOdDjjBj7BV+DamjPYCx+YfSrAAEC5RDHxjAkJq5OPYL/68Ja3U+v3O/5GuPPJNqyVcopAh3Owrd92XbwKTtcZ5Z58XWkpTR2LkUP1AA+cQxDwOyY2wNjENBqQsQwb3oI8PoDdEdGwzuHpqSph1Ti1t2Tcvr0j8NF+nIzQ/0hAYAQnPfC8ebe19Jac8HhgsI3UszhDOItO8ZTt3eY0tgHAaKBCC4HRN+d+t6Lf97GA6vszqWPZd7+xfo/UNy9QHhudrgyPxKtY8W0rSVHl56N12Zyr20UO2mLVGrr18NVBVkNlQV5jQU+Q6YkHA53ELI375a8mdzYBwCSi2A9KUN3cx5xn3M6NBEL7WnHYcU3NwGQ0RxgupXM/NmU2DsDgp9NRRV5ghLARGpAHSQYqBaD7FxxwLC8QJ3EEdn6XdW5EsIDGqMLeB5WmcDJVxgeFoMiMizcakBbryTQJoQ+uha1wEUAoARNedoPENb65eqrXetz48YMBHw7Exbg/J/AQYAjQF+gE4rK5MAAAAASUVORK5CYII="
      }
    }) : _vm._e(), (index !== 0) ? _c('image', {
      staticClass: ["clock"],
      attrs: {
        "src": ""
      }
    }) : _vm._e(), _c('div', {
      staticClass: ["square_dot"]
    }), _c('div', {
      staticClass: ["todolistTitle"]
    }, [_c('text', {
      staticClass: ["todolistTxt"]
    }, [_vm._v(_vm._s(todo.topTxt))]), _c('text', {
      staticClass: ["todolistTxt"]
    }, [_vm._v(_vm._s(todo.bottomTxt))])]), _c('text', {
      staticClass: ["todolistTime"]
    }, [_vm._v(_vm._s(todo.time))])])
  }), _c('loading', {
    staticClass: ["loading"],
    attrs: {
      "display": _vm.loadinging ? 'show' : 'hide'
    },
    on: {
      "loading": function($event) {
        _vm.onloadingMore(_vm.todolist)
      }
    }
  }, [_c('loading-indicator', {
    staticClass: ["indicator"]
  })])], 2)])]), _c('div', {
    staticClass: ["recentlyUsed"]
  }, [_vm._m(1), _c('div', {
    staticClass: ["recentlyUsedIcon"]
  }, [_c('div', {
    staticClass: ["cellIcon"]
  }, _vm._l((_vm.nears), function(near, index) {
    return _c('div', {
      key: index,
      staticClass: ["nearList"],
      on: {
        "click": function($event) {
          _vm.nearMake(near.text)
        }
      }
    }, [_c('div', {
      staticClass: ["img"]
    }, [_c('image', {
      staticClass: ["nearImg"],
      attrs: {
        "src": near.url
      }
    }), _c('text', {
      staticClass: ["newsCount"],
      class: [near.newsCount.length > 2 ? 'newsCountMore' : 'newsCount']
    }, [_vm._v(_vm._s(near.newsCount > 99 ? '99+' : near.newsCount))])]), _c('text', {
      staticClass: ["nearText"]
    }, [_vm._v(_vm._s(near.text))])])
  }))])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["cellLeft"]
  }, [_c('div', {
    staticClass: ["leftBar"]
  }), _c('text', {
    staticClass: ["cellTxt"]
  }, [_vm._v("待办事宜")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["todoTitle"]
  }, [_c('div', {
    staticClass: ["cellLeft"]
  }, [_c('div', {
    staticClass: ["leftBar", "leftBarColor"]
  }), _c('text', {
    staticClass: ["cellTxt"]
  }, [_vm._v("最近使用")])])])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });