// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 263);
/******/ })
/************************************************************************/
/******/ ({

/***/ 263:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(264)
)

/* script */
__vue_exports__ = __webpack_require__(265)

/* template */
var __vue_template__ = __webpack_require__(266)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA-weex\\src\\views\\addressBook.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-e8a0642a"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 264:
/***/ (function(module, exports) {

module.exports = {
  "addressBook": {
    "paddingBottom": "15"
  },
  "fixed": {
    "position": "fixed",
    "backgroundColor": "rgba(249,249,249,0.9)",
    "height": "50",
    "width": "750",
    "top": 0,
    "left": 0
  },
  "header": {
    "paddingTop": "40",
    "paddingBottom": "20",
    "backgroundColor": "#f9f9f9",
    "borderBottomWidth": "1",
    "borderColor": "#cccccc",
    "borderStyle": "solid"
  },
  "title": {
    "fontSize": "34",
    "fontWeight": "500",
    "textAlign": "center"
  },
  "add": {
    "width": "40",
    "height": "40",
    "position": "absolute",
    "right": "32",
    "bottom": "20"
  },
  "search": {
    "paddingTop": "16",
    "paddingRight": "16",
    "paddingBottom": "16",
    "paddingLeft": "16",
    "backgroundColor": "rgb(244,244,244)",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "searchInput": {
    "width": "718",
    "height": "64",
    "backgroundColor": "rgb(255,255,255)",
    "borderRadius": "8",
    "textAlign": "center",
    "color": "rgb(153,153,153)",
    "fontSize": "26"
  },
  "menu": {
    "flexDirection": "row"
  },
  "menuCell": {
    "justifyContent": "center",
    "alignItems": "center",
    "flex": 1,
    "height": "146"
  },
  "cellImg": {
    "marginTop": "24",
    "width": "48",
    "height": "48"
  },
  "cellMsg": {
    "fontSize": "26",
    "marginTop": "24",
    "marginBottom": "24",
    "textAlign": "center"
  },
  "movelightBar": {
    "width": "375",
    "position": "absolute",
    "bottom": 0,
    "left": 0,
    "transitionProperty": "left",
    "transitionDuration": 300,
    "transitionTimingFunction": "linear",
    "justifyContent": "center",
    "alignItems": "center"
  },
  "@TRANSITION": {
    "movelightBar": {
      "property": "left",
      "duration": 300,
      "timingFunction": "linear"
    },
    "searchAside": {
      "property": "right",
      "duration": 300,
      "timingFunction": "ease-in-out"
    }
  },
  "lightBar": {
    "width": "56",
    "height": "6"
  },
  "searchAside": {
    "position": "fixed",
    "right": "-16",
    "top": 0,
    "width": "60",
    "transitionProperty": "right",
    "transitionDuration": 300,
    "transitionTimingFunction": "ease-in-out"
  },
  "searchLetter": {
    "fontSize": "22",
    "height": "30",
    "color": "rgb(102,102,102)",
    "textAlign": "center"
  },
  "txtLetter": {
    "position": "fixed",
    "right": "375",
    "color": "#ffffff",
    "backgroundColor": "#cccccc",
    "width": "60",
    "height": "60",
    "borderRadius": "30",
    "fontSize": "30",
    "textAlign": "center",
    "paddingTop": "10",
    "paddingBottom": "10",
    "opacity": 0
  },
  "addressTitle": {
    "paddingLeft": "32",
    "height": "40",
    "justifyContent": "center",
    "backgroundColor": "rgb(245,245,249)"
  },
  "titleLetter": {
    "color": "rgb(102,102,102)",
    "fontSize": "22"
  },
  "addressInfoList": {
    "paddingLeft": "32",
    "height": "112",
    "flexDirection": "row",
    "alignItems": "center",
    "borderBottomWidth": "1",
    "borderColor": "rgba(0,0,0,0.08)",
    "borderStyle": "solid"
  },
  "avatarImg": {
    "height": "80",
    "width": "80",
    "borderRadius": "40"
  },
  "avatarTxt": {
    "justifyContent": "center",
    "height": "80",
    "width": "80",
    "borderRadius": "40",
    "backgroundColor": "rgb(0,164,234)"
  },
  "avatarName": {
    "textAlign": "center",
    "color": "rgb(255,255,255)",
    "fontSize": "28"
  },
  "info": {
    "marginLeft": "32",
    "justifyContent": "center"
  },
  "name": {
    "fontSize": "28",
    "color": "rgb(51,51,51)"
  },
  "dept": {
    "fontSize": "24",
    "color": "rgb(102,102,102)"
  }
}

/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var modal = weex.requireModule('modal');
var dom = weex.requireModule('dom');
var animation = weex.requireModule('animation');
var navigator = weex.requireModule('navigator');
var stream = weex.requireModule('stream');

exports.default = {
  data: function data() {
    return {
      cellImgs: [{ // 菜单栏图标和文字列表
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTExQjNDMUI1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTExQjNDMUE1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpjOWI1M2YzMC05YTk1LTRiZTktODQ2YS1mNDY0M2EwNjJlNTgiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmNGFhN2FiNS05MmVkLTExN2ItODkyMS1mYzNiNzJhZjZlODkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4OOqxCAAAEj0lEQVR42tRaaWwNURSeV4+isVTstSSWIpZIKSEEIeqHxFJbIkoIEcQu+k+E+CO2CCGEtNrS2JfEEkosQRUhQoXal9La97W+E2eS2+PO67w3M+04yZfO3ead7957lnungZKSEuN/lmAgsyhkO5AMjAISgcZApXLS7StQAJwC0krG1ruk6xQwMl5avaAfsBFo7ZPJ3gtMA5FCtTLKovNU4LiPlCcZBuRhx3QsawVGADs1L/gI3AS+lJPCdYF2mkl+CnQ1VyIoGhsAm0TdM2A+sAv44baWUMSyDbNNjfOABQqROGA9MFy3hahjbaV8l9gC271Q3ga5IiCVlf2tbieQS5QE6DlFHQ+MAZ5X9OYHif34s1xUj5cEaL+p63kCuOwjI14pVqGvJNBCDDjtp4CFVSBvk69UtZRGLAPUBxe8yEieqTj2YqTAHgeTo0bdqjov5JbMAJYCtUR9EjCLo+sE4KHTH4ryQPk1wFqN8obYv5fY7nxFYCIwU7eFNXXkMA7AHVbzC4HqwDJRdwbowfYVy0Hpm9LeirebIwKdeUk7iLZWXJ9ok+ggjuSm5AEDgAu8Am/ZFY7R+XMnBK4CJ4Elom061+da5EZSeooyGfF3Tb99wBWl3B7bqKbXW4hCeVk/Ul+Ur4foe72Msa4TuAG8L6OPjBtxIfo2cSvmBNmIyCO0BUYrbUd5/74GMm28K0+UZwNnNf068WHJlMeIsi+cEFjHz0MFgSPA6jDedQD4BMRwOZnjQSrXm3ayQ0T97X5xo680GSOtLs3uOeA2/22qtBcDK/xCoA5DSgzPfLymjewqwQ8E+gO3LKJwKKEM+DDcaDoQU1EEKDAdc+IKIeOAnEjigVMCFKnTxXt+8rm6D6cPAQXRvJXmAA/Eu7pRwASJqPIiQGfnDKCyUnePz9BTOOd/K8ZQZL7D3o0y0c2ifSDnS+VCIFUEqyc869dsjqebt8mKGzdlEVahvtcE6mgMNoVJhCtzBemYcFYhUgJ0VFTz+N2c+EVy1v2uUXicXVsI8gzU5FRCpse1OYpuA9Q7ySGi7yqHB/YTUPiGktI34jT+oh0CVpEwiUEySRBMVJ7fAOddcMcHxZmkux0CdrdQG+WMW49vHEzJFfc1kYqchLZu2kAW8I6fY0XbM5eieaHGUdiyAfLHDYFe4lRG7m0X38XcDHF/VNklAr8imdwgXzblG6UvdUnu8v2NlEcckKooHimb0++fkWgOA6az9GLN79siEK58YoNL5nI0l40IlbdqyvYyDizUpAluyjq41mteEihgF/vUA+U3cLJneLWFVPcZzylEX3YEkcpXPk9kWX2NtENAGmAtG+M/84xtcBCFw+neQJAutYUKNLm+bwTG3pADaikvpRLIF8GECPTwEYeFfCgyJUcSoPvLrWIQXXk088Hs038KzBLVW3ReaAVfj5jSnC+sKJmrXgGKNwXWckxQZz/bdLO6D92Dgf0acmQ09w39ha0XUsP497udmQl0AYFiKzd6yPj7oWKzaK9quPBFxaHQJ6kBpvKhAlka0Nv4e6nrF6GMOAHKl8qR/ggwALcMFfxO/rb8AAAAAElFTkSuQmCC',
        msg: '常用联系人'
      }, {
        img: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTE5NkUyNTg1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTE5NkUyNTc1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpjOWI1M2YzMC05YTk1LTRiZTktODQ2YS1mNDY0M2EwNjJlNTgiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmNGFhN2FiNS05MmVkLTExN2ItODkyMS1mYzNiNzJhZjZlODkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5RQFpJAAACl0lEQVR42mL8//8/w1AGTAxDHIx6YKABCzmavrU5IHMFgHguEPsDMTMBrb+BeA0QpwPxZ3IdzVV1gDIPoIHJQBxEpFpWII4E4k9AnDFYkpAHGXrcBlMeYEXzjCAOHEKLvMdC5Tx1E4g/4JC7O1oKjXpg1AOjHqB5KaSGpxRSHqwe+I3E3kmknn+DKQntJEPP7sHkgRwgXg/Ef4lQ+weIVwJx6WDywAdoYw6UHBmheCKSfCOSOKjZEQFtzI2WQqMeGPUAreoBYJdRFUg1ALEpELORYa4QErsAiBPIMOMnEJ8A4iZ8TXEWLI7XAFIngZiPSoHED8Xk1uxe0IB8QGwS6qCi46kBRKCpgegkZIHEBnXAX5BZuQVD2QuBeAEZZqgA8Wwo25oUD3AgsU9wVR14QGBYBRsIQGKD9B8gs4LE1u8eLUZHPUD1egCYprmR0hkjkjwfUE4Ayv4OzA8/8ZjFCcTsUDY7Wp4SQCrbv+MxgwMpD/KiBTTMjF8gJ8MkGL+22h/Bl8vROiGgpnAKsgFAIArEy4DYhQgzQJMRW4E4Fi2T8kJLK38iUwWoUIgGBugzJiIdDwsFULFajyY+lUjHw2LXB4j70MTbgTiQhCQNKgZnkZsHnAjwiXUAMnAhwwwXdA9oMuAe10zAUyazoDkMlxl+eAoP5LonBI8ZZkjq2FEsB6anG7i8CszID4kMlYd4RiUeE2nGXTxm3B6tB0Y9MOqB4dqlBJY0tnhKCn0izTPCI6dHpBnGeEohdXz9gUMk9FVx8dcSaQb6KN5XJPYcIs34Rm4S2ozG30KGGduoYMYWmAfWoTXOcIF3DJAhw0408UIgXsJA3MT1J2gIl6OJ1wLxdDxJBxl8gTYq08CNq9HVKgMMAAIMAPxVgHbmULWxAAAAAElFTkSuQmCC',
        msg: '组织架构'
      }],
      msg: '常用联系人', // 点击状态显示长条
      searchLetters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'], // 搜索字母集合
      searchInfos: [{
        "indate": "20180513",
        "position": "主管",
        "phone": "18272161390",
        "FixedPhone": "021-52222251",
        "status": "1",
        "email": "123465@qq.com",
        "trumpet": "21213",
        "branch": "人事部",
        "role": "主管",
        "chName": "小明"
      }, {
        "indate": "20180513",
        "position": "主管",
        "phone": "18272161390",
        "FixedPhone": "021-52222251",
        "status": "1",
        "email": "123465@qq.com",
        "trumpet": "21213",
        "branch": "人事部",
        "role": "主管",
        "chName": "小明"
      }, {
        "indate": "20180513",
        "position": "主管",
        "phone": "18272161390",
        "FixedPhone": "021-52222251",
        "status": "1",
        "email": "123465@qq.com",
        "trumpet": "21213",
        "branch": "人事部",
        "role": "主管",
        "chName": "小明"
      }], // 所有联系人列表
      txtLetter: 'A', // 显示字母悬浮圆圈显示正在点击的字母
      loginMsg: '', // 登录的信息
      Env: WXEnvironment, // 获取设备环境变量
      txtLetterTop: '', // 显示字母悬浮圆圈的top
      searchAsideTop: 0, // 侧边搜索栏的top
      searchAsideRight: '-16px', // 侧边搜索栏的right
      activeTab: 0
    };
  },
  mounted: function mounted() {
    var _this = this;

    // 页面挂载显示第一个字母
    this.searchAsideRight = '16px';
    // 获取显示字母悬浮圆圈的height,然后计算出悬浮圆圈绝对居中的top高度
    dom.getComponentRect('viewport', function (option) {
      _this.txtLetterTop = (option.size.height - 60) / 2 + 'px';
    });
    // 获取侧边搜索栏的height,然后计算出搜索栏绝对居中的top高度
    dom.getComponentRect('viewport', function (option) {
      _this.searchAsideTop = (option.size.height - _this.searchLetters.length * 30) / 2 + 'px';
    });
    // 获取login频道消息
    var login = new BroadcastChannel('login');
    login.onmessage = function (msg) {
      _this.loginMsg = msg;
    };
    // toast弹出login频道消息
    // modal.toast({
    //   message: this.loginMsg,
    //   duration: 3
    // })
    // stream.fetch({
    //   method: 'POST',
    //   url: 'http://192.168.23.1:8080/ifp-emas/mobile.do',
    //   type:'json'
    // }, res => {
    //   if (res.data.body.errorCode === '0') {
    //     // JSON.stringify(res.data.body.mobilelist)
    //     this.searchInfos.push(...res.data.body.mobilelist)
    //   }
    //   else {
    //     modal.alert({message: 'error'})
    //   }
    // })
  },

  methods: {
    // 搜索信息
    searchAddressBook: function searchAddressBook(event) {
      // modal.toast({
      //   message: event.value, // 获取输入的搜索值
      //   duration: 3
      // })
    },

    // 菜单栏切换
    handleJump: function handleJump(msg) {
      this.msg = msg;
    },

    // 点击侧边栏搜索字母滚动到指定位置
    scrollTo: function scrollTo(ref) {
      var _this2 = this;

      this.txtLetter = ref;
      var el = this.$refs[ref][0];
      dom.scrollToElement(el, {});
      // 监听悬浮圆圈现实的字母
      animation.transition(this.$refs.txtLetter, {
        styles: {
          opacity: '1',
          transform: 'scale(1)',
          transformOrigin: 'center center'
        },
        duration: 300, //ms
        timingFunction: 'ease',
        delay: 0 //ms
      }, function () {
        // 回调执行opacity变为零
        animation.transition(_this2.$refs.txtLetter, {
          styles: {
            opacity: '0',
            transform: 'scale(0)',
            transformOrigin: 'center center'
          },
          duration: 300, //ms
          timingFunction: 'ease',
          delay: 0 //ms
        }, function () {
          // modal.toast({ message: 'animation finished.' })
        });
      });
    },

    // 跳转到联系人详情页
    JumpToDetail: function JumpToDetail() {
      var urlFront = weex.config.bundleUrl.split('/').slice(0, -1).join('/') + '/addressBook/addressBookDetail.js';
      navigator.push({
        url: urlFront,
        animated: "true"
      }, function (event) {
        // modal.toast({ message: 'callback: ' + event })
      });
    }
  }
};

/***/ }),

/***/ 266:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('scroller', {
    staticClass: ["addressBook"]
  }, [_c('div', {
    staticClass: ["fixed"],
    style: {
      'height': _vm.Env.deviceModel === 'iPhone10,3' ? '68px' : _vm.Env.platform === 'iOS' ? '40px' : '50px'
    }
  }), _c('div', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '28px'
    }
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("通讯录")]), _c('image', {
    staticClass: ["add"],
    attrs: {
      "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA4BpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkY2QyZGY5Ny04ZGY5LTQ1MTQtOTY4ZC1iMjc1N2MwMWI3ZTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTExQjNDMTc1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTExQjNDMTY1NDAyMTFFODlCNjVDOTZGMjdERjcwRkQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpjOWI1M2YzMC05YTk1LTRiZTktODQ2YS1mNDY0M2EwNjJlNTgiIHN0UmVmOmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpmNGFhN2FiNS05MmVkLTExN2ItODkyMS1mYzNiNzJhZjZlODkiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6Stt9bAAADmElEQVR42syZfWhNYRzHz52JS8QYLdliVhMtmjVimPJeXqKQJOUlYlO28gfxBw3hH42kDFlEQsnL8tI25aVIw7wMW2wzhqh5N9f3p++pn9Pd7j3nPnf2q089z3nOc+73/J6X3+851xcIBCyn+YobrRAWC6aAGWAkSARdwEdQC66Dk6Dc8mCBhfH//JBbmw+2gaQgbfFkOFgDboE8CvZkMS7uFQ8dI0lh9skEZWCzDEw0BXYC5+g92z6DQjAZ9APdwAAwFxwHzfaMAZt4b9QE7gPZqn4CDAKrQQmoB02gBpwCC0Aah9i2lbzfuEBZDEtUfQeYBxpC9KsE4+h53TfJtMAtqnwerHfx/O/05nPW/WCjSYHDQDrLv8Ba2QVcjpIMfb5jF+hkSuA0Vb4EqjzuFmdAHctdwWhTAtNU+aLl3QKO/pmmBMap8gsrMqtR5b6mBHZU5d8RCmxW5QZTAl+pcv8IBer+r00JfKTKEyMUOEmV75kSeFqVp4MEj+ImgGQ1vA9MCXwM7qhkocCDOJnHO1W9yDEfI44keudfDFa5ECeJwl6mX2KfwC7Toe4COKTqhfSkP0S/eCYOS9U1SRbeRyObEa9dU3WJx0+YjKao6x0YGgsYf2erNklyj7qdHz4XKb/MwSNgTpC2nxy+uCAvLfvnBjfzV6f8bjLqL0xGF4G3QRZC7yDPkwU2xuPicn0m8VPcMtAnzD4DmUtKRnM/Whm13LOcsXg/GOHi+T35QhXcU1NMe1DCUzHIclz/xuyknJmzbL5fQXf2SWfkyVB9ZjE7z3NzPmltkYwCZ7ld2FYNtlN0UxjPl+iRA1Y4ktQhfDHPi2Q8uKLE/eBKTOUwN4XpANlqcsFgPs/OsN+xnMJpEP4chPdSOV/8KvOQId5KoV6smkM+lmJlF1gHnvIlEsISCHGd+cmiBy/V8aG3rcgtwDlby3qWWkgZ4XpQ4u5QdTCXQ/kzK/oWE7IB3kt0nL4kbj60/rNp5fkqxZfvKYetdmB/txl4TxbEG35fsVdxqUEn5PKE6HMksfYxQBKRl6pNFmMRtpsb9kY9VYmrNCjOYkazO8Q92UGuzZTTX4x6G/1hyKQ1euxXr0OdXualhgWWMSqlOq7nqEx7D7jrCKUlWmCyaqyIwly/SSxHbLYFXuXnkRZDXS/V+MFqR2YLrNLj3p4sVq0i+ZJ1uQ1/u9kx51oWiGGVmHugjZ1zkPG40mrl74o/AgwAh73M/FeJSVEAAAAASUVORK5CYII="
    }
  })]), _c('div', {
    staticClass: ["search"]
  }, [_c('input', {
    staticClass: ["searchInput"],
    attrs: {
      "type": "text",
      "placeholder": "全行搜索（用户名、手机号、短号、座机）"
    },
    on: {
      "input": _vm.searchAddressBook
    }
  })]), _c('div', {
    staticClass: ["menu"]
  }, [_vm._l((_vm.cellImgs), function(cellImg, index) {
    return _c('div', {
      key: cellImg.msg,
      staticClass: ["menuCell"],
      on: {
        "click": function($event) {
          _vm.handleJump(cellImg.msg, _vm.activeTab = index)
        }
      }
    }, [_c('image', {
      staticClass: ["cellImg"],
      attrs: {
        "src": cellImg.img
      }
    }), _c('text', {
      staticClass: ["cellMsg"]
    }, [_vm._v(_vm._s(cellImg.msg))])])
  }), _c('div', {
    staticClass: ["movelightBar"],
    style: {
      'left': _vm.activeTab * 375 + 'px'
    }
  }, [_c('div', {
    staticClass: ["lightBar"],
    style: {
      'backgroundColor': '#00a4ea'
    }
  })])], 2), _c('div', {
    staticClass: ["session"]
  }, [(this.msg === '常用联系人') ? _c('div', {
    staticClass: ["address"]
  }, [_c('div', {
    ref: "searchAside",
    staticClass: ["searchAside"],
    style: {
      'top': _vm.searchAsideTop,
      'right': _vm.searchAsideRight
    }
  }, [_vm._l((_vm.searchLetters), function(letter, index) {
    return _c('text', {
      key: index,
      staticClass: ["searchLetter"],
      on: {
        "click": function($event) {
          _vm.scrollTo(letter)
        }
      }
    }, [_vm._v(_vm._s(letter))])
  }), _c('text', {
    ref: "txtLetter",
    staticClass: ["txtLetter"],
    style: {
      'top': _vm.txtLetterTop
    }
  }, [_vm._v(_vm._s(_vm.txtLetter))])], 2), _vm._l((_vm.searchLetters), function(letter, index) {
    return _c('div', {
      key: index,
      ref: letter,
      refInFor: true,
      staticClass: ["addressInfo"]
    }, [_c('div', {
      staticClass: ["addressTitle"]
    }, [_c('text', {
      staticClass: ["titleLetter"]
    }, [_vm._v(_vm._s(letter))])]), _vm._l((_vm.searchInfos), function(val, index) {
      return _c('div', {
        key: index,
        staticClass: ["addressInfoList"],
        on: {
          "click": _vm.JumpToDetail
        }
      }, [( false) ? _c('image', {
        staticClass: ["avatarImg"],
        attrs: {
          "src": ""
        }
      }) : _vm._e(), (true) ? _c('div', {
        staticClass: ["avatarTxt"]
      }, [_c('text', {
        staticClass: ["avatarName"]
      }, [_vm._v(_vm._s(val.chName.length > 3 ? val.chName.slice(1) : val.chName))])]) : _vm._e(), _c('div', {
        staticClass: ["info"]
      }, [_c('text', {
        staticClass: ["name"]
      }, [_vm._v(_vm._s(letter + val.chName))]), _c('text', {
        staticClass: ["dept"]
      }, [_vm._v(_vm._s(val.branch))])])])
    }), _c('div', {
      staticClass: ["addressInfoList"]
    }, [(true) ? _c('image', {
      staticClass: ["avatarImg"],
      attrs: {
        "src": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE8AAABQCAYAAABYtCjIAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NEFENURDNTU1NjdGMTFFODhGQ0RDMjE5RkQyODU5OTkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NEFENURDNTY1NjdGMTFFODhGQ0RDMjE5RkQyODU5OTkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0QUE1NzJBODU2N0YxMUU4OEZDREMyMTlGRDI4NTk5OSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0QUE1NzJBOTU2N0YxMUU4OEZDREMyMTlGRDI4NTk5OSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtvtWL0AACqwSURBVHja1H0JsF1lnefvLPfcfXv7/rIRyEZCWITEoCgI04LjjEo7Jdr2OD3lMlPOWINWU2PZjlUirYM6JdjWjIPS6CgjS1s2DUwU2TEkIUDIDsnL8ra87d53393vOfP7/8+5j0Cz5EbB7mN9vLx371m+3/dffv/lOxqe5+FtPIY5NnNczHERx0aO0O95zRrHTo5tHE9xPM4x8nZMxngbwEtzXMnxPo73cix5i+93hOPXHA9yPMCR++cI3gqOD3J8gGML/jjHoxy/5LiX49A/B/CWcnyU41qODfinceziuJPjZxyH/ymCF+H4M45PcGw6kwuUK1UUFgqwTAvlcgmVSgW2ZSEajcqjor297fd9xic4buf4sdzu972Y/QcC7lKOv+C47kxOzs3lcfDQIRzgGBkZQTgcxuzsLObm5pBJpRCLxSCL3N7ZiQsvvBBLhoYU2La2LL/rtHKrTcF4J8f/5HjkjwmewfE5js9yrGrlxFKphPn5PJ577gU8+OBWTE9PopCfw4kTx1Gre5idzmF2bhYe75BIJJBKpzA2No4NGzbg0i1bMD4+jpnZGQwO9GHz5s3Ywr+1tZ22ZMoin89xK8ctHN7brbbiNf8Tx+dbPXFqagZ/8/2/wbZt2zA9M4UigYTnokSV9fizXnf5u4lqrarDDtkIOQ6cUIiqXUG5WKbERVAozFPNc0il0rjiivfh4x//ON7//ve3+jjf5fhO4KXfFvCEo13P8eFWT9y3/wBu/PqNePbZXZz8AjKZFCKRMBYWinBdj8DV+bOBarWKWq3G36t6XjQaQTKZJPDTmOYYGhwkqCGCP41GowGZRkdHF0G8HJ/5zGewevXqVh7rFxzfDLjiWwre5Rw3cFzW6ol///f/gJtv/jZOnpygtMQIRgr5fIHqO08H4dtvAU2eSYATENO0eY1GHYZpIhRyVDKrlRIdiYkEJW52Nqf2LxKJ8Zy6OpqlS5fiy1/+Mq6++upWHu8hjq9zbH2rbN5VHF8JIoSWjp/97Gf42lf/G4Fw0U87FQpbmJgcRz63oJ+7rotisaggWgQmFosSkCgB8SXPIngVqndXexrxjgymcwJ4ld+1CGoI8XhUJZC/4uDBg/jCF75A+3lCVVkczmkcIgzRAJP7T+cE66/+6q9akbivnglw37vle7jpr29Ce1s7DfwAbVwRU9NTlKCK+pxqtaYSI4fjhDlCtGkhnXSI9k4Ar1Ur6Olsx/LhQdrBGmbooT2eK/ZSABfgUqmkfldpT7mChx9+WAFct24d0un06TzqQEDuJbx76c2+bLZg4244E+B++MMf4qYbb0ImnVGvWaFalkucLJ1CJp3l35KwbWtR+nwAQwqC0JXCQgkRSmF/Xy82rFmFpUMDdCxF1AmWQ5oSIa0J2bZ67+npaeWEHR0dBCsFk9L6k5/8BF/72tfUHJzmcXEw14v+EOAtCZxDyzbu7nvuwV9/4yb09fQgS/DEGczl5lS6ert7KU11nbCAJCorn4uaVkiW6/WaAmhb9LCUOpef9fb0oauzCyEhzqLStIUN/l2ItENvLE6nUCioc+kj2N3dXeqJxWTcfPPNrTz6ZcGcl/w+4BkBHWnZq+7YsQNf/OL15GdpdJLcihNo1CkttkNJCWFyclJVyqOH7e7uViPf0dGpBFmkpM7vJpMJtGez6Mq2a9QxNTODcCSCAaq+gCUOwqLU2pQ8AVAkW/4tzkbuJ9ft6yPgXV249dbv85l2tjKFDwdzN84UvM+dCY/L5XL40vVfhEmG29fbiwVKlUhXlbSjTg86OTUJl17zrJUrsXz5coZd7WrbKpUigasQGBtxqmqK4CXoCJLRMGL8W56kOBaLY/36DUjw8xgdRci01QaKN45RZUV6BXzxwCK97e1Z9PR0E9AKbrjhS/qzhePzAQYtg3dpEDm0fHzx+uuxb+8+rF27VkMskTA16rRpheIC0pkMPe4AYokYJ22gQKqiDoTAiSSJZ61ykgv8brmyQC9bQDrmYNlQHzIE1ISLznQCbeR9QpbFtglYBS6SSK5hGKRAeV2wfD5HNQ5j5coV2L79adx2222tTuezARanDV4kiFVXtXqnu+6+G/ff/wDWb1ivqnmI8arIfZSes041k4lGqHpKTWijSvSKniHBf4KgttEBUHpoy0S1M6kMkvxuB2PY9evPxYUXnI9wyEKlOI9UPKYgS2wbo7pKBOJRqkVlRaUFxBIdkw9ijve1qe6D+D4jm5mZ2VamtCrAInK64P3ZmQT5R48exTduvBGDpBNzXPFnn39O1TTCic4zlJJYVIAU1dIVIjAWVU8oTH9PP+IkulHboFSl0E0gE4aJbNjGctqtDAFyGzWkE3EM9XSRtnSiPZOGQVPgNuoankacCB1JRD2vOBJZqGq1QTOSx+joGJ1HBseOHaf9u6XVqV0XYPKm4C0N0kotH397xx2qPmJjn3/+eVUfoQ5TU1OMKk5qCCUTEqMukiefp5IxVMsLGD9+FJXCLKIEa4hgru7O4pLV/bjykrXYcu4yrB7qRFuEXrVRRMRgxFEvwSHDSZLWJKjqBh1GVSIRrk6Y4Z7EwHIPP1au0xMvYGxsDENDQ7j99tuVSLd4fCLA5g0jjI+eST7udwzy77/vPrTROx44cEC5nOTfxNbJEE8Yo+0pkyAv0DkkkxlyPhcuw7M5xqdetYylvW1YubQfa4cHsGKoF739XbAdR6XKolR5BL7E87uySSznd/PTk5jhQmRjSdCgokQHVam5BNQiz0sy7CsqAW/yxxl6a4lIJBz8xje+oRy0xXSWYHPj64G3IsgAt3TIw/34Rz/CAgN9UZfJiUkG/Bmqk8vJMISy7SBuLdOLxjQR4LrkZxLQ8+ea/k5ctHoJ1q9eiWUELEl6E02mlcbUGlWVVnEsDZqAKCUtSbDilND+9ji2P3cQ2/eOUiKjmC6WsECcTEp4jVRHJM4wTPW+osoCnNjhXjKA+++/H1u3bsXll1/eylQFm//bTOm/GrwPnknqfN++fdi9ezeNdkgdhNgy395UlSWJemosSI6XIFmOxBLwqKoxs4Jz1/ThA5duwppzz0GI5Jb6p8a9QeciIVfYdJSzyUIYoveOv1hxLkB7WwbDvV3oze7G488chGkwYCtWVNI92yfOxWJpcYElayNqLBIood9NN92kuUA/U31ax4YAo2+92ualg2JNy8djjz2mKzvBVRUqIjbH51litH3qIcbIoAQWGW6Vp+cwFDbxr951ET77bz+O89/9TkSS7cQtCs8yKWEkuWjApPMQ4E0OSRYYln8dGZLjizK0S3T1YtM7NuDyi9ZhyWAnejOUbDoRr+7xO2FqPJ2J11DvK15cFlV4qBY2du3Cvffe2+p0PxBg9QrwrjyTKpc8yG9+8xuVMuFVkmOrBgxfHtQjgPUK7Q5jWZcSEauXcVbGxgffvREfuua96KYnbdAjNgN6Q8J9BcvWnzoEfFXdYBGofqZlaUwsmZcEPfOac1fgnetXYkl3Bl3tSfIKEvJamedYmtKSBRBPXA+8sISFItmSPGjx2BJg9Qrw3ncmUifA7dy5U2NK4VSO8i13UVVFVcTgE0a0UZLW9iRwzeXvwJb3XIpoyo93PcmCmz50lDMFybAMnaTaO9otmXzzd3HbKo3B7yGCEssksHrlEqxe0o2OeBhxLqJRKWsO0LLDGi+L6oqnl4UV1ZZ/P/3002pqWjzedyp4g/AL0mekskIDRAKbExPpkFWViMIzDY0IsmEDyzriuOyS9bh480WIETjDM30ttH1JMgzJrli+/NF+eaZfXBBnIV/0qYcvmQikUheIP0JCsru6sXLZEiztTCETcehEXJhcHCHQtUZjMbPiL4ihhHr//v1norqC1bB5ShpmSatXECchK2cHKSFJSioLDpLTKjGkDxk6kuFMGJsvWIXNWy5BPJlCnRSirrzM0O8tZiIUF15AKj/BdTTbHZgBGWL/TP0i1INKUkDSUtFoDD39gzj/nGGsaHf4PJbWRUza0BidkVxGkgnqvHiOgCe2+YEHHtDnb7F+s9k8JV+HMwFPyK88gNxcQJQn9NQu2XTlHsMrCytIP67ccgH+5E+uQFtnN21RzXcKNOQeJcJoelK4CprnWvxMhqnekb5WATZUKkUcbYJIdTRCSoNUNXUBDKpvCr1D/bjo3JUYyMrfCRgXKsYFdAimOKKm9MmiCIjCS2UuLR4XN8E7/0zAE5EXwyu5OFXZwOSbptiuOh/cpfpYeOfGFbjyfe9BBz1jVStjnkqYgCLgKOCeTyN81RQACWXDtyyea6jtFKfiKd0wfMlUq+MPuaNcI+SEkezpw9KzlmPzqiF0xiiB/LzMZ4xE/cTpqXUbee6JiQn8+te/brkI1pzx+lbPFMCOHD6iBr8pdf5DGRprWgQmQltz4ZqVeNfF56Kjs4vf9Z2HrLZvrDydtKruKQlEhTUIrVxXHjGk9tB81eeCsKdEu+FTGrWvQDSeRqarE2tp/1Z2pRChmnuUTElCiBc/FTi5jsxB8o8tFsM2NsFruc3rpZdewp59e3wW7/prL0MA0RnyYVcM9OLSi1ajb2BIVVmiCklqipDKT8uKqINwXUP9MQJzqTJlGkFk4UcXcnXLDOmQv4kj8uRahq3qa9pROh4yaElp8V6RaBLdw0uxblkf2qKUSF7eZbQj2WcrWOim4xAAhegfOdJS6TZk4gyPZ597ljcbUa+66MEC2RFgMowizl7Wg8HBbkpCKvgO/CiAnxukD6ZFWsPJk3mhQcly+W/XIrFVYm2ohKpzkPNsAkYDpoLICMS0CKKcH07AZLgWiqTghDNwIkkFz2N4Fs+msXbNWbhgSR+yNjVBw0RLTYKAJibAd1jQ5MWxY8dawuCMwTuw7wBmpqZ9IVPmz9hcfrG4onyonnQC5y4fJOsf8nNtstJ88HA4rnGrk0hz0nHY8SQnHfXBsG0FSmmIaQde2FCVFRNXN1xNDthS9EnG4UgMnOlAOJGFxWuAEYXlJDW/Z1jCHi0MLlmCLe9Yi/WDPXAo4bY6pZpPqWQxtKzpKMFvUfLOrFdFgBihvdNiNChBVJVGwO9C/BGllJy/dhiXbb4QbdlOTb+LBIVjERRp9+bHcpS2ArKdPUgkkzBDDMUMSnCDPKxcpOb7HLFJY+TSrloCgksLUygwFJwqYGR0ChOzJ1WCQry+w5Atly+gpyuNNUNtiEcdiJldtXEdbaOLyfkqnhw5oVKskUetolkXR0l0SXN+bzl4E+Pj2LN3j6qnywn62QsCRwmpV+tYOtyHq6/YjP7BPlTIqyLhiBrsbc8ewL4D45iayVNKaMWcKBbKBSQJ6vL+fpw11Ifezgy/bypnFCfkE2FPbV695mGc1Gj37oPYvWc/pigtU/k5jXMHtEKXUiIyPjaGwwccrCCA/b3dCMXbsO7C8/DB+SJG7slhZHae1ycTEIpTrXAZazqvY0ePvvXgSflw4uQo6lxxl2DF+SAmjXah5lICoBxr9dnnUHoMjT8LxTp27tiH8elZtHV0YKCvkxO2MD4+geOFOaAQwoE9c5idHMNK2qe2NI09J51tS6nKCnz0qZgvFnCC0rFQmcfZZ3XivX2r0UXQMlGpmhkolCq8p83PS3hp5DAWGC4WixWqdRjhVAoXbVyPd+wfwbHHnsMCw8mPfOgabB7qwqe/7ef1jp94G8B7Yc9ejE9MUvI8dFLlhjo7sW9ijmpQRC8pwuqVKxClPdOYtWFqK1n/QAe2bNmATJbOg3YOksAUpBl/Ip/D5NgxTJ6cIZ0gpai6Qap+McSgl3QJjIdsexTtmX5kM2l0DJPokzdWCGiNU+lIZ9EgHbFpAs5btQzVcg1z+Xk+Awk5FzfR1YE1q8/CQ7uP4ATv9fzBl9BLp9RD9T5RrmJ07ARt3wyy2ba3DryDBw6iWhF1NLCyI4FSw0OeUUMi7qC3o03zbKZGBASBrq2nt12zyKVcEbOjkwRmFI9v34H9R8aUYgz1ZrBp7dlY1TeI6coCanVaUskgS3DrL4ECaHOx0skI0uEsnU4Gjz3wJO7d+hiOTo6iN9uOznQcmzaspvp3o0ZaBElbhWnbaPhcehwnEsFwXzedWRx52s0D+w9ids8+xLIJOLzJxNhJpSyXXLLprQNvbHQ0oB60dzQyedqSTCLOCcQQtqEdTJKagisZE2kZq6IwW0Dh5DQOHDyOu7Y+iWqX2MMGDowcw/Gxw7juqsvw4UvPx2p6xTLpjHJZL6DFEsPS01q8noSm0fYEdjy7Hz9/7Fk8MTJJW3UC7908RG8cxXf+10/x0SvehfPWnQ07TQ8fi/oUipQqQmoTDTs4a2k/lqxYhT17nkNsvoRp2tJoKKItb888s+u0wTNb9bJzcznsP3RAfw97ERwrEB6qVIZAXrxuI4aptvQjWhVzRVqoFmEhCOU6crl5PLv7eWSjFr7yl/8V77n03ciN7EGaTuHC938Ie4/PYf/OPcoZJWkgqa1m9kRCsyqRSzF23fHkDjz50BP4xCc+hvVdWQxSA9697hx86dprcRkl7/joMe0qlbYOSYJKclYckEiw1Hl7M1kszUTJ/wZRpXbMVRv6ufDKVuhKS5InkygWF7RGIatph0MYo9St7aY3WzGAMCecSWWpvhFyvboG/PpQtG0hcq9wlnbvXZvQn+1CYm4c7xjuwvWf/DfooApenIngSHsG5fwsb+RoGOXWyxoxSLTSkH492i6DRLpA+3TemqVYbhfxxT//16QbV/MaadRzx/DBq96JBXrQUCrNe8apuc1kQkMjCcmukAJiwKjirLOX4bfP7ef3a2ijSksWW4r0kugQ0P/gaiup9pnpaV9kTbHXVcQ58U9ddSke2b4b81KPJWF13Zp+R8MyPlQ0GcVAtB99vZ0QX1Auz6AvZeKT134I5UIeM2OHsGygDUasHwYBl54Wr16RbKqGYnKSSRsxPzePNRvWwaGUlBgVdMcJTjyGhTrpT4phmd2BJKMU8fKeGWRqaFoM10+4ivYkE0mclY1jplLFAkGlh+H9bFT4mfQ6yxgeHv7Dg5fP5XToirq+N8wVq3DoEC5etxqjhboaaFFlIRmW50ccQjkkdWQ6MXJBaWAkEaYEzU+PEmCGUu2kJaEoo4YYJx6By4kJN3SDXJ4G7fSslk0VY3wraa1wKklQappASIUktmWUEo5BsLbqfowtbRuS/oIhTqPOZwgjv7CARn83o5EqHUeREQ4jHSekyVKRPEmzvSXgjY+Na+NOmNJhWr5BH5vOocoY85w1g5jatl272r36oKaNLAJnNSTccpQuyOqHSYLDIllxgiqSJZUt2rkGV19S6w1OQiZKr6PAmJLXE98tvoPSaFCto4mIn4qQ6MJ0tdzoNUEW68ZzG56p2Z2GbLng9QyvimqpijoXJju8AtMHD2qmJ5qgjeUFbNuPc/PzhbfGYew/eOBl9s//hRwb0/RS0yTCg1s2oUh1OnFiTBOd0gnlBZPSfwsEQT5OkgAeJchjaGRQ0iTIl04p8eD6PcMMJA6LVbNmMajR8DQLYwaJA/97jcVMtEYkQY5LU/gaeDdgS1Gcz9rf1480ncjWp7f7EsTvqG2mpxN75wbJjj8oeGJwJfmpuTMJ4jlZJ8jN/fz//Yb/jZGEXoj5QhUlqrLlNifilw8NyRwbfqbZ8+tkGmN6lu8UDEl61uFnPQTEoGah+Z8guyJAiT313Ipez1LjIGFcKKiBmEGGTeJXqNxJrCySKdn96YV5DJB37n/xJdz58FNBPO6n0qSonp+f1+0LrYBXO60GZsuvwWrnuuHTME3fUfrufPBh3Pt/7sGqVStQ5s1zuYKmxrVg4/qJT1N7VULqMSU1pQTYC7LJQXLYMP0arSyUprtcPzVvS/3Ckq4KyR/WpRgBj/eRjgNPxcwvHvnXJniSxqf3rwddpXI3IexSPD9Oov7f7/oliTTUDtclSVCvL96vEvQ4v8lRa4L37OlKn2SNm9IgFkUW2yZIYnRvuPUHePyZ7ehob8McPahU7SU7IhP290qIiXW0K0AlTcCVaRm+VEnqvlIpoFQpKq8zm5Kp2Wdbi9hyncpCCdWFsubsXFl3ow5doVN6OGUhGiThjOvg1upB1xSflbf70a9+i98dPKqtH+KVzUDfbdpJKQpJQ5BUBN/k2NkEb8fpACcXlIShtCc4ZOTSL2x4lq5Wb2caEzMz+Mubb8XIxEmUZbUNAc/ygQhUVo25aWuuzfCalVpOkmFZ7sQoirOkHCTUIeLhhOIwGPSL+zTIwyLxrLZi1GiX5mg+8iTC0qrrqddvaESjymRZmsISoKXxuyKLR6mTlNYhSt3oVA4xhn+yz0Mk0VJH5c9F5KK5peFNjm1N8E5r54sUtkV1pRfFrxlYfkZW8m0ksG2ZJEamZ3HXPzyMyal5JZ2qmtJ0I62vsptHvV5DDZshpJe0pUE7dOzQIU0lSXe71D6U28XCsLhQ2ikgvcyOoxOVLgFBfmJ8DLOTU6iLByXRFelyRRqFnkiMTJWV7nuNVrjQRS7KiSnG1pbmQeFXDdygkuY7Nrm27Lw8Dcl7qgmebC9/07hEsq3Nzk5ZVbFLAqZ2MHEFXU6gPZ7EbKFCrywb8LjiNHR1Ca/E0HNS0uDjlkilqyWqVUlbZk8ePYKZiTHE0xlt8KlxZvUQzyEHQ9XvIa6Xi+SXs37ngBVcj5IyKxI4I3s6yno/ac0tF+dR4oJUeU5ddhS5UNDnixUcHZuirQsyyepEvKAG7Op1xePPk6o0GzBf5xCsHm/yPEneS+3tU2/oXQhcc0UclQLLbyYM0uxiVGQlGwTz0NgkZvN57eis8KNwsK+M4hDYOlfbxwrTc5rQbFsyhI6eAVT44Cf2HkSJC5Hp7kGSIJi0RfNz05g4ehQzJ2fQM9TNGLcN2fYkpa2MXH6G1KeBcCzplykVxLIvjbLIfJ5IxMGxyRnseWlcW3Pl/ho6LxboPa3pqn3m+aJlb3AIViOnkuQH3wy8VCq12D7mV7+sgEv53lGAs2nYpd1/5+EX+bAn0ZdpR6FGCZDP6y/TFqEFpXIBucIc0j39aOsf8HuXkzFk2rIIFctqz6YmxlV6FkpFzUpLV0AsnEKSIZkrNQ0jhVytjImpcbRlJYKISBir54qzci1NyZAIh/H07hcwMkXwuzqwIL3QTemDv/NIFldsnUQYUhCSOb1cj37F8eCrIwx5mcGjb9QpJcRYOjy9IFsiv8tN84W8JjbF+HLdaLPCOHzsOLbvexHv2rgR86UF5XT1oL6q5T7aoVK5BCeTQXv3IBxSDMKp/DG7YinS0oyTm0MpL+n3MJLZBKywb/fElTZoL4XjSf2kvbcfRw7uZWg1jY6Ofp/yiBcVrig9flTF6ak5PLz9BYTCfpW1Ua8tJjt88tAsbfrbU5tdEPF4/NUwPBpg9QqSLE1rv3wjyRMv6zcaNvy+Ed5YLq4PEHA2sX3KwxgFPLr9eeQpMfLAosp2sE1Aax+uf50Yz3dijFjC0oPCn/SulpNgrJxAPCPF6w50DvRQpXtUVZ1kAqFUHJFkihIeVT63a9sOTE2O6s5HoRsaQ1uWOp0GnVZbWxo7du/FthcOIxFJBLTFr41o2kwIvyE9L44mceU5m/s5XuP4ZYDVP4ptpV3oY6/XHSogSbusE/bzY3IDubGQYcMzgu46T6tq/d1d+N3Tz+HJZ3bjyne/A1PjU9p7YluGto+RmSJGAEpU2/vueYIT7MeKviGJ5OEQlBDvYTcjCnXn4pBoIynFFaqcJAnGqa7/45Yfo5O85j/+lz9HR1e3glkNmnnEqVh0PFVK/F0P/g4VSlvWsZQZaElJvGwwL9ncYmn6y9Bt+2LbX0NldwUYvWZiQBrV7nw98IRAitpKrktAE7tic4YhGRIeERzxZFLBj1KoD0/N4r5Hn8aV79mk8WO9bvhBmQThcYcSSikhaZ4/Polf/vw3+NhnPolllJLcsXHU8lV6ZV5f7E5gI8vytFmRuiQs8sqd9+3Eio42fOrfX4vuZYMEtap90U3TIFrSkU7j4ad24p6HnkQsGlez0JASp+erazOakVCuVq+gjd8Ph6ME7zW7pu7EKa8Yea2sirw+4+rX6ogXDyRbkkwNzut+I6MR9J4YUFcvGRQhpmXyubbOLO576DH8u2uvweolA5iZnUNN0unwVVdCNCuSwsc+9x+w6dwdsLJpxNeuhMn7RKie5RIdhGaR61roLvHe4WhYy7s1GvTNnX0YvOJSRAeyjKVLGlEYmi12aK8qcEyX1yjhtru3okTP2hsnsafUiTKKWkt+0N+v68PQDMkEeHlfgZqol53GEwE2b5gYkPeO3P5asEsK27bFmMoum2iQ/fBjXu+UxkPNfHBlZY/rETL67//t3doCoW0QDUOJbEPtJu0gqUXNcjF40dlISbyx8ziyOarWxDxiVM0keWPSiSNOqY7OFhA9kkds70nUHnkeyb4MKm1xFCsS1FOzuYbp9qxSkyKpiuzx+LsHH8WvHnkG8UQGQY+kX5Tz/MKSbvqz/SyRzENMkTRq+u1n9uI2hACTw6eTVZH3jtzx6j9msxndguQ2PAVRG3dsQ+mLD5y5uHKSNpLAq5e273///G789O/uR1tXJz1nw+9PDpyLUhiS2SrVGH1ZqnIR7sw80lMmsqO8xq4TCO0ahfPUMWRHqnBKBmZmRjGfpEQMdWpeUXrRpIdFqmMVTn6e9kp2kh8bncJtd21FzRHpCpqFROICPup3ZxkKot8XY6mTkHcZ+FGH2wTvjgCT00pJSWAn7x3Z++qsSmdXp1boZQVlxWSlmsWTU9td7UUCTUnj51/61vdx+MgRdHSmUJUkQSNIgio/5GlU0XA6CXdFBkV7Fgu1KeSr8+R25HfVAvKkMZPuLE5M7kMxUkR61RAS8ZRmanQiQStvUfeC8PqMOL5928/w9EvHMdDVJtxEkwzN1lVbmyJtf0dSIHHqOCoVBbApceR9ewMsyq3k8+SFLbf+I6dR9bcoSSYjFo/qdnQENkQCbVlGiTgsiRm16O3Siw7gOHnWdV/4CiOFHFLCFV0vyM25fl+KptkrtGkOwkMdqHdamPPGMdo4geOhSYw5JzEXyaHR6yA71KUtGo7pRwV+k6SnG/VqnHixMI+XDr2IR3bsY6xMbuj65N3TQqihzUJqd6VE6khC11q0bxJqVqqya/ykglgsFm/lz0fOJBkqO9y++zJwNR3xWDwwrv6y1wPe14w2jMXma9ffh6Eq34EnnjuIv/jKd1CmPZLijHA/7TVudjUammvRYkw4GUWqvx2pQQI12M3wrQ/tg/20aR2I8P7hGP15MGl5DmECMmSDSi43QxoU1pc8hO2mtBm+GqIJ0ssmxgg67YV7RshlFwrzqk1kFd8lj73l9Zoe36yGIWfJC1v6OT4sL0OQjSVSO82TzcvKiddEQDT5SH6lPnAc8pnYNElWJkmEZ2eBX/32cTy/5yqctXwYpqSqGG75hS1DSbIX0Aab3/caDiJu3Dfs8FN2YtsaQXba72mrqcoJTTk5NUmONo2BoT7dBFiT5ANtr2hFnc8VtQLz4mmWS6MT7XUOwNO2XIImiYF8Pv8L/v07ZBRes+v+TNLwkkGQF7Y8VCNVqJPHJZL+JmN99wlv6DjOot1pqmLz37Jv1nKsxbZ/yU/O0y5FGQXMnJzyMxhKhKG5Oku+b1DVrAgnEtMkazgcI9mNiJFVRyVv9jlV2iT/NjZ+Avn5GW3tkL1r1VItqNUGW6e8mlKjkCTtDVtbQdTJBRIo35EXQ4jaV6u1hyYmJr7JRTnSlM5X13K8psacxiH5vq/Tpj0lHjIeT/hxrDw8b2YELf5eAFbTuTT3TfjezNBypZjhMXrTdgb/sv9L1GyGIikLsxj2mT630yELFoDULEEK1xT+JgV4AX90dBzlWo7Aydsy0qovtXpVN++pe/D8MmhIym8Cpl898TtDA37nJz8N2b30FIXh6/x9m+xQrzfT86+KtFRaW6j/bCUYX02n0k8NDy0L2h98ldFYsblDx3rZlmiNi3zPqNCTMuiORnwJHeFk5c4piV3TURRoo6YmT6KwMK8OSfJvugmm4e9cFLAkIpD6hxrxBQbtxQpyszlMT0wQmAb6u7p4/ZjyS9le5TZMdVZVknVR21BAzCW2Dgk3ZVQUdmw/Keo2/PjWNCSv+VUKyVbhesePH9ddkq+XVW6pbsvY7/6h4eH6C7v332CHQpeJd5Wgv1aR7gCqAh9IcnTC40SVtTQpvEr3wdY1PpW/lUkjZN+syECCJFg84TyjiqkpcrRwWdNO4UhIg/VmcUgkfWGhQGnLEzyeL5niSlG7oOI0I9Jc5Mp0rKB8SeNoS6FdaJPhe1ep9mlGSIrpps/vHC2haq7yIT7H16v1xlaJjaVKKKGo/8qRBU2A2LZ95uBJ3Gib9tZMOpWPRSPX1yrlD4ukaIxrSbTgP7Asuqy2VKlFEsO0b+X6PBIEJV+sIjc950urMPiG5NI8RiMEtVzjg84xDMz5zijYUqU0ot7Qt/7Ii7mk6GMYDcQS9KikItJ/rItnhvxmbyXr0qrrqv2MSQeUVQhUvq4EWTrwNQMUkbjZ/QW16pu039vEDMm7WI4dP6YvlpAtpZLbE7NxxuCpGlGi5B0pnR1t20KmdT3/eIIr+nlPDL3lUwxZzrJbCSIOf9NwLOYgEbEYm0bU0BfnF7T91tA3UwgfdNQKybZXx5F9uq7ulCyX83pfI6Aa4oZjmr5ygt2R1su7IXkNyexY2kkquUZwsShhZWqH9KuZfmJDNrHUtUDkOwsC9l0+63c4jsg2AzMoFwjIL7744mLWXJxSs9jfMniiPnJiMpn2udBC8UitVv/PvPAhIvTZcNheZWuS069r+KmpxmLVX6KKQWmpTWeotkWqXQWxaEq9tTRYw5CWNJE2IbTCtBu8bMiv0wYFcl+6msnLwAsaWAwNm0Up3cPBCbdHkthwdj+i9PajJ8cUfC/gohRliRxupQ28hVLnGcGze/D3p0lnl7wnQeo22WxWf/ov+nqZOZy2wxBDLd4xkYgFbVue/M3jCn3PML1Pk4TdoaBJQC02RoouXkBMSRnkhTJXvfdSbLr4YkxPjmtOTSZpBZlc3fpph7UzXTK5pryMKyLvJZBXW0qTdwIhOhxpyJEtUiEnwokIpXG0DCq9Lw41QOyZp01FYSxpj+MD/+JKDJBcN7QLwNXXk/CB7qByf9o2je/xeT03KGZJ9lg8fLm0QAdnYd+evdizd+8iezBO2WnZkuRJ6COxoHAhCctEYqSsF+XfSCUeMcLWNtq3x2hHPkFgN4nymGr/dHuQ2qY1Gzbi+b0H4NJhyJYnYavqnd1TmWegesF+UTOg6m6zq0DLnP7LBiWwNYxX8jBZBEl12px8T2ca5553PqbpjLRpoV57wnEit9OS/Jgold2g3CixrWynl+qbvClIuapSJA979uzB5k2bgg3U1ivqGqcteZIAbZJhrZu4fkXfc3315MXLNNQ/oGpfx3Dthrrb2CWZWZmotC+kU1m09S9H7/AS3Ugs+x+aSQRNLNj+ypoatby8NVS3hIb8nY2SJrdMe3FnkPy9mSWRaEFqHOJRxVlJKVJ2Hi0/Zx16B4Z3USJvoGpfZ9vWD/izLPeWe/qdDN5i+CnOT/KBEk3Jmjz++OP6pqBmCeIVm/5OF7zmW3iaq2vplhxXy3tavgsIZzQcOczPbyRb+wihud6re49W6EVjySSi6SzWbbgAS/p6yc/G1cnIpFXapJgUtL9qorKpImLjjOaOPZUfPUfBkhycE9IhPdBms22DIM+S//UNrXh06eqzru/s6vxIOhK50bSdw+rY5PvBNvwmn5R3Uom2CPmW3kJ9ixo9+7PP7MK23z29+IKvM4kwAknwS43jfLCTczNoSD+b5ad0/N1AvliKQeZkDlGKvkUNuobjT5PJxA+JzJFz1q3HsiVLcOLoUXo1z+8VCZIJzeyMny4KnVJDeHnFm3k3BS54g5lfjPILUoZpHXGr5R9OT0786dKNG6/hQ32rvb3zUDwZV54pb+Awmi98kBRUEEFoctbwU/ICUtPGiTo/8cSTGs1Ua9XgpTu+qWjB23oquuI4dtMLTUxOoLunWz2UhgRB57oXxLjSDaqVfbi5SCR0J0O6O03PHE7EQptXbbjk4sm9D11UyE9vjIQHyHr8/j3tXTkly+HbmGAvrhJs/3M3SAwEmesax07haDzzKV7m8dnJkZEwOV7vOefps7d39uq2/NnSOB1MCAv1km4tkH+XPb8fT5xctV7x23ElnDP1HSSMQiI4dPAARkaOYtWqc5DPz2tJUpynfbrANd20tNtL6BKShkDJj3FGEToQ8ZK6yRh+olPUoha8TcdhFJLt7IZjWPI6yZG17/mXP73/uV9jcvSlVclk16pQzF5JaV1G+z9IIHr5nQ6el+a/o7rV3jJKhufmvEZjiiCNUUKOEc6XCPABjr18vr1Nya+SH04cPoj2vqUYWnWBPn+CwMkeXNcYU0ciL7Yp1w0tXNWEw9EZxqMR6SHQdL5QFVopfTNHrVHE9NRJjFBTVqxYoaAJ51Ppe5v/Lx1efawKxkqOZcGLIhQ8+O8uab4tphTUSqckrxC0h8g7PQ8E2e69f4yH//8CDAC+TTsr7dawVgAAAABJRU5ErkJggg=="
      }
    }) : _vm._e(), ( false) ? _c('div', {
      staticClass: ["avatarTxt"]
    }, [_c('text', {
      staticClass: ["avatarName"]
    }, [_vm._v(_vm._s('白帆'))])]) : _vm._e(), _c('div', {
      staticClass: ["info"]
    }, [_c('text', {
      staticClass: ["name"]
    }, [_vm._v(_vm._s(letter + '白帆'))]), _c('text', {
      staticClass: ["dept"]
    }, [_vm._v("电子银行部")])])])], 2)
  })], 2) : _vm._e()])])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });