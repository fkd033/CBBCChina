// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 363);
/******/ })
/************************************************************************/
/******/ ({

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(364)
)

/* script */
__vue_exports__ = __webpack_require__(365)

/* template */
var __vue_template__ = __webpack_require__(366)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "E:\\WorkProject\\Weex\\hzBankOA-weex\\src\\views\\workspace\\commando\\replyList.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-5a42bef9"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__
module.exports.el = 'true'
new Vue(module.exports)


/***/ }),

/***/ 364:
/***/ (function(module, exports) {

module.exports = {
  "marginBot": {
    "height": "16"
  },
  "commandoIndex": {
    "backgroundColor": "rgb(245,245,249)",
    "fontFamily": "PingFangSC-Regular"
  },
  "header": {
    "paddingTop": "68",
    "paddingBottom": "20"
  },
  "title": {
    "fontSize": "34",
    "fontWeight": "500",
    "textAlign": "center"
  },
  "more": {
    "width": "40",
    "height": "44",
    "position": "absolute",
    "left": "32",
    "bottom": "20"
  },
  "search": {
    "backgroundColor": "#ffffff",
    "paddingTop": "14",
    "paddingRight": 0,
    "paddingBottom": "14",
    "paddingLeft": "32",
    "flexDirection": "row"
  },
  "inputSearch": {
    "width": "458",
    "height": "60",
    "backgroundColor": "#f5f5f8",
    "paddingLeft": "62",
    "fontSize": "28",
    "color": "rgb(153,153,153)"
  },
  "searchIcon": {
    "width": "30",
    "height": "30",
    "position": "absolute",
    "left": "48",
    "top": "16"
  },
  "question": {
    "fontFamily": "PingFangSC-Medium",
    "fontSize": "30",
    "color": "rgb(51,51,51)",
    "lineHeight": "60",
    "marginLeft": "40"
  },
  "message": {
    "fontFamily": "PingFangSC-Medium",
    "fontSize": "30",
    "color": "rgb(51,51,51)",
    "lineHeight": "60",
    "marginLeft": "40"
  },
  "questionList": {
    "paddingTop": 0,
    "paddingRight": "32",
    "paddingBottom": 0,
    "paddingLeft": "32",
    "backgroundColor": "#ffffff"
  },
  "userTitle": {
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingTop": "24",
    "paddingRight": 0,
    "paddingBottom": "16",
    "paddingLeft": 0
  },
  "textAndHead": {
    "flexDirection": "row"
  },
  "headPicture": {
    "width": "42",
    "height": "42",
    "backgroundColor": "rgb(0,164,234)",
    "color": "#ffffff",
    "fontSize": "16",
    "textAlign": "center",
    "borderRadius": "21",
    "paddingTop": "13",
    "paddingRight": 0,
    "paddingBottom": "13",
    "paddingLeft": 0,
    "marginRight": "30"
  },
  "userName": {
    "fontSize": "30",
    "color": "rgb(102,102,102)"
  },
  "isOk": {
    "width": "120",
    "height": "50",
    "borderRadius": "8",
    "textAlign": "center",
    "justifyContent": "center",
    "borderWidth": "2",
    "boxSizing": "border-box",
    "borderColor": "rgb(0,164,234)"
  },
  "textContent": {
    "color": "rgb(0,164,234)",
    "fontSize": "24",
    "textAlign": "center"
  },
  "userQuestion": {
    "flexDirection": "row",
    "alignItems": "center",
    "paddingBottom": "32",
    "borderBottomColor": "rgb(235,235,235)",
    "borderBottomWidth": "1"
  },
  "answer": {
    "flexDirection": "row",
    "alignItems": "center",
    "paddingTop": "32"
  },
  "answerAndCare": {
    "borderBottomColor": "rgb(235,235,235)",
    "borderBottomWidth": "1",
    "flexDirection": "row",
    "justifyContent": "space-between",
    "paddingLeft": "72",
    "paddingBottom": "32",
    "paddingTop": "32"
  },
  "mark": {
    "width": "40",
    "height": "40",
    "marginRight": "32"
  },
  "bulb": {
    "width": "40",
    "height": "40",
    "marginRight": "32"
  },
  "answerText": {
    "width": "614",
    "fontSize": "30",
    "color": "rgb(51,51,51)"
  },
  "rowArray": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "agreeNum": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "agree": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "answerNum": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)",
    "marginLeft": "40"
  },
  "answerTo": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "timeYear": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "timeClick": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)",
    "marginLeft": "20"
  },
  "writeAnswerAndCare": {
    "flexDirection": "row",
    "justifyContent": "center",
    "paddingTop": "32",
    "paddingRight": "72",
    "paddingBottom": "32",
    "paddingLeft": "72"
  },
  "write": {
    "width": "24",
    "height": "24",
    "marginRight": "16"
  },
  "careImg": {
    "width": "24",
    "height": "24",
    "marginRight": "16"
  },
  "heart": {
    "width": "24",
    "height": "20",
    "marginRight": "16"
  },
  "careText": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "24",
    "color": "rgb(153,153,153)"
  },
  "wantAnswer": {
    "fontFamily": "PingFangSC-Regular",
    "fontSize": "30",
    "color": "rgb(102,102,102)"
  },
  "writeAnswer": {
    "flexDirection": "row",
    "alignItems": "center"
  },
  "care": {
    "flexDirection": "row",
    "alignItems": "center"
  }
}

/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var navigator = weex.requireModule('navigator');
exports.default = {
    data: function data() {
        return {
            Env: WXEnvironment, // 获取设备环境变量
            mark: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDI4RUQxNTg0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDI4RUQxNTk0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEMjhFRDE1NjREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEMjhFRDE1NzREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqsxABUAAATBSURBVHja1FtNSBVRFL7vZYqZkj4jiVJbWIFlUpBRLop+bKUQRRIS7XMjIm3atsmFGynaRAspKSg0DCyIKAtsFf0RWSEqaeFPWGZqZd/RMzKOc9+byXvvzPvgg3hOc893f84958y9kZHKSqEJGWARmA/mgevALDCNSZhijoNfwCGwD+wBJ7w0ktPW5suoFMUiY2AZWMpCIwmet8RTR2yw/T7Lwl+A3eCIKgNVCCZRxWAFj2hE0TsLmJU84p3gG+6MwASXsEEbhT6Q+M3MfrAdfGlacC5YDW4XZkEdexZ8BbaCwyYEl7PYlSI4UEdvZdFdvqaLDy+dCtawUwoTyKm1wFtPe3k46vGlmWB9CMUKtql+tKoqU5XgbLABLBThBdnWANHZyxVMvVbHQUPYQTbWJRrpaII1W5skYu2iayE69X8E14R8Gseb3jV+BZeH1EF5dmQY5XKvgq2gQhf+crIwBs5obKcaonO9BB66ggpKBB6D7x1C8zhEPQiuUdjeStbSHG+ESzSEi9/Ai+BlDv6do0op4X3wPPhIdUSGUS6RCY5wIqASg+AF8KOHZ6kjbrB4laiE6Iib4GLFWc9v8BKvVz+4DX5SnHAUuwmuUNyzD8Gv//H/KN/tUGxLhdNpxTh5V4mnLr+RUzrJbf0BB8CbXN6x4x04zcGPChRhWseQYIxEbQF4RKHYH+yMnDgD7uSQlcRv4/zWbTkMKi4ilNmndKni0R2X7PlbJOHgapffvyu2qdQygqqL+YpfvsplxuRKAh0KRH4qrMbIkI9pnREV6gpvzrW61zGljkqefcuinchRbBPZUJSiYXQtnAYPgKNivgQb8+GRadat1WBTfgqHdrqwMcHe3iHZc3dpmHVzYWw0wHz3CXhXsnYP6cqXSXBWAGJfg9clf6vSOAhZJDjNsFjyyNckjooioiMa204LQvADyR67Dzymue20qGGx5JWfufy+HjxlwgASPGVQ8ADnx0tSOA2BhhumTAvul0Rlpr5RzQkeNyjYbXTzDI3uXIwfdUnNdMLNM8cMtv8lKknjdEZeS8I9g+0PkeA+gw3uAA9zPpwO7gH3G2y/j9ZOD28XEUONHmeaBmnsoRGeMDzKQaEvp61twvKOVCQvMNTwBzF/ZMGqQmwy1O4Le1Whmzd/3dP6kZg/pmCdxKGTOdUG1vEsa1wouYzwWtYJKszdEYuPHdG/bwmPh9CWgR6qWNoFW72tE1T5+CXpiM+a2+60x9IW3khCP1XIFPLPs2s0ttvP2pYIpunVrrHhdMla3S301K8stGM6LyyjFee2LCoVU5hZqLHiQN94qAY9yZUW6oATGp3lK4hdVEZyC9rJi9KhLx3fiEnYAaZuzLCWJfmwE8NuDyYhWjG6w14EE7qsfStJ0Q2xXbKKhwwtYG8Siu1l24VfwfS5stlwvrzsfJdsjnfuMlERj6qLTUkimmxsgti4Xx29VC3peFFjyKc32dYIsWOJHnTuw/Gm93Mux2wIm4MCr0DspJeH/RTPSPRVMX/OKugD4gv7rMwbqxBs37LeiWCuACxEUMLgFQArOCEPbuKShzMRCOSSh4WX3Nuqr/E4k/fQXOOxDHrN9HtRK947Q3tRyw4y7B7TyFU8v/gnwAAbSxgQOZk8HgAAAABJRU5ErkJggg==',
            bulb: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QjI0MUEyRkQ0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QjI0MUEyRkU0RDE4MTFFODhGODhGNjQwRjkzMjM2NDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCMjQxQTJGQjREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpCMjQxQTJGQzREMTgxMUU4OEY4OEY2NDBGOTMyMzY0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pgz+BRAAAAVmSURBVHja5FtZb1VVFN49vaWlTWmhFOsAFDogEgSNhVAe4AV4sfKAjG0gERNDUBmi/8DEF001ThitGgISo5FQDETgoZjSlLGEIQzSURpaSYQWWqaS8i3uOsnx9px7zz573d7b9Eu+BO6w9/7u2ntNZzdF7fpXxQnpYDE4BSwAJ4E5YAa/R3gA3gdvg91gF9gBNoMP/UwyWJmvtaiQsEgSNB+cC04DLR/zZ4F5YJHj9ccs+ix4ArwjtUApwTPBJeBLYIrAeKlgKfNN8AJ4CPw70YJngRVszXiBdsnLTBK8H7wy3ILHg6vBV9TwogTcztv8N7BnOASXg2scjicRmAfOBneCZ3S+mKLhpceA68AFKrlQB/4Kbz3g93z4AXnSbUkolrAYfC9l980MKcF0Xj8Ep6vkxYvgBxCdbSqYLLsFfFYlPyaD78eytBXjzL47QsTaoKxuE0SHggiuSvJtHG17r9QNS+WcIkriP7AFvOlIFcdxjk0/bK6kI4OVr8Bzn/EjeCK4VmhiKg7qwWNgZ4zPTgUX8o+dJjD3eoi+BtG9sQSv5vNrChK5VyPxb2ce4Py5zHD+sby1a6KdYTtnNbXqDs6CglQ5VCp+D/4APjLNyGDlUi/BVOW8YTgB1bbVYJPADjkOfikgusJL8CyOZSYgy7QKOp/L4I+GY5TCyiVugpcZDnwUPB+HMHMabDQcY0mk4DwuvUzOba3hojLB15mZEe/tAwcMxp5tp5224PmGnYqT4F2D72dzvl7B3OgSw038Aul8zSl4rsC2MxFLRf1zjteeF56D8KotOItzUBM0C4olNAjOYWM6tnVaiM+uyXbu5TMcCUoVF3GoqnP5jJfYv1S4b+U1T9BOC2ktCglY1ytObnaMTdvpM/CeD7E/g4MeYz40bC1NoS1dYCjYawEvOP5dCG7ldC+oWEKG4VoLyMLPGA6SxWGk3yVTWuAiekxAsbkCRcUki0s0E9D5d+tL7wLPRbxWGFCsEqrNcyyBbaI8KhtKFL51ER1ErBKonp4eCUuoFCSnlKMp+k9wt0+xVKPPEVhnuqVkQI5rucd7tuhTjtcOgr9rjL9ChZ83mWIwZBjbnFjIxUOTh+jvwCMcWjo1xi23sySJDozlkTQEBeXAxVHeb9UUSyVrleD6ngruFRwwjUOPhEXKOXlJFVxfDwnuEq5fSfQ74CqDMTYwU4XX1h0PwTZmJOi70dBFgjvU6EE7Cb7qMxaOdFCR02pxBTMarNw8WJn/yE48mkaB4CZni6cxDtvaxMOmCa+FEp+TTsG3VLgHLIl8rn21SziBCi4S57Gd+5yCCYeFJ6G09W1N0blqaMdSAoeci7JxEWzjmlUK9Kz2Y04p78X4bCbX1dK3Ay/Dui32fyKrpdo4/Lq3fYhV3DH5Jw7z10ZuOxVhZfJmUhfOboAfKb2nBuu58pJAA6z7v/auWz38i2AF1a/0H5H0CM3d51Zzu52XW9yJeEtg0iJ2XG0+w94EFe5lS+AnWPeOH8GE45zAS2ytMiXTj9LBYYh17aVFa/HsUQLXdRMA8kN7vd60YiTbX2l2KBINCn87YN3HQQQrDid0heH6CBBLsfYLiI36pwN+upZ08D/hMjJZQTfmq+300VSwbenPVbhxnmyglPjrWJaN5aW9Kg4KV5c4ORibYKF9HHrO6Sb4uqDrfNdU+NLXvASJrWdPrH3NImiiTq3dGt7idLerdBhDzh/soAKXcCagOP0pZ1RLVfjvEKRbq3SUzvJZbZOoWSVACfo3KvysuIyLjyKDzsUDPjZUyJxWQ589J1yw05HUMWlseqZL1x7ooXsBNwPSHQ6vn8XREelmtrMlB+JxJp4IMADW3jkvqWXy8gAAAABJRU5ErkJggg==',
            write: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAlCAYAAADIgFBEAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MDlDNkVFODM1MEQ0MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MDlDNkVFODQ1MEQ0MTFFOEI4OUJFNjM5Q0EyMDZBNzQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGQkI4OTY4ODUwRDMxMUU4Qjg5QkU2MzlDQTIwNkE3NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowOUM2RUU4MjUwRDQxMUU4Qjg5QkU2MzlDQTIwNkE3NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PtX+09MAAAOISURBVHjavJdrSBRRGIZn0wy76J8sSSFDyh8VRNFFuhCVKZl0wS5UpGTsFlQQVHQlCAqKBP9ULpVBVFYUlblmagXZj6IwKIJIJRCLQKMLtOWG2fvBO3Eadmdm3eMeeHC+mW+dd+ac7z3feLxerxGnsQAcBkvAD/Ok3+//lzAoTkJmgNtgHrgDksIlxUPMRBAAIxgvBFdBQrzFZIH7YCTjr/y7ApwDnniJGQ2aQAbjSjAZdDAuBeXxEJNKIdmMZVq2gU5OUxfP7/T5fIcGUsxQcA9MYlwHSkAv4zawCHxnfASCRKjh0VzaUiV3wWLl3HTwIkzubNAIkhlv0PlmpDouW4QYLOmsMPlPwXMl3qpLjIcLtJhxK6jicQbXzyhLfhV9R8YbsFyXmBNgM48/0G1l/qt5LpuCUhlXgI08fg8K4MTdiRqE7Ae7eNxNIZ1K+aaAQpa1LOxHYAevf5JphZBOHdW0BRzlsVRHPninXA+B1aCZcS7Fy/hGIW06SnstOMXjn6AItITJC/Jaq3JO8gvAax2mJzvvRf7+N5/+sU3+fMUAJX8ZqynmvWkuuAEGgz9cF7UOrcN13kvy19FfYt61p/DGplFtB1ds8mfSZ8yWwcsHMWIVM55PlML4IDjt0DrUKq3DbnDe7gZuxWSCh0orUK5UUbgxztI6HAMnnW7iRkwaDSuTcRWfMtJI5xs0W4cz4ICbJ3YSI45ZD3IY3+S899nkNyqVU811ZcQqRlqBGjCVcRMrodcmv15pHQKstN5Yxcjqv6ZsZM/oDSGb/FtgFuNmek8omlKNJOYCWMrjt3TLoMvW4SUdNxitgYUTk8vpMMcYVkek1sFvaR3yue8YOsSUKe2hQV9pABMitA5lltahq7+bnVXMMLCGxysVbxC/eKCUt1ProEWMLLrh4Al31D2Ka2ZSUJqL1qFfIzHCFFXyr/iJj/5RzKmSyhrronWISUwOO/bPls1MfGI9BeUpi1lagVVsHZLoKW5GB/3IVswmxe57LHlDOEV5jM3WIcA4mVXlZgScxCTyQ6vP8k9z+CVYouy+Bs+prcMvsNelmHanaSrkt3EDu/Ui7il5Sm4Pe5OzfEuG5dpxXWumTHn97ZaPrhY6srjsF93fwvi0/U9MOntag7ZvcBFf4vp5Zfl9KadJ+zDXSgLfSj3fQo3NJifipw2UmDlgH7v9jy5+UxFF5UQ1/gowAGFFyymTOV1PAAAAAElFTkSuQmCC',
            careImg: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkJFMEU1NTE1MEQ0MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkJFMEU1NTI1MEQ0MTFFOEI4OUJFNjM5Q0EyMDZBNzQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQkUwRTU0RjUwRDQxMUU4Qjg5QkU2MzlDQTIwNkE3NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQkUwRTU1MDUwRDQxMUU4Qjg5QkU2MzlDQTIwNkE3NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PmYvJOYAAANfSURBVHjaxJjJa1NRFIcTjbW2ggOOOLRaTYIWRNE/oBWLxXTRVaFU0GqjRQS11IXgTlQcUHChEcVaUay6EnEA0Y0uVMSxkzUqzlA3VdpAtam/A78Lx/iSvL6XpBc+ct/8vXvPu/fceMPhsMdl2QSOgjjYBVpGcnEkEvlne4xLmSJwCkwCU+T+YK6bG7oV2g18ajsPNI2W0AxQz/otcJf1BjBtNIR2gnzW94MDrBeC7bkWmgwaWX8IHoB74BH3idDEXAptZSCb1jHlIH8lwLfkSmgCu0vKC8aPKddBJ+tNDPKsC9UzoE2LDKtjcdVKs8GGbAvJJ97MehRctTjnEviohoWxmRaSZi8F1eAEB0Mph8GQxfl/eExKCTjOa5fa6UKvmjpkhF0MgsAPAqwXWYh/A8VgMEWcvQczE/ZLl34A3eQN6AI9mEI+G6Fy/LaCOTZb9TuoBffTnFcJzoPpNu8rQnUSE2eTyHwSc341PXyTbsZH3MYD5OubxRYOEL/qgcQ5T7bPiNB87oiBbeAZBfo97kucXSfcTjhWSLHl4CTja57ERpvq9/Xs10zIpCv9bPE6FextIrRZxUMZuAEKciBUwGeVcVscGkVoAIQ4JxmpK05GWZcyIXxpA+ZzFql14DG3pX4tS1J5vPd/MokDYx+oUFJVWZAyMpXclmdVGxmrkTqZlC+DMlVKpgIyfemmDiP1WkmdzoBQa6IMn2VrLpMTy5XURjWHOSkLQQ3rr5LJpJtce1UqIWW8C6F8Vd+XTMbObB/g72/wzoXQW5UZBN2kH6X87WFa4bQMUkrKEjdC5m06MhDUXW6FxjE/ktKeAaF29ZI+J0J+dWEqoZXgnKQOnLmTlQ71ogucCOmm7bY4LhnjRfCEybz86fAUXFApjVULeZjOjljIXDSk+t+suY5Qspb7homX6YQcO8QFpX6pYTdCQfXJDnIcauLnr9dcsqZfAVZx9WrGnWauTHbw3Bjz6ZSBbafLOtkSXWwZ89YvwVqwBjxnd61mpmBG+KngGK+tUd3mSMgEXoixUsztL5xKJIDvWFx3Eyxj4vdV3esyX8CTahpKJRRVi0MpP8EeDgUtaRL9OBcPi8Be8CvhXlEnQg3sih9cIJbwL5fYCMaeGOeuEt6jl/8H1Ce74K8AAwAjU80xP7hLDAAAAABJRU5ErkJggg==',
            more: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAsCAYAAAAXb/p7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpBNDhCNUY4MDU3NTYxMUU4QjQ2NUM3RDZFRUQxQkE0RCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpBNDhCNUY4MTU3NTYxMUU4QjQ2NUM3RDZFRUQxQkE0RCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkE0OEI1RjdFNTc1NjExRThCNDY1QzdENkVFRDFCQTREIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkE0OEI1RjdGNTc1NjExRThCNDY1QzdENkVFRDFCQTREIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+lyjhmQAAAxJJREFUeNrs2FFIWlEYB/CcS8VkSa6wHhYhOUiEEfYg1EMPg2AwFgZJsrmFbTkdwSTaYA9t7GE+iAtkMmjMokG7m4R5F6MRDYSEGWL4kpXIJNhgMBy7TJGsnU86ID6turrvoe/p+CD84N5z7v9/BAcHBzWY50wN8jkFls7MzMyQzWZ7FwqFLqID+ny+obW1tcG9vT3R5uZmNyrg3NzcQDgcHoS1TCb7OTIy8goNEHDkkV6nOJfLdQvNO8gwzBWKk0qlv46KqyjQ7/f3rays3Ia1RCLh3G73DTS7GHDLy8tWipuamjKhOWZYlu0+xF0ViUTZk+B4By4tLemDweA44Gpra9+Ojo5a0BzUgAsEAg8ozmq1WjQaDYcCSDbDJYoTCoXv+cLxAlxdXdWS4+QxxVkslrt84U4MBNz8/PzTUlxnZ+cPFGGBfLraKE4gECwMDw/f4xt3bCDgSDJxUZzZbHbodLrvKOIWxZEk3k9xer0+hSIPRqPRxtnZ2SKO/Fw0mUwPK4k7EnBjY6Nhenr6xf7+fhFnNBof9fT0JNAkahI4nxcKhQFYGwyGJ729vXFUkb++vh42wSKs4/H45WolccFRaufExMTLTCajhLVarQ47HI5nqEqT0+m8I5fLi8fJ1taWnmS8cXStrhQJ5cfj8djRPOLSIY/3NcdxDbDWarWf7Ha7B1Uvhn4BJYhuGq/Xa0VX3AFZV1dXRMZisT5yTt5E84hLZ2xs7E0ul5PBuqura4GkGh+qqw/oHVCOYB2JRPrhFgHd3QwgoSRBwoFbBCjs6C6PoCRBHwEkFHY+kMLJyUnegE1NTfnW1taPJPWoSKgwpNNpSTab/U0qwDYKYClyfX1dDbEslUrJ8vl8pqOjYwcFkCKbm5s/k6OnDZDJZPLccZEVAcIQ4J9yJEnguyRkpFEAKVKpVIbgnSQ/r5GAcZ60v6/t7e27KIAwLS0tnEKh+EIS+QVAJhKJRrFYvK1Sqb79t9ut8oHeAuUKShYcQSzL3v/X/56tVjI+LFcOhmE40p8/VPVbjCqwngLL5q8AAwBoMnaljJ91xgAAAABJRU5ErkJggg==',
            heart: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAYAAAB6kdqOAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MEUwRkUxNzc1MEQ1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MEUwRkUxNzg1MEQ1MTFFOEI4OUJFNjM5Q0EyMDZBNzQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowRTBGRTE3NTUwRDUxMUU4Qjg5QkU2MzlDQTIwNkE3NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowRTBGRTE3NjUwRDUxMUU4Qjg5QkU2MzlDQTIwNkE3NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PhYMMdUAAAM2SURBVHjaxJfNS1RRGMbPvbXIIcIMg4SgKVdW6iIX+UExm4RaZUKBgkHNtAkzywn/gLCPAds1CFqCFiQpNEEuCgJpnE1FTGS0kNmoFJEEfoCT0/PCc+I03Jq5ozPzwg/u+Xyf855zz32v5ff7lYPZ4AQ4B+qBl3Ur4CN4CR6CT8rZDoMOcBxUgW1gHcyCN+ARmJS6cDj818CtDpM1gvvgoENbCThCesBjcBUssH0v6Aen/7HIA6SdC7sEpsxOVlqEroFbHCw2ByIc/BPsBEfBSeBhn68UIIt7CspYvwyegyj4AXZwkadABftI1IKI0l0nQdfBbcNJL3gAfjmstpTt3cZW6giKkxC4CRYdxm7hdkr7btb1QNQdU9Ax8IqTx3l+5lRma2ZUSlheYbReZDG2gufoEBfhg6jXIkhEfGA4JTK1YF5lb2fAEz63gjEXY/eA94yUHItqm3uqD3CvSzGKAprJmMux8/Sp9PmyuSrdOKRys0mSiw0ZQWgVQQ0sPONeFtRwbtbpW6xeBO1j4bMqnmnfXhFksbBYREHatyWC1ljYVURB2veaCEqwsL+IgrTvhAiK6QNVREHad8w2Pm7V/KoX1AKBgJe+xaZE0ARIsqKjCNHRPkXDhM3UIcLKy/xwFspK6VMsgjtpQacZ+isv6cWNAgoK0ucfDVqQ5CzjfO7mBzbfVsP8S2wc0YmagrSQJSZaI2B7HsXI3KP0tUTfKl2Q5LtdfJY8eNC4xTfTLM5dxXIXojPrJEhsgNHRuU0oD4JCRoYxAjED6Yl3ul0A01o96NtEMX3GLkzTl8okaJVJW9x4E/o3uH0W5wiyHKeP1WwEiX2XHNcQ1ck01ZODGA/HdhpifPShshUk9g008UoQa+FPXqULMZUc02JcL02cW7kVpPMUH19RfXe8449eJmtn3xqWRznXf/MuO4uJZZ/beIkleYcM83e43KF/OduG2TfJsW1OZyYXQWIpvq7ym/2FdWfBDDjPQ2vxeYZtin0bOTaVjSPb5QGN8bNyjw7KeMnFyCDrUuxTa+RbKh+C9D/7Ff6tvGVdHVGsa2CfZbeT2xu4W6IUcZFpcILPdcab6dp+CzAALT3IV5o64UIAAAAASUVORK5CYII='
        };
    },

    methods: {
        back: function back() {
            navigator.pop({
                animated: "true"
            }, function (event) {
                // modal.toast({ message: '跳转'})
            });
        }
    }
};

/***/ }),

/***/ 366:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('list', {
    staticClass: ["commandoIndex"]
  }, [_c('cell', {
    staticClass: ["header"],
    style: {
      'paddingTop': _vm.Env.deviceModel === 'iPhone10,3' ? '116px' : _vm.Env.platform === 'iOS' ? '68px' : '28px'
    },
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('text', {
    staticClass: ["title"]
  }, [_vm._v("回答列表")]), _c('image', {
    staticClass: ["more"],
    attrs: {
      "src": _vm.more
    },
    on: {
      "click": _vm.back
    }
  })]), _c('cell', {
    staticClass: ["questionList"],
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_vm._m(0), _c('div', {
    staticClass: ["userQuestion"]
  }, [_c('image', {
    staticClass: ["mark"],
    attrs: {
      "src": _vm.mark
    }
  }), _c('text', {
    staticClass: ["answerText"]
  }, [_vm._v("请教各位大神，手机下载杭银联的二维码可以在哪里下载？")])]), _c('div', {
    staticClass: ["answerAndCare"]
  }, [_c('div', {
    staticClass: ["care"]
  }, [_c('image', {
    staticClass: ["careImg"],
    attrs: {
      "src": _vm.careImg
    }
  }), _vm._m(1)]), _vm._m(2)]), _c('div', {
    staticClass: ["writeAnswerAndCare"]
  }, [_c('div', {
    staticClass: ["writeAnswer"]
  }, [_c('image', {
    staticClass: ["write"],
    attrs: {
      "src": _vm.write
    }
  }), _c('text', {
    staticClass: ["wantAnswer"]
  }, [_vm._v("我要回答")])])])]), _vm._m(3), _c('cell', {
    staticClass: ["questionList"],
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_vm._m(4), _c('div', {
    staticClass: ["userAnswer"]
  }, [_c('div', {
    staticClass: ["answer"]
  }, [_c('image', {
    staticClass: ["bulb"],
    attrs: {
      "src": _vm.bulb
    }
  }), _c('text', {
    staticClass: ["answerText"]
  }, [_vm._v("OA主页右下角，it栏目内")])])]), _c('div', {
    staticClass: ["answerAndCare"]
  }, [_c('div', {
    staticClass: ["rowArray"]
  }, [_c('image', {
    staticClass: ["heart"],
    attrs: {
      "src": _vm.heart
    }
  }), _c('text', {
    staticClass: ["agreeNum"]
  }, [_vm._v("1")]), _c('text', {
    staticClass: ["agree"]
  }, [_vm._v("个赞")])]), _vm._m(5)])]), _c('cell', {
    staticClass: ["questionList"],
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_vm._m(6), _c('div', {
    staticClass: ["userAnswer"]
  }, [_c('div', {
    staticClass: ["answer"]
  }, [_c('image', {
    staticClass: ["bulb"],
    attrs: {
      "src": _vm.bulb
    }
  }), _c('text', {
    staticClass: ["answerText"]
  }, [_vm._v("OA主页右下角，it栏目内")])])]), _c('div', {
    staticClass: ["answerAndCare"]
  }, [_c('div', {
    staticClass: ["rowArray"]
  }, [_c('image', {
    staticClass: ["heart"],
    attrs: {
      "src": _vm.heart
    }
  }), _c('text', {
    staticClass: ["agreeNum"]
  }, [_vm._v("1")]), _c('text', {
    staticClass: ["agree"]
  }, [_vm._v("个赞")])]), _vm._m(7)])]), _vm._m(8)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["userTitle"]
  }, [_c('div', {
    staticClass: ["textAndHead"]
  }, [_c('text', {
    staticClass: ["headPicture"]
  }, [_vm._v("卢兴")]), _c('text', {
    staticClass: ["userName"]
  }, [_vm._v("卢兴")])]), _c('div', {
    staticClass: ["isOk"]
  }, [_c('text', {
    staticClass: ["textContent"]
  }, [_vm._v("已解决")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["rowArray"]
  }, [_c('text', {
    staticClass: ["agreeNum"]
  }, [_vm._v("32")]), _c('text', {
    staticClass: ["careText"]
  }, [_vm._v("个关注")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["rowArray"]
  }, [_c('text', {
    staticClass: ["timeYear"]
  }, [_vm._v("2018-5-16")]), _c('text', {
    staticClass: ["timeClick"]
  }, [_vm._v("12:38:01")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["marginBot"]
  })])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["userTitle"]
  }, [_c('div', {
    staticClass: ["textAndHead"]
  }, [_c('text', {
    staticClass: ["headPicture"]
  }, [_vm._v("卢兴")]), _c('text', {
    staticClass: ["userName"]
  }, [_vm._v("卢兴")])]), _c('div', {
    staticClass: ["isOk"]
  }, [_c('text', {
    staticClass: ["textContent"]
  }, [_vm._v("推荐答案")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["rowArray"]
  }, [_c('text', {
    staticClass: ["timeYear"]
  }, [_vm._v("2018-5-16")]), _c('text', {
    staticClass: ["timeClick"]
  }, [_vm._v("12:38:01")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["userTitle"]
  }, [_c('div', {
    staticClass: ["textAndHead"]
  }, [_c('text', {
    staticClass: ["headPicture"]
  }, [_vm._v("卢兴")]), _c('text', {
    staticClass: ["userName"]
  }, [_vm._v("卢兴")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: ["rowArray"]
  }, [_c('text', {
    staticClass: ["timeYear"]
  }, [_vm._v("2018-5-16")]), _c('text', {
    staticClass: ["timeClick"]
  }, [_vm._v("12:38:01")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('cell', {
    appendAsTree: true,
    attrs: {
      "append": "tree"
    }
  }, [_c('div', {
    staticClass: ["marginBot"]
  })])
}]}
module.exports.render._withStripped = true

/***/ })

/******/ });